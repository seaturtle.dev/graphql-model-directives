/* eslint quote-props:0, quotes:0, comma-dangle:0, indent:0, semi:0, key-spacing:0 */
module.exports = {
	"plugins": [
		"security",
		"mongodb",
	],
	"env": {
		"node": true,
		"mongo": true,
		"mocha": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:security/recommended",
		"airbnb"
	],
	"parserOptions": {
		"sourceType": "module",
		"ecmaVersion": 2018,
		"impliedStrict": true
	},
	"rules": {
		"no-multi-assign": 0,
		"operator-linebreak": 0,
		"class-methods-use-this": 0,
		"import/no-unresolved": 0,
		"import/extensions": 0,
		"import/no-extraneous-dependencies": 0,
			// ["error", {
			// 	"devDependencies": ["seed/**/*.js", "test/**/*.js"]
			// }],
		"indent": ["error", "tab"],
		"no-console": 0,
		"no-param-reassign": 0,
		"no-restricted-syntax" : 0,
		"no-tabs": 0,
		"security/detect-object-injection": 0
	}
}
