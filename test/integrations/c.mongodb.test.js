const server = require('@seaturtle/gmd-test-server/index.js');

if (!process.env.NO_DB_SERVICES) {
	server('MongoDB', 'mongodb', 4333);
}
