const server = require('@seaturtle/gmd-test-server/index.js');

if (!process.env.NO_DATA_TESTS) {
	server('Data', 'data', 4747);
}
