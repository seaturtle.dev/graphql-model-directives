/* eslint import/no-extraneous-dependencies:0 */
require('fake-indexeddb/auto');

const server = require('@seaturtle/gmd-test-server/index.js');

if (process.env.ENABLE_DEXIE_TESTS) {
	server('Dexie', 'dexie', 4949);
}
