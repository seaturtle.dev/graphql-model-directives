const server = require('@seaturtle/gmd-test-server/index.js');

if (!process.env.NO_DB_SERVICES) {
	server('Redis', 'redis', 4334);
}
