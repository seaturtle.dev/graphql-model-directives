const server = require('@seaturtle/gmd-test-server/index.js');

if (process.env.ENABLE_MEMORY_TESTS) {
	server('Memory', 'memory', 4434);
}
