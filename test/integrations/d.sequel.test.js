const server = require('@seaturtle/gmd-test-server/index.js');

// TODO: this works if we delete database.sqlite before running, good.
// The problem is that most SQL need schema migrations before adding or removing collection fields.

if (!process.env.NO_DB_SERVICES) {
	server('Sequel', 'sequel', 4444);
}
