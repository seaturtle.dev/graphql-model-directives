/* eslint no-unused-vars:0 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

/**
* Sequel Model Implementation
* @exports sequel_model */
const sequel_model = exports;

function count(store, name, target, options, context, info) {
	function countPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		return new Promise((resolve, reject) => (
			model.count({ where: target }).then(resolve, reject)
		));
	}

	return countPromise();
}

sequel_model.count = count;

function find(store, name, target, options, context, info) {
	function findPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const offset = parseInt(options.offset || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const fields = options.fields || null;

		if (fields && collection.nonNulls && collection.nonNulls.length) {
			collection.nonNulls.forEach((nonNullField) => {
				if (fields.indexOf(nonNullField) === -1) {
					fields.push(nonNullField);
				}
			});
		}

		let order = null;

		if (options.sort && options.sort.fields && options.sort.fields.length) {
			order = [];
			/* eslint no-nested-ternary: 1 */
			const dir = options.sort.ascending ? 'ASC' : options.sort.descending ? 'DESC' : null;
			if (dir) { options.sort.fields.forEach((f) => { order.push([f, dir]); }); }
		}

		target = collection.mapQueryOperators(target);

		const sequelOptions = {
			attributes: fields,
			where: target,
			offset,
			limit: count,
		};

		if (order) {
			sequelOptions.order = order;
		}

		return new Promise((resolve, reject) => (
			model.findAll(sequelOptions).then((results) => (
				collection.resolvePagedRelations(resolve, reject, {
					offset,
					count,
					page: collection.filterSecureDataForUser(results, context, target)
						.map((record) => {
							let result = record.dataValues;
							result = collection.convertToGraphQL(result);
							result = collection.redactSecureDataForUser(result, context);

							return result;
						}),
				}, context, info)
			), reject)
		));
	}

	return findPromise();
}

sequel_model.find = find;

function nextPage(store, name, target, options, context, info) {
	function nextPagePromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const offset = parseInt(options.offset || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const sequelOptions = {
			where: target,
			offset,
			limit: count,
			order: options.order,
		};

		return new Promise((resolve, reject) => (
			model.count(sequelOptions).then((count) => resolve(count > 0), reject)
		));
	}

	return nextPagePromise();
}

sequel_model.find.nextPage = nextPage;

function read(store, name, target, options, context, info) {
	function readPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		target = collection.mapQueryOperators(target);

		return new Promise((resolve, reject) => {
			model.findOne({ where: target }).then((record) => {
				let result = record ? record.dataValues : null;

				if (!collection.hasPermission(result, context)) {
					return resolve(null);
				}

				result = collection.convertToGraphQL(result);
				result = collection.redactSecureDataForUser(result, context);

				return collection.resolveDocumentRelations(resolve, reject, result, context, info);
			}, reject);
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, readPromise);
	// }

	return readPromise();
}

sequel_model.read = read;

function remove(store, name, target, options, context, info) {
	function removePromise() {
		return new Promise((resolve, reject) => {
			const collection = store.collection(name);
			const { idProperty, model } = collection;

			// TODO: delete related documents, with a new flag on relation fields "autoDelete"

			return collection.prepareForDestruction(target, context, info).then(() => (
				model.destroy(
					{
						where: target,
						limit: 1,
					},
				).then((/* dCount */) => {
					const doc = { [idProperty]: target[idProperty] };
					collection.emit('removed', doc);
					return resolve(
						doc,
						// dCount > 0
						// 	? doc
						// 	: null,
					);
				}, reject)
			));
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, removePromise);
	// }

	return removePromise();
}

sequel_model.remove = remove;

function scan(store, name, target, options, context, info) {
	function scanPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const fields = options.fields || null;

		if (fields && collection.nonNulls && collection.nonNulls.length) {
			collection.nonNulls.forEach((nonNullField) => {
				if (fields.indexOf(nonNullField) === -1) {
					fields.push(nonNullField);
				}
			});
		}

		target = collection.mapQueryOperators(target);

		const sequelOptions = {
			attributes: fields,
			where: target,
			offset: cursor,
			limit: count,
			order: options.order,
		};

		return new Promise((resolve, reject) => (
			model.findAll(sequelOptions).then((results) => (
				collection.resolvePagedRelations(resolve, reject, {
					cursor: count === results && results.length ? cursor + count : 0,
					count,
					page: collection.filterSecureDataForUser(results, context, target)
						.map((record) => {
							let result = record.dataValues;

							result = collection.convertToGraphQL(result);
							result = collection.redactSecureDataForUser(result, context);

							return result;
						}),
				}, context, info)
			), reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, scanPromise);
	// }

	return scanPromise();
}

sequel_model.scan = scan;

function purge(store, name, target, options, context, info) {
	function purgePromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const fields = [collection.idProperty];

		target = collection.mapQueryOperators(target);

		const sequelOptions = {
			attributes: fields,
			where: target,
			offset: cursor,
			limit: count,
			order: options.order,
		};

		// TODO: emit removed events

		return new Promise((resolve, reject) => (
			model.destroy(sequelOptions).then((results) => {
				resolve(results);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, purgePromise);
	// }

	return purgePromise();
}

sequel_model.purge = purge;

function upsert(store, name, target, options, context, info) {
	const collection = store.collection(name);
	const { model } = collection;

	const { id, document, create } = collection.convertToStore(context, target);

	const safeId = id || collection.generateDocumentId();

	return new Promise((resolve, reject) => {
		const validationErrors = collection.validateDocument(document);

		if (validationErrors) {
			return reject(validationErrors);
		}

		const [relatedPromiseTriggers, relatedError] = (
			collection.gatherRelatedPromiseTriggers(document, context, info)
		);

		if (relatedError) {
			return reject(relatedError);
		}

		// TODO: upsert should support batch updates,
		// 	it would be nice to have two methods one for byId and not

		const fields = options.fields || [];

		if (fields && collection.nonNulls && collection.nonNulls.length) {
			collection.nonNulls.forEach((nonNullField) => {
				if (fields.indexOf(nonNullField) === -1) {
					fields.push(nonNullField);
				}
			});
		}

		return collection.prepareForStorage(document).then((doc) => (
			model.upsert(
				doc,
				{
					attributes: fields,
					where: { [collection.idProperty]: safeId },
				},
			).then(() => (
				model.findOne({
					where: { [collection.idProperty]: safeId },
				}).then((record) => {
					let finalDoc = record ? record.dataValues : null;

					const resolveDoc = (finalDoc) => {
						if (finalDoc) {
							finalDoc = collection.convertToGraphQL(finalDoc);
							finalDoc = collection.redactSecureDataForUser(finalDoc, context);
						}

						collection.emit(safeId === target[collection.idProperty] ? 'updated' : 'created', finalDoc);

						return resolve(finalDoc);
					};

					if (relatedPromiseTriggers.length) {
						const relatedPromises = relatedPromiseTriggers.map((trigger) => trigger(finalDoc));
						return Promise.all(relatedPromises).then(() => resolveDoc(finalDoc), reject);
					}

					return resolveDoc(finalDoc);
				}, reject)
			), reject)
		), reject);
	});
}

sequel_model.upsert = upsert;
