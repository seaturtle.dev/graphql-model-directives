/* eslint no-unused-vars:0 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

/**
 * Sequel Migration Implementation
 * @ignore
 */
const sequel_migration = (migration, options) => {
	migration.execute = () => {};
	return migration;
};

exports = module.exports = sequel_migration;

sequel_migration.changeFieldName = () => {};
sequel_migration.changeFieldType = () => {};
sequel_migration.changeTableName = () => {};

sequel_migration.dropTable = () => {};
sequel_migration.createTable = () => {};

sequel_migration.emptyTableData = () => {};
sequel_migration.exportTableData = () => {};
sequel_migration.importTableData = () => {};

sequel_migration.insertField = () => {};
sequel_migration.removeField = () => {};
sequel_migration.renameField = () => {};

sequel_migration.removeIndex = () => {};
sequel_migration.createIndex = () => {};
sequel_migration.renameIndex = () => {};
