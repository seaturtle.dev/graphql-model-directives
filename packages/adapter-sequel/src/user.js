/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

// const crypto = require('../../plugins/crypto');
const { nanoid } = require('@seaturtle/gmd-core/id.js');

/**
 * Sequel User Implementation
 * @exports sequel_user */
const sequel_user = exports;

const {
	getJWTOptions,
	extractFromToken,
	encodeTokenFromLogin,
	setupIsAuthenticated,
} = require('@seaturtle/gmd-plugin-authorize/authorize.js');

function createHelper(
	context,
	config,
	{
		birthdate,
		login,
		email,
		emailConfirm,
		isSuper,
		now,
		password,
		userId,
	},
	cb,
) {
	const { crypto } = context.store;
	const { userCollection } = context.store;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const birthdateName = userCollection.userBirthdate;
	const passwordName = userCollection.userPassword;
	const passwordResetName = userCollection.userPasswordReset;
	const rolesName = userCollection.userRoles;
	const emailConfirmName = userCollection.userEmailConfirm;
	const tokensName = userCollection.userTokens;
	const maxInvitesName = userCollection.userMaxInvites;
	const createdAtName = userCollection.createdAt;
	const updatedAtName = userCollection.updatedAt;

	const User = userCollection.model;

	const record = {
		[userCollection.idProperty]: userId.toString(),
		[birthdateName]: birthdate,
		[emailName]: email,
		[emailConfirmName]: emailConfirm,
		[rolesName]: isSuper ? [config.superRole] : [],
		[updatedAtName]: now,
		[createdAtName]: now,
		[tokensName]: {},
		[maxInvitesName]: config.defaultMaxInvites,
		[passwordName]: password,
		[passwordResetName]: '',
	};

	if (loginName !== emailName) {
		record[loginName] = login;
	}

	const validationErrors = userCollection.validateDocument(record);

	if (validationErrors) {
		return cb(validationErrors);
	}

	return new Promise((resolve, reject) => (
		crypto.hash(password, 10, (err, hash) => {
			if (err) {
				cb(err);
				return reject(err);
			}
			record[passwordName] = hash;
			return User.create(record).then(resolve, reject);
		})
	)).then(() => cb(), cb);
}

function forgotPassword(context, email) {
	const { userCollection } = context.store;

	const emailName = userCollection.userEmail;
	const passwordResetName = userCollection.userPasswordReset;

	const User = userCollection.model;

	return new Promise((resolve, reject) => (
		User.findOne({
			attributes: [userCollection.idProperty],
			where: { [emailName]: email },
		}).then((user) => {
			const userId = user[userCollection.idProperty];
			if (userId) {
				const passwordReset = nanoid();

				const result = { [emailName]: email, [userCollection.idProperty]: userId };

				const finish = (setErr) => {
					if (setErr) {
						return reject(setErr);
					}

					if (context.store.mailer) {
						return context.store.mailer
							.sendForgotPassword(context.config, email, `${userId}/${passwordReset}`)
							.then(() => resolve(result), reject);
					}

					return resolve(result);
				};

				return User.update({ [passwordResetName]: passwordReset }, {
					where: { [userCollection.idProperty]: userId },
				}).then(() => finish(), finish);
			}

			return resolve({ [emailName]: email, [userCollection.idProperty]: '' });
		}, reject)
	));
}

sequel_user.forgotPassword = forgotPassword;

function resetPassword(
	context,
	email,
	recoveryToken,
	newPassword,
) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;
		const passwordResetName = userCollection.userPasswordReset;

		const User = userCollection.model;

		return User.findOne({
			attributes: [idProperty, passwordResetName],
			where: { [userCollection.userEmail]: email },
		}).then((user) => {
			// TODO: handle null user
			const userId = user[idProperty];

			if (!userId) {
				return reject(new Error('Invalid credentials'));
			}

			const passwordReset = user[passwordResetName];

			if (recoveryToken === passwordReset) {
				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(hashErr);
					}

					return User.update({
						[passwordResetName]: '',
						[passwordName]: hash,
					}, {
						where: { [idProperty]: userId },
						limit: 1,
					}).then(() => (
						exports.encodeTokenFromLogin(email, context.config, (encodeTokenErr, token) => {
							if (encodeTokenErr) {
								return reject(encodeTokenErr);
							}

							return User.findOne({
								attributes: [tokensName],
								where: { [idProperty]: userId },
							}).then((tokensWrapper) => {
								const tokens = tokensWrapper[tokensName] || {};
								tokens[deviceId] = token;
								return User.update(
									{ [tokensName]: tokens },
									{
										attributes: [tokensName],
										where: { [idProperty]: userId },
										limit: 1,
									},
								).then(() => {
									const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

									return resolve({
										[idProperty]: userId.toString(),
										[tokensName]: tokenList,
										[tokenName]: token,
									});
								}, reject);
							}, reject);
						})
					), reject);
				});
			}

			return reject(new Error('Invalid credentials'));
		}, reject);
	});
}

sequel_user.resetPassword = resetPassword;

function updatePassword(
	context,
	userId,
	oldPassword,
	newPassword,
) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;

		const User = userCollection.model;

		if (!userId) {
			return reject(new Error('Invalid user'));
		}

		return User.findOne({
			attributes: [idProperty, passwordName, loginName],
			where: { [idProperty]: userId },
		}).then((user) => {
			const password = user[passwordName];
			const login = user[loginName];

			return crypto.compare(oldPassword, password, (err, authed) => {
				if (err) {
					return reject(err);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(err);
					}

					return User.update(
						{ [passwordName]: hash },
						{
							where: { [idProperty]: userId },
							limit: 1,
						},
					).then(() => (
						exports.encodeTokenFromLogin(login, context.config, (encodeTokenErr, token) => {
							if (encodeTokenErr) {
								return reject(encodeTokenErr);
							}

							return User.findOne({
								attributes: [tokensName],
								where: { [idProperty]: userId },
							}).then((tokensWrapper) => {
								const tokens = tokensWrapper[tokensName] || {};
								tokens[deviceId] = token;
								return User.update(
									{ [tokensName]: tokens },
									{
										attributes: [tokensName],
										where: { [idProperty]: userId },
										limit: 1,
									},
								).then(() => {
									const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

									return resolve({
										[idProperty]: userId.toString(),
										[tokensName]: tokenList,
										[tokenName]: token,
									});
								}, reject);
							}, reject);
						})
					), reject);
				});
			});
		}, reject);
	});
}

sequel_user.updatePassword = updatePassword;

function revokeInvite(context, id) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;
		const { idProperty } = userInviteCollection;

		const UserInvite = userInviteCollection.model;

		return UserInvite.destroy({
			where: { [idProperty]: id },
			limit: 1,
		}).then(() => resolve({ [idProperty]: id.toString() }), reject);
	});
}

sequel_user.revokeInvite = revokeInvite;

function sendInvite(context, authorId, email, message) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;

		const authorName = userInviteCollection.belongsTo[0];
		const emailName = userInviteCollection.userEmail;
		const UserInvite = userInviteCollection.model;

		const inviteId = nanoid();

		return UserInvite.create({
			[userInviteCollection.idProperty]: inviteId,
			[authorName]: authorId,
			[emailName]: email,
		}).then((result) => {

			if (userInviteCollection.permission && !userInviteCollection.hasPermission(result, context)) {
				return resolve(null);
			}

			result = userInviteCollection.convertToGraphQL(result);
			result = userInviteCollection.redactSecureDataForUser(result, context);

			const finish = () => resolve(result);
			// TODO: why does this hang?, but tests pass without it.
			// const finish = () => (
			// 	collection.resolveDocumentRelations(resolve, reject, result, context, info)
			// )

			if (context.store.mailer) {
				return context.store.mailer
					.sendInviteEmail(context.config, email, message, `${inviteId}`)
					.then(finish, reject);
			}
			return finish;
		}, reject);
	});
}

sequel_user.sendInvite = sendInvite;

function signIn(context, login, password) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;

		const User = userCollection.model;

		return User.findOne({
			attributes: [idProperty, passwordName],
			where: { [loginName]: login },
		}).then((user) => {
			if (!user) {
				return reject(new Error('Invalid credentials'));
			}

			const userId = user[idProperty];

			if (!userId) {
				return reject(new Error('Invalid credentials'));
			}

			const secret = user[passwordName];

			return crypto.compare(password, secret, (err, authed) => {
				if (err) {
					return reject(err);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return encodeTokenFromLogin(login, context.config, (tokenErr, token) => {
					if (tokenErr) {
						return reject(tokenErr);
					}

					return User.findOne({
						attributes: [tokensName],
						where: { [idProperty]: userId },
					}).then((tokensWrapper) => {
						const tokens = tokensWrapper[tokensName] || {};
						tokens[deviceId] = token;
						return User.update(
							{ [tokensName]: tokens },
							{
								attributes: [tokensName],
								where: { [idProperty]: userId },
								limit: 1,
							},
						).then(() => {
							const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

							return resolve({
								[idProperty]: userId.toString(),
								[loginName]: login,
								[tokensName]: tokenList,
								[tokenName]: token,
							});
						}, reject);
					}, reject);
				});
			});
		}, reject);
	});
}

sequel_user.signIn = signIn;

function signOut(context, id) {
	return new Promise((resolve, reject) => {
		const { deviceId, user, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const tokensName = userCollection.userTokens;

		const User = userCollection.model;

		if (!user) {
			return resolve();
		}

		const userId = context.super ? id : context.userId;

		if (context.userId === userId) {
			context.userId = null;
			context.user = null;
		}

		return User.findOne({
			attributes: [tokensName],
			where: { [idProperty]: userId },
		}).then((tokensWrapper) => {
			const tokens = tokensWrapper[tokensName] || {};
			delete tokens[deviceId];
			return User.update(
				{ [tokensName]: tokens },
				{
					attributes: [tokensName],
					where: { [idProperty]: userId },
					limit: 1,
				},
			).then(() => resolve({ [idProperty]: userId.toString() }), reject);
		}, reject);
	});
}

sequel_user.signOut = signOut;

function cleanUserData(rawData/* , userCollection */) {
	const user = {};

	Object.keys(rawData).forEach((property) => {
		let value = rawData[property];

		// if (property === userCollection.idProperty) {
		// 	user[userCollection.idProperty] = rawData[userCollection.idProperty].toString();
		// }

		if (property === 'birthdate' || property === 'created' || property === 'updated') {
			value = new Date(value);
		}

		if (value !== null || value !== undefined) {
			user[property] = value;
		}
	});

	return user;
}

sequel_user.cleanUserData = cleanUserData;

function getByLogin(context, login) {
	const { userCollection } = context.store;

	const loginName = userCollection.userLogin;

	const User = userCollection.model;

	return new Promise((resolve, reject) => (
		User.findOne({ where: { [loginName]: login } }).then((userRecord) => {
			if (!userRecord) {
				return resolve(null);
			}

			const user = userRecord.dataValues;

			const cleanUser = cleanUserData(user, userCollection);

			return resolve(cleanUser);
		}, reject)
	));
}

sequel_user.getByLogin = getByLogin;

sequel_user.helpers = {
	create: createHelper,
};

function refreshToken(context, login) {
	return new Promise((resolve, reject) => {
		const { deviceId, store, userId } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;

		const User = userCollection.model;

		if (!login) {
			return reject(new Error('Invalid session'));
		}

		return encodeTokenFromLogin(login, context.config, (err, token) => {
			if (err) {
				return reject(err);
			}

			return User.findOne({
				attributes: [tokensName],
				where: { [idProperty]: userId },
			}).then((tokensWrapper) => {
				const tokens = tokensWrapper[tokensName] || {};
				tokens[deviceId] = token;
				return User.update(
					{ [tokensName]: tokens },
					{
						attributes: [tokensName],
						where: { [idProperty]: userId },
						limit: 1,
					},
				).then(() => {
					const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

					return resolve({
						[idProperty]: userId,
						[loginName]: login,
						[tokensName]: tokenList,
						[tokenName]: token,
					});
				}, reject);
			}, reject);
		});
	});
}

sequel_user.refreshToken = refreshToken;

function register(
	context,
	login,
	email,
	password,
	birthdate,
	invitation,
) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userCollection, userInviteCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;

		const UserInvite = userInviteCollection.model;

		const User = userCollection.model;

		const userId = userCollection.generateDocumentId();

		const isSuper = email === context.config.superLogin;

		const now = new Date();

		function saveUser() {
			return User.count({ where: { [loginName]: login } }).then((count) => {
				const existing = count > 0;

				if (existing) {
					return reject(new Error('Exists'));
				}

				const emailConfirm = isSuper ? null : nanoid();

				const inviteEmailName = userInviteCollection.userEmail;

				return UserInvite.destroy({
					where: { [inviteEmailName]: email },
				}).then(() => {
					function createUser() {
						createHelper(
							context,
							context.config,
							{
								birthdate,
								login,
								email,
								emailConfirm,
								isSuper,
								now,
								password,
								userId,
							},
							(setErr/* , results */) => {
								if (setErr) {
									return reject(setErr);
								}

								context.userId = userId;

								context.user = {
									[idProperty]: userId.toString(),
									[loginName]: login,
								};

								context.super = isSuper || context.super || false;

								const finish = () => (
									refreshToken(context, login).then(resolve, reject)
								);

								userCollection.emit('created', { [idProperty]: userId });

								if (context.store.mailer) {
									return context.store.mailer
										.sendConfirmEmail(context.config, email, `${userId}/${emailConfirm}`)
										.then(finish, reject);
								}

								return finish();
							},
						);
					}

					if (invitation) {
						UserInvite.destroy({
							where: { [userInviteCollection.idProperty]: invitation },
							limit: 1,
						}).then(() => createUser(), reject);
					}

					return createUser();
				}, reject);
			}, reject);
		}

		// console.log('GOT HERE', context.config.inviteOnly, isSuper, context.super, context.user);
		if (context.config.inviteOnly && (!isSuper && !context.super)) {
			return UserInvite.count(
				invitation
					? { [userInviteCollection.idProperty]: invitation }
					: { [userInviteCollection.userEmail]: email },
			).then((amount) => {
				if (amount > 0) {
					return saveUser();
				}

				return reject(new Error('Not invited'));
			}, reject);
		}

		return saveUser();
	});
}

sequel_user.register = register;

function confirmEmail(context, id, emailConfirm) {
	const { userCollection } = context.store;
	const { idProperty } = userCollection;

	const emailConfirmName = userCollection.userEmailConfirm;

	const User = userCollection.model;

	return new Promise((resolve, reject) => {
		if (!emailConfirm) {
			return reject(new Error('Missing @emailConfirm argument'));
		}

		return User.findOne({
			attributes: [idProperty, emailConfirmName],
			where: { [idProperty]: id },
		}).then((user) => {
			if (!user) {
				return reject(new Error('Invalid confirmation'));
			}

			const actualEmailConfirm = user[emailConfirmName];

			if (
				emailConfirm !== null
				&& actualEmailConfirm !== ''
				&& actualEmailConfirm !== emailConfirm
			) {
				return reject(new Error('Invalid confirmation'));
			}

			return User.update(
				{ [emailConfirmName]: '' },
				{
					where: { [idProperty]: id },
					limit: 1,
				},
			).then(() => resolve({ [idProperty]: id }), reject);
		}, reject);
	});
}

sequel_user.confirmEmail = confirmEmail;

function updateEmail(context, id, newEmail) {
	return new Promise((resolve, reject) => {
		const { userCollection } = context.store;
		const { idProperty } = userCollection;

		const emailName = userCollection.userEmail;
		const loginName = userCollection.userLogin;
		const emailConfirmName = userCollection.userEmailConfirm;

		const User = userCollection.model;

		if (!newEmail) {
			return reject(new Error('Missing @email argument'));
		}

		return User.findOne({
			attributes: [idProperty, emailName],
			where: { [idProperty]: id },
		}).then((user) => {
			const userId = user[idProperty];

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const { email } = user;

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = loginName === emailName && email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			return User.update({
				[emailName]: newEmail,
				[emailConfirmName]: emailConfirm,
			}, {
				where: { [idProperty]: id },
				limit: 1,
			}).then(() => {
				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

sequel_user.updateEmail = updateEmail;

function updateEmailAndLogin(context, id, newEmail, newLogin) {
	return new Promise((resolve, reject) => {
		newLogin = newLogin || newEmail;

		const { userCollection } = context.store;
		const { idProperty } = userCollection;

		const emailName = userCollection.userEmail;
		const loginName = userCollection.userLogin;
		const emailConfirmName = userCollection.userEmailConfirm;

		const User = userCollection.model;

		if (!newEmail) {
			return reject(new Error('Missing @email argument'));
		}

		if (!newLogin) {
			return reject(new Error('Missing @login argument'));
		}

		return User.findOne(
			{
				attributes: loginName === emailName
					? [idProperty, loginName]
					: [idProperty, emailName, loginName],
				where: { [idProperty]: id },
			},
		).then((user) => {
			const userId = user[idProperty];

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const { email } = user;

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			const changes = {
				[emailName]: newEmail,
				[emailConfirmName]: emailConfirm,
			};

			if (!loginName !== emailName) {
				changes[loginName] = newLogin;
			}

			return User.update(changes, {
				where: { [idProperty]: id },
				limit: 1,
			}).then(() => {
				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

sequel_user.updateEmailAndLogin = updateEmailAndLogin;

function updateLogin(context, id, newLogin) {
	return new Promise((resolve, reject) => {
		const { userCollection } = context.store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		// const emailName = userCollection.userEmail;
		// if (emailName === loginName) {
		// 	return reject(new Error('Do not use updateLogin when @email and @login are the same.'));
		// }

		const User = userCollection.model;

		if (!newLogin) {
			return reject(new Error('Missing @login argument'));
		}

		return User.findOne({
			attributes: [idProperty, loginName],
			where: { [idProperty]: id },
		}).then((user) => {
			const userId = user[idProperty];

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const login = user[loginName];

			if (login === newLogin) {
				return resolve({ [idProperty]: id });
			}

			return User.update({
				[loginName]: newLogin,
			}, {
				where: { [idProperty]: id },
				limit: 1,
			}).then(() => resolve({ [idProperty]: id }), reject);
		}, reject);
	});
}

sequel_user.updateLogin = updateLogin;

sequel_user.getJWTOptions = getJWTOptions;

sequel_user.isAuthenticated = setupIsAuthenticated(sequel_user.getByLogin);

sequel_user.extractFromToken = extractFromToken;

sequel_user.encodeTokenFromLogin = encodeTokenFromLogin;
