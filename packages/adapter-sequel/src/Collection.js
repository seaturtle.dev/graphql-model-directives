/* eslint global-require:0 */

const { nanoid } = require('@seaturtle/gmd-core/id.js');

const Collection = require('@seaturtle/gmd-adapter-generic/Collection.js');

/**
 * Sequel Collection
 * @exports SequelCollection
 * @class
 * @extends Collection */
class SequelCollection extends Collection {
	/**
	 * [toSequel description]
	 * @param  {[type]} sequelize [description]
	 * @param  {[type]} Sequelize [description]
	 * @return {[type]}           [description]
	 */
	toSequel(sequelize, Sequelize) {
		Sequelize = Sequelize || require('sequelize');

		this.generateDocumentId = () => nanoid();

		this.Op = Sequelize.Op;

		const sequelizeTypeMap = {
			Array: Sequelize.JSON,
			Boolean: Sequelize.BOOLEAN,
			Buffer: Sequelize.BLOB,
			Date: Sequelize.DATEONLY,
			DateTime: Sequelize.DATE,
			Float: Sequelize.DOUBLE,
			ID: Sequelize.STRING(21),
			Int: Sequelize.INTEGER,
			JSON: Sequelize.JSON,
			Object: Sequelize.JSON,
			ObjectId: Sequelize.STRING(36),
			String: Sequelize.STRING,
		};

		const sequelizeFields = {};
		const sequelizeOptions = {
			// don't add the timestamp attributes (updatedAt, createdAt)
			timestamps: false,
			// If don't want createdAt
			createdAt: false,
			// If don't want updatedAt
			updatedAt: false,
		};
		const indexes = [];

		this.fields.forEach((field/* , index */) => {
			const property = this.properties[field];

			if (this.virtuals.indexOf(field) !== -1) {
				return;
			}

			const fieldType = (
				property.modelType
				|| property.field.type.toString().replace(/!/g, '')
			);

			let type;

			if (fieldType.charAt(0) === '[') {
				type = [];

				const arrayOf = fieldType.substring(1, fieldType.length - 1);

				if (sequelizeTypeMap[arrayOf]) {
					type.push(sequelizeTypeMap[arrayOf]);
				} else if (property.relation) {
					type.push(Sequelize.UUID);
				} else {
					type.push(sequelizeTypeMap.Object);
				}
			} else if (sequelizeTypeMap[fieldType]) {
				type = sequelizeTypeMap[fieldType];
			} else {
				type = sequelizeTypeMap.Object;
			}

			const schemaField = {
				type,
			};

			// TODO: generate indexes and other settings for sequelize

			// schemaField.allowNull = true;

			if (property.uniques) {
				property.uniques.forEach((unique) => {
					if (unique.compounds) {
						indexes.push({
							fields: [field].concat(unique.compounds || []),
							unique: true,
						})
					} else {
						schemaField.unique = true;
					}
				});
			}

			if (property.sortedIndexes) {
				property.sortedIndexes.forEach((sortedIndex) => {
					indexes.push({
						fields: [field].concat(sortedIndex.compounds || []),
						unique: false,
					});
				});
			}

			if (field === this.idProperty) {
				schemaField.primaryKey = true;
			}

			sequelizeFields[field] = schemaField;
		});

		const model = (sequelize || this.store.client).define(
			this.name,
			sequelizeFields,
			sequelizeOptions,
		);

		this.model = model;

		return model;
	}

	/**
	 * [definePropertyUserTokens description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserTokens(field) {
		super.definePropertyUserTokens(field);
		return this.definePropertyModelType(field, 'Object');
	}

	/**
	 * [convertToStore description]
	 * @param  {[type]} context  [description]
	 * @param  {[type]} params   [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToStore(context, params, iterator) {
		return super.convertToStore(context, params, (field, property, value, document, id) => {
			// if (property.relation) {
			// 	console.log('SQL', field, property, value, document, id);
			// }
			if (typeof iterator === 'function') {
				iterator(field, property, value, document, id);
			}
		});
	}

	/**
	 * [convertToGraphQL description]
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToGraphQL(document, iterator) {
		return super.convertToGraphQL(document, (field, property, value, doc) => {
			if (property.relation) {
				const related = this.store.collection(
					property.relation.modelName,
				);

				if (!property.relation.many) {
					if (doc[field] && doc[field][related.idProperty]) {
						doc[field][related.idProperty] = doc[field][related.idProperty].toString('hex');
					} else {
						doc[field] = { [related.idProperty]: '' };
					}
				}
			}

			// TODO: sqlite doesnt convert strings to arrays
			// if (Array.isArray(field.field.type)) {
			// }

			if (typeof iterator === 'function') {
				iterator(field, property, value, doc);
			}
		});
	}

	/**
	 * [mapQueryOperators description]
	 * @method
	 * @param  {[type]}  target               [description]
	 * @return {[type]}                       [description]
	 */
	mapQueryOperators(target) {
		const recursivelyMapTargetOperations = (scope) => {
			const newScope = {};
			if (!scope) {
				return scope;
			}
			Object.keys(scope).forEach((key) => {
				let value = scope[key];
				if (value && typeof value === 'object' && !(value instanceof Date)) {
					if (Array.isArray(value)) {
						value = value.map(recursivelyMapTargetOperations);
					} else {
						value = recursivelyMapTargetOperations(value);
					}
				}
				let isOp = false;
				if (key.charAt(0) === '$') {
					const operationName = key.substr(1);
					if (this.Op[operationName]) {
						isOp = true;
						newScope[this.Op[operationName]] = value;
					}
					// TODO: check aliases and handle compatability
				}
				if (!isOp) {
					newScope[key] = value;
				}
			});
			return newScope;
		};
		return recursivelyMapTargetOperations(target);
	}
}

module.exports = SequelCollection;
