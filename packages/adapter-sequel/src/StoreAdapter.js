/* eslint global-require:0 */

// const { EventEmitter } = require('events');

let Sequelize = null;
let model = null;
let user = null;

try {
	Sequelize = require('sequelize');
	model = require('./model');
	user = require('./user');
} catch (err) {
	// console.warn('sequelize not installed');
}

const StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');
const SequelCollection = require('./Collection');

const migration = require('./migration');

/**
 * Uses `Sequelize` ORM to provide adapters for various sequel databases including:
 * `postgres`, `mysql`, `mariadb`, `mssql` and `sqlite3`
 * @exports SequelStore
 * @class
 * @extends StoreAdapter */
class SequelStore extends StoreAdapter {
	/**
	 * [throwSequelizeUnavailable description]
	 * @return {[type]} [description]
	 */
	throwSequelizeUnavailable() {
		/* istanbul ignore next */
		throw new Error('NPM install sequelize, sequelize is not available.');
	}

	/**
	 * [constructor description]
	 * @param {[type]} sequelizeParams [description]
	 * @param {[type]} resolvers       [description]
	 * @param {[type]} pubsub          [description]
	 * @param {[type]} uploader        [description]
	 * @constructs
	 */
	constructor(sequelizeParams, { resolvers, pubsub, uploader, crypto }) {
		super(resolvers);
		/* istanbul ignore next */
		if (!Sequelize) { this.throwSequelizeUnavailable(); }

		const sequelize = new Sequelize(Object.assign(sequelizeParams, {
			logging: false,
			pool: {
				max: 5,
				min: 0,
				acquire: 30000,
				idle: 10000,
			},
		}));

		// TODO:
		// this.uploader = uploader;

		this.crypto = crypto || this.crypto;
		this.pubsub = pubsub || this.pubsub;
		this.client = sequelize;
	}

	/**
	 * [postSchema description]
	 * @param {Function} cb [description]
	 * @return {[type]} [description]
	 */
	postSchema(cb) {
		/* istanbul ignore next */
		if (!this.client) { this.throwSequelizeUnavailable(); }

		const collectionNames = Object.keys(this.models);

		this.sequel = {};

		collectionNames.forEach((collectionName) => {
			const collection = this.models[collectionName];
			this.sequel[collectionName] = collection.toSequel(this.client, Sequelize);
		});

		return this.client.sync().then(() => {
			// console.info('Synced');
			if (typeof cb === 'function') { return cb(); }
			return null;
		}, (err) => {
			console.error('SQL STORE ERR:', err);
			if (typeof cb === 'function') { return cb(); }
			return null;
		});
	}

	/**
	 * [close description]
	 * @param  {Function} cb [description]
	 * @return {[type]}      [description]
	 */
	close(cb) {
		this.client.close().then(cb, cb);
	}

	/**
	 * [mixinUser description]
	 * @param  {[type]} collection [description]
	 * @return {[type]}            [description]
	 */
	mixinUser(collection) {
		/* istanbul ignore next */
		if (!user) { this.throwSequelizeUnavailable(); }

		return super.mixinUser(collection, user, model);
	}

	/**
	 * [authenticate description]
	 * @param  {[type]}   context [description]
	 * @param  {[type]}   info    [description]
	 * @param  {Function} next    [description]
	 * @param  {[type]}   or      [description]
	 * @return {[type]}           [description]
	 */
	authenticate(context, info, next, or, roles = []) {
		/* istanbul ignore next */
		if (!user) { this.throwSequelizeUnavailable(); }

		return new Promise((resolve, reject) => (
			user.isAuthenticated(roles, context, info).then(() => (
				Promise.resolve(next()).then(resolve, reject)
			), (err) => {
				if (or === true) { return resolve(next(err)); }

				if (typeof or === 'function') { return resolve(or(err, next)); }

				return reject(err);
			})
		));
	}

	/**
	 * [readDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	readDocument({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.read(this, name, target, options, context, info);
	}

	/**
	 * [findDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	findDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.find(this, name, target, options, context, info);
	}

	/**
	 * [countDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	countDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.count(this, name, target, options, context, info);
	}

	/**
	 * [scanDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	scanDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.scan(this, name, target, options, context, info);
	}

	/**
	 * [purgeDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	purgeDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.purge(this, name, target, options, context, info);
	}

	/**
	 * [upsertDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	upsertDocument({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.upsert(this, name, target, options, context, info);
	}

	/**
	 * [removeDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	removeDocument({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwSequelizeUnavailable(); }

		return model.remove(this, name, target, options, context, info);
	}

	/**
	 * [migration description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	migration(object, options) {
		return migration(object, options);
	}

	// /**
	//  * [undoMigration description]
	//  * @param  {[type]} channel [description]
	//  * @return {[type]}         [description]
	//  */
	// undoMigration(migrations) {
	// 	return Promise.resolve();
	// }
}

SequelStore.Collection = SequelCollection;

module.exports = SequelStore;
