## Installation

Install Seaturtle GraphQL Model Directives

```sh
> npm install @seaturtle/graphql-model-directives
```

Install the Apollo Server flavor of your choice with subscription transports.

```sh
> npm install apollo-server-express express
```

Install the DB libraries you plan to use:

```sh
> npm install redis redis-mock mongooose sequelize sqlite3
```
