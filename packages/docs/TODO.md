# HIGH PRIORITY:
 - [] fix subscriptions
 - [x] finish update-graphql-apollo branch
 - [ ] finish @resource(belongsTo: [..., "more", "than", "one"], allowAccess: ["teamMembers"])
 - [ ] @resource(quota: ["entity", "owner"]), @quota and @userQuota that prevents users from creating too many resources without being billed, this could connect with @event to make this very trackable

 - [ ] @resolve - rest api call, or shell command, or module/function call, email resolver
 - [ ] @permission - relation based and ensure resource permissions are safely done for each adapter
                   - @permission(read: ['admin'], write: ['admin']) only allow admin to read write settings
 - [ ] better model filtering support with regexp strings and other operators
 - [ ] atomic nested list reconcilation (mongodb using findandupdate reconcilation, redis using locks, sql transactions?)
 - [ ] compound indexes, uniques, filtering and sorting
 - [ ] purge, many orphan removal
 - [ ] only select fields that are being queried
 - [x] move graphql to peerDependencies
 - [x] allow ./crypto override for @encrypt, @password
 - [ ] add @phone for user phone number based registration and invites
 - [ ] add @name for annotating nameProperty on collections which might be handy

 - [ ] @immutabe which marks a field to only be settable on create, this would require more robust create detection in upsert

 - [ ] __pages or @page(cache: true) collection for storing page caching information for other collections, this is a useful caching feature
 - [ ] config.onRegister() additional callback for allowing user setup scripts
 - [ ] config.onInvite() additional callback for allowing invite hooks
 - [ ] @maillist plugin for @api
 - [ ] config.onAddMailListEmail() new callback for allowing maillist email hooks
 - [ ] config.onRemoveMailListEmail() for unsubscribe
 - [ ] deleting parent relation currently leaves orphans, they should be also deleted
 - [ ] @event and @segment plugin for analytic proxying and multiplexing, why use twilio here?
 - [ ] @search plugin for allowing and configuring text based search on documents across feilds
 - [ ] @gps for annotating a set of gps coordinates, adding nearest API calls as well
 - [ ] @spatial for annotating a spatial index, adding spatiel queries to find
 - [ ] @id(strategy: 'MongoID') for selecting the way ids are randomly generated, later on we can support incrementing indexes
 - [ ] use ui project to create an automatic admin interface for editing data, easy lol

# PROJECT:
 - [ ] major test coverage
 - [ ] more complete docs
 - [ ] logo
 - [ ] github/npm release
 - [ ] add cloud deployments to example

# FUTURE:
 - [ ] @adapter directive to specify which adapter strategy to use on a table basis
 - [ ] make it so user auth can be proxied to another service
 - [x] separate adapters from this project
 - [ ] add a multiplexer adapter
 - [ ] add a RIAK adapter soon please, chat dream come true
         - https://github.com/lumen/lumen#goals -- use this lumen thing to make it run in the client too :)
 - [ ] add a elastic adapter
 - [ ] add a neo4j adapter
 - [ ] add a vector db for ai
 - [ ] add a dht adapter (cassandra, riak)
 - [ ] add a rabbitmq adapter for subscriptions
 - [ ] support payment requests api
 - [ ] support real time file streaming?
