# Server Setup

Create a file called `server.js`:

```js
const { ApolloApiServer } = require('@seaturtle/graphql-model-directives');

const { ApolloServer } = require('apollo-server-express')
const express = require('express');

const config = require('./config');

const rawSchema = require('fs').readFileSync(
  require('path').join(__dirname, 'schema.graphql')
);

const resolvers = {
  // (OPTIONAL) defined automatically
};

const store = new MemoryStore(null, {
  resolvers,
});

const server = new ApolloApiServer({
  store,
  rawSchema,
  config,
  resolvers
}, ApolloServer, express);
```

Create a file called `schema.graphql`:

<!-- using js syntax until gql highlighting works -->
```gql
type User @model(name: "users") @user @api {
  id:                ID!      @id            @permission
  created:           DateTime @created       @index(dir: 1)
  updated:           DateTime @updated       @index(dir: 1)
  birthdate:         Date     @birthdate     @validation(params: { minAge: 13 })
  age:               String   @virtual       @permission(condition: "showAge")
  showAge:           Boolean                 @default(value: false)
  maxInvites:        Int      @maxInvites    @default(value: 10)
  email:             String   @email @login  @validation(params: { template: "email" })
  emailConfirm:      String   @emailConfirm
  password:          String   @password      @validation(params: { min: 8, max: 32 })
  passwordReset:     String   @passwordReset
  name:              String   @index
  roles:             [String] @roles
  tokens:            [String] @tokens
  token:             String   @token

  resources(input: SortedPageInput): JSONSortedPage @virtual @resolve(model: {
    name: "Resource",
    method: "find",
    params: [ { author: "root.id" } ]
  })

  finishedResources(page: SortedPageInput): JSONSortedPage @virtual @resolve(model: {
    name: "Resource",
    method: "find",
    params: [ { author: "root.id", isDone: true } ]
  })

  pendingResources(page: SortedPageInput): JSONSortedPage @virtual @resolve(model: {
    name: "Resource",
    method: "find",
    params: [ { author: "root.id", isDone: false } ]
  })
}

type UserInvite @model(name: "userinvites") @resource(belongsTo: "user") @invite(from: "user") @api {
  id:    ID!    @id       @permission
  user:  User   @relation @default(context: "user.id")
  email: String @index    @email @validation(params: { template: "email" })
}

type Resource @model(name: "resource") @permission(condition: "isPublic") @api {
  id:               ID!        @id @permission
  created:          DateTime   @created
  updated:          DateTime   @updated
                               @index(dir: -1, compounds: ["author", "isDone"])
  user:             User       @relation(id: 0)
                               @default(context: "user.id")
  authorName:       String     @virtual(params: { method: "alias", value: "author.name" })
  description:      String     @default(value: "something something something")
  file:             File       @file @permission
  fileUrl:          String     @permission
  finished:         DateTime
  json:             JSON
  boolean:          Boolean
  float:            Float
  integer:          Int
  date:             Date
  string:           String
  nestedResources:  [Resource] @nested
  relatedResources: [Resource] @relation
  isPublic:         Boolean    @default(value: true)
  isDone:           Boolean    @default(value: false) @index
}

type Subscription {
  resourceCreated: Resource
  resourceUpdated: Resource
  resourceRemoved: Resource
}

type Query {
  finishedResources(page: SortedPageInput): JSONSortedPage @resolve(model: {
    name: "Resource",
    method: "find",
    params: [ { user: "context.userId", isDone: true } ]
  }) @auth

  pendingResources(page: SortedPageInput): JSONSortedPage @resolve(model: {
    name: "Resource",
    method: "find",
    params: [ { user: "context.userId", isDone: false } ]
  }) @auth
}

type Mutation {
  upsertResourceFile(id: ID, file: Upload): Resource @resolve(model: {
    method: "upsert",
    params: [ [ ".mixin.args", { user: "context.userId" } ] ]
  }) @auth
}
```

Create a `config.js` file:

```js
const path = require('path');
exports.adminToken = 'admin';
exports.adminEmail = 'admin@local';
exports.superLogin = 'admin@local';
exports.superRole = 'super';
exports.inviteOnly = false;
exports.defaultMaxInvites = 10;
exports.jwtAudience = '';
exports.jwtIssuer = '';
exports.jwtHmacSecret = 'secret';
exports.awsPath = null;
exports.uploader = require('./uploader'); // see example/uploader.js
exports.uploaderS3Bucket = 'uploads';
exports.uploaderS3PublicURL = '';
exports.uploaderLocalPath = path.join(__dirname, 'uploads');
exports.uploaderPublicPath = '/uploads';
exports.mailer = require('./mailer'); // see example/mailer.js
exports.mailerType = 'smtp'; // or aws-ses
exports.mailerSMTP = {};
exports.mailerAWS = null;
exports.mailerLog = true;
exports.noreply = 'noreply@local';
const linkHost = 'http://localhost:3000';
exports.linkConfirmEmail = `${linkHost}/confirmEmail`;
exports.linkForgetPassword = `${linkHost}/forgetPassword`;
exports.linkInviteEmail = `${linkHost}/inviteEmail`;
```
