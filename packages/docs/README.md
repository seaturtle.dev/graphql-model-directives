# graphql-model-directives

[![pipeline status](https://gitlab.com/seaturtle.dev/graphql-model-directives/badges/master/pipeline.svg)](https://gitlab.com/seaturtle.dev/graphql-model-directives/-/commits/master)
[![coverage report](https://gitlab.com/seaturtle.dev/graphql-model-directives/badges/master/coverage.svg)](https://gitlab.com/seaturtle.dev/graphql-model-directives/-/commits/master)

### What does it do?
 * Automatically create GraphQL servers from schema files.
 * Waste less time writing and maintaining your backend code.
 * Simplifies writing queries, mutations and seamlessly defines your resolver functions.
 * Generates useable Model/ORM for any backend adapter.
 * Built-in user authentication with user role-based access.
 * Easy pagination featuring field filtering and sorting.
 * Model validations on save, (nested) relations, and automatic orphan deletion.
 * User resources model with permissions system.
 * GraphQL Subscription over WebSocket connection.
 * Harmonious file uploading, with cloud or local processes.
 * Built-in email invitations using nodemailer, or custom methods.
 * Supports Memory, Redis, MongoDB, and several types of SQL database integrations!

### At a glance

This example will create a server that has a users collection, and a resources collection that belongs to users.

It works dynamically, and doesn't generate code because it is meant to work at runtime.
You can still hook in resolvers for a custom API.

The idea is you provide a `graphql.schema` with `graphql-model-directives` and it will create and ORM, and API that works perfectly with GraphQL Apollo.

```
type User @model(name: "users") @user @api {
  id:                ID!      @id            @permission
  created:           DateTime @created       @index(dir: 1)
  updated:           DateTime @updated       @index(dir: 1)
  birth:             Date     @birthdate     @validation(params: { minAge: 13 })
  age:               String   @virtual       @permission(condition: "showAge")
  showAge:           Boolean                 @default(value: false)
  limitInvites:      Int      @maxInvites    @default(value: 10)
  emailConfirmation: String   @emailConfirm
  emailAddr:         String   @email @login  @validation(params: { template: "email" })
  pass:              String   @password      @validation(params: { min: 4, max: 64 })
  passReset:         String   @passwordReset

  access:            [String] @roles
  tokenList:         [String] @tokens
  tok:               String   @token

  name:              String   @permission
}

type Resource
@model(name: "resource")
@resource(belongsTo: [ "author" ])
@permission(condition: "isPublic")
@api
{
  id:              ID!                  @id @permission
  created:         DateTime             @created
  updated:         DateTime             @updated
                                        @index(dir: -1, compounds: [ "author", "isDone" ])
  authorName:      String               @virtual(params: { method: "alias", value: "author.name" })
  author:          User                 @relation(preserve: true)
                                        @default(context: "user.id")
  description:     String               @default(value: "something something something")
  finished:        DateTime             @index
  json:            JSON
  unique:          String               @unique
  date:            Date                 @index
  float:           Float                @index @default(value: 1.0)
  integer:         Int                  @index @default(value: 0)
  string:          String               @index @default(value: "")
  isDone:          Boolean              @index @default(value: false)
  isPublic:        Boolean              @default(value: true)
}

```

### Documentation Links
* [Quick Start](https://gitlab.com/seaturtle.dev/graphql-model-directives/-/blob/master/packages/docs/README.md)
* [Installation](https://gitlab.com/seaturtle.dev/graphql-model-directives/-/blob/master/packages/docs/INSTALL.md)
* [Server Setup Guide](https://gitlab.com/seaturtle.dev/graphql-model-directives/-/blob/master/packages/docs/GUIDE.md)
* [Server Example](https://gitlab.com/seaturtle.dev/graphql-model-directives/-/blob/master/packages/server-example/README.md)
