/* eslint global-require:1 */
/* eslint import/no-extraneous-dependencies:1 */
/* eslint import/no-unresolved:1 */

import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import * as documentation from 'documentation';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// let hljs;
let vfs;
let streamArray;
try {
	// hljs = await import('documentation/node_modules/highlight.js');
	vfs = await import('documentation/node_modules/vinyl-fs');
	streamArray = await import('documentation/node_modules/stream-array');
} catch (err) {
	console.warn('Using documentation dependencies `hightlight.js` and `vinyl-js`');
}
// if (!hljs) {
	// hljs = await import('highlight.js');
// }
if (!vfs) {
	vfs = await import('vinyl-fs');
}
if (!streamArray) {
	streamArray = await import('stream-array');
}

// hljs = hljs.HighlightJS || hljs;

// if (hljs?.configure) {
// 	hljs.configure({
// 		highlightAuto: true,
// 		languages: ['js', 'javascript', 'css', 'html', 'css', 'gql', 'graphql', 'sh'],
// 	});
// }

function onFormatted(params, output) {
	// console.log(output);
	if (Array.isArray(output)) {
		streamArray(output).pipe(vfs.dest(params.output));
	} else if (output) {
		fs.writeFileSync(params.output, output);
	}
}

function onError(err) {
	/* eslint no-console: 0 */
	if (err instanceof Error) {
		console.error('BUILD DOCS ERROR:', err.stack);
	} else {
		console.error('BUILD DOCS ERROR:', err);
	}
	process.exit(1);
}

export function buildDocs(customParams) {
	let params = {
		config: path.join(__dirname, '..', 'documentation.yml'),
		github: true,
		input: [
			// path.join(__dirname, '..', '..', '..', 'src', 'index.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'core', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'core-directives', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'core-types', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-generic', 'src', 'ModelDefinitions.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-generic', 'src', 'Collection.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-generic', 'src', 'StoreAdapter.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-redis', 'src', 'StoreAdapter.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-mongodb', 'src', 'StoreAdapter.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-sequel', 'src', 'StoreAdapter.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-data', 'src', 'StoreAdapter.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'adapter-memory', 'src', 'StoreAdapter.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'plugin-authorize', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'plugin-crypto', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'plugin-email', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'plugin-upload', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'prepare-schema', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'server-common', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'server-express', 'src', 'ApolloApiExpress.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'server-fastify', 'src', 'ApolloApiFastify.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'server-example', 'src', '*.js'),
			// path.join(__dirname, '..', '..', '..', 'packages', 'test-adapter', 'src', '*.js'),
			path.join(__dirname, '..', '..', '..', 'packages', 'test-server', 'src', '*.js'),
		],
		// theme: path.join('node_modules', '@seaturtle', 'documentationjs-theme', 'index.js'),
		// format: 'html',
		// output: path.join(__dirname, '..', 'web'),
	};

	params = {
		...params,
		...((typeof customParams === 'function' ? customParams(params) : customParams) || {}),
	};

	documentation.build(params.input, {
		...params,
		hljsCustomizer: (hljs) => {
			hljs.registerLanguage('graphql', (e) => ({
				aliases: ['gql'],
				keywords: {
					keyword: 'query mutation subscription|10 type input schema directive interface union scalar fragment|10 enum on ...',
					literal: 'true false null',
				},
				contains: [
					e.HASH_COMMENT_MODE,
					e.QUOTE_STRING_MODE,
					e.NUMBER_MODE,
					{
						className: 'type',
						begin: '[^\\w][A-Z][a-z]',
						end: '\\W',
						excludeEnd: !0,
					},
					{
						className: 'literal',
						begin: '[^\\w][A-Z][A-Z]',
						end: '\\W',
						excludeEnd: !0,
					},
					{
						className: 'variable',
						begin: '\\$',
						end: '\\W',
						excludeEnd: !0,
					},
					{
						className: 'keyword',
						begin: '[.]{2}',
						end: '\\.',
					},
					{
						className: 'meta',
						begin: '@',
						end: '\\W',
						excludeEnd: !0,
					},
				],
				illegal: /([;<']|BEGIN)/,
			}));
		},
		hljs: {
			highlightAuto: false,
			languages: [
				'js', 'javascript',
				'css',
				'html',
				'gql', 'graphql',
				'sh', 'shell',
			],
		},
	}).then((output) => {
		// console.log('here', output);
		documentation.formats[params.format](output, {
			theme: params.theme,
		}).then(onFormatted.bind(null, params), onError);
	}, onError);
};
