import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { buildDocs } from '../buildDocumentation.mjs';

buildDocs({
	format: 'json',
	output: path.join(__dirname, '..', '..', '..', '..', 'api.json'),
});
