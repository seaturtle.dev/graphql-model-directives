// documentation build -f md index.js example/server.js example/templates.js test/integrations/example/server/index.js -o API.md

import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { buildDocs } from '../buildDocumentation.mjs';

buildDocs({
	format: 'md',
	input: [],
	output: path.join(__dirname, '..', '..', '..', '..', 'README.md'),
	config: path.join(__dirname, '..', '..', 'documentation.readme.yml'),
});
