import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { buildDocs } from '../buildDocumentation.mjs';

buildDocs({
  theme: path.join('node_modules', '@seaturtle', 'documentationjs-theme', 'index.js'),
  format: 'html',
  output: path.join(__dirname, '..', '..', 'web'),
});
