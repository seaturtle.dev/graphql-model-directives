// console.log('HELLO DEFINE RESOLVER');

module.exports = function (field, options) {
	// console.log('GOT HERE', 'defineResolver', field.name);
	// debugger;
	// console.log('got here', field, options);
	const modelName = (options.model && options.model.name) || field.modelname || field.type.name;
	if (options.value) {
		field.resolve = (/* root, args, context, info */) => options.value;
	}
	if (options.evaluate) {
		// field.resolve = (root, args, context, info) => eval(options.evaluate);
	}
	// TODO: support graphql resolver, rest, moleculer resolvers
	// if (options.graphql) {}
	// if (options.rest) {}
	// if (options.moleculer) {}
	function parameters(pars, root, args, context, info) {
		const scope = {
			root,
			args,
			context,
			info,
		};
		const utils = {
			str: (...strings) => {
				// debugger;
				// if (strings.length === 0) { return ''; }
				// if (strings.length === 1) { return strings[0]; }
				return strings;
			},
			// TODO: rename value to ref
			value: (name) => {
				const keys = name.split('.');
				let current = scope;
				while (keys.length) {
					const key = keys.shift();
					if (current && current[key]) {
						current = current[key];
					} else {
						current = null;
					}
				}
				return keys.length === 1 && current === null ? name : current;
			},
			// TODO: add pluck method which is the opposite of filter, then use it instead of filter in API directive
			filter: (object, fields) => {
				// debugger;
				// TODO: test
				// if (typeof object.filter === 'function') {
				// 	return object.filter((k) => fields.indexOf(k) !== -1)
				// }
				const newObject = {};
				Object.keys(object).forEach((property) => {
					if (fields.indexOf(property) === -1) {
						newObject[property] = object[property];
					}
				});
				return newObject;
			},
			filterAndMerge: (object, fields, ...items) => {
				// debugger;
				const newObject = utils.filter(object, fields);
				const mergedObject = utils.merge(newObject, ...items);
				return mergedObject;
			},
			mixin: (...items) => Object.assign(...items),
			merge: (...items) => Object.assign(...items.reverse()),
		};
		const mapParams = (p) => {
			if (Array.isArray(p)) {
				if (p[0].charAt(0) === '.') {
					p = p.slice(0);
					const utility = p.shift();
					const keys = utility.split('.');
					keys.shift();
					const method = keys.shift();
					// if (method === 'filter' || method === 'str') {
					// 	debugger;
					// }
					let object = keys.length ? utils.value(keys.shift()) : {};
					while (keys.length) {
						object = object[keys.shift()] || {};
					}
					if (method === 'str') {
						return utils.str(...p);
					}
					const objects = p.map(mapParams);
					return utils[method](object, ...objects);
				}
				return p.map(mapParams);
			}
			if (typeof p === 'object' && p) {
				const o = {};
				Object.keys(p).forEach((key) => {
					o[key] = mapParams(p[key]);
				});
				return o;
			}
			// TODO: allow strings to pass through and force [".ref.my.ref"]
			if (typeof p === 'string') {
				return utils.value(p);
			}
			return p;
		};
		if (Array.isArray(pars)) {
			pars = pars.map(mapParams);
		} else {
			pars = mapParams(pars);
		}
		return pars;
	}
	if (options.model) {
		const isSubscription = options.model.method === 'subscribe';
		if (isSubscription) {
			if (field.subscribe) {
				return;
			}
			const modelSubscriber = (r, a, c, i) => {
				let cachedFilter = null;
				return this.withFilter(
					(root, args, context, info) => {
						const model = context.model[modelName];
						const method = model[options.model.method];
						const getFinal = () => {
							cachedFilter = parameters(options.model.params[1], root, args, context, info);
							let final = method.apply(model, [options.model.params[0]]);
							if (typeof final.exec === 'function') {
								final = final.exec({ context, info });
							}
							return final;
						};
						return getFinal();
						// TODO: auth for subscriptions?
						// const { auth } = field;
						// if (!auth) {
						// 	return getFinal();
						// }
						// return new Promise((resolve, reject) => {
						// 	this.authenticate(context, info, (err) => {
						// 		if (err) { return reject(err); }
						// 		getFinal().then(resolve, reject);
						// 	});
						// });
					},
					(payload, vars, context, info) => {
						const subject = payload[info.fieldName];
						const filter = { ...cachedFilter };
						let failure = false;
						const model = context.model[modelName];
						model.enumerateRelations((key) => {
							if (filter[key] === undefined) { return; }
							if (failure) { return; }
							const value = filter[key];
							delete filter[key];
							const relationProperty = model.properties[key];
							const relatedModel = context.model[
								relationProperty.relation.modelName
							];
							const related = subject[key];
							failure = !(
								related === value
								|| (related && related[relatedModel.idProperty] === value)
							);
						});
						if (failure) { return false; }
						return Object.keys(filter).every((key) => (
							subject[key] === filter[key]
						));
					},
				)(r, a, c, i);
			};
			field.subscribe = field.subscribe || modelSubscriber;
			if (this.resolvers) {
				this.resolvers.Subscription = this.resolvers.Subscription || {};
				this.resolvers.Subscription[field.name] = this.resolvers.Subscription[field.name] || {};
				this.resolvers.Subscription[field.name].subscribe = (
					this.resolvers.Subscription[field.name].subscribe || modelSubscriber
				);
			}
		} else if (!field.resolve) {
			let skipAuth = false;
			if (options.model.skipAuth) {
				skipAuth = true;
			} else if (field.auth !== true) {
				skipAuth = true;
			}
			if (
				!skipAuth
				&& modelName === 'User'
				&& ([
					'register',
					'signIn',
					'confirmEmail',
					'forgotPassword',
					'resetPassword',
				]).indexOf(options.model.method) !== -1
			) {
				skipAuth = true;
			}
			const modelResolverPromise = (
				error,
				root,
				args,
				context,
				info,
			) => new Promise((resolve, reject) => {
				debugger;
				// console.log('model resolver promise');
				if (error) {
					return reject(error);
				}
				const params = parameters(options.model.params, root, args, context, info);
				const model = context.model[modelName];
				const method = model[options.model.method];
				if (!method) {
					return reject(new Error(`${modelName}::${options.model.method} is missing.`));
				}
				// console.log('CALL', options.model.method, modelName, params);
				// debugger;
				let final = method.apply(model, params);
				if (typeof final.exec === 'function') {
					final = final.exec({ context, info });
				}
				if (typeof final.then === 'function') {
					return final.then(resolve, reject);
				};
				return resolve(final);
			});
			const modelResolver = (
				(root, args, context, info) => (
					this.authenticate(
						context,
						info,
						(error) => modelResolverPromise(error, root, args, context, info),
						(error, next) => {
							if (skipAuth) { return next(); }
							return next(error);
						},
						field.allowed,
					)
				)
			);
			field.resolve = modelResolver;
		}
	}
}
