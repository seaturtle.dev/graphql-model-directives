const importJSON = require('./importJSON');
const exportJSON = require('./exportJSON');
const migrateJSON = require('./migrateJSON');

const migrationHelpers = require('./migrationHelpers');

// TODO: In order to avoid up/down this will backup and restore versions of the table,
//       to do this there needs to be a way to determine which schema version the data belonged.

module.exports = exports = async function migrate(model, migrators, options = {}) {
	const query = {};
	const fields = null;
	const {
		// undo,
		purge,
	} = options;
	const {
		store
	} = model;
	const json = await exportJSON(model, query, fields, options);
	const migrations = migrationHelpers(this, migrators)
	// const migrationMethod = undo ? store.undoMigration : store.migration;
	const migrationMethod = store.migration;
	let migration = await migrationMethod.call(store, migrations, options);
	migration = model.migration(migration, options);
	if (migration?.execute) {
		await migration.execute();
	}
	const newJSON = await migrateJSON(json, migration, options);
	// TODO: use json based version of graphql and update the schema as well, this will provide a better stand alone developer experience
	const purgeOptions = {};
	if (purge) {
		await model.purge(query, purgeOptions);
	}
	const importData = await importJSON(model, newJSONData, options);
	return importData;
};
