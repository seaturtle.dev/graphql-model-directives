/**
 * Migration Helpers -- For data only, this is not for altering the schema definition.
 * @ignore
 */

const migrationHelpers = (migrations) => {
	return migrators.map((migrator) => {
		const migrationHelper = migrationHelpers[migrator.shift()];
		const mutator = typeof migrator[migrator.length - 1] === 'function' ? migrator.pop() : undefined;
		return migrationHelper(this, ...migrator, mutator);
	});
};

module.exports = exports = migrationHelpers;


/**
 * [changeFieldName description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.changeFieldName = (collection, oldFieldName, newFieldName, mutator = () => {}) => {
	return {
		name: 'changeFieldName',
		collection,
		params: { oldFieldName, newFieldName },
		mutator,
		apply: (docs) => docs
	};
};

/**
 * [changeFieldType description]
 * @param  {[type]} collection [description]
 * @param  {[type]} fieldName  [description]
 * @param  {[type]} newType    [description]
 * @param  {[type]} mutator    [description]
 * @return {[type]}            [description]
 */
migrationHelpers.changeFieldType = (collection, fieldName, newType, mutator = () => {}) => {
	return {
		name: 'changeFieldType',
		collection,
		params: { fieldName, newType },
		mutator,
		apply: (docs) => docs
	};
};

/**
 * [changeTableName description]
 * @param  {[type]} store        [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.changeTableName = (collection, newTableName) => {
	return {
		name: 'changeTableName',
		collection,
		params: { newTableName },
		apply: (docs) => docs
	};
};

/**
 * [dropTable description]
 * @param  {[type]} store        [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.dropTable = (collection) => {
	return {
		name: 'dropTable',
		collection,
		params: { },
		apply: (docs) => null
	};
};

/**
 * [createTable description]
 * @param  {[type]} store        [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.createTable = (name) => {
	return {
		name: 'createTable',
		params: { name },
		apply: (docs) => docs || []
	};
};

/**
 * [insertField description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.insertField = (collection, newFieldName, newFieldType, mutator = () => {}) => {
	return {
		name: 'insertField',
		collection,
		params: { newFieldName, newFieldType },
		mutator,
		apply: (docs) => docs
	};
};

/**
 * [removeField description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.removeField = (collection, fieldName, mutator = () => {}) => {
	return {
		name: 'removeField',
		collection,
		params: { fieldName },
		mutator,
		apply: (docs) => docs
	};
};

/**
 * [renameField description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.renameField = (collection, fieldName, newFieldName, mutator = () => {}) => {
	return {
		name: 'renameField',
		collection,
		params: { fieldName, newFieldName },
		mutator,
		apply: (docs) => docs
	};
};

/**
 * [createIndex description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.createIndex = (collection, indexField, options) => {
	return {
		name: 'createIndex',
		collection,
		params: { indexField, options },
		apply: (docs) => docs
	};
};

/**
 * [removeIndex description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.removeIndex = (collection, indexField) => {
	return {
		name: 'removeIndex',
		collection,
		params: { indexField },
		apply: (docs) => docs
	};
};

/**
 * [renameIndex description]
 * @param  {[type]} collection   [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.renameIndex = (collection, indexField) => {
	return {
		name: 'renameIndex',
		collection,
		params: { indexField },
		apply: (docs) => docs
	};
};

/**
 * [emptyTableData description]
 * @param  {[type]} store        [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.emptyTableData = (collection) => {
	return {
		name: 'emptyTableData',
		collection,
		params: { },
		apply: (docs) => docs
	};
};

/**
 * [importTableData description]
 * @param  {[type]} store        [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.importTableData = (collection, mutator = () => {}) => {
	return {
		name: 'importTableData',
		collection,
		params: { },
		mutator,
		apply: (docs) => docs
	};
};

/**
 * [exportTableData description]
 * @param  {[type]} store        [description]
 * @param  {[type]} oldFieldName [description]
 * @param  {[type]} newFieldName [description]
 * @return {[type]}              [description]
 */
migrationHelpers.exportTableData = (collection) => {
	return {
		name: 'exportTableData',
		collection,
		params: { },
		apply: (docs) => docs
	};
};
