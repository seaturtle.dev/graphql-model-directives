const migrate = require('./migrate');

const importJSON = require('./importJSON');
const exportJSON = require('./exportJSON');

/**
 * Base class for Collection that provides generic definition,
 * introspection, and invokation methods.
 * @exports ModelDefinitions
 * @class
 */
class ModelDefinitions {
	constructor(name, store) {
		this.idProperty = 'id';
		this.name = name;
		this.plural = null;
		this.store = store;
		this.properties = {};
		this.fields = [];
		this.indexes = [];
		this.nested = [];
		this.relations = [];
		this.uniques = [];
		this.validations = [];
		this.virtuals = [];
	}

	/**
	 * [defineName description]
	 * @param  {[type]} name [description]
	 * @return {[type]}      [description]
	 */
	defineName(name) {
		this.name = name;

		return this;
	}

	/**
	 * [definePlural description]
	 * @param  {[type]} plural [description]
	 * @return {[type]}      [description]
	 */
	definePlural(plural = null) {
		this.plural = plural;

		return this;
	}

	/**
	 * [definePermission description]
	 * @param  {[type]} permission [description]
	 * @return {[type]}            [description]
	 */
	definePermission(permission) {
		this.permission = permission;

		return this;
	}

	/**
	 * [defineResource description]
	 * @param  {[type]} belongsTo [description]
	 * @return {[type]}           [description]
	 */
	defineResource(belongsTo, allowAccess) {
		this.resource = true;

		if (belongsTo) {
			if (!Array.isArray(belongsTo)) {
				belongsTo = [belongsTo];
			}
			this.belongsTo = belongsTo;
		}

		if (allowAccess) {
			if (!Array.isArray(allowAccess)) {
				allowAccess = [allowAccess];
			}
			this.allowAccess = allowAccess;
		}

		return this;
	}

	/**
	 * [defineUser description]
	 * @return {[type]} [description]
	 */
	defineUser() {
		this.user = true;

		return this;
	}

	/**
	 * [defineUserInvite description]
	 * @param  {[type]} from [description]
	 * @return {[type]}      [description]
	 */
	defineUserInvite(from) {
		this.userInvite = true;
		this.userInviteFrom = from;

		return this;
	}

	/**
	 * [defineProperty description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	defineProperty(field) {
		if (this.fields.indexOf(field.name) === -1) {
			this.fields.push(field.name);
		}

		const property = this.properties[field.name] || {
			typeName: field.type?.name || field.type?.ofType?.name || null,
			name: field.name,
		};

		property.field = field;
		this.properties[field.name] = property;

		return this;
	}

	/**
	 * [definePropertyCreatedAt description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyCreatedAt(field) {
		const property = this.properties[field.name];

		property.createdAt = true;
		this.createdAt = field.name;

		return this;
	}

	/**
	 * [definePropertyModelType description]
	 * @param  {[type]} field [description]
	 * @param  {[type]} type  [description]
	 * @return {[type]}       [description]
	 */
	definePropertyModelType(field, type) {
		const property = this.properties[field.name];

		property.modelType = type;

		return this;
	}

	/**
	 * [definePropertyNestedValue description]
	 * @param  {[type]} field [description]
	 * @param  {[type]} many  [description]
	 * @return {[type]}       [description]
	 */
	definePropertyNestedValue(field, many) {
		const property = this.properties[field.name];

		property.nested = {
			modelName: field.type.name || field.modelName,
			many,
		};

		if (this.nested.indexOf(field.name) === -1) {
			this.nested.push(field.name);
		}

		return this;
	}

	/**
	 * [definePropertyDefaultValue description]
	 * @param  {[type]} field           [description]
	 * @param  {[type]} getDefaultValue [description]
	 * @return {[type]}                 [description]
	 */
	definePropertyDefaultValue(field, getDefaultValue) {
		const property = this.properties[field.name];

		property.default = true;
		property.getDefaultValue = getDefaultValue;

		return this;
	}

	/**
	 * [definePropertyEncryption description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyEncryption(field) {
		const property = this.properties[field.name];

		property.encrypt = true;

		return this;
	}

	/**
	 * [definePropertyBuffer description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyBuffer(field) {
		const property = this.properties[field.name];

		property.file = true;
		property.buffer = true;

		return this;
	}

	/**
	 * [definePropertyFile description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyFile(field) {
		const property = this.properties[field.name];

		property.file = true;

		return this;
	}

	/**
	 * [definePropertyIdentifier description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyIdentifier(field) {
		const property = this.properties[field.name];

		property.id = true;
		this.idProperty = field.name;

		return this;
	}

	/**
	 * [definePropertyPermission description]
	 * @param  {[type]} field  [description]
	 * @param  {[type]} params [description]
	 * @return {[type]}        [description]
	 */
	definePropertyPermission(field, params) {
		const property = this.properties[field.name];

		property.permission = params;

		return this;
	}

	/**
	 * [definePropertyRelation description]
	 * @param  {[type]} field  [description]
	 * @param  {[type]} params [description]
	 * @return {[type]}        [description]
	 */
	definePropertyRelation(field, params) {
		const {
			many,
			autoDelete,
		} = params;

		const property = this.properties[field.name];

		property.relation = {
			modelName: field.type?.name || field.type?.ofType?.name || field.modelName,
			many,
			autoDelete,
		};

		if (this.relations.indexOf(field.name) === -1) {
			this.relations.push(field.name);
		}

		this.store.mapRelation(this, field, property.relation);

		return this;
	}

	/**
	 * [definePropertySortedIndex description]
	 * @param  {[type]} field  [description]
	 * @param  {[type]} params [description]
	 * @return {[type]}        [description]
	 */
	definePropertySortedIndex(field, params, index) {
		const {
			dir,
			compounds,
		} = params;

		const property = this.properties[field.name];

		property.sortedIndexes = property.sortedIndexes || [];

		const sortedIndex = {
			dir,
			compounds,
		};

		if (typeof index === 'number') {
			property.sortedIndexes[index] = sortedIndex;
		} else {
			property.sortedIndexes[0] = sortedIndex;
		}

		if (this.indexes.indexOf(field.name) === -1) {
			this.indexes.push(field.name);
		}

		return this;
	}

	/**
	 * [definePropertyUpdatedAt description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUpdatedAt(field) {
		const property = this.properties[field.name];

		property.updatedAt = true;
		this.updatedAt = field.name;

		return this;
	}

	/**
	 * [definePropertyUnique description]
	 * @param  {[type]} field            [description]
	 * @param  {[type]} [compounds=null] [description]
	 * @return {[type]}                  [description]
	 */
	definePropertyUnique(field, compounds = null, index) {
		const property = this.properties[field.name];

		property.uniques = property.uniques || [];

		const unique = {
			compounds: compounds,
		}

		if (typeof index === 'number') {
			property.uniques[index] = unique;
		} else {
			property.uniques[0] = unique;
		}

		if (this.uniques.indexOf(field.name) === -1) {
			this.uniques.push(field.name);
		}

		return this;
	}

	/**
	 * [definePropertyUserBirthdate description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserBirthdate(field) {
		this.userBirthdate = field.name;

		return this;
	}

	/**
	 * [definePropertyUserEmail description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserEmail(field) {
		this.userEmail = field.name;

		return this;
	}

	/**
	 * [definePropertyUserEmailConfirm description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserEmailConfirm(field) {
		this.userEmailConfirm = field.name;

		return this;
	}

	/**
	 * [definePropertyUserLogin description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserLogin(field) {
		this.userLogin = field.name;

		return this;
	}

	/**
	 * [definePropertyUserMaxInvites description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserMaxInvites(field) {
		this.userMaxInvites = field.name;

		return this;
	}

	/**
	 * [definePropertyUserPassword description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserPassword(field) {
		const property = this.properties[field.name];

		property.password = true;
		this.userPassword = field.name;

		return this;
	}

	/**
	 * [definePropertyUserPasswordReset description]
	 * @param {[type]} field [description]
	 */
	definePropertyUserPasswordReset(field) {
		this.userPasswordReset = field.name;

		return this;
	}

	/**
	 * [definePropertyUserRoles description]
	 * @param  {[type]} field  [description]
	 * @param  {[type]} params [description]
	 * @return {[type]}        [description]
	 */
	definePropertyUserRoles(field, params) {
		const property = this.properties[field.name];

		property.roles = params;
		this.userRoles = field.name;

		return this;
	}

	/**
	 * [definePropertyUserTokens description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserTokens(field) {
		this.userTokens = field.name;

		return this;
	}

	/**
	 * [definePropertyUserToken description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserToken(field) {
		this.userToken = field.name;

		return this;
	}

	/**
	 * [definePropertyValidation description]
	 * @param  {[type]} field  [description]
	 * @param  {[type]} params [description]
	 * @return {[type]}        [description]
	 */
	definePropertyValidation(field, params) {
		const property = this.properties[field.name];

		property.validation = params;

		if (this.validations.indexOf(field.name) === -1) {
			this.validations.push(field.name);
		}

		return this;
	}

	/**
	 * [definePropertyVirtual description]
	 * @param  {[type]} field  [description]
	 * @param  {[type]} params [description]
	 * @return {[type]}        [description]
	 */
	definePropertyVirtual(field, params) {
		const property = this.properties[field.name];

		property.virtual = params || {};

		if (this.virtuals.indexOf(field.name) === -1) {
			this.virtuals.push(field.name);
		}

		return this;
	}

	/**
	 * [enumerateFieldsWithPermissions description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	enumerateFieldsWithPermissions(iterator) {
		this.fields.forEach((field) => {
			const property = this.properties[field];

			if (property.permission) {
				iterator(field, property);
			}
		});
	}

	/**
	 * [enumerateWritableFields description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	enumerateWritableFields(iterator) {
		this.fields.forEach((field) => {
			if (this.virtuals.indexOf(field) === -1) {
				iterator(field, this.properties[field]);
			}
		});
	}

	/**
	 * [enumerateFields description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	enumerateFields(iterator) {
		this.fields.forEach((field) => {
			iterator(field, this.properties[field]);
		});
	}

	/**
	 * [enumerateFileFields description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	enumerateFileFields(iterator) {
		this.fields.forEach((field) => {
			const property = this.properties[field];
			if (property.file) {
				iterator(field, property);
			}
		});
	}

	/**
	 * [enumerateRelations description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	enumerateRelations(iterator) {
		this.relations.forEach((field) => {
			iterator(field, this.properties[field]);
		});
	}

	/**
	 * [enumerateNested description]
	 * @param  {[type]} iteartor [description]
	 * @return {[type]}          [description]
	 */
	enumerateNested(iteartor) {
		this.nested.forEach((field) => {
			iteartor(field, this.properties[field]);
		});
	}

	/**
	 * [enumerateVirtuals description]
	 * @param  {[type]} iteartor [description]
	 * @return {[type]}          [description]
	 */
	enumerateVirtuals(iteartor) {
		this.virtuals.forEach((field) => {
			iteartor(field, this.properties[field]);
		});
	}

	/**
	 * [enumerateUniques description]
	 * @param  {[type]} iteartor [description]
	 * @return {[type]}          [description]
	 */
	enumerateUniques(iteartor) {
		this.uniques.forEach((field) => {
			iteartor(field, this.properties[field]);
		});
	}

	/**
	 * [enumerateIndexes description]
	 * @param  {[type]} iteartor [description]
	 * @return {[type]}          [description]
	 */
	enumerateIndexes(iteartor) {
		this.indexes.forEach((field) => {
			iteartor(field, this.properties[field]);
		});
	}

	/**
	 * [enumerateValidations description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	enumerateValidations(iterator) {
		this.validations.forEach((field) => {
			iterator(field, this.properties[field]);
		});
	}

	/**
	 * [read description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {runner}              [description]
	 */
	read(target = {}, options = {}) {
		/**
		 * [runner description]
		 * @type {Object}
		 * @property
		 * @ignore
		 */
		const runner = {
			/** @property {Object} target target */
			target,
			/** @property {Object} options options */
			options,
			/**
			 * [exec description]
			 * @param  {[type]} params [description]
			 * @return {[type]}        [description]
			 */
			exec: (params) => this.store.readDocument(this, runner, params),
		};

		return runner;
	}

	/**
	 * [count description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {[type]}              [description]
	 */
	count(target = {}, options = {}) {
		options = {
			offset: 0,
			count: 100,
			sort: {},
			...options,
		};

		/**
		 * [countRunner description]
		 * @type {Object}
		 * @property
		 * @ignore
		 */
		const countRunner = {
			/** @property {Object} target target */
			target,
			/** @property {Object} options options */
			options,
			/**
			 * [filter description]
			 * @param  {[type]} filter [description]
			 * @return {[type]}        [description]
			 */
			filter: (filter) => {
				Object.assign(countRunner.target, filter);

				return countRunner;
			},
			/**
			 * [offset description]
			 * @param {[type]} offset [description]
			 */
			offset: (offset) => {
				countRunner.options.offset = offset;

				return countRunner;
			},
			/**
			 * [exec description]
			 * @param  {[type]} params [description]
			 * @return {[type]}        [description]
			 */
			exec: (params) => this.store.countDocuments(this, countRunner, params),
		};

		return countRunner;
	}

	/**
	 * [find description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {[type]}              [description]
	 */
	find(target = {}, options = {}) {
		options = {
			offset: 0,
			count: 100,
			sort: {},
			...options,
		};

		/**
		 * [findRunner description]
		 * @type {Object}
		 * @property
		 * @ignore
		 */
		const findRunner = {
			/** @property {Object} target target */
			target,
			/** @property {Object} options options */
			options,
			/**
			 * [count description]
			 * @param  {[type]} count [description]
			 * @return {[type]}       [description]
			 */
			count: (count) => {
				findRunner.options.count = count;
				return findRunner;
			},
			/**
			 * [filter description]
			 * @param  {[type]} filter [description]
			 * @return {[type]}        [description]
			 */
			filter: (filter) => {
				Object.assign(findRunner.target, filter);

				return findRunner;
			},
			/**
			 * [offset description]
			 * @param {[type]} offset [description]
			 */
			offset: (offset) => {
				findRunner.options.offset = offset;

				return findRunner;
			},
			/**
			 * [sort description]
			 * @param  {[type]} sort [description]
			 * @return {[type]}      [description]
			 */
			sort: (sort) => {
				Object.assign(findRunner.options.sort, sort);

				return findRunner;
			},
			/**
			 * [exec description]
			 * @param  {[type]} params [description]
			 * @return {[type]}        [description]
			 */
			exec: (params) => this.store.findDocuments(this, findRunner, params),
		};

		return findRunner;
	}

	/**
	 * [scan description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {[type]}              [description]
	 */
	scan(target = {}, options = {}) {
		options = {
			cursor: '0',
			count: 100,
			...options,
		};

		/**
		 * [scanRunner description]
		 * @type {Object}
		 * @ignore
		 */
		const scanRunner = {
			/** @property {Object} target target */
			target,
			/** @property {Object} options options */
			options,
			/**
			 * [count description]
			 * @param  {[type]} count [description]
			 * @return {[type]}       [description]
			 */
			count: (count) => {
				scanRunner.options.count = count;

				return scanRunner;
			},
			/**
			 * [filter description]
			 * @param  {[type]} filter [description]
			 * @return {[type]}        [description]
			 */
			filter: (filter) => {
				Object.assign(scanRunner.target, filter);

				return scanRunner;
			},
			/**
			 * [offset description]
			 * @param {[type]} offset [description]
			 */
			offset: (offset) => {
				scanRunner.options.offset = offset;

				return scanRunner;
			},
			/**
			 * [exec description]
			 * @param  {[type]} params [description]
			 * @return {[type]}        [description]
			 */
			exec: (params) => this.store.scanDocuments(this, scanRunner, params),
		};

		return scanRunner;
	}

	/**
	 * [purge description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {[type]}              [description]
	 */
	purge(target = {}, options = {}) {
		const purgeRunner = {
			target,
			options,
			filter: (filter) => {
				Object.assign(purgeRunner.target, filter);
				return purgeRunner;
			},
			exec: (params) => this.store.purgeDocuments(this, purgeRunner, params),
		};

		return purgeRunner;
	}

	/**
	 * [remove description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {[type]}              [description]
	 */
	remove(target = {}, options = {}) {
		const runner = {
			target,
			options,
			exec: (params) => this.store.removeDocument(this, runner, params),
		};

		return runner;
	}

	/**
	 * [upsert description]
	 * @param  {Object} [target={}]  [description]
	 * @param  {Object} [options={}] [description]
	 * @return {[type]}              [description]
	 */
	upsert(target = {}, options = {}) {
		const runner = {
			target,
			options,
			exec: (params) => this.store.upsertDocument(this, runner, params),
		};

		return runner;
	}

	/**
	 * [migrate description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	migrate(migrators, options) {
		return migrate(this, migrators, options);
	}

	/**
	 * [importJSON description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	importJSON(json, options) {
		return importJSON(this, json, options);
	}

	/**
	 * [importJSON description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	exportJSON(query, fields, options) {
		return exportJSON(this, query, fields, options);
	}

	/**
	 * [subscribe description]
	 * @param  {[type]} event [description]
	 * @return {[type]}       [description]
	 */
	subscribe(event) {
		return {
			event,
			exec: () => this.store.subscribeEvent(`${this.name}:${event}`),
		};
	}

	/**
	 * [publish description]
	 * @param  {[type]} event   [description]
	 * @param  {[type]} payload [description]
	 * @return {[type]}         [description]
	 */
	publish(event, payload) {
		function capitalize(s) {
			return `${s.charAt(0).toUpperCase()}${s.slice(1)}`;
		}
		const payloadKey = `${this.name.toLowerCase()}${capitalize(event)}`;
		return {
			event,
			payload,
			payloadKey,
			exec: () => this.store.publishEvent(`${this.name}:${event}`, { [payloadKey]: payload }),
		};
	}

	/**
	 * [emit description]
	 * @param  {[type]} event   [description]
	 * @param  {[type]} payload [description]
	 * @return {[type]}         [description]
	 */
	emit(event, payload) {
		// console.log('emit', this.name, event, payload);
		return this.publish(event, payload).exec();
	}
}

module.exports = ModelDefinitions;
