module.exports = exports = async function exportCSV(model, query, fields, options) {
	const cursor = model.find(query, {
		fields,
		limit: Infinity,
	});

	const data = await cursor.exec({
		context: { super: true }
	});

	if (options?.purge) {
		const purger = model.purge(query, { limit: Infinity });

		await purger.exec({ context: { super: 'true' }})
	}

	return data;
};
