const migrationHelpers = require('./migrationHelpers');

module.exports = exports = async function migrateCSV(json, migration, options) {
	// const collection = json.map((docs) => migration.apply(item, options));
	const collection = migration.apply(json, options)
	return collection;
};
