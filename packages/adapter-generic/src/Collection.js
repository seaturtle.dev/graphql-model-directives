
const { nanoid } = require('@seaturtle/gmd-core/id.js');

const helpers = require('./helpers');

const ModelDefinitions = require('./ModelDefinitions');

/**
 * Used by custom adapters to model a collection of documents or rows
 * that map to the desired database.
 * @exports Collection
 * @class
 * @extends ModelDefinitions
 */
class Collection extends ModelDefinitions {
	/**
	 * [generateDocumentId description]
	 * @return {[type]} [description]
	 */
	generateDocumentId() {
		return nanoid();
	}

	/**
	 * [convertToStore description]
	 * @param  {[type]} context  [description]
	 * @param  {[type]} params   [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToStore(context, params, iterator) {
		const document = {};

		let create = true;

		let createdAt = null;
		let updatedAt = null;

		let id = this.generateDocumentId();

		if (!params) {
			return null;
		}

		Object.keys(params).forEach((key) => {
			if (typeof params[key] === 'function') {
				params[key] = params[key]();
			}
		});

		this.enumerateFields((field, property) => {
			let value = params[field];

			if (property.createdAt) { createdAt = field; }

			if (property.updatedAt) { updatedAt = field; }

			if (property.relation) { value = value || ''; }

			if (property.virtual) { value = undefined; }

			if (value !== undefined) {
				if (field === this.idProperty) {
					create = false;

					id = value;
				}

				document[field] = value;
			}
		});

		if (updatedAt && !document[updatedAt]) {
			document[updatedAt] = new Date();
		}

		if (create) {
			this.enumerateFields((field, property) => {
				const value = document[field];

				if (property.getDefaultValue && value === undefined) {
					document[field] = property.getDefaultValue(context, document, id);
				}
			});

			document[this.idProperty] = id;

			if (createdAt && !document[createdAt]) {
				document[createdAt] = new Date();
			}
		}

		this.enumerateFields((field, property) => {
			const value = document[field];

			if (typeof iterator === 'function') {
				iterator(field, property, value, document, id);
			}
		});

		return { id, document, create };
	}

	/**
	 * [gatherRelatedPromiseTriggers description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} context  [description]
	 * @param  {[type]} info     [description]
	 * @return {[type]}          [description]
	 */
	gatherRelatedPromiseTriggers(document, context, info) {
		// TODO: test this code, rename the method because it is only used on upsert
		let relatedError = null;

		const relatedPromiseTriggers = [];

		// TODO: move this to a new method, or rename?
		this.enumerateFileFields((field) => {
			// console.log('GOT HERE HERE HREE', this.store.uploader, this.store.config)
			if (this.store?.uploader?.recall) {
				relatedPromiseTriggers.push(() => new Promise((resolve, reject) => {
					const value = document[field];
					const url = typeof value === 'object' && value  && typeof value.then !== 'function' ? value.url : value;
					const stream = false;
					this.store.uploader.recall(url, stream, (err, newValue) => {
						if (err) { return reject(err); }
						document[field] = newValue;
						return resolve(newValue);
					}, this.store.config);
				}));
			}
		});

		this.enumerateRelations((field) => {
			if (relatedError) {
				return;
			}

			const relatedPromises = [];

			let related = document[field];

			if (!related) {
				delete document[field];
				return;
			}

			if (typeof related !== 'object') {
				return;
			}

			const property = this.properties[field];
			const { relation } = property;

			const relatedModel = context.model[relation.modelName];

			// TODO: make sure these are upserted
			if (relation.many) {
				related = related.filter((r) => (
					r[relatedModel.idProperty]
				));
				document[field] = related;
			} else if (!related[relatedModel.idProperty]) {
				delete document[field];
			}

			const relationalIds = [];

			const pushRelatedPromiseTrigger = (record) => {
				relatedPromiseTriggers.push(() => {
					// TODO: this is dangerous because its hard to tell between reading and writing among all dbs
					const promise = relatedModel.upsert(record).exec({ context, info }).then((rdoc) => {
						relationalIds.push(rdoc[relatedModel.idProperty]);
					});
					relatedPromises.push(promise);
					return promise;
				});
			};

			if (Array.isArray(related)) {
				if (relation.many) {
					related.forEach((record) => {
						pushRelatedPromiseTrigger(record);
					});
				} else {
					relatedError = new Error('Upserted many into singular relation.');
				}
			} else {
				pushRelatedPromiseTrigger(related);
			}
		});

		return [relatedPromiseTriggers, relatedError];
	}

	/**
	 * [prepareForDestruction description]
	 * @method
	 * @param  {[type]} target  [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	prepareForDestruction(target, context, info) {
		// TODO: test this code
		const promiseTriggers = [];

		this.enumerateFields((field, property) => {
			const { relation, file } = property;
			if (file && this.store.uploader) {
				promiseTriggers.push((document) => new Promise((resolve, reject) => {
					const value = document && document[field];
					const filepath = typeof value === 'object' && value && typeof value.then !== 'function' ? value.url : value;
					if (!filepath || !this.store.uploader || !this.store.uploader.remove) {
						return resolve();
					}
					return this.store.uploader.remove(filepath, (err) => {
						if (err) { return reject(err); }
						return resolve();
					}, this.store.config);
				}));
			}
			if (relation) {
				const relatedCollection = this.store.collection(relation.modelName);
				promiseTriggers.push((document) => new Promise((resolve, reject) => {
					if (relation.autoDelete) {
						let relatedIds = (document && document[field]) || null;
						relatedIds = Array.isArray(relatedIds) ? relatedIds : [relatedIds];
						relatedIds = relatedIds.map((rId) => (
							(typeof rId === 'object' && rId)
								? rId[relatedCollection.idProperty] || ''
								: rId || '')).filter((rId) => rId);
						if (!relatedIds.length) {
							return resolve();
						}
						if (!context.deletionLoopDetectionMap) {
							context.deletionLoopDetectionMap = {};
						}
						const promises = relatedIds.map(
							(relatedId) => {
								const detectionKey = `${relatedCollection.name}:${relatedId}`;
								if (!context.deletionLoopDetectionMap[detectionKey]) {
									return context.deletionLoopDetectionMap[detectionKey];
								}
								const promise = relatedCollection
									.remove({ [relatedCollection.idProperty]: relatedId })
									.exec({ context, info });
								context.deletionLoopDetectionMap[detectionKey] = promise;
								return promise;
							},
						);
						if (!promises.length) {
							return resolve();
						}
						return Promise.all(promises).then(resolve, reject);
					}
					// TODO: check this
					const storeRelations = this.store.relations[relatedCollection.name];

					if (storeRelations && storeRelations.many) {
						const manyRelations = storeRelations.many[this.name];

						manyRelations.forEach((relationField) => {
							if (!document || !document[this.idProperty]) {
								return;
							}
							if (!context.deletionLoopDetectionMap) {
								context.deletionLoopDetectionMap = {};
							}
							promiseTriggers.push(() => {
								const detectionKey = `many:${this.name}:${document[this.idProperty]}`;
								if (!context.deletionLoopDetectionMap[detectionKey]) {
									return context.deletionLoopDetectionMap[detectionKey];
								}
								const promise = relatedCollection.purge({
									[relationField]: document[this.idProperty],
								}).exec({ context, info });
								context.deletionLoopDetectionMap[detectionKey] = promise;
								return promise;
							});
						});
					}

					return resolve();
				}));
			}
		});

		if (promiseTriggers.length) {
			return new Promise((resolve, reject) => (
				this.read(target)
					.exec({ context, info })
					.then((document) => {
						const promises = promiseTriggers.map((trigger) => trigger(document));
						return Promise.all(promises).then(resolve, reject);
					}, reject)
			));
		}

		return Promise.resolve();
	}

	/**
	 * [hashEncrypt description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	hashEncrypt(document, field) {
		if (this?.config?.crypto?.hash) {
			return new Promise((resolve, reject) => (
				crypto.hash(document[field], 10).then((encrypted) => {
					document[field] = encrypted;
					return resolve(encrypted);
				}, reject)
			));
		} else {
			console.warn('Unimplementated hash encrypt method, set config.crypto to enable this feature.');
			return Promise.resolve(document[field]);
		}
	}

	/**
	 * [prepareForStorage description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	prepareForStorage(document, iterator) {
		const promises = [];

		this.enumerateFields((field, property) => {
			if (property.enrypt && document.hasOwnProperty(field)) {
				promises.push(this.hashEncrypt(document, field));
			}
			// console.log('UPLOAD STORE', property.file, this.store.uploader)
			if (property.file && this.store.uploader && document.hasOwnProperty(field)) {
				promises.push(new Promise((resolve, reject) => {
					const value = document[field];
					const url = typeof value === 'object' && value && typeof value.then !== 'function' ? value.url : value;
					const id = document[this.idProperty];
					// debugger;
					this.read({
						[this.idProperty]: id,
					}).exec({
						context: { super: true },
						info: {},
					}).then((previousDoc) => {
						// debugger;
						const prevVal = previousDoc && previousDoc[field];
						const prevUrl = typeof prevVal === 'object' && prevVal && typeof prevVal.then !== 'function' ? prevVal.url : prevVal;
						this.store.uploader.upload(url, id, prevUrl, (err, name) => {
							if (err) { return reject(err); }
							// debugger;
							document[field] = name;
							return resolve(name);
						}, this.store.config);
					}, reject);
				}));
			}
			if (typeof iterator === 'function') {
				iterator(field, property, promises, document);
			}
			/*
			if (field === 'nestedResources' && document[field]) {
				console.log(document[field]);
			}
			const fixNestedDoc = (d, i) => {
				// HACK:
				d.id = i.toString();
				// d = this.convertToGraphQL(d);
				// d = this.redactSecureDataForUser(d, context);
				return d;
			};
			if (property.nested && document[field]) {
				if (property.nested.many) {
					if (Array.isArray(document[field])) {
						document[field] = document[field].map(fixNestedDoc);
					} else {
						document[field] = [fixNestedDoc(document[field], 0)];
					}
				} else {
					document[field] = fixNestedDoc(document[field], 0);
				}
			}
			if (field === 'nestedResources' && document[field]) {
				console.log(document[field]);
			}
			*/
		});

		return new Promise((resolve, reject) => {
			Promise.all(promises).then(() => {
				resolve(document);
			}, reject);
		});
	}

	/**
	 * [validateDocument description]
	 * @method
	 * @param  {[type]} document [description]
	 * @return {[type]}          [description]
	 */
	validateDocument(document) {
		const { isFinite } = Number;
		let errors = null;

		// console.log('VALIDATE', this.validations, this.name);

		this.enumerateValidations((field, property) => {
			const params = property.validation;

			let error = null;

			if (!Object.prototype.hasOwnProperty.call(document, field)) {
				return;
			}

			// console.log(field, document[field], params);
			if (params.template) {
				switch (params.template) {
				case 'email': error = helpers.validateEmail(document[field]); break;
				default: break;
				}
			} else if (params.minAge) {
				error = helpers.validateMinimumAge(document[field], params.minAge);
			} else if (isFinite(params.minNum) || isFinite(params.maxNum)) {
				error = helpers.validateRangeNumber(document[field], params.minNum, params.maxNum);
			} else if (isFinite(params.min) || isFinite(params.max)) {
				error = helpers.validateRange(document[field], params.min, params.max);
			}

			if (error) {
				if (Array.isArray(errors)) {
					errors.push([field, error]);
				} else if (errors) {
					errors = [errors, error];
				} else {
					errors = [field, document[field], error];
				}
			}
		});

		return errors;
	}

	/**
	 * [resolveVirtual description]
	 * @method
	 * @param  {[type]} value    [description]
	 * @param  {[type]} field    [description]
	 * @param  {[type]} property [description]
	 * @param  {[type]} document [description]
	 * @return {[type]}          [description]
	 */
	resolveVirtual(value, field, property, document) {
		if (property.virtual && property.virtual.method) {
			switch (property.virtual.method) {
			case 'alias': helpers.virtualAlias(value, document, field, property.virtual); break;
			default: break;
			}
		}

		if (value !== null || value !== undefined) {
			document[field] = value;
		}
	}

	/**
	 * [detectRelationField description]
	 * @method
	 * @param  {[type]}  field        [description]
	 * @param  {[type]}  context      [description]
	 * @param  {[type]}  info         [description]
	 * @param  {Boolean} [list=false] [description]
	 * @return {[type]}               [description]
	 */
	detectRelationField(field, context, info, list = false) {
		const mapQueryFields = (target, name, root) => {
			const obj = (root && root.mappedFields) || {};
			if (!root || !root.mappedFields) {
				target.forEach((leaf) => {
					if (leaf.kind === 'Field') {
						obj[leaf.name.value] = leaf.selectionSet
							? mapQueryFields(leaf.selectionSet.selections)
							: true;
					}
				});
				if (root) {
					root.mappedFields = obj;
				}
			}
			if (name && obj[name]) {
				return obj[name];
			}
			return obj;
		};
		let query = (info && info.fieldNodes && info.fieldName)
			? mapQueryFields(info.fieldNodes, info.fieldName, info)
			: {};
		if (list && query.page) {
			query = query.page;
		}
		if (context.parentRelations) {
			context.parentRelations.some((relation) => {
				if (!query[relation] && query.page) {
					query = query.page;
				}
				if (query[relation]) {
					query = query[relation];
					return false;
				}
				return true;
			});
		}
		if (query[field]) {
			return true;
		}
		return false;
	}

	/**
	 * [mapQueryOperators description]
	 * @method
	 * @param  {[type]}  target               [description]
	 * @return {[type]}                       [description]
	 */
	mapQueryOperators(target) {
		return target;
	}

	/**
	 * [resolvePagedRelations description]
	 * @method
	 * @param  {[type]}  resolve               [description]
	 * @param  {[type]}  reject                [description]
	 * @param  {[type]}  output                [description]
	 * @param  {[type]}  context               [description]
	 * @param  {[type]}  info                  [description]
	 * @param  {Boolean} [skipDetection=false] [description]
	 * @return {[type]}                        [description]
	 */
	resolvePagedRelations(resolve, reject, output, context, info, skipDetection = false) {
		let detected = skipDetection;

		if (!detected) {
			this.enumerateRelations((field) => {
				detected = this.detectRelationField(field, context, info, true);
			});
		}

		if (detected) {
			return Promise.all(output.page.map((item, i) => (
				new Promise((res, rej) => (
					this.resolveDocumentRelations((doc) => {
						Object.assign(output.page[i], doc);
						res(doc);
					}, rej, item, context, info, true)
				))
			))).then(() => resolve(output), reject);
		}

		resolve(output);
		return Promise.resolve(output);
	}

	/**
	 * [resolveDocumentRelations description]
	 * @method
	 * @param  {[type]}  resolve               [description]
	 * @param  {[type]}  reject                [description]
	 * @param  {[type]}  document              [description]
	 * @param  {[type]}  context               [description]
	 * @param  {[type]}  info                  [description]
	 * @param  {Boolean} [skipDetection=false] [description]
	 * @return {[type]}                        [description]
	 */
	resolveDocumentRelations(resolve, reject, document, context, info, skipDetection = false) {
		const promises = [];

		let detected = skipDetection;

		if (!detected) {
			this.enumerateRelations((field) => {
				detected = this.detectRelationField(field, context, info);
			});
		}

		if (detected) {
			this.enumerateRelations((field) => {
				if (!document[field]) {
					return;
				}

				const property = this.properties[field];

				const related = this.store.collection(
					property.relation.modelName,
				);

				const relatedIdProperty = related.idProperty;

				const pushRelation = (id) => {
					id = id[relatedIdProperty];
					if (id) {
						const detectionKey = `${related.name}:${id}`;
						if (!context.relationLoopDetectionMap) {
							context.relationLoopDetectionMap = {};
						}
						if (context.relationLoopDetectionMap[detectionKey]) {
							return context.relationLoopDetectionMap[detectionKey];
						}
						const promise = new Promise((res, rej) => (
							related
								.read({ [relatedIdProperty]: id }, { })
								.exec({
									context: {
										...context,
										parentRelations:
											context.parentRelations
												? [...context.parentRelations, field]
												: [field],
									},
									info,
								})
								.then((record) => res({ field, record }), rej)
						));
						context.relationLoopDetectionMap[detectionKey] = promise;
						return promise;
					}
					return Promise.resolve();
				};

				if (property.relation.many) {
					promises.push(new Promise((res, rej) => {
						let values = document[field] || [];
						if (!Array.isArray(values)) {
							values = [values];
						}
						return Promise.all(
							values.map(pushRelation),
						).then((docs) => {
							docs.forEach((doc, i) => {
								Object.assign(document[field][i], doc);
							});
							res({ /* field, record: docs */ });
						}, rej);
					}));
				} else {
					promises.push(
						pushRelation(document[field]),
					);
				}
			});
		}

		const finalizeDocument = (doc) => {
			if (doc) {
				this.enumerateVirtuals((field, property) => {
					this.resolveVirtual(doc[field], field, property, doc);
				});
			}
			return doc;
		};

		if (promises.length) {
			return Promise.all(promises).then((relations) => {
				if (relations) {
					relations.forEach((params = {}) => {
						const { field, record } = params;
						if (field && record) {
							// console.log('relate', document, field, record);
							Object.assign(document[field], record);
						}
					});
				}
				return resolve(finalizeDocument(document));
			}, reject);
		}

		return resolve(finalizeDocument(document));
	}

	/**
	 * [convertToGraphQL description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToGraphQL(document, iterator) {
		this.enumerateFields((field, property) => {
			const target = property && (property.relation || property.nested);

			if (document && target) {
				const related = this.store.collection(
					target.modelName,
				);

				const fixId = (id) => (
					typeof id && id === 'object' ? id
						: { [related.idProperty]: id || '' }
				);

				const isMany = target.many;

				if (isMany) {
					let values = document[field] || [];
					if (!Array.isArray(values)) {
						values = [values];
					}
					if (property.relation) {
						values = values.map(fixId);
					}
					document[field] = values;
				} else if (property.relation) {
					document[field] = fixId(document[field]);
				}
			}

			if (document && property.file) {
				document[field] = { url: document[field] };
			}

			if (document && typeof iterator === 'function') {
				iterator(field, property, document[field], document);
			}
		});

		return document;
	}

	/**
	 * [filterSecureDataForUser description]
	 * @method
	 * @param  {[type]} list    [description]
	 * @param  {[type]} context [description]
	 * @return {[type]}         [description]
	 */
	filterSecureDataForUser(list, context/* , target */) {
		if (!list) {
			return [];
		}

		if (!this.permission) {
			return list;
		}

		return list.filter((item) => item && this.hasPermission(item, context));
	}

	/**
	 * [redactSecureDataForUser description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} context  [description]
	 * @return {[type]}          [description]
	 */
	redactSecureDataForUser(document, context) {
		const hasAccess = this.hasPermission(document, context);

		this.enumerateFieldsWithPermissions((field) => {
			if (!hasAccess) {
				delete document[field];
			}
		});

		return document;
	}

	/**
	 * [hasPermission description]
	 * @method
	 * @param  {[type]}  document [description]
	 * @param  {[type]}  context  [description]
	 * @return {Boolean}          [description]
	 */
	hasPermission(document, context) {
		if (context.super === true) {
			return true;
		}

		// TODO: remove config from context?
		const { user /* , config */ } = context;

		if (!document || !user) {
			// console.log('missing doc or user fail');
			return false;
		}

		const { userCollection, config } = this.store;

		if (!userCollection) {
			return true;
		}

		const userId = user[userCollection.idProperty];

		const roles = user[userCollection.userRoles] || [];

		if (roles.indexOf(config.superRole) !== -1) {
			return true;
		}

		// if (userCollection === this) {
		// 	console.log(
		// 		userId === document[this.idProperty],
		// 		document,
		// 	);
		// }
		// if (userCollection === this && userId === document[this.idProperty]) {
		if (userCollection === this) {
			const strUserId = userId ? userId.toString() : '';
			const strDocId = document[this.idProperty] ? document[this.idProperty].toString() : '';

			if (strUserId === strDocId) {
				return true;
			}
		}

		if (this.belongsTo && this.belongsTo.length) {
			// TODO: enumerate all belongsTo
			const belongsTo = this.belongsTo[0];

			const relationProperty = this.properties[belongsTo];

			if (!relationProperty) {
				return false;
			}

			if (relationProperty.relation) {
				const related = this.store.collection(
					relationProperty.relation.modelName,
				);

				let relatedUserId = document[belongsTo];

				if (relatedUserId && relatedUserId[related.idProperty]) {
					relatedUserId = relatedUserId[related.idProperty];
				}

				if (relatedUserId instanceof Buffer) {
					relatedUserId = relatedUserId.toString('hex');
				}

				if (userId === relatedUserId) {
					return true;
				}
			}
		}

		return false;
	}
}

module.exports = Collection;
