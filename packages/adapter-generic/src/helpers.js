/**
* Helpers
* @exports helpers */
const helpers = exports;

/**
 * [virtualAlias description]
 * @param  {[type]} value    [description]
 * @param  {[type]} document [description]
 * @param  {[type]} field    [description]
 * @param  {[type]} virtual  [description]
 * @return {[type]}          [description]
 */
helpers.virtualAlias = (value, document, field, virtual) => {
	const path = virtual.value.split('.');
	const last = path.pop();
	let context = document;
	path.forEach((key) => {
		if (context[key]) {
			context = context[key];
		}
	});
	if (context && (context[last] !== undefined)) {
		document[field] = context[last];
	}
	return document[field];
};

/**
 * [validateEmail description]
 * @param  {[type]} email [description]
 * @return {[type]}       [description]
 */
helpers.validateEmail = (email) => {
	if (email === '' || email === null) {
		return 'email can not be empty';
	}
	const atIndex = email.indexOf('@');
	if (atIndex === -1) {
		return 'email must contain "@"';
	}
	if (atIndex <= 2) {
		return 'email name must be at least 2 character(s)';
	}
	const secondPart = email.substr(atIndex);
	const periodIndex = secondPart.indexOf('.');
	if (periodIndex === -1) {
		// return 'Email must contain "."';
		return null;
	}
	if ((periodIndex - 1) <= 2) {
		return 'email host must be at least 2 character(s)';
	}
	return null;
};

/**
 * [validateRange description]
 * @param  {[type]} val             [description]
 * @param  {Number} [min=-Infinity] [description]
 * @param  {Number} [max=Infinity]  [description]
 * @return {[type]}                 [description]
 */
helpers.validateRange = (val, min = -Infinity, max = Infinity) => {
	if (val && val.length) {
		const error = helpers.validateRangeNumber(val.length, min, max);
		if (error) {
			return `${typeof val} ${error}`;
		}
	} else if (typeof val === 'number') {
		const error = helpers.validateRangeNumber(val, min, max);
		if (error) {
			return `number ${error}`;
		}
	}
	return null;
};

/**
 * [validateRangeNumber description]
 * @param  {[type]} num             [description]
 * @param  {Number} [min=-Infinity] [description]
 * @param  {Number} [max=Infinity]  [description]
 * @return {[type]}                 [description]
 */
helpers.validateRangeNumber = (num, min = -Infinity, max = Infinity) => {
	if (num < min) {
		return `range must be at least ${min}`;
	}
	if (num > max) {
		return `range must be below ${max}`;
	}
	return null;
};

/**
 * [validateMinimumAge description]
 * @param  {[type]} date   [description]
 * @param  {[type]} minAge [description]
 * @return {[type]}        [description]
 */
helpers.validateMinimumAge = (date, minAge) => {
	const minAgeDate = new Date();
	minAgeDate.setUTCFullYear(
		minAgeDate.getUTCFullYear() - minAge,
	);
	const dateValue = date.valueOf();
	const minAgeValue = minAgeDate.valueOf();
	if (dateValue >= minAgeValue) {
		return `must be at least ${minAge} years old`;
	}
	return null;
};
