const { PubSub, withFilter } = require('graphql-subscriptions');

const defineResolver = require('./defineResolver');

const Collection = require('./Collection');

/**
 * A generic example of a store that has no database model, or user methods implemented.
 * @exports StoreAdapter
 * @class
 */
class StoreAdapter {
	static PubSub = PubSub;

	static withFilterPubSub = withFilter;

	static insecureCrypto = {
		// TODO: use web based two way encryption replacement
		hash: (val, num, cb) => (
			new Promise((resolve) => {
				resolve(val);
				if (cb) { cb(null, `_${val.split('').reverse().join('')}_`); }
			})
		),
		compare: (a, b, cb) => (
			new Promise((resolve) => {
				const match = (`_${a.split('').reverse().join('')}_`) === b;
				resolve(null, match);
				if (cb) { cb(null, match); }
			})
		),
	};

	/**
	 * [constructor description]
	 * @param {[type]} resolvers [description]
	 * @constructs
	 */
	constructor(
		resolvers = {},
		_PubSub = this.constructor.PubSub || PubSub,
		_withFilter = this.constructor.withFilter || withFilter,
		insecureCrypto = this.constructor.insecureCrypto,
	) {
		this.pubsub = new _PubSub();
		this.withFilter = withFilter;
		this.models = {};
		this.resolvers = resolvers;
		this.relations = {};
		this.config = null;
		this.uploader = null;
		this.mailer = null;
		// TODO: allow crypto to be null
		this.crypto = insecureCrypto;
	}

	/**
	 * [configure description]
	 * @param  {[type]} config [description]
	 * @return {[type]}        [description]
	 */
	configure(config) {
		// TODO: this is unreliable in practice
		this.config = config;
		this.uploader = config ? config.uploader : null;
		this.mailer = config ? config.mailer : null;
		this.crypto = config?.crypto || this.constructor.insecureCrypto;
	}

	/**
	 * [postSchema description]
	 * @return {[type]} [description]
	 */
	postSchema() {}

	/**
	 * [close description]
	 * @param  {Function} cb [description]
	 * @return {[type]}      [description]
	 */
	close(cb) {
		cb();
	}

	/**
	 * [mapRelation description]
	 * @param  {[type]} collection [description]
	 * @param  {[type]} field      [description]
	 * @param  {[type]} relation   [description]
	 * @return {[type]}            [description]
	 */
	mapRelation(collection, field, relation) {
		/* eslint no-multi-assign: 0 */
		const currentModelRelation = (
			this.relations[collection.name] = (this.relations[collection.name]) || {
				many: {},
				one: {},
			}
		);
		const relatedModelRelation = (
			this.relations[relation.modelName] = (this.relations[relation.modelName]) || {
				many: {},
				one: {},
			}
		);
		if (relation.many) {
			const list = currentModelRelation.many[relation.modelName] || [];
			currentModelRelation.many[relation.modelName] = list;
			if (list.indexOf(field.name) === -1) { list.push(field.name); }
		} else {
			const oneList = currentModelRelation.one[relation.modelName] || [];
			currentModelRelation.one[relation.modelName] = oneList;
			if (oneList.indexOf(field.name) === -1) { oneList.push(field.name); }
			const manyList = relatedModelRelation.many[collection.name] || [];
			relatedModelRelation.many[collection.name] = manyList;
			if (manyList.indexOf(field.name) === -1) { manyList.push(field.name); }
		}
		// console.log(JSON.stringify(this.relations));
		return this;
	}

	/**
	 * [collection description]
	 * @param  {[type]} name  [description]
	 * @param  {[type]} _info [description]
	 * @return {[type]}       [description]
	 */
	collection(name, _info) {
		if (!name) {
			if (_info) { console.error('COLLECTION NAME ERROR:', _info); }

			throw new Error('Missing model name.');
		}

		return this.models[name];
	}

	/**
	 * [defineModel description]
	 * @param  {[type]} name   [description]
	 * @param  {[type]} object [description]
	 * @return {[type]}        [description]
	 */
	defineModel(name, object, plural) {
		if (!name) {
			throw new Error('Missing model name.');
		}

		let collection = this.collection(name);

		if (!collection) {
			const C = this.constructor.Collection || SafeCollection;
			collection = new C(name, this);
			this.models[name] = collection;
		} else {
			collection.defineName(name);
		}
		if (plural) {
			collection.definePlural(plural);
		}

		if (object) {
			object.collection = collection;
		}

		return collection;
	}

	/**
	 * [defineModelProperty description]
	 * @param  {[type]} name     [description]
	 * @param  {[type]} property [description]
	 * @return {[type]}          [description]
	 */
	defineModelProperty(name, property) {
		let collection = this.collection(name, property);

		if (!collection) {
			collection = this.defineModel(name, null);
		} else {
			collection.defineProperty(property);
		}

		if (property) {
			property.modelName = name;
			property.collection = collection;
		}

		return collection;
	}

	/**
	 * [defineResolver description]
	 * @param  {[type]} field   [description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	defineResolver(field, options) {
		defineResolver.call(this, field, options);

		return this;
	}

	/**
	 * [defineQueryPropertyAuth description]
	 * @param  {[type]} property [description]
	 * @param  {[type]} allowed  [description]
	 * @return {[type]}          [description]
	 */
	defineQueryPropertyAuth(property, allowed) {
		if (property) {
			property.auth = true;
			property.allowed = allowed;

			if (property.resolve) {
				const originalResolve = property.resolve;
				// TODO: remove this eventually, it is redundant once it can be removed and the tests pass
				property.resolve = (root, args, context, info) => new Promise((resolve, reject) => {
					const finish = () => {
						// TODO: ?
						// debugger;
						const roles = (context.user && context.user[
							this.userCollection && this.userCollection.userRoles
						]) || [];
						const isAllowed = !allowed || (
							Array.isArray(roles)
								? roles.some((role) => allowed.indexOf(role) !== -1)
								: allowed.indexOf(roles) !== -1 // TODO: dry this
						);
						if (context.super || isAllowed) { // (context.user && isAllowed)) {
							return originalResolve(root, args, context, info).then(resolve, reject);
						}
						return reject(new Error('User is not authenticated.'))
						// return reject(new Error('User does not have the right role to do that.'));
					}
					this.authenticate(context, info, finish, finish);
					// () => (
					// 	// reject(new Error('User is not authenticated.'))
					// ));
				});
			}
		}
		return this;
	}

	/**
	 * [mixinUserInvite description]
	 * @param  {[type]} collection [description]
	 * @return {[type]}            [description]
	 */
	mixinUserInvite(collection) {
		this.userInviteCollection = collection;

		if (this.userCollection) {
			this.userCollection.inviteCollection = collection;

			const { userCollection } = this;

			/**
			 * User Invite Mixin
			 * @mixin
			 */
			const UserInviteMixin = {
				/**
				 * [revokeInvite description]
				 * @param  {[type]} args [description]
				 * @return {[type]}      [description]
				 */
				revokeInvite: userCollection.revokeInvite,

				/**
				 * [sendInvite description]
				 * @param  {[type]} args [description]
				 * @return {[type]}      [description]
				 */
				sendInvite: userCollection.sendInvite,
			};

			Object.assign(
				collection,
				UserInviteMixin,
			);
		}

		return this;
	}

	/**
	 * [mixinUser description]
	 * @param  {[type]} collection   [description]
	 * @param  {Object} [methods={}] [description]
	 * @return {[type]}              [description]
	 */
	mixinUser(collection, methods = {}/* , model */) {
		this.userCollection = collection;

		/**
		 * User Mixin
		 * @mixin
		 */
		const UserMixin = {
			/**
			 * [me description]
			 * @return {[type]} [description]
			 */
			me: () => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					(methods.me
						? methods.me
						: () => (!context.userId
							? context.user
							: collection
								.read({
									[collection.idProperty]: context.userId,
								})
								.exec({ context, info }))),
				),
			}),

			/**
			 * [forgotPassword description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			forgotPassword: (args) => ({
				exec: ({ context/* , info */ }) => (
					(methods.forgotPassword
						? () => methods.forgotPassword(context, args[collection.userEmail])
						: () => { throw new Error('Not implemented'); })()
				),
			}),

			/**
			 * [refreshToken description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			refreshToken: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.refreshToken
						? () => methods.refreshToken(context, args[collection.userEmail])
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [register description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			register: (args) => ({
				exec: ({ context/* , info */ }) => (
					(methods.register
						? () => methods.register(
							context,
							args[collection.userLogin],
							args[collection.userEmail],
							args[collection.userPassword],
							args[collection.userBirthdate],
							args.invitation,
						)
						: () => { throw new Error('Not implemented'); })()
				),
			}),

			/**
			 * [resetPassword description]
			 * @param {[type]} args [description]
			 */
			resetPassword: (args) => ({
				exec: ({ context/* , info */ }) => (
					(methods.resetPassword
						? () => methods.resetPassword(
							context,
							args[this.userCollection.userEmail] || (
								context.user
								&& context.user[collection.userEmail]
							),
							args.recoveryToken,
							args.newPassword,
						)
						: () => { throw new Error('Not implemented'); })()
				),
			}),

			/**
			 * [revokeInvite description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			revokeInvite: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.revokeInvite
						? () => methods.revokeInvite(
							context,
							args.id,
						)
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [sendInvite description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			sendInvite: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.sendInvite
						? () => methods.sendInvite(
							context,
							context.super ? args.id : context.userId,
							args[collection.inviteCollection.userEmail],
							args.message,
						)
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [signIn description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			signIn: (args) => ({
				exec: ({ context/* , info */ }) => (
					(methods.signIn
						? () => methods.signIn(
							context,
							args[collection.userLogin],
							args[collection.userPassword],
						)
						: () => { throw new Error('Not implemented'); })()
				),
			}),

			/**
			 * [signOut description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			signOut: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.signOut
						? () => methods.signOut(context, context.super ? args.id : context.userId)
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [confirmEmail description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			confirmEmail: (args) => ({
				exec: ({ context/* , info */ }) => {
					if (!methods.confirmEmail) {
						throw new Error('Not implemented');
					}
					return methods.confirmEmail(
						context,
						args.id || context.userId,
						args[collection.userEmailConfirm],
					);
				},
			}),

			/**
			 * [updateLogin description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			updateLogin: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.updateLogin
						? () => methods.updateLogin(
							context,
							context.super ? args.id : context.userId,
							args[collection.userLogin],
						)
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [updateEmail description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			updateEmail: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.updateEmail
						? () => methods.updateEmail(
							context,
							context.super ? args.id : context.userId,
							args[collection.userEmail],
						)
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [updateEmailAndLogin description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			updateEmailAndLogin: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.updateEmailAndLogin
						? () => methods.updateEmailAndLogin(
							context,
							context.super ? args.id : context.userId,
							args[collection.userEmail],
							args[collection.userLogin],
						)
						: () => { throw new Error('Not implemented'); },
				),
			}),

			/**
			 * [updatePassword description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			updatePassword: (args) => ({
				exec: ({ context, info }) => this.authenticate(
					context,
					info,
					methods.updatePassword
						? () => methods.updatePassword(
							context,
							context.userId,
							args.oldPassword,
							args.newPassword,
						)
						: () => { throw new Error('Not implemented'); },
				),
			}),
		};

		Object.assign(
			collection,
			UserMixin,
		);

		if (!collection.inviteCollection && this.userInviteCollection) {
			collection.inviteCollection = this.userInviteCollection;

			Object.assign(
				this.userInviteCollection,
				{
					revokeInvite: collection.revokeInvite,
					sendInvite: collection.sendInvite,
				},
			);
		}

		return this;
	}

	/**
	 * [authenticate description]
	 * @param  {[type]}   context [description]
	 * @param  {[type]}   info    [description]
	 * @param  {Function} next    [description]
	 * @param  {boolean|Function} or [desc]
	 * @return {[type]}           [description]
	 */
	authenticate(context, info, next/* , or, roles */) {
		try {
			return next();
		} catch (err) {
			return next(err);
		}
	}

	/**
	 * [readDocument description]
	 * @return {[type]} [description]
	 */
	readDocument(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve(null);
	}

	/**
	 * [findDocuments description]
	 * @return {[type]} [description]
	 */
	findDocuments(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve([]);
	}

	/**
	 * [countDocuments description]
	 * @return {[type]} [description]
	 */
	countDocuments(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve(0);
	}

	/**
	 * [scanDocuments description]
	 * @return {[type]} [description]
	 */
	scanDocuments(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve([]);
	}

	/**
	 * [purgeDocuments description]
	 * @return {[type]} [description]
	 */
	purgeDocuments(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve(null);
	}

	/**
	 * [upsertDocument description]
	 * @return {[type]} [description]
	 */
	upsertDocument(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve(null);
	}

	/**
	 * [removeDocument description]
	 * @return {[type]} [description]
	 */
	removeDocument(/* { name }, { target, options }, { context, info } */) {
		return Promise.resolve(null);
	}

	// changeFieldName(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// changeFieldType(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// insertField(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// removeField(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// createIndex(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// removeIndex(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// changeTableName(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// dropTable(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// emptyTableData(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// importTableData(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }
	//
	// exportTableData(/* { name }, { target, options }, { context, info } */) {
	// 	return Promise.resolve(null);
	// }

	/**
	 * [publishEvent description]
	 * @param  {[type]} channel [description]
	 * @param  {[type]} payload [description]
	 * @return {[type]}         [description]
	 */
	publishEvent(channel, payload) {
		// console.log('publish', channel);
		if (this?.pubsub?.publish) {
			return this.pubsub.publish(channel, payload);
		}
		return Promise.resolve()
	}

	/**
	 * [subscribeEvent description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	subscribeEvent(channel) {
		if (this?.pubsub?.asyncIterator) {
			return this.pubsub.asyncIterator(channel);
		}
		return Promise.resolve();
	}

	/**
	 * [migration description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	migration(object, options) {
		return object;
	}

	// /**
	//  * [undoMigration description]
	//  * @param  {[type]} channel [description]
	//  * @return {[type]}         [description]
	//  */
	// undoMigration(tasks, options) {
	// 	return Promise.resolve();
	// }
}

StoreAdapter.Collection = Collection;

module.exports = StoreAdapter;
