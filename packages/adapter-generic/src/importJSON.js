module.exports = exports = async function importCSV(model, json, options) {
	if (options?.purge) {
		const purger = model.purge({}, { limit: Infinity });
		await purge.exec({ context: 'super' });
	}

	const saves = json.map(async (doc) => {
		const upsert = model.upsert(doc, {});

		const saved = await upsert.exec({ context: 'super' });
	});

	await Promise.all(saves);

	return json;
};
