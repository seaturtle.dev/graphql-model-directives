function createDate(timestamp, dateOnly) {
	// debugger;
	timestamp = parseInt(timestamp, 10);
	if (isNaN(timestamp)) {
		return null;
	}
	const date = new Date(timestamp);
	if (dateOnly) {
		date.setUTCHours(0);
		date.setUTCMilliseconds(0);
		date.setUTCMinutes(0);
		date.setUTCSeconds(0);
	}
	return date;
}

module.exports = createDate;
