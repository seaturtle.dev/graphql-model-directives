/* eslint no-underscore-dangle:0 */

const jwt = require('@seaturtle/gmd-core/jwt.js');

const getAuthorizationHeader = require('./getAuthorizationHeader.js');
const getDeviceIdHeader = require('./getDeviceIdHeader.js');

/**
* JWT Authentication Helpers
* @exports authorize
*/
const authorize = exports;

function getJWTOptions(context, mixin) {
	const jwtOpts = {
	};

	// debugger;
	jwtOpts.audience = '';
	if (context.config.jwtAudience) {
		jwtOpts.audience = context.config.jwtAudience;
	}

	jwtOpts.issuer = '';
	if (context.config.jwtIssuer) {
		jwtOpts.issuer = context.config.jwtIssuer;
	}

	if (mixin) {
		return Object.assign(jwtOpts, mixin);
	}

	return jwtOpts;
}

/**
 * Get JWT options
 * @param {Object} context
 * @param {Object} mixin
 * @returns {Object} */
authorize.getJWTOptions = getJWTOptions;

function extractFromToken(token, config, callback) {
	// debugger;
	return jwt.decode(
		token,
		config.jwtHmacSecret,
		getJWTOptions({ config }, {
			algorithms: ['HS256'],
		}),
		callback,
	);
}

/**
 * Extracts login from a token
 * @param {string} token
 * @param {Object} config
 * @param {Function} callback
 * @returns {Promise} */
authorize.extractFromToken = extractFromToken;

function encodeTokenFromLogin(login, config, callback) {
	const object = {
		sub: login,
	};

	return jwt.sign(
		object,
		config.jwtHmacSecret,
		getJWTOptions({ config }, { }),
		callback,
	);
}

/**
 * Encodes token from login
 * @param {string} login
 * @param {Object} config
 * @param {Function} callback
 * @returns {Promise} */
authorize.encodeTokenFromLogin = encodeTokenFromLogin;

function setupIsAuthenticated(getByLogin) {
	return function isAuthenticated(roles, context/* , info */) {
		// console.log('isAuthenticated');
		// debugger;

		if (context.userId && context.user) {
			return Promise.resolve();
		}

		if (typeof roles === 'string') {
			roles = [roles];
		}

		roles = roles || [];

		const { headers } = context;

		// console.log('auth setup');
		// debugger;

		const deviceId = context.deviceId || getDeviceIdHeader(context.headers);

		let token = null;

		try {
			const authorization = getAuthorizationHeader(context.headers);

			if (!authorization) {
				return Promise.reject(new Error('Missing Authorization Header'));
			}

			token = authorization.replace(/Bearer /i, '');
		} catch (err) {
			return Promise.reject(new Error(`Not Authorized: ${err.message}`));
		}

		if (!token) {
			return Promise.reject(new Error('Bearer Token Not Found'));
		}

		const { userCollection } = context.store;
		const { userLogin, userRoles, idProperty } = userCollection;

		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;

		// console.log('auth tokens');
		// debugger;

		if (context.config.adminToken && token === context.config.adminToken) {
			context.userId = ''; // '(admin)';
			context.user = {
				[idProperty]: '', // '(admin)',
				[userLogin]: context.config.superLogin,
				[userRoles]: [context.config.superRole],
				[tokenName]: token,
				[tokensName]: '{}',
			};
			if (context.config.superRole !== 'super') {
				context[context.config.superRole] = true;
			}
			context.super = true;
			return Promise.resolve();
		}

		return new Promise((resolve, reject) => jwt.verify(
			token,
			context.config.jwtHmacSecret,
			getJWTOptions(context, {
				algorithms: ['HS256'],
			}),
			(err, decoded) => {
				// console.log('auth decoded', err, decoded);
				// debugger;

				if (err) {
					return reject(err);
				}

				if (!decoded) {
					return reject(new Error('Invalid token'));
				}

				// console.log('getbylogin', decoded.sub);
				// debugger;

				return getByLogin(context, decoded.sub).then((user) => {
					// TODO: make so an array is stored at deviceId in user tokens
					const strictToken = user && user[tokensName] && user[tokensName][deviceId];
					let matchedDeviceId = null;
					const matchedToken = strictToken === token ? strictToken : (() => {
						if (
							user
							&& user[tokensName]
							&& Object.keys(user[tokensName]).some((key) => {
								if (user[tokensName][key] === token) {
									matchedDeviceId = key;
									return true;
								}
								return false;
							})
						) {
							if (context.config.strictAuth) {
								return null;
							}
							return token;
						}
						return null;
					})();
					// console.log('matching tokens', matchedToken, token);
					// debugger;
					// TODO: ensure this is working in every edge case
					if (matchedToken === token) {
						if (user[userLogin] === context.config.superLogin) {
							context.super = true;
						}

						if (!context.super) {
							// TODO: add config for free access or not, then add this check if no
							// if (!roles?.length) {
							// 	return reject(new Error('Not Authorized: No access'));
							// }
							if (roles.length === 1) {
								if (user[userRoles].indexOf(roles[0]) === -1) {
									return reject(new Error('Not Authorized: No access'));
								}
							} else {
								let error = null;
								if (
									roles.some((role) => {
										if (user[userRoles].indexOf(role) === -1) {
											error = new Error('Not Authorized: No access');
											return true;
										}
										return false;
									})
								) {
									return reject(error);
								}
							}
						}

						context.user = user;
						context.userId = user[idProperty];

						return resolve();
					}

					// console.log('REJECTED');

					return reject(new Error('Not Authorized: No user'));
				}, reject);
			},
		));
	};
}

/**
 * Setup isAuthenticated method for context
 * @param {Function} setupIsAuthenticated
 * @returns {Promise} */
authorize.setupIsAuthenticated = setupIsAuthenticated;
