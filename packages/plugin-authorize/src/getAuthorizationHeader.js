module.exports = function (headers) {
	return (
		headers
		&& (
			headers.Authorization
			|| headers.authorization
			// || headers.AUTHORIZATION
		)
	) || null;
}
