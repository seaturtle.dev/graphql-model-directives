module.exports = function (headers) {
	return (
		headers.deviceId
		|| headers.deviceid
		// || headers.deviceID
		// || headers.DeviceId
		// || headers.DeviceID
		// || headers.DEVICEID
		|| 'web'
	)
};
