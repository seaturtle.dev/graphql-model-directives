/* eslint global-require:0 */

// const { EventEmitter } = require('events');

const StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');
const ApiSnocodeCollection = require('@seaturtle/gmd-adapter-data/Collection.js');

const user = require('@seaturtle/gmd-adapter-data/user.js');
const model = require('./model');

/**
* Stores document collections in a very simple array<object> structure.
* @exports ApiSnocodeStore
* @class
* @extends StoreAdapter */
class ApiSnocodeStore extends StoreAdapter {
	/**
	 * [constructor description]
	 * @param {[type]} options  [description]
	 * @param {[type]} crypto [description]
	 * @constructs
	 */
	constructor({
		// getSchema, // get { ...schema }
		// setSchema, // set { ...prevSchema, ...newSchema }
		getData, // get { ...data }
		setData, // set { ...prevData, ...newData }
		removeData, // unset
		resolvers, // object
		ApiSnocode, // ApiSnocode library
		pubsub, // like an event emitter
		...clientMixin
	}, crypto) {
		super(resolvers);
		// TODO: use ApiSnocode.Table?
		this.ApiSnocode = ApiSnocode || this.ApiSnocode;
		this.crypto = crypto || this.crypto;
		this.pubsub = pubsub || this.pubsub;
		this.client = {
			...clientMixin,
			getData,
			setData,
			removeData,
			// getSchema,
			// setSchema,
		};
	}

	/**
	 * [mixinUser description]
	 * @param  {[type]} collection [description]
	 * @return {[type]}            [description]
	 */
	mixinUser(collection) {
		return super.mixinUser(collection, user, model);
	}

	/**
	 * [authenticate description]
	 * @param  {[type]}   context [description]
	 * @param  {[type]}   info    [description]
	 * @param  {Function} next    [description]
	 * @param  {[type]}   roles   required user roles for to authenticate
	 * @return {[type]}           [description]
	 */
	authenticate(context, info, next, or, roles = []) {
		// TODO: add try catches from this to other authenticate implmentations
		// console.log('authenticate');
		// debugger;
		return new Promise((resolve, reject) => {
			try {
				const onAuthError = (err) => {
					if (or === true) { return resolve(next(err)); }
					if (typeof or === 'function') { return resolve(or(err, next)); }
					return reject(err);
				};
				try {
					// console.log('user isAuthenticated');
					// debugger
					return user.isAuthenticated(roles, context, info).then(() => (
						Promise.resolve(next()).then(resolve, reject)
					), onAuthError);
				} catch (authErr) {
					// console.log('authenticate user error');
					// debugger;
					return onAuthError(authErr);
				}
			} catch (err) {
				// console.log('authenticate error');
				// debugger;
				return reject(err);
			}
		});
	}

	/**
	 * [readDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	readDocument({ name }, { target, options }, { context, info }) {
		return model.read(this, name, target, options, context, info);
	}

	/**
	 * [findDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	findDocuments({ name }, { target, options }, { context, info }) {
		return model.find(this, name, target, options, context, info);
	}

	/**
	 * [countDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	countDocuments({ name }, { target, options }, { context, info }) {
		return model.count(this, name, target, options, context, info);
	}

	/**
	 * [scanDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	scanDocuments({ name }, { target, options }, { context, info }) {
		return model.scan(this, name, target, options, context, info);
	}

	/**
	 * [purgeDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	purgeDocuments({ name }, { target, options }, { context, info }) {
		return model.purge(this, name, target, options, context, info);
	}

	/**
	 * [upsertDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	upsertDocument({ name }, { target, options }, { context, info }) {
		return model.upsert(this, name, target, options, context, info);
	}

	/**
	 * [removeDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	removeDocument({ name }, { target, options }, { context, info }) {
		return model.remove(this, name, target, options, context, info);
	}
}

ApiSnocodeStore.Collection = ApiSnocodeCollection;

module.exports = ApiSnocodeStore;
