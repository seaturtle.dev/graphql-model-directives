/* eslint global-require: 1 */

// const config = require('./config');

const createMailer = require('@seaturtle/gmd-server-example/mailer.js');

module.exports = function (parentConfig, nodemailer, aws) {
	// const mailer = createMailer(parentConfig, nodemailer, aws);
	const mailer = {};

	const { mailerType, mailerLog, mailerSMTP/* , mailerAWS */ } = parentConfig;

	function send(message) {
		// if (mailerLog) { console.log('MAIL:\n', message); }

		return Promise.resolve({
			message,
			mailerType,
			mailerSMTP,
		});
	}

	mailer.send = send;

	mailer.sendConfirmEmail = require(
		'@seaturtle/gmd-plugin-email/email/sendConfirmEmail.js',
	)(send);

	mailer.sendForgotPassword = require(
		'@seaturtle/gmd-plugin-email/email/sendForgotPassword.js',
	)(send);

	mailer.sendInviteEmail = require(
		'@seaturtle/gmd-plugin-email/email/sendInviteEmail.js',
	)(send);

	return mailer;
};
