/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */

function cleanup() {
	after(function (done) {
		try {
			if (!this.started) {
				done();

				return;
			}

			this.server.stop(function () {
				done();
			});
		} catch (err) {
			done(err);
		}
	});
}

module.exports = cleanup;
