/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

// const templates = require('../../../../example/templates');

function seed(done, seedData) {
	if (!seedData) {
		return done();
	}
	const promises = Object.keys(seedData).map((key) => (
		new Promise((resolve, reject) => {
			const dataParams = seedData[key];
			if (!dataParams.collection || !dataParams.collection.length) {
				this[key] = [];
				return resolve(this[key]);
			}
			const collectionPromises = dataParams.collection.map((data, i) => (
				new Promise((res, rej) => setTimeout(() => (
					this.userClient.request(dataParams.template, {
						data: typeof data === 'function' ? data.call(this, this) : data,
					}).then(res, rej)
				), i * (dataParams.wait || 0)))
			));
			return Promise.all(collectionPromises).then((collection) => {
				if (collection) {
					this[key] = collection.map((item) => (item[dataParams.templateName]));
				} else {
					this[key] = [];
				}
				return resolve(collection);
			}, reject);
		})
	));
	if (!promises.length) {
		return done();
	}
	return Promise.all(promises).then(() => done(), (err) => {
		if (err) { console.error('SEED ERROR:', err); }
	});
}

module.exports = seed;
