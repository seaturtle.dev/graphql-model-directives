/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */

const { GraphQLClient } = require('graphql-request');

const {
	ApolloClient,
	ApolloLink,
	// HttpLink,
} = require('@apollo/client');

const {
	// ApolloCache,
	InMemoryCache,
} = require('@apollo/client/cache');

const { setContext } = require('@apollo/client/link/context');
const { WebSocketLink } = require('@apollo/client/link/ws');
const { getMainDefinition } = require('@apollo/client/utilities');

const { fetch } = require('cross-fetch/polyfill');

const { createUploadLink } = require('apollo-upload-client');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const WebSocket = require('ws');

const servers = require('@seaturtle/gmd-server-example/servers.js');

const wrapClientRequest = require('./wrapClientRequest');

const config = require('./config');

function setup(endpoint, serverName, host, port) {
	before(function (done) {
		if (!servers[serverName]) {
			return done(new Error(`Missing server: ${serverName}`));
		}

		this.serverPromise = servers[serverName](
			config,
		);

		this.serverPromise.then((server) => {
			this.server = server;

			this.server.start(host, port, () => {
				this.started = true;

				this.headers = {
					deviceId: 'web',
					authorization: 'admin',
				};

				this.adminClient = wrapClientRequest(
					new GraphQLClient(endpoint, {
						headers: this.headers,
					})
				);

				this.noClient = wrapClientRequest(
					new GraphQLClient(endpoint, {
						headers: { deviceId: 'web', authorization: 'Bearer token' },
					})
				);

				const authLink = setContext((_, { headers }) => (
					{
						headers: {
							...headers,
							...this.headers,
						},
					}
				));

				const httpLink = createUploadLink({
				// const httpLink = new HttpLink({
					uri: endpoint,
					fetch,
				});

				const wsUri = endpoint.replace('http://', 'ws://'/* 'admin@' */);

				this.subscriptionClient = new SubscriptionClient(
					wsUri,
					{
						lazy: true,
						connectionParams: () => ({
							headers: {
								...this.headers,
							},
						}),
					},
					WebSocket,
				);

				const wsLink = new WebSocketLink(
					this.subscriptionClient,
				);

				const link = ApolloLink.split(
					({ query }) => {
						const definition = getMainDefinition(query);
						return (
							definition.kind === 'OperationDefinition'
							&& definition.operation === 'subscription'
						);
					},
					authLink.concat(wsLink),
					authLink.concat(httpLink),
				);

				this.apolloClient = new ApolloClient({
					link,
					cache: new InMemoryCache(),
				});

				done();
			});
		}, done);
	});
}

module.exports = setup;
