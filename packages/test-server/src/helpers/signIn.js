/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');
const { GraphQLClient } = require('graphql-request');

const {
	ApolloClient,
	ApolloLink,
	// HttpLink,
} = require('@apollo/client');

const {
	// ApolloCache,
	InMemoryCache,
} = require('@apollo/client/cache');

const { setContext } = require('@apollo/client/link/context');
const { WebSocketLink } = require('@apollo/client/link/ws');
const { getMainDefinition } = require('@apollo/client/utilities');

const { fetch } = require('cross-fetch/polyfill');
const { createUploadLink } = require('apollo-upload-client');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const WebSocket = require('ws');

const templates = require('@seaturtle/gmd-server-example/templates.js');

function signIn(
	endpoint,
	seedData,
	unseedData,
	wait = 0,
	{
		login = 'user@local',
		password = 'user@local',
		userProperty = 'user',
	} = { },
) {
	before(function (done) {
		setTimeout(() => {
			this.adminClient.request(templates.signIn, {
				emailAddr: login,
				pass: password,
			}).then((data) => {
				this[userProperty] = data.signIn;
				this[`${userProperty}Id`] = data.signIn.id;

				const token = this[userProperty].tokenList[0].split(':')[1];

				this[`${userProperty}Client`] = new GraphQLClient(endpoint, {
					headers: {
						deviceId: 'web',
						authorization: `Bearer ${token}`,
					},
				});

				this[`${userProperty}Token`] = token;

				const authLink = setContext((_, { headers }) => (
					{
						headers: {
							...headers,
							deviceId: 'web',
							authorization: `Bearer ${token}`,
						},
					}
				));

				const httpLink = createUploadLink({
				// const httpLink = new HttpLink({
					uri: endpoint,
					fetch,
				});

				const wsUri = endpoint.replace('http://', 'ws://'/* 'admin@' */);

				this.subscriptionClient = new SubscriptionClient(
					wsUri,
					{
						lazy: true,
						connectionParams: () => ({
							headers: {
								...this.headers,
							},
						}),
					},
					WebSocket,
				);

				const wsLink = new WebSocketLink(
					this.subscriptionClient,
				);

				const link = ApolloLink.split(
					({ query }) => {
						const definition = getMainDefinition(query);
						return (
							definition.kind === 'OperationDefinition'
							&& definition.operation === 'subscription'
						);
					},
					authLink.concat(wsLink),
					authLink.concat(httpLink),
				);

				this[`${userProperty}ApolloClient`] = new ApolloClient({
					link,
					cache: new InMemoryCache(),
				});

				if (seedData) {
					seedData.call(this, done, this);
				} else {
					done();
				}
			}, done);
		}, wait);
	});

	after(function (done) {
		const context = this;

		function finish() {
			delete context.user;
			delete context.userId;
			delete context.userToken;
			delete context.userClient;
			done();
		}

		if (unseedData) {
			unseedData.call(this, finish, this);
		} else {
			finish();
		}
	});

	it('user signed in is ok', function () {
		expect(this[userProperty]).to.be.ok;
		expect(this[`${userProperty}Id`]).to.be.ok;
		expect(this[`${userProperty}Token`]).to.be.ok;
		expect(this[`${userProperty}Client`]).to.be.ok;
		expect(this[`${userProperty}ApolloClient`]).to.be.ok;
	});
}

module.exports = signIn;
