function seedResource(
	offset,
	props = {},
) {
	const {
		author,
		name = 'name',
		description = 'description',
		float = Math.random(),
		date = new Date(),
		...otherProps
	} = props;
	return (context) => ({
		created:
			new Date(Date.now() - (1000 * 60 * (offset + 1))),
		updated:
			new Date(Date.now() - (1000 * 60 * (offset))),
		author: author || context.userId,
		name,
		description,
		float,
		date,
		...otherProps,
	});
}

module.exports = seedResource;
