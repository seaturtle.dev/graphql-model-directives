/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const templates = require('@seaturtle/gmd-server-example/templates.js');

function unregister(userProperty = 'user', cb = null) {
	after(function (done) {
		this.adminClient.request(templates.removeUser, {
			id:
				this[`${userProperty}Id`]
				|| (this[userProperty] && this[userProperty].id),
		}).then((data) => {
			if (!data || !data.removeUser || !data.removeUser.id) {
				return done(new Error(`Invalid response when unregistering "${userProperty}".`));
			}
			if (cb) {
				return cb(done);
			}
			return done();
		}, done);
	});
}

module.exports = unregister;
