/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

// const templates = require('@seaturtle/gmd-server-example/templates.js');

function unseed(done, unseedData) {
	if (!unseedData) {
		return done();
	}
	// console.log('unseedData', unseedData);
	const promises = Object.keys(unseedData).map((key) => (
		new Promise((resolve, reject) => {
			const dataParams = unseedData[key];
			let { collection } = dataParams;
			collection = typeof collection === 'function' ? collection.call(this, this) : collection;
			if (!collection || !collection.length) {
				delete this[key];
				return resolve(null);
			}
			// console.log('got here 2', collection);
			const collectionPromises = collection.map((data) => {
				const id = (data && data.id) || data;
				// console.log('delete', id, data);
				if (!id) {
					return Promise.resolve();
				}
				return this.userClient.request(dataParams.template, {
					id,
				});
			});
			return Promise.all(collectionPromises).then((result) => {
				delete this[key];
				return resolve(result);
			}, reject);
		})
	));
	if (!promises.length) {
		return done();
	}
	return Promise.all(promises).then(() => done(), (err) => {
		if (err) {
			console.error('UNSEED ERROR:', err);
		}
		return done(err);
	});
}

module.exports = unseed;
