/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { GraphQLClient } = require('graphql-request');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const wrapClientRequest = require('./wrapClientRequest');

function register(
	endpoint = '',
	{
		name = 'name',
		login = 'user@local',
		password = 'user@local',
		birthdate = '2007-01-01',
		userProperty = 'user',
		idProperty = 'id',
		tokenProperty = null,
	} = {},
	cb = null,
) {
	before(function (done) {
		this.adminClient.request(templates.registerUser, {
			emailAddr: login,
			pass: password,
			birth: birthdate,
		}).then((data) => {
			if (userProperty) {
				this[userProperty] = data.registerUser;
				this[`${userProperty}Id`] = data.registerUser.id;
			}

			const token = this[userProperty].tokenList[0].split(':')[1];

			if (tokenProperty) {
				this[tokenProperty] = token;
			}

			const client = wrapClientRequest(
				new GraphQLClient(endpoint, {
					headers: {
						deviceId: 'web',
						authorization: `Bearer ${token}`,
					},
				}),
			);

			if (userProperty) {
				this[`${userProperty}Client`] = client;
			}

			// debugger;

			client.request(templates.upsertUser, {
				data: { name: name },
			}).then(function () {
				// debugger;

				if (cb) {
					cb(client, token, done);
				} else {
					done();
				}
			}, done);
		}, done);
	});
}

module.exports = register;
