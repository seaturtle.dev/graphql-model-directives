// This makes promises work with mocha expect test assertions without altering default Promise behavior
module.exports = function wrapClientRequest(client) {
	const originalRequest = client.request;
	client.request = function (a, b, c, d) {
		const promise = originalRequest.call(this, a, b, c, d);
		const originalThen = promise.then;
		promise.then = function (onResolve, onReject) {
			function handleError(err) {
				try {
					onReject(err);
				} catch (tryErr) {
					console.error(tryErr);
					process.exit();
					// throw tryErr;
				}
			}
			function handleSuccess(payload1/* , payload2, payload3 */) {
				try {
					return onResolve(payload1/* , payload2, payload3 */);
				} catch (err) {
					return handleError(err);
				}
			}
			const result = originalThen.call(
				this,
				handleSuccess,
				handleError,
			);
		};
		return promise;
	};
	return client;
}
