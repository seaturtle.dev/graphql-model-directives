const path = require('path');

const createUpload = require('@seaturtle/gmd-server-example/uploader.js');

const createCrypto = require('@seaturtle/gmd-server-example/crypto.js');

const createMailer = require('./mailer');

const config = exports;

config.adminToken = 'admin';

config.adminEmail = 'admin@local';

config.superLogin = 'admin@local';

config.superRole = 'super';

config.inviteOnly = true;

config.defaultMaxInvites = 10;

config.jwtAudience = '';

config.jwtIssuer = '';

config.jwtHmacSecret = 'test';

config.awsPath = null;

// config.uploaderS3Bucket = 'uploads';

// config.uploaderS3PublicURL = '';

config.uploaderLocalPath = path.join(__dirname, '..', 'uploads');

config.uploaderPublicPath = '/uploads';

config.uploader = createUpload(config);

config.mailerType = 'smtp'; // or aws-ses

config.mailerSMTP = {};

// config.mailerAWS = null;

config.mailerLog = true;

config.mailer = createMailer(config);

config.noreply = 'noreply@local';

const linkHost = 'http://localhost:3000';

config.linkConfirmEmail = `${linkHost}/confirmEmail`;

config.linkForgetPassword = `${linkHost}/forgetPassword`;

config.linkInviteEmail = `${linkHost}/inviteEmail`;

config.sqlStoragePath = path.join(__dirname, '..', '..', 'database.sqlite');

config.crypto = createCrypto(config);
