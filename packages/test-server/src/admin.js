/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

function admin() {
	describe('Admin Access', () => {
		it('can fetch admin id', function (done) {
			this.adminClient.request(templates.myId).then((data) => {
				expect(data.me).to.be.ok;
				done();
			}, done);
		});

		it('can find and remove all users', function (done) {
			this.adminClient.request(templates.users).then((data) => {
				if (data.users && data.users.page) {
					// if (data.users.page.length) {
					// 	console.warn('WARN: removing users', data.users.page);
					// }
					const promises = data.users.page.map((user) => {
						const params = { id: user.id };
						return this.adminClient.request(templates.removeUser, params);
					});
					Promise.all(promises).then(() => done(), done);
				} else {
					done();
				}
			}, done);
		});

		it('removed all users', function (done) {
			this.adminClient.request(templates.users).then((data) => {
				expect(data.users.page).to.be.ok;
				expect(data.users.page.length).to.equal(0);
				done();
			}, done);
		});

		it('can find and remove all user invites', function (done) {
			this.adminClient.request(templates.invites).then((data) => {
				if (data.userInvites && data.userInvites.page) {
					const promises = data.userInvites.page.map((invite) => {
						const params = { id: invite.id };
						return this.adminClient.request(templates.revokeInvite, params);
					});
					Promise.all(promises).then(() => done(), done);
				} else {
					done();
				}
			}, done);
		});

		it('removed all user invites', function (done) {
			this.adminClient.request(templates.invites).then((data) => {
				expect(data.userInvites.page).to.be.ok;
				expect(data.userInvites.page.length).to.equal(0);
				done();
			}, done);
		});

		it('can upsert a new admin resource', function (done) {
			this.adminClient.request(templates.upsertResource, {
				data: {
					description: 'adminTest',
					isDone: false,
				},
			}).then((data) => {
				this.adminResourceId = data.upsertResource.id;
				expect(data.upsertResource.id).to.be.ok;
				expect(data.upsertResource.description).to.equal('adminTest');
				expect(data.upsertResource.isDone).to.equal(false);
				done();
			}, done);
		});

		it('can find and remove all resources', function (done) {
			this.adminClient.request(templates.purgeResources, {}).then((data) => {
				expect(data).to.be.ok;
				expect(data.purgeResources).to.be.ok;
				done();
			}, done);
			// this.adminClient.request(templates.adminAllResources).then((data) => {
			// 	if (data.adminAllResources && data.adminAllResources.page.length) {
			// 		const promises = data.adminAllResources.page.map((resource) => {
			// 			const params = { id: resource.id };
			// 			return this.adminClient.request(templates.removeResource, params);
			// 		});
			// 		Promise.all(promises).then(() => done(), done);
			// 	} else {
			// 		done();
			// 	}
			// }, done);
		});

		it('removed all resources', function (done) {
			this.adminClient.request(templates.resources).then((data) => {
				expect(data.resources.page).to.be.ok;
				expect(data.resources.page.length).to.equal(0);
				done();
			}, done);
		});

		it('can count resources', function (done) {
			this.adminClient.request(templates.resourcesCount).then((data) => {
				expect(data).to.be.ok;
				expect(data.resourcesCount).to.equal(0);
				done();
			}, done);
		});

		it('can upsert a new admin resource', function (done) {
			this.adminClient.request(templates.upsertResource, {
				data: {
					description: 'adminTest',
					isDone: false,
				},
			}).then((data) => {
				this.adminResourceId = data.upsertResource.id;
				expect(data.upsertResource.id).to.be.ok;
				expect(data.upsertResource.description).to.equal('adminTest');
				expect(data.upsertResource.isDone).to.equal(false);
				done();
			}, done);
		});

		it('can count a resource', function (done) {
			this.adminClient.request(templates.resourcesCount).then((data) => {
				expect(data).to.be.ok;
				expect(data.resourcesCount).to.equal(1);
				done();
			}, done);
		});

		it('can modify an existing admin resource', function (done) {
			this.adminClient.request(templates.upsertResource, {
				data: {
					id: this.adminResourceId,
					isDone: true,
				},
			}).then((data) => {
				expect(data.upsertResource.id).to.equal(this.adminResourceId);
				expect(data.upsertResource.isDone).to.equal(true);
				done();
			}, done);
		});

		it('can remove an existing admin resource', function (done) {
			this.adminClient.request(templates.removeResource, {
				id: this.adminResourceId,
			}).then((data) => {
				expect(data.removeResource.id).to.equal(this.adminResourceId);
				done();
			}, done);
		});
	});
}

module.exports = admin;
