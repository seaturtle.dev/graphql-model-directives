/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */

const admin = require('./admin');
const user = require('./user');
const model = require('./model');

const cleanup = require('./helpers/cleanup');
const setup = require('./helpers/setup');

function testServer(label, serverName, port, host) {
	host = host || '127.0.0.1';
	const endpoint = `http://${host}:${port}/graphql`;
	describe(label, () => {
		setup(endpoint, serverName, host, port);
		cleanup();
		admin();
		user(endpoint);
		model(endpoint);
	});
}

/**
 * [testServer description]
 * @exports testServer
 * @function testServer
 * @param  {[type]} label      [description]
 * @param  {[type]} serverName [description]
 * @param  {[type]} port       [description]
 * @param  {[type]} host       [description]
 * @return {[type]}            [description]
 */
module.exports = testServer;
