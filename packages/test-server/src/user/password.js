/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const register = require('../helpers/register');
// const unregister = require('../helpers/unregister');

function userPassword(endpoint) {
	describe('User Password', () => {
		signIn(endpoint);

		it('is accessible but not visible to admins', function (done) {
			this.userClient.request(templates.user, { id: this.userId }).then(
				(data) => {
					expect(data).to.be.ok;
					expect(data.user).to.be.ok;
					expect(data.user.pass).to.be.ok;
					expect(data.user.pass !== 'user@local').to.be.ok;
					done();
				},
				(err) => done(err),
			);
		});

		it('is not accessible to anyone', function (done) {
			this.noClient.request(templates.user, { id: this.userId }).then(
				(data) => done(data && data.user ? new Error('user was accessible') : null),
				() => done(),
			);
		});

		describe('with a second User', function () {
			register(endpoint, {
				login: 'pwdUser@local',
				password: 'pwdUser@local',
				userProperty: 'pwdUser',
			});

			// unregister('pwdUser');

			it('can not access the other user', function (done) {
				this.pwdUserClient.request(templates.user, { id: this.userId }).then(
					(data) => done(data && data.user ? new Error('user was accessible') : null),
					() => done(),
				);
			});

			it('is accessible', function (done) {
				this.pwdUserClient.request(templates.user, { id: this.pwdUserId }).then(
					(data) => {
						expect(data).to.be.ok;
						expect(data.user).to.be.ok;
						expect(data.user.pass).to.be.ok;
						expect(data.user.password !== 'pwdUser@local').to.be.ok;
						done();
					},
					done,
				);
			});

			it('can update users current password', function (done) {
				this.pwdUserClient.request(templates.updatePassword, {
					oldPassword: 'pwdUser@local',
					newPassword: 'newPassword',
				}).then((data) => {
					expect(data).to.be.ok;
					expect(data.updatePassword).to.be.ok;
					expect(data.updatePassword.id).to.be.ok;
					done();
				}, done);
			});

			it('can forget user password', function (done) {
				this.noClient.request(templates.forgotPassword, {
					emailAddr: 'pwdUser@local',
				}).then((data) => {
					expect(data).to.be.ok;
					expect(data.forgotPassword).to.be.ok;
					expect(data.forgotPassword.id).to.be.ok;
					setTimeout(done, 100);
				}, done);
			});

			it('can change users forgoten password', function (done) {
				this.adminClient.request(templates.user, {
					id: this.pwdUserId,
				}).then((d) => {
					expect(d).to.be.ok;
					expect(d.user).to.be.ok;
					expect(d.user.passReset).to.be.ok;
					this.passReset = d.user.passReset;
					this.noClient.request(templates.resetPassword, {
						emailAddr: 'pwdUser@local',
						recoveryToken: this.passReset,
						newPassword: 'newPassword',
					}).then((data) => {
						expect(data).to.be.ok;
						expect(data.resetPassword).to.be.ok;
						expect(data.resetPassword.id).to.be.ok;
						done();
					}, done);
				}, done);
			});
		});
	});
}

module.exports = userPassword;
