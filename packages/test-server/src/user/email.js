/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const register = require('../helpers/register');
// const unregister = require('../helpers/unregister');

function userEmail(endpoint) {
	describe('User Email', () => {
		signIn(endpoint);

		it('is accessible to admins', function (done) {
			this.userClient.request(templates.user, { id: this.userId }).then(
				(data) => {
					expect(data).to.be.ok;
					expect(data.user).to.be.ok;
					expect(data.user.emailAddr).to.equal('user@local');
					done();
				},
				(err) => done(err),
			);
		});

		describe('with a new User', function () {
			register(endpoint, {
				login: 'emailUser@local',
				password: 'emailUser@local',
				userProperty: 'emailUser',
			});

			// unregister('emailUser');

			it('can not access the other user', function (done) {
				this.emailUserClient.request(templates.user, { id: this.userId }).then(
					(data) => done(data && data.user ? new Error('user was accessible') : null),
					() => done(),
				);
			});

			it('is accessible', function (done) {
				this.emailUserClient.request(templates.user, { id: this.emailUserId }).then(
					(data) => {
						expect(data).to.be.ok;
						expect(data.user).to.be.ok;
						expect(data.user.emailAddr).to.equal('emailUser@local');
						done();
					},
					done,
				);
			});

			it('can update your email', function (done) {
				this.emailUserClient.request(templates.updateEmail, {
					id: this.emailUserId,
					emailAddr: 'changedEmailUser@local',
				}).then(
					(data) => {
						expect(data).to.be.ok;
						expect(data.updateEmail).to.be.ok;
						expect(data.updateEmail.id).to.be.ok;
						done();
					},
					done,
				);
			});

			it('can confirm updated email', function (done) {
				this.adminClient.request(templates.user, {
					id: this.emailUserId,
				}).then((d) => {
					expect(d).to.be.ok;
					expect(d.user).to.be.ok;
					expect(d.user.emailConfirmation).to.be.ok;
					this.emailConfirmation = d.user.emailConfirmation;
					this.noClient.request(templates.confirmEmail, {
						id: this.emailUserId,
						emailConfirmation: this.emailConfirmation,
					}).then((data) => {
						expect(data).to.be.ok;
						expect(data.confirmEmail).to.be.ok;
						expect(data.confirmEmail.id).to.be.ok;
						done();
					}, done);
				}, done);
			});

			it('was confirmed and updated', function (done) {
				this.adminClient.request(templates.user, {
					id: this.emailUserId,
				}).then((d) => {
					expect(d).to.be.ok;
					expect(d.user).to.be.ok;
					expect(!d.user.emailConfirmation).to.be.ok;
					done();
				}, done);
			});
		});
	});
}

module.exports = userEmail;
