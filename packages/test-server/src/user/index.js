const email = require('./email');
const name = require('./name');
const invites = require('./invites');
const login = require('./login');
const password = require('./password');
const registration = require('./registration');
const resources = require('./resources');
const roles = require('./roles');

function user(endpoint) {
	registration(endpoint);
	name(endpoint);
	roles(endpoint);
	invites(endpoint);
	login(endpoint);
	password(endpoint);
	email(endpoint);
	resources(endpoint);
}

module.exports = user;
