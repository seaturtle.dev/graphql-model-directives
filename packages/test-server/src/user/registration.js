/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const register = require('../helpers/register');

function modelIndexes(endpoint) {
	describe('User Registration', () => {
		register(endpoint, {
			login: 'user@local',
			password: 'user@local',
			birthdate: '1990-01-01',
		});

		it('can register a test user', function () {
			expect(this.user.id).to.be.ok;
			expect(this.user.tokenList).to.be.ok;
			expect(this.user.tokenList[0]).to.be.ok;
		});

		it('can get user info', function (done) {
			this.userClient.request(templates.myId).then((data) => {
				expect(data.me.id).to.be.ok;
				done();
			}, done);
		});

		describe('User Resources', function () {
			it('can upsert a new user resource', function (done) {
				this.userClient.request(templates.upsertResource, {
					data: {
						description: 'userTestA',
						isDone: false,
					},
				}).then((data) => {
					this.userResourceIdA = data.upsertResource.id;
					expect(data.upsertResource.id).to.be.ok;
					expect(data.upsertResource.description).to.equal('userTestA');
					expect(data.upsertResource.isDone).to.equal(false);
					done();
				}, done);
			});

			it('can upsert another new user resource', function (done) {
				this.userClient.request(templates.upsertResource, {
					data: {
						description: 'userTestB',
						isDone: false,
					},
				}).then((data) => {
					this.userResourceIdB = data.upsertResource.id;
					expect(data.upsertResource.id).to.be.ok;
					expect(data.upsertResource.description).to.equal('userTestB');
					expect(data.upsertResource.isDone).to.equal(false);
					done();
				}, done);
			});

			it('can get user resources', function (done) {
				this.userClient.request(templates.myResources).then((data) => {
					expect(data.me.resources).to.be.ok;
					expect(data.me.resources.page).to.be.ok;
					expect(data.me.resources.page.length).to.equal(2);
					done();
				}, done);
			});

			it('can modify an existing user resource', function (done) {
				this.userClient.request(templates.upsertResource, {
					data: {
						id: this.userResourceIdA,
						isDone: true,
					},
				}).then((data) => {
					expect(data.upsertResource.id).to.equal(this.userResourceIdA);
					expect(data.upsertResource.isDone).to.equal(true);
					done();
				}, done);
			});

			it('can remove first user resource', function (done) {
				this.userClient.request(templates.removeResource, {
					id: this.userResourceIdA,
				}).then(() => {
					this.userClient.request(templates.myResources).then((data) => {
						expect(data.me.resources).to.be.ok;
						expect(data.me.resources.page.length).to.equal(1);
						expect(data.me.resources.page[0].id).to.equal(this.userResourceIdB);
						done();
					}, done);
				}, done);
			});

			it('can remove second user resource', function (done) {
				this.userClient.request(templates.removeResource, {
					id: this.userResourceIdB,
				}).then(() => {
					this.userClient.request(templates.myResources).then((data) => {
						expect(data.me.resources).to.be.ok;
						expect(data.me.resources.page.length).to.equal(0);
						done();
					}, done);
				}, done);
			});
		});
	});
}

module.exports = modelIndexes;
