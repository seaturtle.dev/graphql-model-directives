/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');

function userResources(endpoint) {
	const seedResourcesCollection = [
		seedResource(0, { name: 'a' }),
		seedResource(0, { name: 'b' }),
		seedResource(0, { name: 'c' }),
	];

	describe('User Resources', () => {
		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		it('is ok', function (done) {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
			this.userClient.request(templates.resources, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
				},
				filter: null,
			}).then((data) => {
				// debugger;
				this.serverResources = data.resources.page;
				[this.firstServerResource] = this.serverResources;
				this.firstServerResourceId = this.firstServerResource.id;
				done();
			}, done);
		});

		// TODO: author id is available by relation but not name
		it('has a virtual alias of the authors name', function () {
			// debugger;
			expect(this.firstServerResource.author).to.be.ok;
			expect(this.firstServerResource.author.id).to.be.ok;
			expect(this.firstServerResource.authorName).to.equal(this.firstServerResource.author.name);
			// debugger;
			try {
				expect(this.firstServerResource.authorName).to.be.ok;
			} catch (err) {
				// TODO:
				// console.warn('Author name was null, this could be a proble');
			}
		});
	});
}

module.exports = userResources;
