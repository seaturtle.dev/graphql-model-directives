/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');

function apiRoleBasedAuth(endpoint) {
	describe('API Role Based Authentication', () => {
		signIn(endpoint);

		it('admins can call any api method', function (done) {
			this.adminClient.request(templates.adminAllUsers, {}).then((data) => {
				expect(data).to.be.ok;
				expect(data.adminAllUsers).to.be.ok;
				expect(data.adminAllUsers.page).to.be.ok;
				expect(data.adminAllUsers.page.length).to.be.ok;
				done();
			}, done);
		});

		it('anybody can NOT call an admin api', function (done) {
			this.noClient.request(templates.adminAllUsers, {}).then(() => {
				done(new Error('anybody could call an admin api'));
			}, () => {
				done();
			});
		});

		it('a user can NOT call an admin api', function (done) {
			this.userClient.request(templates.adminAllUsers, {}).then(() => {
				done(new Error('a user could call an admin api'));
			}, () => {
				done();
			});
		});
	});
}

module.exports = apiRoleBasedAuth;
