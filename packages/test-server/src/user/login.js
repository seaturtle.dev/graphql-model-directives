/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const register = require('../helpers/register');
const signIn = require('../helpers/signIn');
// const signOut = require('../helpers/signOut');
// const unregister = require('../helpers/unregister');

function userLogin(endpoint) {
	describe('User Login', () => {
		register(endpoint, {
			login: 'loginUser@local',
			password: 'loginUser@local',
			userProperty: 'loginUser',
		});

		// unregister('loginUser');

		it('can not access the other user', function (done) {
			this.loginUserClient.request(templates.user, { id: this.userId }).then(
				(data) => done(data && data.user ? new Error('user was accessible') : null),
				() => done(),
			);
		});

		it('is accessible', function (done) {
			this.loginUserClient.request(templates.user, { id: this.loginUserId }).then(
				(data) => {
					expect(data).to.be.ok;
					expect(data.user).to.be.ok;
					expect(data.user.emailAddr).to.equal('loginUser@local');
					done();
				},
				done,
			);
		});

		it('can update your login', function (done) {
			this.loginUserClient.request(templates.updateLogin, {
				id: this.loginUserId,
				emailAddr: 'changedLoginUser@local',
			}).then(
				(data) => {
					expect(data).to.be.ok;
					expect(data.updateLogin).to.be.ok;
					expect(data.updateLogin.id).to.be.ok;
					done();
				},
				done,
			);
		});
	});

	describe('login again to update both email and login', () => {
		signIn(endpoint, null, null, 0, {
			login: 'changedLoginUser@local',
			password: 'loginUser@local',
			userProperty: 'loginUser',
		});

		it('can update your email and login', function (done) {
			this.loginUserClient.request(templates.updateEmailAndLogin, {
				id: this.loginUserId,
				emailAddr: 'changedEmailAndLoginUser@local',
			}).then(
				(data) => {
					// console.log(data);
					expect(data).to.be.ok;
					expect(data.updateEmailAndLogin).to.be.ok;
					expect(data.updateEmailAndLogin.id).to.be.ok;
					done();
				},
				done,
			);
		});
	});

	describe('login again to sign out', () => {
		signIn(endpoint, null, null, 0, {
			login: 'changedEmailAndLoginUser@local',
			password: 'loginUser@local',
			userProperty: 'loginUser',
		});

		it('can sign out', function (done) {
			this.loginUserClient.request(templates.signOut, {}).then(
				(data) => {
					// console.log(data);
					expect(data).to.be.ok;
					expect(data.signOut).to.be.ok;
					expect(data.signOut.id).to.be.ok;
					done();
				},
				done,
			);
		});
	});
}

module.exports = userLogin;
