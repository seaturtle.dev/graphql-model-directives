/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const register = require('../helpers/register');
const unregister = require('../helpers/unregister');

function userName(endpoint) {
	describe('User Name', () => {
		signIn(endpoint);

		it('is not accessible to anyone', function (done) {
			this.noClient.request(templates.user, { id: this.userId }).then(
				(data) => done(data && data.user ? new Error('user was accessible') : null),
				() => done(),
			);
		});

		it('can be updated', function (done) {
			this.userClient.request(templates.upsertUser, {
				data: {
					name: 'user name'
				},
			}).then((data) => {
				expect(data).to.be.ok;
				expect(data.upsertUser).to.be.ok;
				expect(data.upsertUser.id).to.be.ok;
				expect(data.upsertUser.name).to.be.ok;
				expect(data.upsertUser.name).to.equal('user name');
				done();
			}, done);
		});

		it('can\'t be used to update malicous fields', function (done) {
			this.userClient.request(templates.upsertUser, {
				data: {
					name: 'user',
					malicous: 'field',
					emailAddr: 'nope',
					bith: 'lol',
				},
			}).then((data) => {
				expect(data).to.be.ok;
				expect(data.upsertUser).to.be.ok;
				expect(data.upsertUser.id).to.be.ok;
				expect(data.upsertUser.name).to.be.ok;
				done();
			}, done);
		});

		describe('with a second User', function () {
			register(endpoint, {
				login: 'nameUser@local',
				password: 'nameUser@local',
				userProperty: 'nameUser',
			});

			unregister('nameUser');

			it('can not access the other user', function (done) {
				this.nameUserClient.request(templates.user, { id: this.userId }).then(
					(data) => done(data && data.user ? new Error('user was accessible') : null),
					() => done(),
				);
			});
		});
	});
}

module.exports = userName;
