/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');

function userInvites(endpoint) {
	describe('User Invites', () => {
		signIn(endpoint);

		it('can purge all invites', function (done) {
			this.adminClient.request(templates.purgeInvites, {}).then((data) => {
				expect(data).to.be.ok;
				done();
			}, done);
		});

		it('can count zero invites', function (done) {
			this.adminClient.request(templates.invitesCount, {}).then((data) => {
				expect(data).to.be.ok;
				expect(data.userInvitesCount).to.equal(0);
				done();
			}, done);
		});

		it('can send a user invite', function (done) {
			this.userClient.request(templates.sendInvite, {
				emailAddr: 'test2@local',
				message: 'message',
			}).then((data) => {
				expect(data.sendUserInvite).to.be.ok;
				expect(data.sendUserInvite.id).to.be.ok;
				this.lastInviteId = data.sendUserInvite.id;
				done();
			}, done);
		});

		it('can revoke a user invite', function (done) {
			expect(this.lastInviteId).to.be.ok;
			this.userClient.request(templates.revokeInvite, {
				id: this.lastInviteId,
			}).then((data) => {
				expect(data.revokeUserInvite).to.be.ok;
				expect(data.revokeUserInvite.id).to.be.ok;
				done();
			}, done);
		});

		it('has no user invites', function (done) {
			this.adminClient.request(templates.invites).then((data) => {
				expect(data.userInvites.page).to.be.ok;
				expect(data.userInvites.page.length).to.equal(0);
				done();
			}, done);
		});

		it('can not register without an invite', function (done) {
			this.noClient.request(templates.registerUser, {
				emailAddr: 'shouldnotexists@local',
				pass: 'shouldnotexists@local',
				birth: '2001-01-01',
			}).then((data) => {
				if (data && data.registerUser && data.registerUser.id) {
					done(new Error('Register worked without an invite.'));
				} else if (data) {
					console.warn('INVITE WARNING', data);
					done();
				}
			}, (err) => {
				expect(err).to.be.ok;
				done();
			});
		});

		it('can register after an invite is sent', function (done) {
			this.userClient.request(templates.sendInvite, {
				emailAddr: 'invited@local',
				message: 'message',
			}).then((d) => {
				if (d && !this.lastInviteId) {
					expect(d).to.be.ok;
					expect(d.sendUserInvite).to.be.ok;
					expect(d.sendUserInvite.id).to.be.ok;
					this.lastInviteId = d.sendUserInvite.id;
				}
				expect(this.lastInviteId).to.be.ok;
				this.noClient.request(templates.registerUser, {
					emailAddr: 'invited@local',
					pass: 'invited@local',
					birth: '2002-02-02',
				}).then((data) => {
					if (!data) {
						console.warn('invite no response');
						done();
					}
					expect(data).to.be.ok;
					expect(data.registerUser).to.be.ok;
					expect(data.registerUser.id).to.be.ok;
					expect(data.registerUser.tokenList).to.be.ok;
					expect(data.registerUser.tokenList[0]).to.be.ok;
					this.newUser = data.registerUser;
					this.newUserId = data.registerUser.id;
					done();
				}, done);
			}, done);
		});

		it('can confirm email', function (done) {
			this.adminClient.request(templates.user, {
				id: this.newUserId,
			}).then((d) => {
				expect(d).to.be.ok;
				expect(d.user).to.be.ok;
				expect(d.user.emailConfirmation).to.be.ok;
				this.emailConfirmation = d.user.emailConfirmation;
				this.noClient.request(templates.confirmEmail, {
					id: this.newUserId,
					emailConfirmation: this.emailConfirmation,
				}).then((data) => {
					expect(data.confirmEmail).to.be.ok;
					expect(data.confirmEmail.id).to.be.ok;
					done();
				}, done);
			}, done);
		});

		it('was confirmed', function (done) {
			this.adminClient.request(templates.user, {
				id: this.newUserId,
			}).then((d) => {
				expect(d).to.be.ok;
				expect(d.user).to.be.ok;
				expect(!d.user.emailConfirmation).to.be.ok;
				done();
			}, done);
		});
	});
}

module.exports = userInvites;
