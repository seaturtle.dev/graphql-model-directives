/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');

function modelBenchmark(endpoint/* , label */) {
	const seedResourcesCollection = [];

	const amount = 20;
	// const amount = 256 - (label === 'MongoDB' ? 150 : 0);
	// const pages = label === 'MongoDB' ? 2 : 3;

	for (let i = 0; i < amount; i += 1) {
		seedResourcesCollection.push(seedResource(i));
	}

	// const desc = (process.env.BENCHMARK || true)
	// 	? describe : xdescribe;

	// TODO:
	xdescribe('Model Benchmark', function () {
		/* eslint no-nested-ternary: 1 */
		// const wait = label === 'MongoDB' ? 60 : label === 'Redis' ? 30 : 15;
		const wait = 0;

		this.timeout((amount + 1) * (wait * 2));

		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					wait,
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		this.timeout(3000);

		it('is ok', function () {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
		});

		it('can query', function (done) {
			this.userClient.request(templates.resources, {
			}).then((result) => {
				expect(result).to.be.ok;
				// console.log('got here p', result.resources);
				done();
			}, done);
		});
	});
}

module.exports = modelBenchmark;
