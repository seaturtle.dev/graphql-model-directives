/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { EventEmitter } = require('events');
const { expect } = require('chai');
const gql = require('graphql-tag');

const templates = require('@seaturtle/gmd-server-example/templates.js');

function modelSubscriptions() {
	describe('Model Subscriptions', () => {
		before(function (done) {
			this.events = new EventEmitter();

			const createObserver = (prefix, query, filter) => {
				const observer = this.apolloClient.subscribe({
					query: gql(query),
					variables: {
						filter,
					},
				});

				const subscription = observer.subscribe({
					next: (data) => {
						// console.log(`${prefix}data`, data);
						this.events.emit(`${prefix}data`, data);
					},
					error: (error) => {
						console.error(`${prefix}error`, error);
						this.events.emit(`${prefix}error`, error);
					},
					complete: () => {
						// console.log(`${prefix}complete`);
						this.events.emit(`${prefix}complete`);
					},
				});

				return {
					observer,
					subscription,
				};
			};

			this.resourceCreated = createObserver(
				'created:',
				templates.subscribeCreatedResources,
				{ description: 'subscriptionTest' },
			);
			this.resourceUpdated = createObserver(
				'updated:',
				templates.subscribeUpdatedResources,
				{ description: 'subscriptionTest' },
			);

			this.createObserver = createObserver;

			done();
		});

		after(function () { // done) {
			/* eslint no-underscore-dangle: 1 */
			this.events.removeAllListeners();
			this.subscriptionClient.close();
		});

		it('is ok', function () {
			expect(this.events).to.be.ok;
			expect(this.apolloClient).to.be.ok;
			expect(this.resourceCreated).to.be.ok;
			expect(this.resourceUpdated).to.be.ok;
			// expect(this.resourceRemoved).to.be.ok;
		});

		xit('can track a created document', function (done) {
			// this.timeout(5000);
			this.events.once('created:data', (doc) => {
				expect(doc).to.be.ok;
				done();
			});
			this.adminClient.request(templates.upsertResource, {
				data: {
					description: 'subscriptionTest',
					isDone: false,
				},
			}).then((data) => {
				this.subscriptionResourceId = data.upsertResource.id;

				expect(data.upsertResource.id).to.be.ok;
				expect(data.upsertResource.description).to.equal('subscriptionTest');
				expect(data.upsertResource.isDone).to.equal(false);
			}, done);
		});

		xit('can track a updated document', function (done) {
			// this.timeout(5000);
			this.events.once('updated:data', (doc) => {
				expect(doc).to.be.ok;
				done();
			});
			this.adminClient.request(templates.upsertResource, {
				data: {
					id: this.subscriptionResourceId,
					// NOTE: redis currently doesn't load the document on upsert
					description: 'subscriptionTest',
					isDone: true,
				},
			}).then((data) => {
				expect(data.upsertResource.id).to.equal(this.subscriptionResourceId);
				expect(data.upsertResource.isDone).to.equal(true);
			}, done);
		});

		xdescribe('with id filter', () => {
			before(function (done) {
				this.resourceRemoved = this.createObserver(
					'deleted:',
					templates.subscribeRemovedResources,
					{ id: this.subscriptionResourceId },
				);
				done();
			});

			it('can track a removed document', function (done) {
				// this.timeout(5000);
				this.events.once('deleted:data', (doc) => {
					expect(doc).to.be.ok;
					done();
				});
				this.adminClient.request(templates.removeResource, {
					id: this.subscriptionResourceId,
				}).then((data) => {
					expect(data.removeResource.id).to.equal(this.subscriptionResourceId);
				}, done);
			});
		});
	});
}

module.exports = modelSubscriptions;
