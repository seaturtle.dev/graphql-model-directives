/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');

function modelValues(endpoint) {
	const resource = {
		name: 'a',
		description: 'description',
		// file: '',
		// fileUrl: '',
		// finished: -1,
		date: new Date(),
		boolean: true,
		float: 1.00,
		integer: 100,
		json: {
			array: [0, 1, 2, 3, 4],
			object: {
				a: 'a',
				b: 'b',
				c: 'c',
				d: 'd',
			},
			number: 1.0,
			string: 'string',
			boolean: false,
			// date: new Date(),
		},
		string: 'string',
		isDone: false,
		isPublic: true,
	};

	const seedResourcesCollection = [
		seedResource(0, resource),
	];

	describe('Model Values', () => {
		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		it('is ok', function (done) {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
			this.userClient.request(templates.resources, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
				},
				filter: null,
			}).then((data) => {
				this.serverResources = data.resources.page;
				[this.firstServerResource] = this.serverResources;
				this.firstServerResourceId = this.firstServerResource.id;
				done();
			}, done);
		});

		it('can store a boolean', function () {
			expect(this.firstServerResource.boolean).to.equal(resource.boolean);
		});

		it('can store a float', function () {
			expect(this.firstServerResource.float).to.equal(resource.float);
		});

		it('can store a integer', function () {
			expect(this.firstServerResource.integer).to.equal(resource.integer);
		});

		it('can store a json', function () {
			expect(this.firstServerResource.json).to.eql(resource.json);
		});

		it('can store a string', function () {
			expect(this.firstServerResource.string).to.equal(resource.string);
		});
	});
}

module.exports = modelValues;
