/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');

function modelIndexes(endpoint) {
	const seedResourcesCollection = [
		seedResource(20, { name: 'j' }),
		seedResource(18, { name: 'i' }),
		seedResource(16, { name: 'h' }),
		seedResource(12, { name: 'g' }),
		seedResource(10, { name: 'f' }),

		seedResource(8, { name: 'e' }),
		seedResource(6, { name: 'd' }),
		seedResource(4, { name: 'c' }),
		seedResource(2, { name: 'b' }),
		seedResource(0, { name: 'a' }),
	];

	describe('Model Indexes', () => {
		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		it('is ok', function () {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
		});

		function checkResourceResults(result, start, end) {
			expect(result).to.be.ok;
			expect(result.resources).to.be.ok;
			expect(result.resources.page).to.be.ok;
			// console.log(result.resources.page);
			expect(result.resources.page.length).to.equal(5);
			expect(result.resources.page[0].name).to.equal(start);
			expect(result.resources.page[4].name).to.equal(end);
		}

		it('can query a page of 5 by updated (asc)', function (done) {
			this.userClient.request(templates.resources, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
					ascending: true,
				},
				filter: null,
			}).then((result) => {
				checkResourceResults(result, 'j', 'f');
				done();
			}, done);
		});

		it('can query the next page of 5 by updated (asc)', function (done) {
			this.userClient.request(templates.resources, {
				page: {
					offset: 5,
					count: 5,
				},
				sort: {
					fields: ['updated'],
					ascending: true,
				},
				filter: null,
			}).then((result) => {
				checkResourceResults(result, 'e', 'a');
				done();
			}, done);
		});

		xit('can query a page of 5 by updated (desc)', function (done) {
			this.userClient.request(templates.resources, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
					descending: true,
				},
				filter: null,
			}).then((result) => {
				checkResourceResults(result, 'a', 'e');
				done();
			}, done);
		});

		xit('can query the next page of 5 by updated (asc)', function (done) {
			this.userClient.request(templates.resources, {
				page: {
					offset: 5,
					count: 5,
				},
				sort: {
					fields: ['updated'],
					descending: true,
				},
				filter: null,
			}).then((result) => {
				checkResourceResults(result, 'f', 'j');
				done();
			}, done);
		});
	});
}

module.exports = modelIndexes;
