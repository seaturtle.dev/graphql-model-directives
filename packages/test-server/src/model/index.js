const benchmark = require('./benchmark');
const indexes = require('./indexes');
const nested = require('./nested');
const permissions = require('./permissions');
const relations = require('./relations');
const subscriptions = require('./subscriptions');
const uploads = require('./uploads');
const validations = require('./validations');
const values = require('./values');

function model(endpoint) {
	validations(endpoint);
	values(endpoint);
	indexes(endpoint);
	relations(endpoint);
	permissions(endpoint);
	nested(endpoint);
	subscriptions();
	uploads(endpoint);
	benchmark(endpoint);
}

module.exports = model;
