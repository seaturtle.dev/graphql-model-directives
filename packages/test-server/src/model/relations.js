/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');

function modelRelations(endpoint) {
	const seedResourcesCollection = [
		seedResource(0, {
			name: 'main',
			parentResource: {
				name: 'parent',
			},
			childResources: [
				{ name: 'childA' },
				{ name: 'childB' },
			],
		}),
	];

	describe('Model Relations', () => {
		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		it('is ok', function (done) {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
			this.userClient.request(templates.resourcesWithRelations, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
				},
				filter: null,
			}).then((data) => {
				this.serverResources = data.resources.page;
				[this.firstServerResource] = this.serverResources;
				this.firstServerResourceId = this.firstServerResource.id;
				done();
			}, done);
		});

		xdescribe('Auto Orphans Removal', () => {
			it('is ok', function () {
				expect(true).to.be.ok;
			});
		});

		xdescribe('Auto Upsert Resolvers', () => {
			it('is ok', function () {
				expect(true).to.be.ok;
			});
		});
	});
}

module.exports = modelRelations;
