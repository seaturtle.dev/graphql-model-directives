/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');

function modelNested(endpoint) {
	const seedResourcesCollection = [
		seedResource(0, {
			name: 'root',
			nestedResources: [
				{
					id: '0',
					name: 'nestedA',
					nestedResources: [
						{ id: '0', name: 'nestedNested' },
					],
				},
				{ id: '1', name: 'nestedB' },
			],
		}),
	];

	describe('Model Nested', () => {
		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		it('is ok', function (done) {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
			this.userClient.request(templates.resourcesWithNested, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
				},
				filter: null,
			}).then((data) => {
				// console.log(data);
				expect(data).to.be.ok;
				expect(data.resources).to.be.ok;
				expect(data.resources.page).to.be.ok;
				this.serverResources = data.resources.page;
				[this.firstServerResource] = this.serverResources;
				this.firstServerResourceId = this.firstServerResource.id;
				done();
			}, done);
		});

		it('saved nested resources', function () {
			expect(this.firstServerResource.nestedResources).to.be.ok;
		});
	});
}

module.exports = modelNested;
