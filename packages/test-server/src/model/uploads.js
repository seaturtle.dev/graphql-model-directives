/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const gql = require('graphql-tag');
const path = require('path');
const fs = require('fs');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');
const seed = require('../helpers/seed');
const seedResource = require('../helpers/seed/resource');
const unseed = require('../helpers/unseed');
const config = require('../helpers/config');

const imageBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAADcCAYAAAAbWs+BAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAodJREFUeNrs3NFtg0AQRVGPRRNWyoCeUhM9LWWkjUw6iIQyCRvmnAKQF3S1/nqRmQ/gbzy9AhAcCA4QHAgOEBwIDgQHCA4EBwgOBAeCAwQHggMEB4IDBAeCg/tbvAJuJaJ2pCcz3HDgLyUgOBAcCA4QHAgOEBwIDhAcCA4EBwgOBAcIDgQHggMEB4IDTmm3aRKxlW5erOva6v2NsUft96jdIMndDQcIDgQHggMEB4IDBAeCA8EBggPBAYIDwQGCA8GB4ADBgeAAwcHFpt806bZBMvvv27b32g2SzOgUnBsOBAeCAwQHggMEB4IDwQGCA8EBggPBgeAAwYHgAMGB4ADBgeCggcjM2gcWb5BUm30zpNt5j+Mofd4Y+9QbKW44EBwIDhAcCA4QHAgOBAcIDgQHCA4EB4IDBAeCAwQHggMEB4KDBpZuB67e0Oi2GdJtE8YNB4IDBAeCA8EBggPBAYIDwQGCA8GB4ADBgeAAwYHgQHCA4EBwwGmRmbUPjLfaBz5erT6IzZCfGWMPNxwgOBAcCA4QHAgOEBwIDgQHCA4EBwgOBAeC8wpAcCA4QHAgOEBwcKl/sGlS7eWr31jmsGkCCA4EB4IDBAeCAwQHggPBAYIDwQGCA8EBggPBgeAAwYHgAMHBtco3Tcp/4PQbKdV6ba7MvkHihgPBAYIDwYHgAMGB4ADBgeAAwYHgQHCA4EBwgOBAcCA4QHAgOOC06TdNyg8cz+IDd9sg+QjZuOFAcIDgQHAgOEBwIDhAcCA4EBwgOBAcIDgQHCA4EBwIDhAcCA74xtLtwJmfNjlww4HgAMGB4ADBgeBAcIDgQHCA4EBwgOBAcCA4QHAgOEBwIDgQHPB7vgQYAPVcRHV5+lfgAAAAAElFTkSuQmCC';

async function getImageFile() {
	const extractFiles = await import('extract-files/extractFiles.mjs');
	const { ReactNativeFile } = extractFiles;

	const imageFile = new ReactNativeFile({
		uri: imageBase64,
		name: 'image.png',
		type: 'image/png',
	});

	// HACK: this is needed to make it work;
	imageFile.mimetype = imageFile.type;
	imageFile.filename = imageFile.name;

	return imageFile;
}


function fileUploads(endpoint) {
	const seedResourcesCollection = [
		seedResource(0, { name: 'a' }),
	];

	// const desc = (process.env.NO_UPLOAD || false)
	// 	? xdescribe : xdescribe; // describe;

	// TODO: fix this since upgrading to latest apollo client
	describe('File Uploads', () => {
		signIn(
			endpoint,
			(done, context) => seed.call(context, done, {
				resources: {
					templateName: 'upsertResource',
					template: templates.upsertResource,
					collection: seedResourcesCollection,
				},
			}),
			(done, context) => unseed.call(context, done, {
				resources: {
					templateName: 'removeResource',
					template: templates.removeResource,
					collection: (ctx) => ctx.resources,
				},
			}),
		);

		it('is ok', function (done) {
			expect(this.resources).to.be.ok;
			expect(this.resources.length).to.equal(seedResourcesCollection.length);
			this.userClient.request(templates.resources, {
				page: {
					offset: 0,
					count: 5,
				},
				sort: {
					fields: ['updated'],
				},
				filter: null,
			}).then((data) => {
				this.serverResources = data.resources.page;
				[this.firstServerResource] = this.serverResources;
				this.firstServerResourceId = this.firstServerResource.id;
				done();
			}, done);
		});

		xit('can upload a file', function (done) {
			// console.log(imageFile);
			getImageFile().then((imageFile) => {
				this.userApolloClient.mutate({
					mutation: gql(templates.upsertResourceFile),
					variables: {
						id: this.serverResources[0].id,
						file: imageFile,
					},
				}).then((result) => {
					// console.log(data);
					expect(result).to.be.ok;
					expect(result.data).to.be.ok;
					expect(result.data.upsertResourceFile).to.be.ok;
					this.actualResourceId = result.data.upsertResourceFile.id;
					done();
				}, done);
			}, done);
		});

		// TODO: fix this expectation
		xit('modified the existing resource by id', function () {
			expect(this.actualResourceId).to.equal(this.serverResources[0].id);
		});

		xit('uploaded the file', function (done) {
			this.userClient.request(templates.resource, {
				id: this.actualResourceId,
			}).then((data) => {
				// console.log('GOT HERE', data);
				expect(data).to.be.ok;
				expect(data.resource).to.be.ok;
				expect(data.resource.file).to.be.ok;
				expect(data.resource.file.url).to.be.ok;
				const filename = data.resource.file.url.substr(1).split('/')[1];
				const filepath = path.join(config.uploaderLocalPath, filename);
				fs.exists(filepath, (exists) => {
					expect(exists).to.be.ok;
					this.existingFilePath = filepath;
					done();
				});
			}, done);
		});

		xit('can remove the file when the resource is deleted', function (done) {
			this.userClient.request(templates.removeResource, {
				id: this.actualResourceId,
			}).then((data) => {
				// console.log(data);
				expect(data).to.be.ok;
				expect(data.removeResource).to.be.ok;
				expect(data.removeResource.id).to.be.ok;
				fs.exists(this.existingFilePath, (exists) => {
					expect(!exists).to.be.ok;
					done();
				});
			}, done);
		});
	});
}

module.exports = fileUploads;
