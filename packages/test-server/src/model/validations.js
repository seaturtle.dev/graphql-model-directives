/* eslint import/no-extraneous-dependencies:0 prefer-arrow-callback:0 */
/* eslint no-unused-expressions: 1 */

const { expect } = require('chai');

const templates = require('@seaturtle/gmd-server-example/templates.js');

const signIn = require('../helpers/signIn');

function modelValidations(endpoint) {
	describe('Model Validations', () => {
		signIn(
			endpoint,
		);

		it('rejects invalid emails', function (done) {
			this.adminClient.request(templates.registerUser, {
				emailAddr: 'bad',
				pass: 'pass',
				birth: '1999-01-01',
			}).then((data) => {
				if (data && data.registerUser && data.registerUser.id) {
					done(new Error('created user with bad email'));
				}
			}, (err) => {
				expect(err).to.be.ok;
				done();
			});
		});

		it('rejects short passwords', function (done) {
			this.adminClient.request(templates.registerUser, {
				emailAddr: 'rejectShortPass@local',
				pass: 'p',
				birth: '1999-01-01',
			}).then((data) => {
				if (data && data.registerUser && data.registerUser.id) {
					done(new Error('created user with short password'));
				}
			}, (err) => {
				expect(err).to.be.ok;
				done();
			});
		});

		it('rejects long passwords', function (done) {
			this.adminClient.request(templates.registerUser, {
				emailAddr: 'rejectLongPass@local',
				pass: 'p1234567890123456789012345678901234,p1234567890123456789012345678901234',
				birth: '1999-01-01',
			}).then((data) => {
				if (data && data.registerUser && data.registerUser.id) {
					done(new Error('created user with long password'));
				}
			}, (err) => {
				expect(err).to.be.ok;
				done();
			});
		});

		it('rejects birthdates before 13 years ago', function (done) {
			this.adminClient.request(templates.registerUser, {
				emailAddr: 'rejectYoung@local',
				pass: 'pass',
				birth: `${new Date().getUTCFullYear() - 12}-01-01`,
			}).then((data) => {
				if (data && data.registerUser && data.registerUser.id) {
					done(new Error('created user with bad birthdate'));
				}
			}, (err) => {
				expect(err).to.be.ok;
				done();
			});
		});
	});
}

module.exports = modelValidations;
