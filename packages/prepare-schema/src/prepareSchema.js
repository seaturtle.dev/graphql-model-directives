const { makeExecutableSchema } = require('@graphql-tools/schema');
const { mergeTypeDefs, mergeResolvers } = require('@graphql-tools/merge');
const { print } = require('graphql');

// const gql = require('@seaturtle/gmd-core/gql.js');

const { SchemaDirectiveVisitor } = require('@seaturtle/gmd-core-directives/SchemaDirectiveVisitor.js');

const gqlTypes = require('@seaturtle/gmd-core/gqlTypes.js');

const directives = require('@seaturtle/gmd-core-directives/directives.js');

const SafeDataStore = require('@seaturtle/gmd-adapter-data/StoreAdapter.js');

const customTypesAndInputs = `
scalar Date
scalar DateTime
scalar Upload
scalar Time
scalar JSON
scalar Upload

interface ScannedPage {
	id: ID
	cursor: String
	count: Int
	skipped: Int
}

interface SortedPage {
	id: ID
	offset: Int
	count: Int
	skipped: Int
}

type File {
	filename: String
	mimetype: String
	encoding: String
	url: String
}

type JSONScannedPage implements ScannedPage @page @scanned {
	id: ID
	cursor: String
	count: Int
	skipped: Int
	page: JSON
}

type JSONSortedPage implements SortedPage @page @sorted {
	id: ID
	offset: Int
	count: Int
	skipped: Int
	page: JSON
}

input ScannedPageInput {
	cursor: String
	count: Int
}

input SortedPageInput {
	offset: Int
	count: Int
}

input SortInput {
	fields: [String]
	ascending: Boolean
	descending: Boolean
}
`;

/**
 * Simplifies getting a schema ready to run as a server from raw GQL.
 * @exports prepareSchema
 * @function
 * @param {Object=} options
 * @param {StoreAdapter} options.store
 * @param {string} options.rawSchema
 * @param {Object} [options.resolvers]
 * @param {Object} [options.schemaDirectives]
 * @param {boolean=true} options.allowUndefinedInResolve
 * @param {Object[]} [options.directiveTypeDefs]
 * @returns {Object} */
module.exports = async function prepareSchema(options = {}) {
	const {
		GraphQLDate,
		GraphQLTime,
		GraphQLDateTime,
		GraphQLJSON,
		GraphQLUpload,
	} = await gqlTypes;

	let directiveTypeDefs = directives.list.map((Directive) => Directive.typeDef);

	if (options.directiveTypeDefs) {
		directiveTypeDefs = directiveTypeDefs.concat(options.directiveTypeDefs);
	}

	const injectedRawSchema = directiveTypeDefs.concat([
		customTypesAndInputs,
	]).join('\n');

	const rawSchema = ([
		injectedRawSchema,
		options && options.rawSchema,
	]).join('\n');

	const store = options.store || new SafeDataStore();

	const resolvers = {
		Date: GraphQLDate,
		DateTime: GraphQLDateTime,
		// FileUpload: GraphQLUpload,
		JSON: GraphQLJSON,
		Time: GraphQLTime,
		Upload: GraphQLUpload,
		...options.resolvers || {},
	};

	const { config } = options;
	delete options.config;

	const customDirectives = directives.map(store, config);

	const schemaDirectives = Object.assign(
		customDirectives,
		options.schemaDirectives,
	);

	let autoResolvers = [];
	let autoTypeDefs = [];

	// let autoResolvers = null;
	// let autoTypeDefs = null;

	const state = {};
	if (typeof window !== 'undefined') {
		window.state = state; // ?
	}

	function recursiveSchemaGeneration() {
		const params = {
			...options,
			allowUndefinedInResolve: options.allowUndefinedInResolve !== false,
			resolvers,
			// schemaDirectives,
			typeDefs: null,
		};

		try {
			let mergedResolvers = {
				...resolvers,
			};
			let mergedTypeDefs = rawSchema;

			if (autoResolvers && autoResolvers.length) {
				mergedResolvers = mergeResolvers(
					[resolvers].concat(autoResolvers),
					// [mergedResolvers, autoResolvers],
					{
						// all: true
					},
				);
			}

			if (autoTypeDefs && autoTypeDefs.length) {
				// debugger;
				// console.log('BEFORE');
				// mergedTypeDefs = autoTypeDefs;
				mergedTypeDefs = mergeTypeDefs(
					// [mergedTypeDefs, autoTypeDefs],
					[rawSchema].concat(autoTypeDefs),
					{
						// all: true,
						sort: true,
						// throwOnConflict: false,
						// reverseDirectives: true,
						// ignoreFieldConflicts: true,
						// useSchemaDefinition: true,
						// forceSchemaDefinition: true,
					},
				);
			}

			// const typeDefs = mergedTypeDefs;
			// let typeDefs = null;
			// try {
			// 	typeDefs = gql`${mergedTypeDefs}`;
			// } catch (err) {
			// 	const injectedLines = injectedRawSchema.match(/\n/g).length + 1;
			// 	err.locations.forEach((location) => {
			// 		location.line += -injectedLines;
			// 	});
			// 	throw err;
			// }

			const typeDefs = typeof mergedTypeDefs === 'string' ? mergedTypeDefs : print(mergedTypeDefs);
			// debugger;

			// params.typeDefs = mergedTypeDefs;
			params.typeDefs = typeDefs;
			params.resolvers = mergedResolvers;

			// let state = {};
			let schema = makeExecutableSchema(params);

			Object.keys(schemaDirectives).forEach((name) => {
				const schemaDirectiveClass = schemaDirectives[name];
				const transformer = SchemaDirectiveVisitor.createTransformer(
					name,
					schemaDirectiveClass
				);
				// console.log('GOT HERE', name);
				schema = transformer(schema, state);
			})

			// debugger;
			if (state.pendingSchemaInjection) {
				state.pendingSchemaInjection();
			}

			const output = {
				config,
				directiveTypeDefs,
				params,
				rawSchema,
				resolvers: mergedResolvers,
				schema,
				store,
				typeDefs, //: mergedTypeDefs,
			};

			// console.log('CONFIGURE', config);
			store.configure(config);
			store.postSchema(output);

			// console.log('FINAL SCHEMA', rawSchema);

			return output;
		} catch (err) {
			// debugger;
			// console.log("ERR", err);
			if (err.schemaInjection) {
				// console.log('GOT HERE');
				if (err.schemaInjection.resolvers) {
					autoResolvers.push(err.schemaInjection.resolvers);
				}
				if (err.schemaInjection.typeDefs) {
					autoTypeDefs.push(err.schemaInjection.typeDefs);
				}
				return recursiveSchemaGeneration();
			}
			// console.log('-- BEGIN RAW SCHEMA SRC --');
			// console.log(rawSchema);
			// console.log('-- END RAW SCHEMA SRC --')
			// const rawSchemaLines = rawSchema.split('\n');
			// if (err && err.locations) {
			// 	console.log('-- BEGIN ERROR LOCATION BEGIN --');
			// 	err.locations.forEach((errLocation) => {
			// 		const line = errLocation.line;
			// 		const begin = line - 3
			// 		const end = line + 2
			// 		const column = errLocation.column;
			// 		const errorLines = rawSchemaLines.slice(begin, end);
			// 		console.log('AT LINE:', line, ', COLUMN:', column, ':');
			// 		console.log(errorLines.join('\n'));
			// 	});
			// 	console.log('-- END ERROR LOCATION REPORT --');
			// }
			// console.log('-- BEGIN ERROR --');
			// console.error(err);
			// console.log('-- END ERROR --');
			throw err;
		}
	}

	return recursiveSchemaGeneration();
};
