/* eslint global-require:0 */

// const { EventEmitter } = require('events');

let Dexie = null;
try {
	Dexie = import('dexie')
	Dexie.catch(() => {});
} catch (err) {
	// console.warn('dexie not installed', err);
}

const model = require('./model');
const user = require('./user');

const StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');
const DexieCollection = require('./Collection');

/**
* Stores document collections in a very simple array<object> structure.
* @exports DexieStore
* @class
* @extends StoreAdapter */
class DexieStore extends StoreAdapter {
	/**
	 * [throwMongooseUnavailable description]
	 * @return {[type]} [description]
	 */
	throwDexieUnavailable() {
		/* istanbul ignore next */
		throw new Error('NPM install dexie, dexie is not available.');
	}

	/**
	 * [constructor description]
	 * @param {[type]} mongoUrl [description]
	 * @param {[type]} options  [description]
	 * @constructs
	 */
	constructor({
		// getSchema, // get { ...schema }
		// setSchema, // set { ...prevSchema, ...newSchema }
		// getData, // get { ...data }
		// setData, // set { ...prevDexie, ...newDexie }
		resolvers, // object
		pubsub, // like an event emitter
		db,
		// ...clientMixin
	}, crypto) {
		super(resolvers);
		this.crypto = crypto || this.crypto;;
		this.pubsub = pubsub || this.pubsub;
		// this.client = {
		// 	...clientMixin,
		// 	getData,
		// 	setData,
		// 	getSchema,
		// 	setSchema,
		// };
		this.db = db || new Promise((resolve, reject) => {
			Dexie.then((dexieModule) => {
				db = new dexieModule.Dexie('DB');
				this.db = db;
				this.db.version(1);
				resolve(db);
			}, reject);
		});
	}

	/**
	 * [postSchema description]
	 * @return {[type]} [description]
	 */
	postSchema() {
		/* istanbul ignore next */
		if (!Dexie) { this.throwDexieUnavailable(); }

		const collectionNames = Object.keys(this.models);

		collectionNames.forEach((collectionName) => {
			const collection = this.models[collectionName];
			const finishToDexie = () => collection.toDexie(this.db);
			if (this.db.then) {
				this.db.then(finishToDexie)
			} else {
				finishToDexie();
			}
		});

		const finishOpen = () => this.db.open();
		// console.log('GOT HERE');
		if (this.db.then) {
			this.db.then(finishOpen)
		} else {
			finishOpen();
		}
		// console.log('AND HERE');
	}

	/**
	 * [mixinUser description]
	 * @param  {[type]} collection [description]
	 * @return {[type]}            [description]
	 */
	mixinUser(collection) {
		return super.mixinUser(collection, user, model);
	}

	/**
	 * [authenticate description]
	 * @param  {[type]}   context [description]
	 * @param  {[type]}   info    [description]
	 * @param  {Function} next    [description]
	 * @param  {[type]}   roles   required user roles for to authenticate
	 * @return {[type]}           [description]
	 */
	authenticate(context, info, next, or, roles = []) {
		return new Promise((resolve, reject) => {
			const onAuthError = (err) => {
				if (or === true) { return resolve(next(err)); }
				if (typeof or === 'function') { return resolve(or(err, next)); }
				return reject(err);
			};
			return user.isAuthenticated(roles, context, info).then(() => (
				Promise.resolve(next()).then(resolve, reject)
			), onAuthError);
		});
	}

	/**
	 * [readDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	readDocument({ name }, { target, options }, { context, info }) {
		return model.read(this, name, target, options, context, info);
	}

	/**
	 * [findDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	findDocuments({ name }, { target, options }, { context, info }) {
		return model.find(this, name, target, options, context, info);
	}

	/**
	 * [countDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	countDocuments({ name }, { target, options }, { context, info }) {
		return model.count(this, name, target, options, context, info);
	}

	/**
	 * [scanDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	scanDocuments({ name }, { target, options }, { context, info }) {
		return model.scan(this, name, target, options, context, info);
	}

	/**
	 * [purgeDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	purgeDocuments({ name }, { target, options }, { context, info }) {
		return model.purge(this, name, target, options, context, info);
	}

	/**
	 * [upsertDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	upsertDocument({ name }, { target, options }, { context, info }) {
		return model.upsert(this, name, target, options, context, info);
	}

	/**
	 * [removeDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	removeDocument({ name }, { target, options }, { context, info }) {
		return model.remove(this, name, target, options, context, info);
	}
}

DexieStore.Collection = DexieCollection;

module.exports = DexieStore;
