/* eslint no-unused-vars:1 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

// TODO: create id map in memory and sort by indexes, use binary search for filtering

/**
* Dexie Model Implementation
* @exports dexie_model */
const dexie_model = exports;

function sortFilterPage(table, filter = {}, options = {}) {
	const fields = filter ? Object.keys(filter) : [];

	let collection = table;

	const { sort } = options;

	if (sort) {
		collection = collection.orderBy(sort.fields[0]);

		if (sort.descending) {
			collection = collection.reverse();
		}
	}

	fields.forEach((field, index) => {
		if (index !== 0) {
			collection = collection.and();
		}

		collection = collection
			.where(field)
			.equals(filter[field]);
	});

	const offset = parseInt(options.offset || 0, 10);
	const cursor = parseInt(options.cursor || 0, 10);
	const count = parseInt(options.count || 100, 10);

	collection = collection
		.offset(offset || cursor)
		.limit(count);

	return collection;
}

exports.sortFilterPage = sortFilterPage;

function count(store, name, target, options, context, info) {
	function countPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		return new Promise((resolve, reject) => (
			sortFilterPage(model, target, options)
				.count()
				.then(resolve, reject)
		));
	}

	return countPromise();
}

dexie_model.count = count;

function find(store, name, target, options, context, info) {
	function findPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const offset = parseInt(options.offset || 0, 10);
		const count = parseInt(options.count || 100, 10);

		// target = collection.mapQueryOperators(target);

		return new Promise((resolve, reject) => (
			sortFilterPage(model, target, options)
				.toArray().then((results) => (
					collection.resolvePagedRelations(resolve, reject, {
						offset,
						count,
						page: collection.filterSecureDataForUser(results, context, target)
							.map((record) => {
								let result = record.dataValues;
								result = collection.convertToGraphQL(result);
								result = collection.redactSecureDataForUser(result, context);

								return result;
							}),
					}, context, info)
				), reject)
		));
	}

	return findPromise();
}

dexie_model.find = find;

function nextPage(store, name, target, options, context, info) {
	function nextPagePromise() {
		const collection = store.collection(name);
		const { model } = collection;

		return new Promise((resolve, reject) => (
			sortFilterPage(model, target, options)
				.count()
				.then((count) => resolve(count > 0), reject)
		));
	}

	return nextPagePromise();
}

dexie_model.find.nextPage = nextPage;

function read(store, name, target, options, context, info) {
	function readPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		return new Promise((resolve, reject) => {
			sortFilterPage(model, target, {
				sort: options.sort,
				offset: options.offset || 0,
				count: 1,
			}).toArray().then((records) => {
				const [record] = records;
				let result = record ? record.dataValues : null;

				if (!collection.hasPermission(result, context)) {
					return resolve(null);
				}

				result = collection.convertToGraphQL(result);
				result = collection.redactSecureDataForUser(result, context);

				return collection.resolveDocumentRelations(resolve, reject, result, context, info);
			}, reject);
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, readPromise);
	// }

	return readPromise();
}

dexie_model.read = read;

function remove(store, name, target, options, context, info) {
	function removePromise() {
		return new Promise((resolve, reject) => {
			const collection = store.collection(name);
			const { idProperty, model } = collection;

			// TODO: delete related documents, with a new flag on relation fields "autoDelete"

			return collection.prepareForDestruction(target, context, info).then(() => (
				sortFilterPage(model, target, {
					sort: options.sort,
					offset: options.offset || 0,
					count: 1,
				}).delete().then((/* dCount */) => {
					const doc = { [idProperty]: target[idProperty] };
					collection.emit('removed', doc);
					return resolve(
						doc,
						// dCount > 0
						// 	? doc
						// 	: null,
					);
				}, reject)
			));
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, removePromise);
	// }

	return removePromise();
}

dexie_model.remove = remove;

function scan(store, name, target, options, context, info) {
	function scanPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		return new Promise((resolve, reject) => (
			sortFilterPage(model, target, options)
				.toArray()
				.then((results) => (
					collection.resolvePagedRelations(resolve, reject, {
						cursor: count === results && results.length ? cursor + count : 0,
						count,
						page: collection.filterSecureDataForUser(results, context, target)
							.map((record) => {
								let result = record.dataValues;

								result = collection.convertToGraphQL(result);
								result = collection.redactSecureDataForUser(result, context);

								return result;
							}),
					}, context, info)
				), reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, scanPromise);
	// }

	return scanPromise();
}

dexie_model.scan = scan;

function purge(store, name, target, options, context, info) {
	function purgePromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		// TODO: emit removed events

		return new Promise((resolve, reject) => (
			sortFilterPage(model, target, options).delete().then((results) => {
				resolve(results);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, purgePromise);
	// }

	return purgePromise();
}

dexie_model.purge = purge;

function upsert(store, name, target, options, context, info) {
	const collection = store.collection(name);
	const { model } = collection;

	const { id, document, create } = collection.convertToStore(context, target);

	const safeId = id || collection.generateDocumentId();

	return new Promise((resolve, reject) => {
		const validationErrors = collection.validateDocument(document);

		if (validationErrors) {
			return reject(validationErrors);
		}

		const [relatedPromiseTriggers, relatedError] = (
			collection.gatherRelatedPromiseTriggers(document, context, info)
		);

		if (relatedError) {
			return reject(relatedError);
		}

		return collection.prepareForStorage(document).then((doc) => (
			model.put(
				doc,
				safeId,
			).then(() => (
				model.where(collection.idProperty).equals(safeId).toArray().then((records) => {
					const [record] = records;
					let finalDoc = record ? record.dataValues : null;

					const resolveDoc = (finalDoc) => {
						if (finalDoc) {
							finalDoc = collection.convertToGraphQL(finalDoc);
							finalDoc = collection.redactSecureDataForUser(finalDoc, context);
						}

						collection.emit(safeId === target[collection.idProperty] ? 'updated' : 'created', finalDoc);

						return resolve(finalDoc);
					};

					if (relatedPromiseTriggers.length) {
						const relatedPromises = relatedPromiseTriggers.map((trigger) => trigger(finalDoc));
						return Promise.all(relatedPromises).then(() => resolveDoc(finalDoc), reject);
					}

					return resolveDoc(finalDoc);
				}, reject)
			), reject)
		), reject);
	});
}

dexie_model.upsert = upsert;
