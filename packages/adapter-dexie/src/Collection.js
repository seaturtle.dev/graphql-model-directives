/* eslint global-require:0 */

const { nanoid } = require('@seaturtle/gmd-core/id.js');

const Collection = require('@seaturtle/gmd-adapter-generic/Collection.js');

/**
* Dexie Collection
* @exports DexieCollection
* @class
* @extends Collection */
class DexieCollection extends Collection {
	/**
	 * [toDexie description]
	 * @param  {[type]} dexie [description]
	 * @return {[type]}          [description]
	 */
	toDexie(db) {
		const { name } = this;

		this.generateDocumentId = () => nanoid();

		const multiEntryPrefix = '*';
		const uniquePrefix = '&';

		// const dexieTypeMap = {
		// 	Array: '',
		// 	Boolean: '',
		// 	Buffer: '',
		// 	Date: '',
		// 	DateTime: '',
		// 	Float: '',
		// 	ID: '',
		// 	Int: '',
		// 	JSON: '',
		// 	Object: '',
		// 	ObjectId: '',
		// 	String: '',
		// };

		const indexes = [];

		this.fields.forEach((field/* , index */) => {
			const property = this.properties[field];

			if (this.virtuals.indexOf(field) !== -1) {
				return;
			}

			const fieldType = (
				property.modelType
				|| property.field.type.toString().replace(/!/g, '')
			);

			// let type = fieldType;
			let prefix = '';

			if (fieldType.charAt(0) === '[') {
				// type = fieldType.substring(1, fieldType.length - 1);
				prefix = multiEntryPrefix;
			}

			if (property.uniques) {
				property.uniques.forEach((unique) => {
					if (!unique.compounds || !unique.compounds.length) {
						prefix = uniquePrefix;
					} else {
						// TODO:
					}
				});
			}

			// if (property.sortedIndexes) {
			// 	property.sortedIndexes.forEach((sortedIndex) => {
			// 		if (!sortedIndexe.compounds || !sortedIndex.compounds.length) {
			// 			schemaField.index = true;
			// 			schemaField.sparse = true;
			// 		}
			// 	});
			// }

			indexes.push(`${prefix}${field}`);
		});

		db.version(1).stores({
			[name]: indexes.join(', '),
		});

		const model = db[name];

		this.model = model;

		return model;
	}

	/**
	 * [mapQueryOperators description]
	 * @method
	 * @param  {[type]}  target               [description]
	 * @return {[type]}                       [description]
	 */
	mapQueryOperators(target) {
		return target;
	}
}

module.exports = DexieCollection;
