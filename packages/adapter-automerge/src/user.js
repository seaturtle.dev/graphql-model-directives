/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

const { nanoid } = require('@seaturtle/gmd-core/id.js');

const {
	getJWTOptions,
	extractFromToken,
	encodeTokenFromLogin,
	setupIsAuthenticated,
} = require('@seaturtle/gmd-plugin-authorize/authorize.js');

// Example of a generate user implementation that uses the model methods,
// instead of direct db calls.
const model = require('./model');

/**
* Automerge User Implementation
	* @exports automerge_user */
const automerge_user = exports;

function createHelper(
	context,
	config,
	{
		birthdate,
		login,
		email,
		emailConfirm,
		isSuper,
		now,
		password,
		userId,
	},
	cb,
) {
	const { store } = context;
	const { crypto, userCollection } = store;

	const { idProperty, name } = userCollection;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const birthdateName = userCollection.userBirthdate;
	const passwordName = userCollection.userPassword;
	const passwordResetName = userCollection.userPasswordReset;
	const rolesName = userCollection.userRoles;
	const emailConfirmName = userCollection.userEmailConfirm;
	const tokensName = userCollection.userTokens;
	const maxInvitesName = userCollection.userMaxInvites;
	const createdAtName = userCollection.createdAt;
	const updatedAtName = userCollection.updatedAt;

	return crypto.hash(password, 10, (hashErr, hash) => {
		if (hashErr) {
			return cb(hashErr);
		}

		const record = {
			[idProperty]: userId.toString(),
			[birthdateName]: birthdate,
			[emailName]: email,
			[emailConfirmName]: emailConfirm,
			[rolesName]: isSuper ? [config.superRole] : [],
			[updatedAtName]: now,
			[createdAtName]: now,
			[tokensName]: {},
			[maxInvitesName]: config.defaultMaxInvites,
			[passwordName]: password,
			[passwordResetName]: '',
		};

		if (loginName !== emailName) {
			record[loginName] = login;
		}

		const validationErrors = userCollection.validateDocument(record);

		if (validationErrors) {
			return cb(validationErrors);
		}

		record[passwordName] = hash;

		return model.upsert(store, name, record, {}, context, {}).then(
			(user) => cb(null, user),
			cb,
		);
	});
}

function forgotPassword(context, email) {
	const { userCollection } = context.store;

	const emailName = userCollection.userEmail;
	const passwordResetName = userCollection.userPasswordReset;

	return new Promise((resolve, reject) => (
		model.find(
			context.store,
			userCollection.name,
			{ [emailName]: email },
			{ count: 1 },
			{ super: true },
			{},
		).then((results) => {
			const user = results && results.page[0];

			const userId = user[userCollection.idProperty];

			if (userId) {
				const passwordReset = nanoid();

				const result = { [emailName]: email, [userCollection.idProperty]: userId };

				return model.upsert(
					context.store,
					userCollection.name,
					{
						[userCollection.idProperty]: userId,
						[passwordResetName]: passwordReset,
					},
					{},
					{ super: true },
					{},
				).then((upsertResult) => {
					if (context.store.mailer) {
						return context.store.mailer
							.sendForgotPassword(context.config, email, `${userId}/${passwordReset}`)
							.then(() => resolve(result), reject);
					}

					return resolve(result);
				}, reject);
			}

			return resolve({ [emailName]: email, [userCollection.idProperty]: '' });
		}, reject)
	));
}

automerge_user.forgotPassword = forgotPassword;

function resetPassword(
	context,
	email,
	recoveryToken,
	newPassword,
) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty, name, userEmail } = userCollection;

		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;
		const passwordResetName = userCollection.userPasswordReset;

		return model.find(store, name, {
			[userEmail]: email,
		}, {
			count: 1,
		}, context, {}).then((users) => {
			const user = users && users.page[0];
			const userId = user[idProperty];

			if (!userId) {
				return reject(new Error('Invalid credentials'));
			}

			const passwordReset = user[passwordResetName];

			if (recoveryToken === passwordReset) {
				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(hashErr);
					}
					return model.upsert(store, name, {
						[idProperty]: userId,
						[passwordResetName]: '',
						[passwordName]: hash,
					}, {}, context, {}).then((resetUser) => {
						const finish = (encodeTokenErr, token) => {
							if (encodeTokenErr) {
								return reject(encodeTokenErr);
							}

							return model.upsert(store, name, {
								[idProperty]: userId,
								[tokensName]: {
									...((resetUser && resetUser[tokensName]) || {}),
									[deviceId]: token,
								},
							}, {}, context, {}).then((tokenWrapper) => {
								const tokens = tokenWrapper[tokensName];
								const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

								return resolve({
									[idProperty]: userId.toString(),
									[tokensName]: tokenList,
									[tokenName]: token,
								});
							}, reject);
						};
						return automerge_user.encodeTokenFromLogin(email, context.config, finish);
					}, reject);
				});
			}

			return reject(new Error('Invalid credentials'));
		}, reject);
	});
}

automerge_user.resetPassword = resetPassword;

function updatePassword(
	context,
	userId,
	oldPassword,
	newPassword,
) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty, name } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;

		if (!userId) {
			return reject(new Error('Invalid user'));
		}

		return model.read(
			store,
			name,
			{ [idProperty]: userId },
			{},
			context,
			{},
		).then((user) => {
			const password = user[passwordName];
			const login = user[loginName];

			return crypto.compare(oldPassword, password, (authErr, authed) => {
				if (authErr) {
					return reject(authErr);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(hashErr);
					}

					return model.upsert(
						store,
						name,
						{
							[idProperty]: userId,
							[passwordName]: hash,
						},
						{},
						context,
						{},
					).then((lastUser) => (
						automerge_user.encodeTokenFromLogin(
							login,
							context.config,
							(encodeTokenErr, token) => {
								if (encodeTokenErr) {
									return reject(encodeTokenErr);
								}

								return model.upsert(
									store,
									name,
									{
										[idProperty]: userId,
										[tokensName]: {
											...((lastUser && lastUser[tokensName]) || {}),
											[deviceId]: token,
										},
									},
									{},
									context,
									{},
								).then((tokenWrapper) => {
									const tokens = tokenWrapper[tokensName];
									const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

									return resolve({
										[idProperty]: userId.toString(),
										[tokensName]: tokenList,
										[tokenName]: token,
									});
								}, reject);
							},
						)
					));
				});
			});
		}, reject);
	});
}

automerge_user.updatePassword = updatePassword;

function revokeInvite(context, id) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;
		const { idProperty, name } = userInviteCollection;

		return model.remove(
			store,
			name,
			{ [idProperty]: id },
			{},
			context,
			{},
		).then(
			() => resolve({ [idProperty]: id.toString() }),
			reject,
		);
	});
}

automerge_user.revokeInvite = revokeInvite;

function sendInvite(context, authorId, email, message) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;

		// debugger;
		const authorName = userInviteCollection.belongsTo[0];
		const emailName = userInviteCollection.userEmail;

		const objId = userInviteCollection.generateDocumentId();

		const { idProperty, name } = userInviteCollection;

		return model.upsert(
			store,
			name,
			{
				[idProperty]: objId,
				[authorName]: authorId,
				[emailName]: email,
			},
			{},
			context,
			{},
		).then((result) => {
			if (context.store.mailer) {
				return context.store.mailer
					.sendInviteEmail(context.config, email, message, `${objId.toString()}`)
					.then(() => resolve(result), reject);
			}

			return resolve(result);
		}, reject);
	});
}

automerge_user.sendInvite = sendInvite;

function signIn(context, login, password) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty, name } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;

		return model.find(
			store,
			name,
			{ [loginName]: login },
			{
				count: 1,
			},
			context,
			{},
		).then((users) => {
			const user = users && users.page[0];

			if (!user) {
				return reject(new Error('Invalid credentials'));
			}

			const userId = user[idProperty];

			if (!userId) {
				return reject(new Error('Invalid credentials'));
			}

			const secret = user[passwordName];

			return crypto.compare(password, secret, (authErr, authed) => {
				if (authErr) {
					return reject(authErr);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return encodeTokenFromLogin(login, context.config, (tokenErr, token) => {
					if (tokenErr) {
						return reject(tokenErr);
					}

					return model.upsert(
						store,
						name,
						{
							[idProperty]: userId,
							[tokensName]: {
								...((user && user[tokensName]) || {}),
								[deviceId]: token,
							},
						},
						{},
						context,
						{},
					).then((tokenWrapper) => {
						const tokens = tokenWrapper[tokensName];
						const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

						return resolve({
							[idProperty]: userId.toString(),
							[loginName]: login,
							[tokensName]: tokenList,
							[tokenName]: token,
						});
					}, reject);
				});
			});
		});
	});
}

automerge_user.signIn = signIn;

function signOut(context, id) {
	return new Promise((resolve, reject) => {
		const { deviceId, user, store } = context;
		const { userCollection } = store;
		const { idProperty, name } = userCollection;

		const tokensName = userCollection.userTokens;

		if (!user) {
			return resolve();
		}

		const userId = context.super ? id : context.userId;

		if (context.userId === userId) {
			context.userId = null;
			context.user = null;
		}

		return model.read(
			store,
			name,
			{
				[idProperty]: userId,
			},
			{},
			context,
			{},
		).then((lastUser) => (
			model.upsert(
				store,
				name,
				{
					[idProperty]: userId,
					[tokensName]: {
						...((lastUser && lastUser[tokensName]) || {}),
						[deviceId]: '',
					},
				},
				{},
				context,
				{},
			).then(
				() => resolve({ [idProperty]: userId.toString() }),
				reject,
			)
		), reject);
	});
}

automerge_user.signOut = signOut;

function cleanUserData(rawData/* , userCollection */) {
	const user = {};

	Object.keys(rawData).forEach((property) => {
		let value = rawData[property];

		if (property === 'birthdate' || property === 'created' || property === 'updated') {
			value = new Date(value);
		}

		if (value !== null || value !== undefined) {
			user[property] = value;
		}
	});

	return user;
}

automerge_user.cleanUserData = cleanUserData;

function getByLogin(context, login) {
	const { userCollection } = context.store;

	const { name } = userCollection;
	const loginName = userCollection.userLogin;

	return new Promise((resolve, reject) => (
		model.find(
			context.store,
			name,
			{ [loginName]: login },
			{ count: 1 },
			context,
			{},
		).then((users) => {
			const user = users && users.page[0];

			if (!user) {
				return resolve(null);
			}

			const cleanUser = cleanUserData(user, userCollection);

			return resolve(cleanUser);
		}, reject)
	));
}

automerge_user.getByLogin = getByLogin;

automerge_user.helpers = {
	create: createHelper,
};

function refreshToken(context, login) {
	return new Promise((resolve, reject) => {
		const { deviceId, store, userId } = context;
		const { userCollection } = store;
		const { idProperty, name } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;

		if (!login) {
			return reject(new Error('Invalid session'));
		}

		return encodeTokenFromLogin(login, context.config, (err, token) => {
			if (err) {
				return reject(err);
			}

			return model.read(
				store,
				name,
				{
					[idProperty]: userId,
				},
				context,
				{},
			).then((lastUser) => (
				model.upsert(
					store,
					name,
					{
						[idProperty]: userId,
						[tokensName]: {
							...(lastUser && lastUser[tokensName]),
							[deviceId]: token,
						},
					},
					context,
					{},
				).then((user) => {
					const tokenList = Object.keys(user[tokensName]).map(
						(id) => `${id}:${user[tokensName][id]}`,
					);

					return resolve({
						[idProperty]: userId,
						[loginName]: login,
						[tokensName]: tokenList,
						[tokenName]: token,
					});
				}, reject)
			), reject);
		});
	});
}

automerge_user.refreshToken = refreshToken;

function register(
	context,
	login,
	email,
	password,
	birthdate,
	invitation,
) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userCollection, userInviteCollection } = store;
		const { idProperty, name } = userCollection;

		const loginName = userCollection.userLogin;

		const userId = userCollection.generateDocumentId();

		const isSuper = email === context.config.superLogin;

		const now = new Date();

		const { name: inviteName } = userInviteCollection;

		function saveUser() {
			return model.count(
				store,
				name,
				{ [loginName]: login },
				{},
				context,
				{},
			).then((amount) => {
				if (amount > 0) {
					return reject(new Error('Exists'));
				}

				const emailConfirm = isSuper ? null : nanoid();

				const inviteEmailName = userInviteCollection.userEmail;

				return model.purge(
					store,
					inviteName,
					{ [inviteEmailName]: email },
					{
						// count: Infinity,
					},
					context,
					{},
				).then(() => {
					function createUser() {
						return createHelper(
							context,
							context.config,
							{
								birthdate,
								login,
								email,
								emailConfirm,
								isSuper,
								now,
								password,
								userId,
							},
							(setErr/* , results */) => {
								if (setErr) {
									return reject(setErr);
								}

								context.userId = userId;

								context.user = {
									[idProperty]: userId.toString(),
									[loginName]: login,
								};

								context.super = isSuper || context.super || false;

								const finish = () => (
									refreshToken(context, login).then(resolve, reject)
								);

								userCollection.emit('created', { [idProperty]: userId });

								if (context.store.mailer) {
									return context.store.mailer
										.sendConfirmEmail(context.config, email, `${userId}/${emailConfirm}`)
										.then(finish, reject);
								}

								return finish();
							},
						);
					}

					if (invitation) {
						model.remove(
							store,
							inviteName,
							{ [userInviteCollection.idProperty]: invitation },
							{},
							context,
							{},
						).then(() => createUser(), reject);
					}

					return createUser();
				}, reject);
			});
		}

		if (context.config.inviteOnly && (!isSuper && !context.super)) {
			return model.count(
				store,
				inviteName,
				invitation
					? { [userInviteCollection.idProperty]: invitation }
					: { [userInviteCollection.userEmail]: email },
				{},
				context,
				{},
			).then((amount) => {
				if (amount > 0) {
					return saveUser();
				}

				return reject(new Error('Not invited'));
			}, reject);
		}

		return saveUser();
	});
}

automerge_user.register = register;

function confirmEmail(context, id, emailConfirm) {
	const { userCollection } = context.store;
	const { idProperty, name } = userCollection;

	const emailConfirmName = userCollection.userEmailConfirm;

	return new Promise((resolve, reject) => {
		if (!emailConfirm) {
			return reject(new Error('Missing @emailConfirm argument'));
		}

		return model.read(
			context.store,
			name,
			{ [idProperty]: id },
			{},
			{ super: true },
			{},
			true,
		).then((user) => {
			if (!user) {
				return reject(new Error('Invalid confirmation'));
			}

			const actualEmailConfirm = user[emailConfirmName];

			if (
				emailConfirm !== null
				&& actualEmailConfirm !== ''
				&& actualEmailConfirm !== emailConfirm
			) {
				return reject(new Error('Invalid confirmation'));
			}

			return model.upsert(
				context.store,
				name,
				{
					[idProperty]: id,
					[emailConfirmName]: '',
				},
				{},
				context,
				{},
			).then(() => resolve({ [idProperty]: id }), reject);
		});
	});
}

automerge_user.confirmEmail = confirmEmail;

function updateEmail(context, id, newEmail) {
	return new Promise((resolve, reject) => {
		const { userCollection } = context.store;
		const { idProperty, name } = userCollection;

		const emailName = userCollection.userEmail;
		const loginName = userCollection.userLogin;
		const emailConfirmName = userCollection.userEmailConfirm;

		if (!newEmail) {
			return reject(new Error('Missing @email argument'));
		}

		return model.read(
			context.store,
			name,
			{ [idProperty]: id },
			{},
			context,
			{},
		).then((user) => {
			const userId = user[idProperty].toString();

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const { email } = user;

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = loginName === emailName && email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			return model.upsert(
				context.store,
				name,
				{
					[idProperty]: id,
					[emailName]: newEmail,
					[emailConfirmName]: emailConfirm,
				},
				{},
				context,
				{},
			).then(() => {
				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			}, reject);
		});
	});
}

automerge_user.updateEmail = updateEmail;

function updateEmailAndLogin(context, id, newEmail, newLogin) {
	return new Promise((resolve, reject) => {
		newLogin = newLogin || newEmail;

		const { store } = context;
		const { userCollection } = store;
		const { idProperty, name } = userCollection;

		const emailName = userCollection.userEmail;
		const loginName = userCollection.userLogin;
		const emailConfirmName = userCollection.userEmailConfirm;

		if (!newEmail) {
			return reject(new Error('Missing @email argument'));
		}

		if (!newLogin) {
			return reject(new Error('Missing @login argument'));
		}

		return model.read(
			store,
			name,
			{ [idProperty]: id },
			{},
			context,
			{},
		).then((user) => {
			const userId = user[idProperty].toString();

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const { email } = user;

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			const changes = {
				[emailName]: newEmail,
				[emailConfirmName]: emailConfirm,
			};

			if (!loginName !== emailName) {
				changes[loginName] = newLogin;
			}

			return model.upsert(
				store,
				name,
				{
					...changes,
					[idProperty]: id,
				},
				{},
				context,
				{},
			).then(() => {
				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

automerge_user.updateEmailAndLogin = updateEmailAndLogin;

function updateLogin(context, id, newLogin) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userCollection } = store;
		const { idProperty, name } = userCollection;

		const loginName = userCollection.userLogin;
		// const emailName = userCollection.userEmail;
		// if (emailName === loginName) {
		// 	return reject(new Error('Do not use updateLogin when @email and @login are the same.'));
		// }

		if (!newLogin) {
			return reject(new Error('Missing @login argument'));
		}

		return model.read(
			store,
			name,
			{ [idProperty]: id },
			{},
			context,
			{},
		).then((user) => {
			const userId = user[idProperty].toString();

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const login = user[loginName];

			if (login === newLogin) {
				return resolve({ [idProperty]: id });
			}

			return model.upsert(
				store,
				name,
				{
					[idProperty]: id,
					[loginName]: newLogin,
				},
				{},
				context,
				{},
			).then(() => resolve({ [idProperty]: id }), reject);
		});
	});
}

automerge_user.updateLogin = updateLogin;

automerge_user.getJWTOptions = getJWTOptions;

automerge_user.isAuthenticated = setupIsAuthenticated(automerge_user.getByLogin);

automerge_user.extractFromToken = extractFromToken;

automerge_user.encodeTokenFromLogin = encodeTokenFromLogin;
