/* eslint no-unused-vars:1 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

// TODO: create id map in memory and sort by indexes, use binary search for filtering

/**
* Automerge Model Implementation
* @exports automerge_model */
const automerge_model = exports;

const isNull = (item, tk) => {
	if (
		item[tk] === null
		|| item[tk] === undefined
		|| item[tk] === ''
	) {
		return true;
	}
	if (
		typeof item[tk] === 'object'
		&& Object.keys(item[tk]).every(
			(c) => isNull(item[tk], c),
		)
	) {
		return true;
	}
	if (
		item[tk]
		&& item[tk].length === 0
	) {
		return true;
	}
	return false;
};

exports.isNull = isNull;

function count(store, name, target, options, context, info) {
	function countPromise() {
		const collection = store.collection(name);
		const { client } = store;

		return new Promise((resolve, reject) => {
			const data = client.getData() || {};
			let localData = [];
			try {
				localData = data[name] || [];
			} catch (err) {
				return reject(err);
			}
			try {
				const targetKeys = target ? Object.keys(target) : null;
				if (!targetKeys || !targetKeys.length || !localData) {
					return resolve(localData ? localData.length || 0 : 0);
				}
				const filtered = localData.filter((item) => (
					targetKeys.every((tk) => (
						target[tk] === null
							? isNull(item, tk)
							: item[tk] === target[tk]
					))
				));
				return resolve(filtered.length);
			} catch (err) {
				// console.error(err);
				return reject(err);
			}
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, countPromise);
	// }

	return countPromise();
}

/**
* [count description]
* @method
* @param  {[type]} store   [description]
* @param  {[type]} name    [description]
* @param  {[type]} target  [description]
* @param  {[type]} options [description]
* @param  {[type]} context [description]
* @param  {[type]} info    [description]
* @return {[type]}         [description]
*/
automerge_model.count = count;

function find(store, name, target, options, context, info) {
	function findPromise() {
		const collection = store.collection(name);
		const { client } = store;

		const offset = parseInt(options.offset || 0, 10);
		const count = parseInt(options.count || 100, 10);
		const sort = options.sort || {};

		return new Promise((resolve, reject) => {
			const data = client.getData() || {};
			let localData = [];
			try {
				localData = data[name] || [];
			} catch (err) {
				return reject(err);
			}
			const normalize = (results) => {
				if (
					sort
					&& sort.fields
					&& sort.fields.length
				) {
					const {
						fields: sortFields,
						ascending,
						// descending,
					} = sort;
					results.sort((a, b) => {
						const valueA = sortFields.map((f) => a[f]).join(',');
						const valueB = sortFields.map((f) => a[f]).join(',');
						// TODO: check this
						if (ascending) {
							if (valueA > valueB) { return -1; }
							if (valueA < valueB) { return 1; }
							return 0;
						}
						if (valueA < valueB) { return -1; }
						if (valueA > valueB) { return 1; }
						return 0;
					});
				}
				if (!results.slice) {
					throw new Error('Invalid results');
				}
				results = results.slice(offset, Math.min(results.length, offset + count));
				return collection.resolvePagedRelations(resolve, reject, {
					offset,
					count,
					page: collection.filterSecureDataForUser(results, context, target)
						.map((result) => {
							result = {
								...result,
							};
							const id = result[collection.idProperty];
							result = collection.convertToGraphQL(result);
							result = collection.redactSecureDataForUser(result, context);

							// NOTE: currently can't hide ids with @permission using the data store
							// TODO: fix this by disabling for user calls in options.
							result[collection.idProperty] = id;

							return result;
						}),
				}, context, info);
			};
			try {
				// target = collection.mapQueryOperators(target);
				const targetKeys = target ? Object.keys(target) : [];
				if (!targetKeys.length) {
					return normalize(localData);
				}
				return normalize(
					localData.filter((item) => (
						targetKeys.every((tk) => {
							let result = false;
							if (target[tk] === null) {
								result = isNull(item, tk)
							} else {
								result = item[tk] === target[tk];
							}
							return result;
						})
					)),
				);
			} catch (err) {
				// console.error(err);
				return reject(err);
			}
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, findPromise);
	// }

	return findPromise();
}

/**
 * [find description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.find = find;

function nextPage(store, name, target, options, context, info) {
	function nextPagePromise() {
		const count = parseInt(options.count || 100, 10);
		const offset = parseInt((options.offset || 0) + count, 10);

		return new Promise((resolve, reject) => (
			automerge_model.find(store, name, target, {
				...options,
				offset,
				count,
			}, context, info).then(
				(results) => resolve(results && results.length > 0),
				reject,
			)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, nextPagePromise);
	// }

	return nextPagePromise();
}

/**
 * [nextPage description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.find.nextPage = nextPage;

function read(store, name, target, options, context, info, force) {
	function readPromise() {
		const collection = store.collection(name);
		const { client } = store;

		const { model, idProperty } = collection;
		const id = target[idProperty];

		return new Promise((resolve, reject) => {
			const data = client.getData() || {};
			automerge_model.find(store, name, target, {
				...options,
				count: 1,
				// 	data[name] && data[name].length,
				// offset: 0,
			}, context, info).then((results) => {
				let result = results && results.page[0];

				if (!result) {
					return resolve(null);
				}

				if (!collection.hasPermission(result, context)) {
					return resolve(null);
				}

				try {
					result = { ...result };
					// TODO: why do tests pass without this?
					// result = collection.convertToGraphQL(result);

					if (!collection.hasPermission(result, context)) {
						return resolve(null);
					}

					result = collection.redactSecureDataForUser(result, context);

					return collection.resolveDocumentRelations(resolve, reject, result, context, info);
					// return resolve(result);
				} catch (err) {
					return reject(err);
				}
			}, reject);
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, readPromise);
	// }

	return readPromise();
}

/**
 * [read description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.read = read;

function remove(store, name, target, options, context, info) {
	function removePromise() {
		return new Promise((resolve, reject) => {
			const { client } = store;
			const data = client.getData() || {};

			const collection = store.collection(name);
			const { idProperty } = collection;
			const id = target[idProperty];

			try {
				return collection.prepareForDestruction(target, context, info).then(() => {
					let localData = [];

					try {
						localData = data[name] || [];
					} catch (err) {
						return reject(err);
					}

					let index = null;
					let doc = null;

					localData.some((item, i) => {
						// TODO: check other fields
						if (item[idProperty] === target[idProperty]) {
							index = i;
							doc = item;
							return true;
						}
						return false;
					});

					if (index !== null) {
						client.removeData(`${name}.${index}`);
					}

					collection.emit('removed', doc);

					return resolve(doc);
				});
			} catch (err) {
				// console.error(err);
				return reject(err);
			}
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, removePromise);
	// }

	return removePromise();
}

/**
 * [remove description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.remove = remove;

function scan(store, name, target, options, context, info) {
	function scanPromise() {
		// const { client } = store;
		const collection = store.collection(name);

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		return new Promise((resolve, reject) => (
			automerge_model.find(store, name, target, {
				offset: cursor,
				count,
			}, context, info).then((results) => (
				resolve({
					cursor: (results && results.offset) || 0,
					count: (results && results.count) || 0,
					page: (results && results.page) || [],
				})
			), reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, scanPromise);
	// }

	return scanPromise();
}

/**
 * [scan description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.scan = scan;

function purge(store, name, target, options, context, info) {
	function purgePromise() {
		const { client } = store;

		const collection = store.collection(name);
		const { idProperty } = collection;

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		return new Promise((resolve, reject) => {
			const data = client.getData() || {};
			let localData = [];
			try {
				localData = data[name] || [];
			} catch (err) {
				return reject(err);
			}
			const slicePage = (results) => {
				let end = cursor + count;
				if (results.length) {
					end = Math.min(results.length, cursor + count);
				}
				results = results.slice(cursor, end);
				return results;
			};
			const targetKeys = target ? Object.keys(target) : [];
			let itemsToDelete = [];

			try {
				if (!targetKeys.length) {
					itemsToDelete = slicePage(localData);
				} else {
					itemsToDelete = slicePage(
						localData.filter((item) => (
							targetKeys.every((tk) => (
								target[tk] === null
									? isNull(item, tk)
									: item[tk] === target[tk]
							))
						)),
					);
				}
			} catch (err) {
				// console.error(err);
				return reject(err);
			}

			const removalPromises = itemsToDelete.map(
				(item) => collection.prepareForDestruction(
					{ [idProperty]: item[idProperty] },
					context,
					info,
				),
			);

			const finish = () => {
				const itemIndexes = itemsToDelete.map((item) => {
					let index = null;
					localData.some((d, i) => {
						if (d[idProperty] === item[idProperty]) {
							index = i;
							return true;
						}
					});
					return index;
				});

				itemsToDelete.forEach((item, i) => {
					if (itemIndexes[i] !== null) {
						client.removeData(`${name}.${itemIndexes[i]}`);
					}
					collection.emit('removed', item);
				});

				return resolve(itemsToDelete.length);
			};

			if (removalPromises.length) {
				return Promise.all(removalPromises).then(finish, reject);
			}

			return finish();
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, purgePromise);
	// }

	return purgePromise();
}

/**
 * [purge description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.purge = purge;

function upsert(store, name, target, options, context, info) {
	const { client } = store;

	const collection = store.collection(name);
	const { idProperty } = collection;

	const { id: newId, document } = collection.convertToStore(context, target);

	let id = newId;

	// if (name === 'User') {
	// 	debugger;
	// }

	return new Promise((resolve, reject) => {
		// TODO:
		const [relatedPromiseTriggers, relatedError] = (
			collection.gatherRelatedPromiseTriggers(document, context, info)
		);

		if (relatedError) {
			// console.warn(relatedError);
			return reject(relatedError);
		}

		const validationErrors = collection.validateDocument(document);

		if (validationErrors) {
			return reject(validationErrors);
		}

		const saveDoc = (doc) => {
			const data = client.getData() || {};
			let localData = [];
			try {
				localData = data[name] || [];
			} catch (err) {
				return reject(err);
			}
			let index = localData.length;
			let lastDoc = null;
			// TODO: sort local data, and apply offset/limit
			localData.some((item, i) => {
				// TODO: check unique fields
				if (item[idProperty] === target[idProperty]) {
					id = target[idProperty];
					index = i;
					lastDoc = item;
					return true;
				}
				return false;
			});
			const { filter } = options || {};
			if (filter && lastDoc) {
				if (
					Object.keys(options.filter).some(
						(tk) => (
							filter[tk] === null
								? isNull(lastDoc, tk)
								: lastDoc[tk] === filter[tk]
						),
					)
				) {
					const err = new Error('Filtered data selection from upserting');
					err.id = 'FilteredUpsertData';
					return reject(err);
				}
			}
			try {
				const newDoc = {
					[idProperty]: id,
					...(lastDoc || {}),
					...doc,
				};

				Object.freeze(newDoc);

				Object.keys(newDoc).forEach((key) => {
					const type = typeof newDoc[key];
					if (
						type === 'string'
						|| type === 'number'
						|| type === 'boolean'
					) {
						return;
					}
					let value = JSON.stringify(newDoc[key]);
					// HACK to make date objects work, not sure how this will effect other types
					// TODO: fix this?
					if (value && value.charAt(0) === '"') {
						value = value.substring(1, value.length - 1);
						newDoc[key] = value;
					}
				});

				const itemKey = `${name}.${index}`;
				debugger;
				if (lastDoc === null) {
					client.setData(itemKey, newDoc);
				} else {
					const beforeKeys = Object.keys(lastDoc);
					const afterKeys = Object.keys(newDoc);
					const addedKeys = [];
					const commonKeys = [];
					const removedKeys = [];
					afterKeys.forEach((key, i) => {
						const index = beforeKeys.indexOf(key);
						if (index === -1) {
							afterKeys.splice(i, 0);
							addedKeys.push(key);
						} else {
							beforeKeys.splice(index, 1);
							commonKeys.push(key)
						}
					});
					beforeKeys.forEach((key) => {
						if (afterKeys.indexOf(key) === -1) {
							removedKeys.push(key);
						}
					});
					addedKeys.forEach((key) => {
						client.setData(`${itemKey}.${key}`, newDoc[key]);
					});
					removedKeys.forEach((key) => {
						client.removeData(`${itemKey}.${key}`);
					});
					commonKeys.forEach((key) => {
						if (lastDoc[key] !== newDoc[key]) {
							client.setData(`${itemKey}.${key}`, newDoc[key]);
						}
					});
				}

				const resolveDoc = (doc) => {
					doc = {
						...doc,
					};
					const id = doc[idProperty];
					doc = collection.convertToGraphQL(doc);
					doc = collection.redactSecureDataForUser(doc, context);

					// See other note where redactSecureDataForUser is called elsewhere in this file
					doc[idProperty] = id;

					collection.emit(lastDoc && target[idProperty] === id ? 'updated' : 'created', doc);

					return resolve(doc);
				};

				if (relatedPromiseTriggers.length) {
					const relatedPromises = relatedPromiseTriggers.map((trigger) => trigger(newDoc));
					return Promise.all(relatedPromises).then(() => resolveDoc(newDoc), reject);
				}

				return resolveDoc(newDoc);
			} catch (err) {
				// console.error(err);
				return reject(err);
			}
		};

		return collection.prepareForStorage(document).then(saveDoc, reject);
	});
}

/**
 * [upsert description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
automerge_model.upsert = upsert;
