/* eslint global-require:0 */

const Collection = require('@seaturtle/gmd-adapter-generic/Collection.js');

const createDate = require('@seaturtle/gmd-adapter-generic/createDate.js');

/**
 * Automerge Collection
 * @exports AutomergeCollection
 * @class
 * @extends Collection */
class AutomergeCollection extends Collection {
	/**
	 * [mapQueryOperators description]
	 * @method
	 * @param  {[type]}  target               [description]
	 * @return {[type]}                       [description]
	 */
	mapQueryOperators(target) {
		return target;
	}

	/**
	 * [convertToGraphQL description]
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToGraphQL(document, iterator) {
		const parseJSON = (json) => {
			if (json) {
				try {
					const v = JSON.parse(json);
					return v;
				} catch (err) {
					console.warn('JSON PARSE ERR:', json, err);
				}
			}
			return null;
		};
		return super.convertToGraphQL(document, (field, property, value, doc) => {
			switch (property.typeName) {
			case 'Boolean':
				doc[field] = doc[field] === 'true' || doc[field] === true;
				break;

			case 'Int':
				doc[field] = parseInt(doc[field], 10);
				doc[field] = isFinite(doc[field]) ? doc[field] : null;
				break;

			case 'Float':
				doc[field] = parseFloat(doc[field]);
				doc[field] = isFinite(doc[field]) ? doc[field] : null;
				break;

			case 'Date':
				// debugger;
				doc[field] = createDate(doc[field], true);
				break;

			case 'DateTime':
				// debugger;
				doc[field] = createDate(doc[field]);
				break;

			case 'JSON':
				doc[field] = parseJSON(doc[field]);
				break;

			default: break;
			}

			if (property.nested) {
				if (Array.isArray(doc[field])) {
					let id = 0;
					const a = [];
					const merge = (v) => {
						if (typeof v === 'string') {
							merge(parseJSON(v));
						} else if (Array.isArray(v)) {
							v.forEach(merge);
						} else if (v) {
							const related = this.store.collection(
								property.nested.modelName,
							);
							const relatedIdProperty = related.idProperty;
							v[relatedIdProperty] = v[relatedIdProperty] || id.toString();
							a.push(v);
							id += 1;
						}
					};
					doc[field].forEach(merge);
					doc[field] = a;
				} else if (typeof doc[field] === 'string') {
					doc[field] = parseJSON(doc[field]);
				}
			}

			if (typeof iterator === 'function') {
				iterator(field, property, value, doc);
			}
		});
	}

	/**
	 * [filterSecureDataForUser description]
	 * @param  {[type]} list    [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} target  [description]
	 * @return {[type]}         [description]
	 */
	filterSecureDataForUser(list, context, target) {
		if (!target) {
			return list;
		}
		// return super.filterSecureDataForUser(list, context);
		const targetKeys = Object.keys(target);
		if (targetKeys.length < 1) {
			return list;
		}
		const compareObjects = (item, targ) => {
			// TODO: finish this, also ensure mongodb and sequal can filter based on the same object rules
			if (item) {
				if (typeof item === 'string' && targ.regex) {
					return new RegExp(targ.regex).test(item);
				}
				return item === targ;
			}
			return true;
		};
		// console.log('FILTER', target, list);
		return list.filter((item) => targetKeys.every((key) => {
			let skip = false;
			this.enumerateRelations((relation) => {
				if (relation === key) {
					skip = true;
				}
			});
			if (skip) {
				return true;
			}
			// console.log('KEY', key, target[key], item[key]);
			switch (typeof target[key]) {
			case 'string':
			case 'number':
				return (
					item[key] === target[key]
					|| item[key].toString() === target[key].toString()
				);
			case 'date':
			case 'object':
			default:
				return target[key]
					? compareObjects(item[key], target[key])
					: !item[key];
			}
		}));
	}

	/**
	 * [convertToStore description]
	 * @param  {[type]} context  [description]
	 * @param  {[type]} params   [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToStore(context, params, iterator) {
		return super.convertToStore(context, params, (field, property, value, document, id) => {
			/* eslint no-underscore-dangle:0 */
			// debugger;
			if (document[field]) {
				switch (property.typeName) {
				case 'Boolean':
					document[field] = document[field] === 'true' || document[field] === true;
					break;

				case 'Int':
				case 'Float':
					document[field] = isFinite(document[field]) ? String(document[field]) : '';
					break;

				case 'DateTime':
				case 'Date':
					document[field] = new Date(document[field]).getTime();
					document[field] = String(document[field]);
					break;

				default: break;
				}
			}

			if (typeof iterator === 'function') {
				iterator(field, property, value, document, id);
			}
		});
	}

	/**
	 * [prepareForStorage description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	prepareForStorage(document, iterator) {
		// debugger;
		return super.prepareForStorage(document, (field, property, promises) => {
			if (property.nested) {
				document[field] = JSON.stringify(document[field]);
			}
			// console.log(property.typeName);
			if (property.typeName === 'JSON') {
				document[field] = JSON.stringify(document[field]);
			}
			if (typeof iterator === 'function') {
				iterator(field, property, promises, document);
			}
		});
	}
}

module.exports = AutomergeCollection;
