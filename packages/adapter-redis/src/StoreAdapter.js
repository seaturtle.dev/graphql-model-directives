/* eslint global-require:0 */

let Redis = null;

try {
	Redis = require('redis');
} catch (err) {
	// console.warn('redis not installed');
}

const StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');
const RedisCollection = require('./Collection');

const model = require('./model');
const user = require('./user');
const migration = require('./migration');

/**
 * Uses a hand written `redis` ORM, to provide a comprehensive Redis adapter.
 * @exports RedisStore
 * @class
 * @extends StoreAdapter */
class RedisStore extends StoreAdapter {
	/**
	 * [constructor description]
	 * @param {[type]} client  [description]
	 * @param {[type]} options [description]
	 * @constructs
	 */
	constructor(client, options, _Redis = Redis, RedisPubSub, crypto) {
		super(options && options.resolvers, RedisPubSub, null, crypto);
		this.pubsub = (options && options.pubsub)
			|| (
				RedisPubSub
					? (
						new RedisPubSub({
							connection: options && options.redisPubSubConnection,
						})
					)
					: null
			);

		if (!client && !Redis) {
			/* istanbul ignore next */
			throw new Error('NPM install redis, redis is not installed.');
		}

		this.client = client || Redis.createClient(options.client);

		if (this.client.connect) {
			this.conn = this.client.connect();
			this.conn.catch((err) => { console.warn(err.message); });
		}
	}

	/**
	 * [close description]
	 * @param  {Function} cb [description]
	 * @return {[type]}      [description]
	 */
	close(cb) {
		if (this.pubsub && this.pubsub.close) {
			this.pubsub.close();
		}
		this.client.quit(cb);
	}

	/**
	 * [mixinUser description]
	 * @param  {[type]} collection [description]
	 * @return {[type]}            [description]
	 */
	mixinUser(collection) {
		return super.mixinUser(collection, user, model);
	}

	/**
	 * [authenticate description]
	 * @param  {[type]}   context [description]
	 * @param  {[type]}   info    [description]
	 * @param  {Function} next    [description]
	 * @param  {[type]}   or      [description]
	 * @param  {[type]}   roles   required user roles for to authenticate
	 * @return {[type]}           [description]
	 */
	authenticate(context, info, next, or, roles = []) {
		return new Promise((resolve, reject) => {
			const onAuthError = (err) => {
				if (or === true) { return resolve(next(err)); }

				if (typeof or === 'function') { return resolve(or(err, next)); }

				return reject(err);
			};
			return user.isAuthenticated(roles, context, info).then(() => (
				Promise.resolve(next()).then(resolve, reject)
			), onAuthError);
		});
	}

	/**
	 * [readDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	readDocument({ name }, { target, options }, { context, info }) {
		return model.read(this, name, target, options, context, info);
	}

	/**
	 * [findDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	findDocuments({ name }, { target, options }, { context, info }) {
		return model.find(this, name, target, options, context, info);
	}

	/**
	 * [countDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	countDocuments({ name }, { target, options }, { context, info }) {
		return model.count(this, name, target, options, context, info);
	}

	/**
	 * [scanDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	scanDocuments({ name }, { target, options }, { context, info }) {
		return model.scan(this, name, target, options, context, info);
	}

	/**
	 * [purgeDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	purgeDocuments({ name }, { target, options }, { context, info }) {
		return model.purge(this, name, target, options, context, info);
	}

	/**
	 * [upsertDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	upsertDocument({ name }, { target, options }, { context, info }) {
		return model.upsert(this, name, target, options, context, info);
	}

	/**
	 * [removeDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	removeDocument({ name }, { target, options }, { context, info }) {
		return model.remove(this, name, target, options, context, info);
	}

	/**
	 * [migration description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	migration(object, options) {
		return migration(object, options);
	}

	// /**
	//  * [undoMigration description]
	//  * @param  {[type]} channel [description]
	//  * @return {[type]}         [description]
	//  */
	// undoMigration(migrations) {
	// 	return Promise.resolve();
	// }
}

RedisStore.Collection = RedisCollection;

module.exports = RedisStore;
