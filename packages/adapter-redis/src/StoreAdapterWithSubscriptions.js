/* eslint global-require:0 */

const { RedisPubSub } = require('graphql-redis-subscriptions');

const RedisStoreAdapter = require('./StoreAdapter');
const RedisCollection = require('./Collection');

/**
 * Uses a hand written `redis` ORM, to provide a comprehensive Redis adapter.
 * @exports RedisStore
 * @class
 * @extends StoreAdapter */
class RedisStoreWithSubscriptions extends RedisStoreAdapter {
	constructor(resolvers, options) {
		super(resolvers, options, Redis, RedisPubSub);
	}
}

RedisStore.Collection = RedisCollection;

module.exports = RedisStore;
