/* eslint no-unused-vars:0 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

/**
 * Sequel Migration Implementation
 * @ignore
 */
const redis_migration = (migration, options) => {
	migration.execute = () => {};
	return migration;
};

exports = module.exports = redis_migration;

redis_migration.changeFieldName = () => {};
redis_migration.changeFieldType = () => {};
redis_migration.changeTableName = () => {};

redis_migration.dropTable = () => {};
redis_migration.createTable = () => {};

redis_migration.emptyTableData = () => {};
redis_migration.exportTableData = () => {};
redis_migration.importTableData = () => {};

redis_migration.insertField = () => {};
redis_migration.removeField = () => {};
redis_migration.renameField = () => {};

redis_migration.removeIndex = () => {};
redis_migration.createIndex = () => {};
redis_migration.renameIndex = () => {};
