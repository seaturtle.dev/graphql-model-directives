
const model = require('./index');

const getBatch = require('./helpers').getBatch;
// TODO: sort, filter

module.exports = function scan(store, name, target, options, context, info) {
	function scanPromise() {
		const key = model.documentKey(name);
		const cursor = options.cursor || '0';
		const count = options.count || 100;

		return new Promise((resolve, reject) => (
			store.client.scan(
				cursor,
				'MATCH',
				`${key}:*`,
				'COUNT',
				count,
				(err, results) => {
					if (err) { return reject(err); }

					context.redisPipeline = getBatch(store.client);

					const nextCursor = results[0];

					results = results[1].map((id) => id.split(':').pop());

					const collection = store.collection(name);

					const promises = results.map((id) => (
						model.read(store, name, {
							...target,
							[collection.idProperty]: id,
						}, options, context, info)
					));

					context.redisPipeline.exec();

					delete context.redisPipeline;

					return Promise.all(promises).then((page) => {
						page = collection.filterSecureDataForUser(page, context, target);
						const skipped = results.length - page.length;
						return collection.resolvePagedRelations(resolve, reject, {
							cursor: nextCursor,
							count,
							skipped,
							page,
						}, context, info);
					}, reject);
				},
			)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, scanPromise);
	// }

	return scanPromise();
};
