
const model = require('./index');

const getBatch = require('./helpers').getBatch;

module.exports = function purge(store, name, target, options, context, info) {
	function scanPromise() {
		const key = model.documentKey(name);
		const cursor = options.cursor || '0';
		const count = options.count || 100;

		return new Promise((resolve, reject) => (
			// TODO: scan filtering doesnt work well, use find or fix scan
			store.client.scan(
				cursor,
				'MATCH',
				`${key}:*`,
				'COUNT',
				count,
				(err, results) => {
					if (err) { return reject(err); }

					context.redisPipeline = getBatch(store.client);

					// const nextCursor = results[0];

					results = results[1].map((id) => id.split(':').pop());

					const collection = store.collection(name);

					const promises = results.map((id) => (
						model.remove(store, name, {
							...target,
							[collection.idProperty]: id,
						}, options, context, info)
					));

					context.redisPipeline.exec();

					delete context.redisPipeline;

					return Promise.all(promises).then(
						(r) => resolve((r && r.length) || 0),
						reject,
					);
				},
			)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, scanPromise);
	// }

	return scanPromise();
};
