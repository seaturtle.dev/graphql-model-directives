
const model = require('./index');

const getBatch = require('./helpers').getBatch;

module.exports = function remove(store, name, target, options, context, info) {
	function removePromise() {
		return new Promise((resolve, reject) => {
			const collection = store.collection(name);

			const id = target[collection.idProperty] || target;

			const document = { [collection.idProperty]: id };

			const docKey = model.documentKey(name, id);

			const indexPipeline = getBatch(store.client);

			const docFields = [];

			// debugger;

			collection.enumerateUniques((field) => {
				if (docFields.indexOf(field) === -1) {
					docFields.push(field);
					indexPipeline.hget(docKey, field);
				}
			});

			collection.enumerateRelations((field) => {
				// TODO:
				if (collection.properties[field].relation.many) {
					return;
				}
				if (docFields.indexOf(field) === -1) {
					docFields.push(field);
					indexPipeline.hget(docKey, field);
				}
			});

			collection.enumerateIndexes((field) => {
				if (docFields.indexOf(field) === -1) {
					docFields.push(field);
					indexPipeline.hget(docKey, field);
				}
			});

			// debugger;

			indexPipeline.exec((indexErr, indexResults) => {
				if (indexErr) { return reject(indexErr); }

				indexResults.some((result, i) => {
					if (docFields[i]) {
						document[docFields[i]] = result[1]; // [1] ??? is [0] and err?
					}

					return false;
				});

				// debugger;

				const documentPipeline = getBatch(store.client);

				collection.uniques.forEach((field) => {
					if (document[field] !== undefined) {
						const property = collection.properties[field];
						const { uniques } = property;

						uniques.forEach((unique) => {
							const { compounds } = unique;

							let uniqueKey = field;
							if (compounds && compounds.length) {
								uniqueKey = compounds.map((c) => `${c}:${document[c] || ''}`).join(':');
							}

							// debugger;
							const uk = model.uniqueKey(name, uniqueKey, document[field]);
							documentPipeline.del(uk);
						});
					}
				});

				function getValue(field/* , oldOnly */) {
					if (document[field] !== undefined) {
						return document[field];
					}

					return '';
				}

				collection.indexes.forEach((index) => {
					const property = collection.properties[index];
					const { sortedIndexes } = property;

					sortedIndexes.forEach((sortedIndex) => {
						const { compounds } = sortedIndex;

						// debugger;

						const compoundIndex = (compound) => {
							const compoundKey = ['idx', name];

							const skip = document[index] === undefined;

							if (
								compound
								&& document[compound] !== undefined
							) {
								compoundKey.push(compound);
								compoundKey.push(getValue(compound));
							}

							compoundKey.push(index);

							if (skip) {
								return;
							}

							const indexKey = compoundKey.join(':');

							// debugger;

							documentPipeline.zrem(indexKey, id);
						};

						// debugger;

						if (!compounds || !compounds.length) {
							compoundIndex('');
						} else {
							compoundIndex('');
							compounds.forEach(compoundIndex);
						}
					});
				});

				// debugger;

				documentPipeline.del(docKey);

				return collection.prepareForDestruction(target, context, info).then(() => (
					documentPipeline.exec((err/* , results */) => {
						// debugger;
						if (err) { return reject(err); }

						collection.emit('removed', document);

						return resolve(document);
					})
				));
			});
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, removePromise);
	// }

	return removePromise();
};
