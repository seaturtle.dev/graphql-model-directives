
function getBatch(client) {
	if (client.pipeline) {
		return client.pipeline();
	}
	if (client.batch) {
		return client.batch();
	}
	throw new Error('Redis batch method is unavailable.');
}

exports.getBatch = getBatch;

function findIndexKey(store, name, target/* , options, context, info */) {
	const collection = store.collection(name);
	let key = ['idx', name];

	let targetIndex = collection.indexes[0] || null;
	let targetRelation = null;

	if (target) {
		collection.indexes.some((index/* , id */) => {
			const indexProperty = collection.properties[index];

			// TODO: test this
			return indexProperty.sortedIndexes.some((sortedIndex) => {
				const { compounds } = sortedIndex;

				if (compounds) {
					return compounds.some((field) => {
						// NOTE: must now explicitly set empty filters to an empty string ''
						if (target[field] !== undefined && target[field] !== null) {
							targetRelation = field;
							targetIndex = targetIndex || index;
							return true;
						}
						return false;
					});
				}
				if (target[index] !== undefined && target[index] !== null) {
					targetIndex = index;
					return true;
				}

				return false;
			});
		});

		if (targetRelation) {
			key.push(targetRelation);
			key.push(target[targetRelation] || '');
		}

		if (targetIndex) {
			key.push(targetIndex);
			if (target[targetIndex]) {
				key.push(target[targetIndex]);
			}
		}
	}

	key = key.join(':');

	return [key, targetIndex];
}

exports.findIndexKey = findIndexKey;
