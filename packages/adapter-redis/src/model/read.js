const model = require('./index');

module.exports = function read(store, name, target, options, context, info) {
	function readPromise() {
		return new Promise((resolve, reject) => {
			const key = model.documentKey(name, target.id);

			let cursor = '0';

			const count = options.count || 100;

			let document = {};

			function scanPropertiesPromise() {
				store.client.hscan(key, cursor, 'COUNT', count, (err, results) => {
					if (err) {
						return reject(err);
					}

					[cursor, results] = results;

					for (let i = 0; i < results.length; i += 2) {
						document[results[i]] = results[i + 1];
					}

					if (cursor !== '0') {
						return scanPropertiesPromise();
					}

					const collection = store.collection(name);

					if (collection.permission && !collection.hasPermission(document, context)) {
						return resolve(null);
					}

					document = collection.convertToGraphQL(document);
					document = collection.redactSecureDataForUser(document, context);

					return collection.resolveDocumentRelations(resolve, reject, document, context, info);
				});
			}

			scanPropertiesPromise();
		});
	}

	if (context.redisPipeline) {
		return readPromise();
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, readPromise);
	// }

	return readPromise();
};
