/* eslint camelcase:1 */

/**
 * Redis User Implementation
 * @exports redis_model */
const redis_model = exports;

/**
 * [documentKey description]
 * @param  {[type]} name [description]
 * @param  {[type]} id   [description]
 * @return {[type]}      [description]
 */
redis_model.documentKey = (name, id) => {
	if (id) {
		return `mdl:${name}:${id}`;
	}
	return `mdl:${name}`;
};

/**
 * [indexKey description]
 * @param  {[type]} name     [description]
 * @param  {[type]} id       [description]
 * @param  {[type]} property [description]
 * @return {[type]}          [description]
 */
redis_model.indexKey = (name, id, property) => {
	if (id && property) {
		return `idx:${name}:${property}:${id}`;
	}
	if (id) {
		return `idx:${name}::${id}`;
	}
	if (property) {
		return `idx:${name}:${property}`;
	}
	return `idx:${name}`;
};

/**
 * [uniqueKey description]
 * @param  {[type]} name     [description]
 * @param  {[type]} property [description]
 * @param  {[type]} value    [description]
 * @return {[type]}          [description]
 */
redis_model.uniqueKey = (name, property, value) => {
	if (property && (value !== null && value !== undefined)) {
		return `uqe:${name}:${property}:${value}`;
	}
	if (property) {
		return `uqe:${name}:${property}`;
	}
	if (value !== null && value !== undefined) {
		return `uqe:${name}:::${value}`;
	}
	return `uqe:${name}`;
};

/**
 * [findIndexKey description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.findIndexKey = require('./helpers').findIndexKey;

/**
 * [count description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.count = require('./count');

/**
 * [find description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.find = require('./find');

const { nextPage } = redis_model.find;

/**
 * [nextPage description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.find.nextPage = nextPage;

/**
 * [purge description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.purge = require('./purge');

/**
 * [read description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.read = require('./read');

/**
 * [remove description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.remove = require('./remove');

/**
 * [scan description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.scan = require('./scan');

/**
 * [upsert description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
redis_model.upsert = require('./upsert');
