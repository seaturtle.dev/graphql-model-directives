
const model = require('./index');

const getBatch = require('./helpers').getBatch;

module.exports = function upsert(store, name, target, options, context, info) {
	const collection = store.collection(name);

	const { id, create, document } = collection.convertToStore(context, target);

	const key = model.documentKey(name, id);

	// if (name === 'UserInvite') {
	// 	debugger;
	// }

	return new Promise((resolve, reject) => {
		const validationErrors = collection.validateDocument(document);

		if (validationErrors) {
			return reject(validationErrors);
		}

		const uniquesPipeline = getBatch(store.client);

		collection.uniques.forEach((field) => {
			const property = collection.properties[field];
			const { uniques } = property;

			uniques.forEach((unique) => {
				const { compounds } = unique;

				let uniqueKey = field;
				if (compounds && compounds.length) {
					uniqueKey = compounds.map((c) => `${c}:${document[c] || ''}`).join(':');
				}

				const uk = model.uniqueKey(name, uniqueKey, document[field]);
				uniquesPipeline.get(uk);
			});
		});

		const oldDocFields = [];

		if (!create) {
			collection.enumerateFields((field) => {
				const fieldDef = collection.properties[field];
				// TODO:
				if (fieldDef && fieldDef.relation && fieldDef.relation.many) {
					return;
				}
				oldDocFields.push(field);
				uniquesPipeline.hget(key, field);
			});
		}

		const oldDocument = create ? null : { [collection.idProperty]: id };

		return uniquesPipeline.exec((uniqueErr, uniqueResults) => {
			if (uniqueErr) { return reject(uniqueErr); }

			let existing = null;

			uniqueResults.some((result, i) => {
				const field = oldDocFields[i];
				if (i < collection.uniques.length) {
					if (result && (
						Object.prototype.hasOwnProperty.call(document, field)
						&& document[field] !== result
					)) {
						if (!id || result !== id) {
							existing = collection.uniques[i];
						}

						return true;
					}

					return false;
				}

				i -= collection.uniques.length;

				if (oldDocFields[i]) {
					oldDocument[oldDocFields[i]] = result;
				}

				return false;
			});

			if (existing && create) {
				return reject(new Error(
					`${model.uniqueKey(name, existing, document[existing])} exists.`,
				));
			}

			const [relatedPromiseTriggers, relatedError] = (
				collection.gatherRelatedPromiseTriggers(document, context, info)
			);

			if (relatedError) {
				return reject(relatedError);
			}

			return collection.prepareForStorage(document).then((doc) => {
				const newDocument = doc;

				const documentPipeline = getBatch(store.client);

				collection.fields.forEach((field) => {
					if (newDocument[field] !== undefined) {
						documentPipeline.hset(key, field, newDocument[field]);
					}
				});

				collection.uniques.forEach((field) => {
					if (newDocument[field] !== undefined && (
						create || newDocument[field] !== oldDocument[field]
					)) {
						const property = collection.properties[field];
						const { uniques } = property;

						uniques.forEach((unique) => {
							const { compounds } = unique;

							let uniqueKey = field;
							if (compounds && compounds.length) {
								uniqueKey = compounds.map((c) => `${c}:${document[c] || ''}`).join(':');
							}

							if (!create) {
								const oldUK = model.uniqueKey(name, uniqueKey, oldDocument[field]);

								documentPipeline.del(oldUK);
							}
							const newUK = model.uniqueKey(name, uniqueKey, newDocument[field]);

							documentPipeline.set(newUK, id);
						});
					}
				});

				function getValue(field, oldOnly) {
					if (!oldOnly && newDocument[field] !== undefined) {
						return newDocument[field];
					}

					if (oldDocument[field] !== undefined) {
						return oldDocument[field];
					}

					return '';
				}

				collection.indexes.forEach((index) => {
					const property = collection.properties[index];
					const { sortedIndexes } = property;

					sortedIndexes.forEach((sortedIndex) => {
						const { compounds } = sortedIndex;

						const compoundIndex = (compound) => {
							const oldCompoundKey = ['idx', name];
							const newCompoundKey = ['idx', name];

							const { dir } = sortedIndex;
							let value = 0;
							let skip = true;

							if (
								compound
								&& newDocument[compound] !== undefined
							) {
								if (!create) {
									oldCompoundKey.push(compound);
									oldCompoundKey.push(getValue(compound, true));
								}

								newCompoundKey.push(compound);
								newCompoundKey.push(getValue(compound));
							}

							if (!create) {
								oldCompoundKey.push(index);
							}

							newCompoundKey.push(index);

							if (newDocument[index] !== undefined) {
								value = newDocument[index] === null ? null : newDocument[index].valueOf();
								skip = false;
							}

							if (skip) {
								return;
							}

							if (!create) {
								documentPipeline.zrem(oldCompoundKey.join(':'), id);
							}

							documentPipeline.zadd(newCompoundKey.join(':'), value * (dir || 1), id);
						};

						if (!compounds || !compounds.length) {
							compoundIndex('');
						} else {
							compoundIndex('');
							compounds.forEach(compoundIndex);
						}
					});
				});

				return documentPipeline.exec((err/* , results */) => {
					if (err) { return reject(err); }

					let finalDocument = {
						...oldDocument,
						...newDocument,
					};

					const resolveDoc = (finalDocument) => {
						finalDocument = collection.convertToGraphQL(finalDocument);
						finalDocument = collection.redactSecureDataForUser(finalDocument, context);

						// TODO: actually check for existing document?
						collection.emit(existing || target[collection.idProperty] === id ? 'updated' : 'created', finalDocument);

						return resolve(finalDocument);
					};

					if (relatedPromiseTriggers.length) {
						const relatedPromises = relatedPromiseTriggers.map((trigger) => trigger(finalDocument));
						return Promise.all(relatedPromises).then(() => resolveDoc(finalDocument), reject);
					}

					return resolveDoc(finalDocument);
				});
			}, reject);
		});
	});
};
