
const model = require('./index');

const { findIndexKey } = require('./helpers');

module.exports = function count(store, name, target, options/* , context, info */) {
	function countPromise() {
		// const keyold = model.indexKey(name, null, 'updated');
		const [key] = findIndexKey(store, name, target/* , options, context, info */);
		const start = options.start || '-inf';
		const end = options.end || '+inf';

		// console.log(name, keyold, key);

		return new Promise((resolve, reject) => (
			store.client.zcount(
				key,
				start,
				end,
				(err, c) => {
					if (err) { return reject(err); }

					return resolve(c);
				},
			)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, countPromise);
	// }

	return countPromise();
};
