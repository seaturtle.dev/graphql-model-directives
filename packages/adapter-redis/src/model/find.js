/* eslint no-unused-vars:1 */

const model = require('./index');

const { findIndexKey, getBatch } = require('./helpers');

// TODO: sort, filter

module.exports = function find(store, name, target, options, context, info) {
	function findPromise() {
		const [key, targetIndex] = findIndexKey(store, name, target/* , options, context, info */);

		let start = options.start || '-inf';
		let end = options.end || '+inf';
		const offset = options.offset || 0;
		const count = options.count || 100;

		let reverse = false;
		if (options.sort && options.sort.fields && options.sort.fields.indexOf(targetIndex) !== -1) {
			if (options.sort.ascending) {
				reverse = true;
				const tmp = start;
				start = end;
				end = tmp;
			}
		}

		// target = collection.mapQueryOperators(target);
		// TODO: add better filtering support for redis

		return new Promise((resolve, reject) => {
			const finish = (err, results) => {
				if (err) { return reject(err); }

				context.redisPipeline = getBatch(store.client);

				const collection = store.collection(name);

				const promises = results.map((id) => {
					const params = { ...target, [collection.idProperty]: id };
					return model.read(store, name, params, options, context, info);
				});

				context.redisPipeline.exec();

				delete context.redisPipeline;

				return Promise.all(promises).then((page) => {
					if (store.client.isMemory && results.length > count) {
						// todo: slice offset?
						page.length = count;
					}
					page = collection.filterSecureDataForUser(page, context, target);
					const skipped = results.length - page.length;
					return collection.resolvePagedRelations(resolve, reject, {
						start,
						end,
						offset,
						count,
						skipped,
						page,
					}, context, info);
				}, reject);
			};

			const method = reverse ? 'zrevrangebyscore' : 'zrangebyscore';

			if (store.client.isMemory) {
				// TODO: is this wrong?
				return store.client[method](
					key,
					start,
					end,
					'',
					'LIMIT',
					offset,
					count,
					finish,
				);
			}

			return store.client[method](
				key,
				start,
				end,
				'LIMIT',
				offset,
				count,
				finish,
			);
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, findPromise);
	// }

	return findPromise();
};

module.exports.indexKey = findIndexKey;

module.exports.nextPage = function nextPage(store, name, target, options, context, info) {
	function nextPagePromise() {
		const [key] = findIndexKey(store, name, target, options, context, info);

		const start = options.start || '-inf';
		const end = options.end || '+inf';
		const offset = options.offset || 0;
		const count = options.count || 100;

		return new Promise((resolve, reject) => (
			store.client.zrangebyscore(
				key,
				start,
				end,
				'LIMIT',
				offset + count,
				count,
				(err, results) => {
					if (err) { return reject(err); }

					return resolve(results && results.length > 0);
				},
			)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, nextPagePromise);
	// }

	return nextPagePromise();
};
