
const get = require('./get');

module.exports = (context, login) => {
	const { userCollection } = context.store;
	const { name } = userCollection;

	const loginName = userCollection.userLogin;

	return new Promise((resolve, reject) => (
		context.store.client.get(`uqe:${name}:${loginName}:${login}`, (err, userId) => {
			if (err) {
				return reject(err);
			}

			return get(context, userId).then(resolve, reject);
		})
	));
};
