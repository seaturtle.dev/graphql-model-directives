// const crypto = require('../../../plugins/crypto');

const { encodeTokenFromLogin } = require('@seaturtle/gmd-plugin-authorize/authorize.js');

const getBatch = require('../model/helpers').getBatch;

module.exports = (
	context,
	email,
	recoveryToken,
	newPassword,
) => new Promise((resolve, reject) => {
	const { crypto } = context.store;

	const { deviceId } = context;
	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const tokenName = userCollection.userToken;
	const tokensName = userCollection.userTokens;
	const passwordName = userCollection.userPassword;
	const passwordResetName = userCollection.userPasswordReset;

	return context.store.client.get(`uqe:${name}:${emailName}:${email}`, (err, userId) => {
		if (err) {
			return reject(err);
		}

		if (!userId) {
			return reject(new Error('Invalid credentials'));
		}

		return context.store.client.hget(`mdl:${name}:${userId}`, passwordResetName, (getErr, passwordReset) => {
			if (getErr) {
				return reject(getErr);
			}

			if (recoveryToken === passwordReset) {
				const pipeline = getBatch(context.store.client);

				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(hashErr);
					}

					pipeline.hset(`mdl:${name}:${userId}`, passwordResetName, '');
					pipeline.hset(`mdl:${name}:${userId}`, passwordName, hash);

					return pipeline.exec((setErr/* , results */) => {
						if (setErr) {
							return reject(setErr);
						}

						return context.store.client.hget(`mdl:${name}:${userId}`, loginName, (loginErr, login) => {
							if (getErr) {
								return reject(getErr);
							}

							return encodeTokenFromLogin(login, context.config, (encodeTokenErr, token) => {
								if (encodeTokenErr) {
									return reject(encodeTokenErr);
								}

								return context.store.client.hget(`mdl:${name}:${userId}`, tokensName, (getTokenErr, rawTokens) => {
									if (getTokenErr) {
										return reject(getTokenErr);
									}

									let tokens = {};
									try {
										tokens = JSON.parse(rawTokens || '{}');
									} catch (parseErr) {
										console.warn('RESET PASSWORD WARNING:', parseErr);
									}

									tokens[deviceId] = token;

									return context.store.client.hset(
										`mdl:${name}:${userId}`,
										tokensName,
										JSON.stringify(tokens),
										(setTokenErr) => {
											if (setTokenErr) {
												return reject(setTokenErr);
											}

											const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

											return resolve({
												[idProperty]: userId,
												[emailName]: email,
												[tokensName]: tokenList,
												[tokenName]: token,
											});
										},
									);
								});
							});
						});
					});
				});
			}

			return reject(new Error('Invalid credentials'));
		});
	});
});
