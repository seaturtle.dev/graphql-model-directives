/* eslint global-require:0 */
/* eslint camelcase:1 */

const getByLogin = require('./getByLogin');

/**
 * Redis User Implementation
 * @exports redis_user */
const redis_user = exports;

/**
 * [forgotPassword description]
 * @method
 * @param  {[type]} context [description]
 * @param  {[type]} email   [description]
 * @return {[type]}         [description]
 */
redis_user.forgotPassword = require('./forgotPassword');

/**
 * [resetPassword description]
 * @method
 * @param  {[type]} context       [description]
 * @param  {[type]} email         [description]
 * @param  {[type]} recoveryToken [description]
 * @param  {[type]} newPassword   [description]
 * @return {[type]}               [description]
 */
redis_user.resetPassword = require('./resetPassword');

/**
 * [updatePassword description]
 * @method
 * @param  {[type]} context     [description]
 * @param  {[type]} email       [description]
 * @param  {[type]} oldPassword [description]
 * @param  {[type]} newPassword [description]
 * @return {[type]}             [description]
 */
redis_user.updatePassword = require('./updatePassword');

/**
 * [refreshToken description]
 * @method
 * @param  {[type]} context [description]
 * @param  {[type]} login   [description]
 * @return {[type]}         [description]
 */
redis_user.refreshToken = require('./refreshToken');

/**
 * [signIn description]
 * @method
 * @param  {[type]} context  [description]
 * @param  {[type]} login    [description]
 * @param  {[type]} password [description]
 * @return {[type]}          [description]
 */
redis_user.signIn = require('./signIn');

/**
 * [signOut description]
 * @method
 * @param  {[type]} context [description]
 * @param  {[type]} id      [description]
 * @return {[type]}         [description]
 */
redis_user.signOut = require('./signOut');

/** @function
 * @see {@link authorize.getjwtoptions} */
redis_user.getJWTOptions = require('@seaturtle/gmd-plugin-authorize/authorize.js').getJWTOptions;

/** @function
 * @see {@link authorize.setupIsAuthenticated} */
redis_user.isAuthenticated = require('@seaturtle/gmd-plugin-authorize/authorize.js').setupIsAuthenticated(getByLogin);

/** @function
 * @see {@link authorize.extractFromToken} */
redis_user.extractFromToken = require('@seaturtle/gmd-plugin-authorize/authorize.js').extractFromToken;

/** @function
 * @see {@link authorize.encodeTokenFromLogin} */
redis_user.encodeTokenFromLogin = require('@seaturtle/gmd-plugin-authorize/authorize.js').encodeTokenFromLogin;

/** @member {Object} */
redis_user.helpers = {
	/**
	 * [createHelper description]
	 * @method
	 * @param  {[type]}   pipeline     [description]
	 * @param  {[type]}   context      [description]
	 * @param  {[type]}   config       [description]
	 * @param  {[type]}   birthdate    [description]
	 * @param  {[type]}   login        [description]
	 * @param  {[type]}   email        [description]
	 * @param  {[type]}   emailConfirm [description]
	 * @param  {Boolean}  isSuper      [description]
	 * @param  {[type]}   now          [description]
	 * @param  {[type]}   password     [description]
	 * @param  {[type]}   userId       [description]
	 * @param  {[type]}   }            [description]
	 * @param  {Function} cb           [description]
	 * @return {[type]}                [description]
	 */
	create: require('./createHelper'),
};

/**
 * [sendInvite description]
 * @method
 * @param  {[type]} context  [description]
 * @param  {[type]} authorId [description]
 * @param  {[type]} email    [description]
 * @param  {[type]} message  [description]
 * @return {[type]}          [description]
 */
redis_user.sendInvite = require('./sendInvite');

/**
 * [revokeInvite description]
 * @method
 * @param  {[type]} context [description]
 * @param  {[type]} id      [description]
 * @return {[type]}         [description]
 */
redis_user.revokeInvite = require('./revokeInvite');

/**
 * [register description]
 * @method
 * @param  {[type]} context   [description]
 * @param  {[type]} login     [description]
 * @param  {[type]} email     [description]
 * @param  {[type]} password  [description]
 * @param  {[type]} birthdate [description]
 * @return {[type]}           [description]
 */
redis_user.register = require('./register');

/**
 * [confirmEmail description]
 * @method
 * @param  {[type]} context      [description]
 * @param  {[type]} id           [description]
 * @param  {[type]} emailConfirm [description]
 * @return {[type]}              [description]
 */
redis_user.confirmEmail = require('./confirmEmail');

/**
 * [updateEmail description]
 * @method
 * @param  {[type]} context  [description]
 * @param  {[type]} id       [description]
 * @param  {[type]} newEmail [description]
 * @param  {[type]} newLogin [description]
 * @return {[type]}          [description]
 */
redis_user.updateEmail = require('./updateEmail');

/**
 * [updateEmailAndLogin description]
 * @method
 * @param  {[type]} context  [description]
 * @param  {[type]} id       [description]
 * @param  {[type]} newEmail [description]
 * @param  {[type]} newLogin [description]
 * @return {[type]}          [description]
 */
redis_user.updateEmailAndLogin = require('./updateEmailAndLogin');

/**
 * [updateLogin description]
 * @method
 * @param  {[type]} context  [description]
 * @param  {[type]} id       [description]
 * @param  {[type]} newLogin [description]
 * @return {[type]}          [description]
 */
redis_user.updateLogin = require('./updateLogin');

/**
 * [cleanUserData description]
 * @method
 * @param  {[type]} rawData        [description]
 * @param  {[type]} userCollection [description]
 * @return {[type]}                [description]
 */
redis_user.cleanUserData = require('./cleanUserData');

/**
 * [getByLogin description]
 * @method
 * @param  {[type]} context [description]
 * @param  {[type]} login   [description]
 * @return {[type]}         [description]
 */
redis_user.getByLogin = getByLogin;
