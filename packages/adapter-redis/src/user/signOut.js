
module.exports = (context, id) => new Promise((resolve, reject) => {
	const { deviceId } = context;

	if (!context.user) {
		return resolve();
	}

	const userId = context.super ? id : context.userId;

	if (context.userId === userId) {
		context.userId = null;
		context.user = null;
	}

	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const tokensName = userCollection.userTokens;

	return context.store.client.hget(`mdl:${name}:${userId}`, tokensName, (getTokenErr, rawTokens) => {
		if (getTokenErr) {
			return reject(getTokenErr);
		}

		let tokens = {};

		try {
			tokens = JSON.parse(rawTokens || '{}');
		} catch (parseErr) {
			console.warn('SIGN OUT WARNING:', parseErr);
			// return reject(parseErr);
		}

		delete tokens[deviceId];

		return context.store.client.hset(
			`mdl:${name}:${userId}`,
			tokensName,
			JSON.stringify(tokens),
			(setTokenErr) => {
				if (setTokenErr) {
					return reject(setTokenErr);
				}

				return resolve({ [idProperty]: userId });
			},
		);
	});
});
