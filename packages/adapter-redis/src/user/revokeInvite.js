
function revokeInvite(
	context,
	id,
) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;
		const target = { [userInviteCollection.idProperty]: id };
		// debugger;
		userInviteCollection.remove(target, {}).exec({ context, info: {} })
			.then(resolve, reject);
	});
}

module.exports = revokeInvite;
