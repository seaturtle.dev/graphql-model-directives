
function sendInvite(
	context,
	authorId,
	email,
	message,
) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;

		const authorName = userInviteCollection.belongsTo[0];
		const emailName = userInviteCollection.userEmail;

		const objId = userInviteCollection.generateDocumentId();

		// debugger;

		const beforeReject = (err) => {
			// debugger;
			reject(err);
		}

		try {
			return userInviteCollection.upsert({
				[userInviteCollection.idProperty]: objId,
				[authorName]: authorId,
				[emailName]: email,
			}, {}).exec({
				context, // : { super: true },
				info: {}
			}).then((result) => {
				// debugger;
				if (context.store.mailer) {
					return context.store.mailer
						.sendInviteEmail(context.config, email, message, `${objId.toString()}`)
						.then(() => resolve(result), beforeReject);
				}

				return resolve(result);
			}, beforeReject);
		} catch (err) {
			reject(err);
		}
	});
}

module.exports = sendInvite;
