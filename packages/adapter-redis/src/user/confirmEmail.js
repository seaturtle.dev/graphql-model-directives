
module.exports = (context, id, emailConfirm) => {
	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const emailConfirmName = userCollection.userEmailConfirm;

	return new Promise((resolve, reject) => {
		if (!emailConfirm) {
			return reject(new Error('Missing @emailConfirm argument'));
		}

		// debugger;

		return context.store.client.hget(`mdl:${name}:${id}`, emailConfirmName, (getErr, actualEmailConfirm) => {
			if (getErr) {
				return reject(getErr);
			}

			if (
				emailConfirm !== null
				&& actualEmailConfirm !== ''
				&& actualEmailConfirm !== emailConfirm
			) {
				return reject(new Error('Invalid confirmation'));
			}

			return context.store.client.hset(`mdl:${name}:${id}`, emailConfirmName, '', (setErr) => {
				if (setErr) {
					return reject(setErr);
				}

				return resolve({ [idProperty]: id });
			});
		});
	});
};
