// const crypto = require('../../../plugins/crypto');

const { encodeTokenFromLogin } = require('@seaturtle/gmd-plugin-authorize/authorize.js');

module.exports = (context, login, password) => new Promise((resolve, reject) => {
	const { crypto } = context.store;

	const { deviceId } = context;
	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const loginName = userCollection.userLogin;
	const tokenName = userCollection.userToken;
	const tokensName = userCollection.userTokens;
	const passwordName = userCollection.userPassword;
	const passwordResetName = userCollection.userPasswordReset;

	return context.store.client.get(`uqe:${name}:${loginName}:${login}`, (err, userId) => {
		if (err) {
			return reject(err);
		}

		if (!userId) {
			return reject(new Error('Invalid credentials'));
		}

		return context.store.client.hget(`mdl:${name}:${userId}`, passwordName, (getErr, secret) => {
			if (getErr) {
				return reject(getErr);
			}

			return crypto.compare(password, secret, (authErr, authed) => {
				if (authErr) {
					return reject(authErr);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return encodeTokenFromLogin(login, context.config, (tokenErr, token) => {
					if (tokenErr) {
						return reject(tokenErr);
					}

					return context.store.client.hget(`mdl:${name}:${userId}`, tokensName, (getTokenErr, rawTokens) => {
						if (getTokenErr) {
							return reject(getTokenErr);
						}

						let tokens = {};

						try {
							tokens = JSON.parse(rawTokens || '{}');
						} catch (parseErr) {
							console.warn('SIGN IN WARNING:', parseErr);
						}

						tokens[deviceId] = token;

						const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

						const finish = () => (
							context.store.client.hset(`mdl:${name}:${userId}`, passwordResetName, '', (setErr) => {
								if (setErr) {
									return reject(setErr);
								}

								return resolve({
									[idProperty]: userId,
									[loginName]: login,
									[tokensName]: tokenList,
									[tokenName]: token,
								});
							})
						);

						return context.store.client.hset(
							`mdl:${name}:${userId}`,
							tokensName,
							JSON.stringify(tokens),
							(setErr) => {
								if (setErr) {
									return reject(setErr);
								}

								return finish();
							},
						);
					});
				});
			});
		});
	});
});
