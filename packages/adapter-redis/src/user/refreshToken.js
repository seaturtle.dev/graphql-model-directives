
const { encodeTokenFromLogin } = require('@seaturtle/gmd-plugin-authorize/authorize.js');

module.exports = (context, login) => (
	new Promise((resolve, reject) => {
		const { deviceId, userId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userEmail;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;

		if (!login) {
			return reject(new Error('Invalid session'));
		}

		return encodeTokenFromLogin(login, context.config, (err, token) => {
			if (err) {
				return reject(err);
			}

			return context.store.client.hget(`mdl:User:${userId}`, tokensName, (getTokenErr, rawTokens) => {
				if (getTokenErr) {
					return reject(getTokenErr);
				}

				let tokens = {};

				try {
					tokens = JSON.parse(rawTokens || '{}');
				} catch (parseErr) {
					console.warn('REFRESH TOKEN WARNING:', parseErr);
				}

				tokens[deviceId] = token;

				return context.store.client.hset(
					`mdl:User:${userId}`,
					tokensName,
					JSON.stringify(tokens),
					(setTokenErr) => {
						if (setTokenErr) {
							return reject(setTokenErr);
						}

						const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

						return resolve({
							[idProperty]: userId,
							[loginName]: login,
							[tokensName]: tokenList,
							[tokenName]: token,
						});
					},
				);
			});
		});
	})
);
