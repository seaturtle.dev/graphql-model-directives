
const cleanUserData = require('./cleanUserData');

module.exports = (context, userId) => {
	const { userCollection } = context.store;
	const { name } = userCollection;

	return new Promise((resolve, reject) => (
		context.store.client.hgetall(`mdl:${name}:${userId}`, (err, results) => {
			if (err) {
				return reject(err);
			}

			if (!results) {
				return resolve(null);
			}

			const user = cleanUserData(results, userCollection);

			return resolve(user);
		})
	));
};
