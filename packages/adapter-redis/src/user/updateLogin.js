
const getBatch = require('../model/helpers').getBatch;

module.exports = (context, id, newLogin) => new Promise((resolve, reject) => {
	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const loginName = userCollection.userLogin;
	// const emailName = userCollection.userEmail;
	// if (emailName === loginName) {
	// 	return reject(new Error('Do not use updateLogin when @email and @login are the same.'));
	// }

	if (!newLogin) {
		return reject(new Error('Missing @login argument'));
	}

	return context.store.client.hget(`mdl:${name}:${id}`, idProperty, (err, userId) => {
		if (err) {
			return reject(err);
		}

		if (userId !== id && !context.super) {
			return reject(new Error('Not permitted'));
		}

		return context.store.client.hget(`mdl:${name}:${id}`, loginName, (getErr, login) => {
			if (getErr) {
				return reject(getErr);
			}

			if (login === newLogin) {
				return resolve({ [idProperty]: id });
			}

			const pipeline = getBatch(context.store.client);

			pipeline.del(`uqe:${name}:${loginName}:${login}`);
			pipeline.set(`uqe:${name}:${loginName}:${newLogin}`, id);

			return pipeline.exec((setErr/* , results */) => {
				if (setErr) { return reject(setErr); }

				return resolve({ [idProperty]: id });
			});
		});
	});
});
