
const { nanoid } = require('@seaturtle/gmd-core/id.js');

const getBatch = require('../model/helpers').getBatch;

module.exports = (context, id, newEmail) => new Promise((resolve, reject) => {
	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const emailConfirmName = userCollection.userEmailConfirm;

	if (!newEmail) {
		return reject(new Error('Missing @email argument'));
	}

	return context.store.client.hget(`mdl:${name}:${id}`, idProperty, (err, userId) => {
		if (err) {
			return reject(err);
		}

		if (userId !== id && !context.super) {
			return reject(new Error('Not permitted'));
		}

		return context.store.client.hget(`mdl:${name}:${id}`, emailName, (getErr, email) => {
			if (getErr) {
				return reject(getErr);
			}

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = loginName === emailName && email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			const pipeline = getBatch(context.store.client);

			pipeline.del(`uqe:${name}:${emailName}:${email}`);
			pipeline.set(`uqe:${name}:${emailName}:${newEmail}`, id);

			pipeline.hset(`mdl:${name}:${id}`, emailName, newEmail);
			pipeline.hset(`mdl:${name}:${id}`, emailConfirmName, emailConfirm);

			return pipeline.exec((setErr/* , results */) => {
				if (setErr) { return reject(setErr); }

				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			});
		});
	});
});
