
const { nanoid } = require('@seaturtle/gmd-core/id.js');

const getBatch = require('../model/helpers').getBatch;

module.exports = (context, id, newEmail, newLogin) => new Promise((resolve, reject) => {
	newLogin = newLogin || newEmail;

	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const emailConfirmName = userCollection.userEmailConfirm;

	if (!newEmail) {
		return reject(new Error('Missing @email argument'));
	}

	if (!newLogin) {
		return reject(new Error('Missing @login argument'));
	}

	return context.store.client.hget(`mdl:${name}:${id}`, idProperty, (err, userId) => {
		if (err) {
			return reject(err);
		}

		if (userId !== id && !context.super) {
			return reject(new Error('Not permitted'));
		}

		const finish = (email, login) => {
			const isSuper = login === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			const pipeline = getBatch(context.store.client);

			pipeline.del(`uqe:${name}:${loginName}:${login}`);
			pipeline.set(`uqe:${name}:${loginName}:${newLogin}`, id);

			if (loginName !== emailName) {
				pipeline.del(`uqe:${name}:${emailName}:${email}`);
				pipeline.set(`uqe:${name}:${emailName}:${newEmail}`, id);
			}

			pipeline.hset(`mdl:${name}:${id}`, emailName, newEmail);
			pipeline.hset(`mdl:${name}:${id}`, emailConfirmName, emailConfirm);

			return pipeline.exec((setErr/* , results */) => {
				if (setErr) { return reject(setErr); }

				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			});
		};

		return context.store.client.hget(`mdl:${name}:${id}`, emailName, (getEmailErr, email) => {
			if (getEmailErr) {
				return reject(getEmailErr);
			}

			if (emailName === loginName) {
				if (email === newEmail) {
					return resolve({ [idProperty]: id });
				}

				return finish(email, email);
			}

			return context.store.client.hget(
				`mdl:${name}:${id}`,
				loginName,
				(getLoginErr, login) => {
					if (getLoginErr) {
						return reject(getLoginErr);
					}

					if (login === newLogin) {
						return resolve({ [idProperty]: id });
					}

					return finish(email, login);
				},
			);
		});
	});
});
