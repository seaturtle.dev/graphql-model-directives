
const cleanUserData = require('./cleanUserData');

module.exports = (context, userId) => {
	const { userCollection } = context.store;
	const { name } = userCollection;

	const object = {};

	const scan = (cursor) => new Promise((resolve, reject) => (
		context.store.client.hscan(
			`mdl:${name}:${userId}`,
			cursor,
			// 'MATCH', `mdl:User:${userId}`,
			// 'COUNT', 100,
			(err, results) => {
				if (err) { return reject(err); }

				for (let i = 0; i < results[1].length; i += 2) {
					object[results[1][i]] = results[1][i + 1];
				}

				if (results[0] === '0') {
					const user = cleanUserData(object, userCollection);

					return resolve(user);
				}

				return scan(results[0]).then(resolve, reject);
			},
		)
	));

	return scan('0');
};
