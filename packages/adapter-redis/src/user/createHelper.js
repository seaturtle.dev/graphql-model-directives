// const crypto = require('../../../plugins/crypto');

module.exports = (
	pipeline,
	context,
	config,
	{
		birthdate,
		login,
		email,
		emailConfirm,
		isSuper,
		now,
		password,
		userId,
	},
	cb,
) => {
	const { crypto } = context.store;

	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const birthdateName = userCollection.userBirthdate;
	const passwordName = userCollection.userPassword;
	const passwordResetName = userCollection.userPasswordReset;
	const rolesName = userCollection.userRoles;
	const emailConfirmName = userCollection.userEmailConfirm;
	const tokensName = userCollection.userTokens;
	const maxInvitesName = userCollection.userMaxInvites;
	const createdAtName = userCollection.createdAt;
	const updatedAtName = userCollection.updatedAt;

	const userDocument = {
		[birthdateName]: birthdate,
		[emailName]: email,
		[passwordName]: password,
	};

	if (loginName !== emailName) {
		userDocument[loginName] = login;
	}

	const validationErrors = userCollection.validateDocument(userDocument);

	if (validationErrors) {
		return cb(validationErrors);
	}

	const tsNow = now.getTime();
	const tsBirth = birthdate.getTime();
	// debugger;

	pipeline.set(`uqe:${name}:${loginName}:${login}`, userId);
	pipeline.zadd(`idx:${name}:${updatedAtName}`, tsNow, userId);
	pipeline.hset(`mdl:${name}:${userId}`, idProperty, userId);
	pipeline.hset(`mdl:${name}:${userId}`, rolesName, JSON.stringify(isSuper ? [config.superRole] : ['user']));
	pipeline.hset(`mdl:${name}:${userId}`, birthdateName, tsBirth.toString());
	pipeline.hset(`mdl:${name}:${userId}`, createdAtName, tsNow.toString());
	pipeline.hset(`mdl:${name}:${userId}`, emailName, email);

	if (loginName !== emailName) {
		pipeline.hset(`mdl:${name}:${userId}`, loginName, login);
	}

	pipeline.hset(`mdl:${name}:${userId}`, emailConfirmName, emailConfirm);
	pipeline.hset(`mdl:${name}:${userId}`, maxInvitesName, config.defaultMaxInvites || 10);
	pipeline.hset(`mdl:${name}:${userId}`, passwordResetName, '');
	pipeline.hset(`mdl:${name}:${userId}`, tokensName, '{}');
	pipeline.hset(`mdl:${name}:${userId}`, updatedAtName, tsNow.toString());

	return crypto.hash(password, 10, (hashErr, hash) => {
		if (hashErr) {
			return cb(hashErr);
		}

		pipeline.hset(`mdl:${name}:${userId}`, passwordName, hash);

		return cb(null, pipeline);
	});
};
