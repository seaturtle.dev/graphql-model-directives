
module.exports = (rawData, userCollection) => {
	const user = {};

	const tokensName = userCollection.userTokens;
	const birthdateName = userCollection.userBirthdate;
	const rolesName = userCollection.userRoles;

	Object.keys(rawData).forEach((property) => {
		let value = rawData[property];
		if (typeof value === 'string' && (property === tokensName || property === rolesName)) {
			try {
				value = value ? JSON.parse(value) : null;
			} catch (err) {
				console.warn('CLEAN USER DATA WARNING:', property, value, typeof value, err);
				value = {};
			}
		}

		if (property === birthdateName || property === 'created' || property === 'updated') {
			value = new Date(value);
		}

		if (value !== null || value !== undefined) {
			user[property] = value;
		}
	});

	return user;
};
