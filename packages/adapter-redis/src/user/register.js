const { nanoid } = require('@seaturtle/gmd-core/id.js');

const createHelper = require('./createHelper');
const refreshToken = require('./refreshToken');

const getBatch = require('../model/helpers').getBatch;

module.exports = (
	context,
	login,
	email,
	password,
	birthdate,
	invitation,
) => new Promise((resolve, reject) => {
	const { userCollection, userInviteCollection } = context.store;
	const { name, idProperty } = userCollection;

	const loginName = userCollection.userLogin;

	const userId = userCollection.generateDocumentId();

	const isSuper = email === context.config.superLogin;

	const now = new Date();

	const invitesName = userInviteCollection.name;

	function saveUser() {
		return context.store.client.exists(`uqe:${name}:${loginName}:${login}`, (err, existing) => {
			if (err) {
				return reject(err);
			}

			if (existing) {
				return reject(new Error('Exists'));
			}

			const emailConfirm = isSuper ? null : nanoid();

			return context.store.client.smembers(`mdl:${name}:${invitesName}:${email}`, (inviteErr, inviteMembers) => {
				if (inviteErr) {
					return reject(inviteErr);
				}

				const pipeline = getBatch(context.store.client);

				// TODO: I think this out of date?
				inviteMembers.forEach((authorUserId) => {
					pipeline.hdel(`mdl:${name}:${invitesName}:${authorUserId}`, email);
					pipeline.srem(`mdl:${name}:${invitesName}:${email}`, authorUserId);
					pipeline.zrem(`idx:${name}:${invitesName}:${authorUserId}`, email);
				});

				return createHelper(
					pipeline,
					context,
					context.config,
					{
						birthdate,
						login,
						email,
						emailConfirm,
						isSuper,
						now,
						password,
						userId,
					},
					(createErr) => {
						if (createErr) {
							return reject(createErr);
						}

						return pipeline.exec((setErr/* , results */) => {
							if (setErr) {
								return reject(setErr);
							}

							context.userId = userId;
							context.user = {
								[idProperty]: userId,
								[loginName]: login,
							};

							context.super = isSuper || context.super || false;

							const finish = () => (
								refreshToken(context, login).then((result) => {
									resolve(result);
								}, reject)
							);

							if (context.store.mailer) {
								return context.store.mailer
									.sendConfirmEmail(context.config, email, `${userId}/${emailConfirm}`)
									.then(finish, reject);
							}

							return finish();
						});
					},
				);
			});
		});
	}

	// console.log('GOT HERE', context.config.inviteOnly, isSuper, context.super, context.user);
	if (context.config.inviteOnly && (!isSuper && !context.super)) {
		// return context.store.client.scard(`mdl:${name}:_${invitesName}:${email}`, (err, amount) => {
		if (invitation) {
			return userInviteCollection.read({
				[userInviteCollection.idProperty]: invitation,
			}, {}).exec({ context, info: {} }).then((invite) => {
				if (invite && invite[userInviteCollection.idProperty] === invitation) {
					return saveUser();
				}

				return reject(new Error('Not invited'));
			}, reject);
		}
		return userInviteCollection.count({
			[userInviteCollection.userEamil]: email,
		}, {}).exec({ context, info: {} }).then((amount) => {
			if (amount > 0) {
				return saveUser();
			}

			return reject(new Error('Not invited'));
		}, reject);
	}

	return saveUser();
});
