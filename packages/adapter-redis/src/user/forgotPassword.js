
const { nanoid } = require('@seaturtle/gmd-core/id.js');

module.exports = (context, email) => {
	const { userCollection } = context.store;
	const { name } = userCollection;

	const emailName = userCollection.userEmail;
	const passwordResetName = userCollection.userPasswordReset;

	return new Promise((resolve, reject) => (
		context.store.client.get(`uqe:${name}:${emailName}:${email}`, (err, userId) => {
			if (err) {
				return reject(err);
			}

			if (userId) {
				const passwordReset = nanoid();

				const result = {
					[emailName]: email,
					[userCollection.idProperty]: userId,
				};

				const finish = (setErr) => {
					if (setErr) {
						return reject(setErr);
					}

					if (context.store.mailer) {
						return context.store.mailer
							.sendForgotPassword(context.config, email, `%{userId}/${passwordReset}`)
							.then(() => resolve(result), reject);
					}

					return resolve(result);
				};

				return context.store.client.hset(`mdl:${name}:${userId}`, passwordResetName, passwordReset, finish);
			}

			return resolve({ [emailName]: email, [userCollection.idProperty]: '' });
		})
	));
};
