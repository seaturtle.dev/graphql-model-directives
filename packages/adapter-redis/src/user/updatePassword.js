// const crypto = require('../../../plugins/crypto');

const { encodeTokenFromLogin } = require('@seaturtle/gmd-plugin-authorize/authorize.js');

const getBatch = require('../model/helpers').getBatch;

module.exports = (
	context,
	userId,
	oldPassword,
	newPassword,
) => new Promise((resolve, reject) => {
	const { crypto } = context.store;

	const { deviceId } = context;

	const { userCollection } = context.store;
	const { name, idProperty } = userCollection;

	const loginName = userCollection.userLogin;
	const tokenName = userCollection.userToken;
	const tokensName = userCollection.userTokens;
	const passwordName = userCollection.userPassword;

	if (!userId) {
		return reject(new Error('Invalid user'));
	}

	return context.store.client.hget(`mdl:${name}:${userId}`, passwordName, (getErr, password) => {
		if (getErr) {
			return reject(getErr);
		}

		return crypto.compare(oldPassword, password, (authErr, authed) => {
			if (authErr) {
				return reject(authErr);
			}

			if (!authed) {
				return reject(new Error('Invalid credentials'));
			}

			const pipeline = getBatch(context.store.client);

			return crypto.hash(newPassword, 10, (hashErr, hash) => {
				if (hashErr) {
					return reject(hashErr);
				}

				pipeline.hset(`mdl:${name}:${userId}`, passwordName, hash);

				return pipeline.exec((setErr/* , results */) => {
					if (setErr) {
						return reject(setErr);
					}

					return context.store.client.hget(`mdl:${name}:${userId}`, loginName, (loginErr, login) => {
						if (loginErr) {
							return reject(loginErr);
						}

						return encodeTokenFromLogin(login, context.config, (encodeTokenErr, token) => {
							if (encodeTokenErr) {
								return reject(encodeTokenErr);
							}

							return context.store.client.hget(`mdl:${name}:${userId}`, tokensName, (getTokenErr, rawTokens) => {
								if (getTokenErr) {
									return reject(getTokenErr);
								}

								let tokens = {};

								try {
									tokens = JSON.parse(rawTokens || '{}');
								} catch (parseErr) {
									console.warn('UPDATE PASSWORD WARNING:', parseErr);
								}

								tokens[deviceId] = token;

								return context.store.client.hset(
									`mdl:${name}:${userId}`,
									tokensName,
									JSON.stringify(tokens),
									(setTokenErr) => {
										if (setTokenErr) {
											return reject(setTokenErr);
										}

										const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

										return resolve({
											[idProperty]: userId,
											[tokensName]: tokenList,
											[tokenName]: token,
										});
									},
								);
							});
						});
					});
				});
			});
		});
	});
});
