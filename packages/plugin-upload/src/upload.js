
const fs = require('fs');
const path = require('path');
const { nanoid } = require('nanoid');

// const aws = require('./aws.js');

// const config = require('./config.js');

module.exports = exports = (parentConfig) => {
	const { uploaderType } = parentConfig;

	const uploader = {};

	uploader.recall = async function recallFile(fileUrl, stream, callback) {
		let name = fileUrl;
		if (typeof fileUrl === 'string') {
			name = path.basename(fileUrl);
		}
		if (typeof name !== 'string') {
			return callback();
		}
		if (!stream) {
			fileUrl = `${parentConfig.uploaderPublicPath}/${name}`;
			return callback(null, fileUrl);
		}
		try {
			return callback(
				null,
				await new Promise((resolve, reject) => {
					const filepath = path.join(parentConfig.uploaderLocalPath, name);
					// fs.stat(filepath, (err/* , stats */) => {
					// 	if (err) {
					// 		return reject(err);
					// 	}
					const readStream = fs.createReadStream(filepath);
					return resolve(readStream);
					// });
				})
			);
		} catch (err) {
			return callback(err);
		}
	};

	uploader.upload = async function uploadFile(fileUpload, id, name, callback) {
		if (!fileUpload) {
			return callback();
		}

		const file = await fileUpload;

		if (!file) {
			return callback();
		}

		const { filename, mimetype, createReadStream } = file;

		// TODO: CHECK FILE SIZE
		if (!mimetype) {
			return callback(new Error('no mimetype'));
		}

		if (typeof name === 'string' && name.indexOf('/') !== -1) {
			name = path.basename(name);
		}
		id = id || nanoid();
		name = name || `${id}-${filename.toLowerCase()}`;
		// TODO: delete old file or archive it somehow so that it can be deleted to save monies

		let fileUrl = null;

		// if (uploaderType === 'aws-s3') {
		// 	const s3 = new aws.S3();
		//
		// 	const params = {
		// 		Bucket: config.uploaderS3Bucket,
		// 		Key: name,
		// 		ACL: 'public-read',
		// 		Body: createReadStream(),
		// 	};
		//
		// 	// TODO: wait for stream to finish
		// 	await new Promise((resolve) => setTimeout(resolve, 3000));
		//
		// 	try {
		// 		await new Promise((resolve, reject) => (
		// 			s3.putObject(params, (perr/* , pres */) => {
		// 				if (perr) {
		// 					console.log('Error uploading data: ', perr);
		// 					return reject(new Error('Upload error'));
		// 				}
		// 				const location = `${config.uploaderS3Bucket}/${name}`;
		// 				console.log(`Successfully uploaded data to ${location}`);
		// 				fileUrl = `${config.uploaderS3PublicURL}/${name}`;
		// 				// TODO: add aws_public_s3 in the client
		// 				return resolve(
		// 					// config.uploaderS3PublicURL + location
		// 				);
		// 			})
		// 		));
		//
		// 		return callback(null, fileUrl);
		// 	} catch (err) {
		// 		return callback(err);
		// 	}
		// }

		// debugger;
		const readStream = createReadStream();
		const writeStream = fs.createWriteStream(path.join(parentConfig.uploaderLocalPath, name));
		readStream.pipe(writeStream);

		// TODO: path.join?, actually no because this is for a web url mostly
		// fileUrl = `${parentConfig.uploaderPublicPath}/${name}`;

		// console.log('WROTE FILE', fileUrl);
		// TODO: add uploader_local in the client

		return callback(null, name);
	};

	uploader.remove = async function removeFile(fileUrl, callback) {
		let name = fileUrl;
		if (typeof fileUrl === 'string') {
			name = path.basename(fileUrl);
		}

		if (typeof name !== 'string') {
			return callback();
		}
		// if (uploaderType === 'aws-s3') {
		// 	const s3 = new aws.S3();
		//
		// 	const params = {
		// 		Bucket: config.uploaderS3Bucket,
		// 		Key: name,
		// 	};
		//
		// 	try {
		// 		await new Promise((resolve, reject) => {
		// 			s3.deleteObject(params, (perr/* , pres */) => {
		// 				if (perr) {
		// 					console.log('Error deleting data: ', perr);
		// 					return reject(new Error('Delete file error'));
		// 				}
		// 				console.log(`Successfully deleted file: ${fileUrl}`);
		// 				return resolve();
		// 			});
		// 		});
		//
		// 		return callback();
		// 	} catch (err) {
		// 		return callback(err);
		// 	}
		// }

		// debugger;
		try {
			await new Promise((resolve, reject) => {
				const filepath = path.join(parentConfig.uploaderLocalPath, name);
				fs.stat(filepath, (err/* , stats */) => {
					if (err) {
						return reject(err);
					}
					return fs.unlink(filepath, (unlinkErr) => {
						if (unlinkErr) {
							return reject(unlinkErr);
						}
						return resolve();
					});
				});
			});

			return callback();
		} catch (err) {
			return callback(err);
		}
	};

	return uploader;
}
