// const nodemailer = require('nodemailer');

// const aws = require('./aws');
// const config = require('./config');

// console.log(config);
module.exports = function (parentConfig, nodemailer, aws) {
	const { mailerType, mailerLog, mailerSMTP/* , mailerAWS */ } = parentConfig;

	/** @exports mailer */
	const mailer = {};

	function send(message) {
		let transporter;

		// console.log(message);
		return Promise.resolve({
			message: message,
		});

		if (mailerType === 'aws-ses') {
			transporter = nodemailer.createTransport({
				SES: new aws.SES({ apiVersion: '2010-12-01' }),
			});
		} else {
			if (mailerLog) { console.log('MAIL:\n', message); }
			transporter = nodemailer.createTransport(mailerSMTP);
		}

		return new Promise((resolve, reject) => {
			transporter.sendMail(message, (err, res) => {
				if (err) {
					if (err.errno === 'ECONNREFUSED' && mailerType === 'smtp' && mailerLog) {
						console.warn(err);
						resolve(res);
						return;
					}
					reject(err);
				} else {
					resolve(res);
				}
			});
		});
	}

	/** @method */
	mailer.send = send;

	/** @method */
	mailer.sendConfirmEmail = require('./email/sendConfirmEmail')(send);

	/** @method */
	mailer.sendForgotPassword = require('./email/sendForgotPassword')(send);

	/** @method */
	mailer.sendInviteEmail = require('./email/sendInviteEmail')(send);

	return mailer;
}
