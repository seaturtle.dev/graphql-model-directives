module.exports = function createSendConfirmEmail(send) {
	return function sendConfirmEmail(config, to, link) {
		link = `${config.linkConfirmEmail}/${link}`;
		const from = config.noreply;
		const text = `Dear User,
An account has been connected to this email. Please confirm you're a user by opening the following link:

${link}

-Your friendly Email Bot
`;
		return send({
			from,
			to,
			subject: 'Confirm Email',
			text,
		});
	};
};
