module.exports = function createSendForgotPassword(send) {
	return function sendForgotPassword(config, to, link) {
		link = `${config.linkForgotPassword}/${link}`;
		const from = config.noreply;
		const text = `Dear User,
A password reset request has been issued, open the following link to change your password:

${link}

-Your friendly Email Bot
`;
		return send({
			from,
			to,
			subject: 'Reset Password',
			text,
		});
	};
};
