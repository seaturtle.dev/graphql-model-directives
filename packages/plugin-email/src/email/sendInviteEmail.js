module.exports = function createSendInviteEmail(send) {
	return function sendInviteEmail(config, to, message, link) {
		link = `${config.linkInviteEmail}/${link}`;
		const from = config.noreply;
		const text = `Hello,
Someone has invited you to join App, please open the following link if you're inteterested:

${link}

-Your friendly Email Bot
${message ? `\n\n"${message}"\n\n-App User` : ''}
`;
		return send({
			from,
			to,
			subject: 'Invite',
			text,
		});
	};
};
