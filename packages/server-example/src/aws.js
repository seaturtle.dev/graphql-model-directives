/* eslint global-require:1 */

let aws = null;

try {
	aws = require('aws-sdk');
} catch (err) {
	console.warn('`aws-sdk` is not available.');
}

const config = require('./config');

// configure AWS SDK
if (config.awsPath) {
	aws.config.loadFromPath(config.awsPath);
}

module.exports = aws;
