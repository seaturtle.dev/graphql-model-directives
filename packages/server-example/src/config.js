const path = require('path');

/** @exports config */
const config = exports;

/** @member */
config.adminToken = 'admin';

/** @member */
config.adminEmail = 'admin@local';

/** @member */
config.superLogin = 'admin@local';

/** @member */
config.superRole = 'super';

/** @member */
config.strictAuth = false;

/** @member */
config.inviteOnly = false;

/** @member */
config.defaultMaxInvites = 10;

/** @member */
config.jwtAudience = '';

/** @member */
config.jwtIssuer = '';

/** @member */
config.jwtHmacSecret = 'secret';

/** @member */
config.awsPath = null;

/** @member */
config.uploaderType = 'file';

/** @member */
config.uploaderS3Bucket = 'uploads';

/** @member */
config.uploaderS3PublicURL = '';

/** @member */
config.uploaderLocalPath = path.join(__dirname, 'uploads');

/** @member */
config.uploaderPublicPath = '/uploads';

/** @member */
config.noreply = 'noreply@local';

/** @private */
const linkHost = 'http://localhost:3000';

/** @member */
config.linkConfirmEmail = `${linkHost}/confirmEmail`;

/** @member */
config.linkForgetPassword = `${linkHost}/forgetPassword`;

/** @member */
config.linkInviteEmail = `${linkHost}/inviteEmail`;

/** @member */
config.sqlStoragePath = null;

/** @member */
config.mailerType = 'smtp'; // or aws-ses

/** @member */
config.mailerSMTP = {};

/** @member */
config.mailerAWS = null;

/** @member */
config.mailerLog = true;

/** @member */
config.uploader = require('./config/uploader')(config);

/** @member */
config.mailer = require('./config/mailer')(config);

/** @member */
config.crypto = require('./config/crypto')(config);
