/** @exports templates */
const templates = exports;

templates.adminAllUsers = `query {
  adminAllUsers {
    page {
      id
      emailAddr
      emailConfirmation
      pass
      passReset
      birth
    }
  }
}`;

templates.adminAllInvites = `query {
  adminAllInvites {
    page {
      id
      emailAddr
      user { id, name }
    }
  }
}`;

templates.adminAllResources = `query {
  adminAllResources {
    page {
      id
      authorName
      author {
        id
        name
      }
      description
      isDone
    }
  }
}`;

templates.myId = `query {
  me {
    id
    emailAddr
  }
}`;

templates.myPassword = `query {
  me {
    id
    pass
    passReset
  }
}`;

templates.myResources = `query {
  me {
    resources {
      page
    }
  }
}`;

// page {
// 	id
// 	authorName
// 	author {
// 		id
// 		name
// 	}
// 	description
// 	isDone
// 	fileUrl
// 	file { url }
// }

templates.myPendingResources = `query {
  me {
    pendingResources {
      page {
        id
        authorName
        author {
          id
          name
        }
        description
        isDone
        fileUrl
        file { url }
      }
    }
  }
}`;

templates.myFinishedResources = `query {
  me {
    finishedResources {
      page {
        id
        authorName
        author {
          id
          name
        }
        description
        isDone
        fileUrl
        file { url }
      }
    }
  }
}`;

templates.resource = `query resource($id: ID!) {
  resource(id: $id) {
    id
    authorName
    author {
      id
      name
    }
    description
    isDone
    fileUrl
    file { url }
  }
}`;

templates.resourcesCount = `query resourcesCount($page: SortedPageInput, $filter: JSON) {
  resourcesCount(page: $page, filter: $filter)
}`;

templates.purgeResources = `mutation purgeResources($filter: JSON) {
  purgeResources(filter: $filter)
}`;

const resourcesTemplate = (fields) => `query resources($page: SortedPageInput, $sort: SortInput, $filter: JSON) {
  resources(page: $page, sort: $sort, filter: $filter) {
    id
    offset
    count
    skipped
    page {
      id
      authorName
      author {
        id
        name
      }
      boolean
      created
      updated
      name
      description
      fileUrl
      file { url }
      date
      finished
      float
      integer
      json
      string
      isDone
      isPublic
      ${fields || ''}
    }
  }
}`;

templates.resources = resourcesTemplate();

templates.resourcesWithNested = resourcesTemplate(`
      nestedResources { id, updated, name, description, isDone }
`);

templates.resourcesWithRelations = resourcesTemplate(`
      childResources { id, updated, name, description, isDone }
      parentResource { id, updated, name, description, isDone }
`);

templates.finishedResources = `query {
  finishedResources {
    page {
      id
      author
      description
      isDone
    }
  }
}`;

templates.pendingResources = `query {
  pendingResources {
    page {
      id
      author {
        id
      }
      description
      isDone
    }
  }
}`;

templates.user = `query user($id: ID!) {
  user(id: $id) {
    id
    emailAddr
    emailConfirmation
    pass
    passReset
  }
}`;

templates.users = `query {
  users {
    page {
      id
      emailAddr
      emailConfirmation
      pass
      passReset
      birth
    }
  }
}`;

templates.invite = `query invite($id: ID!) {
  userInvite(id: $id) {
    id
    emailAddr
    user { id }
  }
}`;

templates.invites = `query {
  userInvites {
    page {
      id
      emailAddr
      user { id }
    }
  }
}`;

templates.invitesCount = `query userInvitesCount($page: ScannedPageInput, $filter: JSON) {
  userInvitesCount(page: $page, filter: $filter)
}`;

templates.purgeInvites = `mutation purgeUserInvites($filter: JSON) {
  purgeUserInvites(filter: $filter)
}`;

templates.removeUser = `mutation removeUser($id: ID!) {
  removeUser(id: $id) {
    id
  }
}`;

templates.registerUser = `mutation registerUser($emailAddr: String!, $birth: Date!, $pass: String!) {
  registerUser(emailAddr: $emailAddr, birth: $birth, pass: $pass) {
    id
    emailAddr
    tokenList
    birth
  }
}`;

templates.revokeInvite = `mutation revokeUserInvite($id: ID!) {
  revokeUserInvite(id: $id) {
    id
  }
}`;

templates.sendInvite = `mutation sendUserInvite($emailAddr: String!, $message: String) {
  sendUserInvite(emailAddr: $emailAddr, message: $message) {
    id
    emailAddr
    user { id }
  }
}`;

templates.signIn = `mutation signIn($emailAddr: String!, $pass: String!) {
  signIn(emailAddr: $emailAddr, pass: $pass) {
    id
    tokenList
  }
}`;

templates.signOut = `mutation {
  signOut {
    id
  }
}`;

templates.refreshToken = `mutation {
  refreshToken {
    id
    tokenList
    emailAddr
  }
}`;

templates.forgotPassword = `mutation forgotPassword($emailAddr: String!) {
  forgotPassword(emailAddr: $emailAddr) {
    id
  }
}`;

templates.resetPassword = `mutation resetPassword($emailAddr: String, $recoveryToken: String, $newPassword: String!) {
  resetPassword(emailAddr: $emailAddr, recoveryToken: $recoveryToken, newPassword: $newPassword) {
    id
  }
}`;

templates.updatePassword = `mutation updatePassword($oldPassword: String, $newPassword: String!) {
  updatePassword(oldPassword: $oldPassword, newPassword: $newPassword) {
    id
  }
}`;

templates.confirmEmail = `mutation confirmEmail($id: ID, $emailConfirmation: String!) {
  confirmEmail(id: $id, emailConfirmation: $emailConfirmation) {
    id
  }
}`;

templates.updateLogin = `mutation updateLogin($id: ID, $emailAddr: String!) {
  updateLogin(id: $id, emailAddr: $emailAddr) {
    id
  }
}`;

templates.updateEmail = `mutation updateEmail($id: ID, $emailAddr: String!) {
  updateEmail(id: $id, emailAddr: $emailAddr) {
    id
  }
}`;

templates.updateEmailAndLogin = `mutation updateEmailAndLogin($id: ID, $emailAddr: String!) {
  updateEmailAndLogin(id: $id, emailAddr: $emailAddr) {
    id
  }
}`;

templates.upsertUser = `mutation upsertUser($data: JSON) {
  upsertUser(data: $data) {
    id
    name
    birth
    emailAddr
    emailConfirmation
    pass
    passReset
  }
}`;

templates.upsertResource = `mutation upsertResource($data: JSON) {
  upsertResource(data: $data) {
    id
    authorName
    author {
      id
      name
    }
    boolean
    created
    updated
    name
    description
    fileUrl
    file { url }
    date
    finished
    float
    integer
    json
    string
    isDone
    isPublic
    # childResources { id, updated, name, description, isDone }
    # nestedResources { id, updated, name, description, isDone }
    # parentResource { id, updated, name, description, isDone }
    # permissions { id, created, user { id }, resource { id } }
  }
}`;

templates.removeResource = `mutation removeResource($id: ID!) {
  removeResource(id: $id) {
    id
  }
}`;

templates.subscribeCreatedResources = `subscription subscribeCreatedResources($filter: JSON) {
  resourceCreated(filter: $filter) {
    id
    author { id }
    description
    isDone
  }
}
`;

templates.subscribeUpdatedResources = `subscription subscribeUpdatedResources($filter: JSON) {
  resourceUpdated(filter: $filter) {
    id
    author { id }
    description
    isDone
  }
}
`;

templates.subscribeRemovedResources = `subscription subscribeRemovedResources($filter: JSON) {
  resourceRemoved(filter: $filter) {
    id
  }
}`;

templates.upsertResourceFile = `mutation upsertResourceFile($file: Upload) {
  upsertResourceFile(file: $file) {
    id
  }
}`;
