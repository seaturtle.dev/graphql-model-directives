/* eslint import/no-extraneous-dependencies:0 */
require('fake-indexeddb/auto');

// const { ApolloServer } = require('apollo-server-express');

const express = require('express');
const Fastify = require('fastify');

const fs = require('fs');
const path = require('path');
const Redis = require('ioredis');

const expressCors = require('cors');
const expressBodyParser = require('body-parser');

const fastifyCors = require('@fastify/cors');
const fastifyFormBody = require('@fastify/formbody');
const fastifyStatic = require('@fastify/static');

const {
	ApolloApiFastify: ApolloApiFastifyPromise,
	ApolloApiExpress: ApolloApiExpressPromise,
	// ApolloApiServer,
} = require('@seaturtle/gmd-core/servers.js');

const MemoryStore = require('@seaturtle/gmd-adapter-memory/StoreAdapter.js');
const MongoDBStore = require('@seaturtle/gmd-adapter-mongodb/StoreAdapter.js');
const RedisStore = require('@seaturtle/gmd-adapter-redis/StoreAdapter.js');
const SequelStore = require('@seaturtle/gmd-adapter-sequel/StoreAdapter.js');
const DataStore = require('@seaturtle/gmd-adapter-data/StoreAdapter.js');
const DexieStore = require('@seaturtle/gmd-adapter-dexie/StoreAdapter.js');

const resolvers = require('./resolvers');
const config = require('./config');

const schemaPath = path.join(__dirname, 'schema.graphql');
const rawSchema = fs.readFileSync(schemaPath);

// console.log('ENV', process.env);

const host = '127.0.0.1';

const uploadsDir = config.uploaderLocalPath || path.join(__dirname, 'uploads');

function expressWrapper(config) {
	const app = express(config);
	app.use('/uploads', express.static(uploadsDir, {
		redirect: true,
	}));
	return app;
}

function fastifyWrapper(config) {
	const app = Fastify(config);
	app.register(fastifyStatic, {
		root: uploadsDir,
		prefix: '/uploads/',
	});
	return app;
}

async function createMongoDBServer(c) {
	let url = `mongodb://${host}:27017/example`;
	if (process.env.MONGO_URI) {
		url = process.env.MONGO_URI;
	} else if (process.env.MONGO_PORT) {
		url = process.env.MONGO_PORT.replace('tcp', 'mongodb');
	}
	// console.log('MongoDB URL:', url);
	const mongodbStore = new MongoDBStore(
		url,
		{ resolvers },
	);

	const ApolloApiExpress = await ApolloApiExpressPromise;

	if (!ApolloApiExpress.extraMiddleware) {
		ApolloApiExpress.installMiddleware({ bodyParser: expressBodyParser, cors: expressCors });
	}

	const serverMongoDB = new ApolloApiExpress({
		store: mongodbStore,
		rawSchema,
		config: { ...config, ...c },
		resolvers,
	}, expressWrapper);

	return serverMongoDB;
}

async function createRedisServer(c) {
	let url = `redis://${host}:6379/0`;
	if (process.env.REDIS_URI) {
		url = process.env.REDIS_URI;
	} else if (process.env.REDIS_PORT) {
		url = process.env.REDIS_PORT.replace('tcp', 'redis');
	}
	// console.log('Redis URL:', url);
	const redisStore = new RedisStore(
		Redis.createClient(url),
		{
			resolvers,
			redisPubSubConnection: url,
		},
	);

	const ApolloApiExpress = await ApolloApiExpressPromise;

	if (!ApolloApiExpress.extraMiddleware) {
		ApolloApiExpress.installMiddleware({ bodyParser: expressBodyParser, cors: expressCors });
	}

	const serverRedis = new ApolloApiExpress({
		store: redisStore,
		rawSchema,
		config: { ...config, ...c },
		resolvers,
	}, expressWrapper);

	return serverRedis;
}

async function createMemoryServer(c) {
	const memStore = new MemoryStore(null, {
		resolvers,
	});

	const ApolloApiFastify = await ApolloApiFastifyPromise;

	if (!ApolloApiFastify.extraMiddleware) {
		ApolloApiFastify.installMiddleware({ bodyParser: fastifyFormBody, cors: fastifyCors });
	}

	const serverMemory = new ApolloApiFastify({
		store: memStore,
		rawSchema,
		config: { ...config, ...c },
		resolvers,
	}, fastifyWrapper);

	return serverMemory;
}

async function createDataServer(c) {
	let data = null;
	const dataStore = new DataStore({
		resolvers,
		// Very basic, keep data safe in memory
		setData: (d) => {
			if (typeof d === 'function') {
				data = {
					...data,
					...d(data),
				};
			} else {
				data = {
					...data,
					...d,
				};
			}
		},
		getData: () => data,
	});

	const ApolloApiExpress = await ApolloApiExpressPromise;

	if (!ApolloApiExpress.extraMiddleware) {
		ApolloApiExpress.installMiddleware({ bodyParser: expressBodyParser, cors: expressCors });
	}

	const serverData = new ApolloApiExpress({
		store: dataStore,
		rawSchema,
		config: { ...config, ...c },
		resolvers,
	}, expressWrapper);

	return serverData;
}

async function createDexieServer(c) {
	const dexieStore = new DexieStore({
		resolvers,
		// pubsub,
		// db,
	});

	const ApolloApiFastify = await ApolloApiFastifyPromise;

	if (!ApolloApiFastify.extraMiddleware) {
		ApolloApiFastify.installMiddleware({ bodyParser: fastifyFormBody, cors: fastifyCors });
	}

	const serverData = new ApolloApiFastify({
		store: dexieStore,
		rawSchema,
		config: { ...config, ...c },
		resolvers,
	}, fastifyWrapper);

	return serverData;
}

async function createSequelServer(c) {
	const seqStore = new SequelStore(
		{
			dialect: 'sqlite',
			storage: (c && c.sqlStoragePath)
				|| path.join(__dirname, '..', 'database.sqlite'),
		},
		{ resolvers },
	);

	const ApolloApiFastify = await ApolloApiFastifyPromise;
	// const ApolloApiExpress = await ApolloApiExpressPromise;

	if (!ApolloApiFastify.extraMiddleware) {
		ApolloApiFastify.installMiddleware({ bodyParser: fastifyFormBody, cors: fastifyCors });
	}
	// if (!ApolloApiExpress.extraMiddleware) {
	// 	ApolloApiExpress.installMiddleware({ bodyParser: expressBodyParser, cors: expressCors });
	// }

	const serverSequel = new ApolloApiFastify({
	// const serverSequel = new ApolloApiExpress({
		store: seqStore,
		rawSchema,
		config: { ...config, ...c },
		resolvers,
	}, fastifyWrapper);
	// }, expressWrapper);

	return serverSequel;
}

/** @exports server */
const server = exports;

/** @method */
server.mongodb = createMongoDBServer;

/** @method */
server.redis = createRedisServer;

/** @method */
server.memory = createMemoryServer;

/** @method */
server.sequel = createSequelServer;

/** @method */
server.data = createDataServer;

/** @method */
server.dexie = createDexieServer;

if (require.main === module) {
	createMongoDBServer().then((server) => server.start(host, 3433));

	createRedisServer().then((server) => server.start(host, 3334));

	createMemoryServer().then((server) => server.start(host, 3434));

	createSequelServer().then((server) => server.start(host, 3444));

	createDataServer().then((server) => server.start(host, 3737));

	createDexieServer().then((server) => server.start(host, 3939));
}
