const {
	// data: createServer,
	// mongodb: createServer,
	// redis: createServer,
	memory: createServer,
	// sequel: createServer,
	// dexie: createServer,
} = require('./servers.js');

const host = '127.0.0.1';

createServer().then((server) => server.start(host, 2324));
