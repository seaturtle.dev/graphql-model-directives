const nodemailer = require('nodemailer');

let aws = null;

try {
	aws = require('../aws');
} catch (err) {}

// const config = require('./config');

// console.log(config);
module.exports = (parentConfig) => {
	const mailer = require('@seaturtle/gmd-plugin-email/email.js')(parentConfig, nodemailer, aws);

	/** @method */
	mailer.sendConfirmEmail = require('@seaturtle/gmd-plugin-email/email/sendConfirmEmail.js')(mailer.send);

	/** @method */
	mailer.sendForgotPassword = require('@seaturtle/gmd-plugin-email/email/sendForgotPassword.js')(mailer.send);

	/** @method */
	mailer.sendInviteEmail = require('@seaturtle/gmd-plugin-email/email/sendInviteEmail.js')(mailer.send);

	return mailer;
}
