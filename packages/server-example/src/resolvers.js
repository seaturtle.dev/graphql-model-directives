const moment = require('moment');

module.exports = {
	User: {
		age: (obj) => {
			const birth = obj.birth || this.birth;
			if (birth) {
				return moment(birth).diff(moment(), 'years');
			}
			return 0;
		},
	},
	// Subscription: {},
};
