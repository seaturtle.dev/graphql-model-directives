# Server Example

A basic example of how to use `graphql-model-directives` to describe a `User` model, which has many of the `Resource` model.

It runs several different servers that each connect to one of the supported backends. As of writing this: `Redis`, `Memory`, `MongoDB`, and `SQLite3`.

Each server runs on locally on a different port. See `server.js`.
