/* eslint global-require:0 */

// const { EventEmitter } = require('events');

const model = require('./model');
const user = require('./user');
const migration = require('./migration');

let mongoose = null;
try {
	mongoose = require('mongoose');
} catch (err) {
	// console.warn('mongoose not installed');
}

const StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');
const MongoDBCollection = require('./Collection');

/**
* Stores document collections in MongoDB using `mongoose`.
* @exports MongoDBStore
* @class
* @extends StoreAdapter */
class MongoDBStore extends StoreAdapter {
	/**
	 * [throwMongooseUnavailable description]
	 * @return {[type]} [description]
	 */
	throwMongooseUnavailable() {
		/* istanbul ignore next */
		throw new Error('NPM install mongoose, mongoose is not available.');
	}

	/**
	 * [constructor description]
	 * @param {[type]} mongoUrl [description]
	 * @param {[type]} options  [description]
	 * @constructs
	 */
	constructor(mongoUrl, options) {
		super(options && options.resolvers);
		/* istanbul ignore next */
		try {
			mongoose = mongoose || require('mongoose')
		} catch (err) {
			console.warn('mongoose not installed');
		}
		if (!mongoose) { this.throwMongooseUnavailable(); }

		this.connect = mongoose.connect(mongoUrl || 'mongodb://localhost:27017/api', {});

		this.crypto = (options && options.crypto) || this.crypto;
		this.pubsub = (options && options.pubsub) || this.pubsub;

		this.client = mongoose;
	}

	/**
	 * [postSchema description]
	 * @return {[type]} [description]
	 */
	postSchema() {
		/* istanbul ignore next */
		if (!mongoose) { this.throwMongooseUnavailable(); }

		const collectionNames = Object.keys(this.models);

		this.mongo = {};

		collectionNames.forEach((collectionName) => {
			const collection = this.models[collectionName];
			this.mongo[collectionName] = collection.toMongoose(mongoose);
		});
	}

	/**
	 * [close description]
	 * @param  {Function} cb [description]
	 * @return {[type]}      [description]
	 */
	close(cb) {
		this.client.disconnect(cb);
	}

	/**
	 * [mixinUser description]
	 * @param  {[type]} collection [description]
	 * @return {[type]}            [description]
	 */
	mixinUser(collection) {
		/* istanbul ignore next */
		if (!user) { this.throwMongooseUnavailable(); }

		return super.mixinUser(collection, user, model);
	}

	/**
	 * [authenticate description]
	 * @param  {[type]}   context [description]
	 * @param  {[type]}   info    [description]
	 * @param  {Function} next    [description]
	 * @param  {[type]}   or      [description]
	 * @param  {[type]}   roles   required user roles for to authenticate
	 * @return {[type]}           [description]
	 */
	authenticate(context, info, next, or, roles = []) {
		/* istanbul ignore next */
		if (!user) { this.throwMongooseUnavailable(); }

		return new Promise((resolve, reject) => {
			const onAuthError = (err) => {
				if (or === true) { return resolve(next(err)); }
				if (typeof or === 'function') { return resolve(or(err, next)); }
				return reject(err);
			};
			return user.isAuthenticated(roles, context, info).then(() => (
				Promise.resolve(next()).then(resolve, reject)
			), onAuthError);
		});
	}

	/**
	 * [readDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	readDocument({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.read(this, name, target, options, context, info);
	}

	/**
	 * [findDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	findDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.find(this, name, target, options, context, info);
	}

	/**
	 * [countDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	countDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.count(this, name, target, options, context, info);
	}

	/**
	 * [scanDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	scanDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.scan(this, name, target, options, context, info);
	}

	/**
	 * [purgeDocuments description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	purgeDocuments({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.purge(this, name, target, options, context, info);
	}

	/**
	 * [upsertDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	upsertDocument({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.upsert(this, name, target, options, context, info);
	}

	/**
	 * [removeDocument description]
	 * @param  {[type]} name    [description]
	 * @param  {[type]} target  [description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} context [description]
	 * @param  {[type]} info    [description]
	 * @return {[type]}         [description]
	 */
	removeDocument({ name }, { target, options }, { context, info }) {
		/* istanbul ignore next */
		if (!model) { this.throwMongooseUnavailable(); }

		return model.remove(this, name, target, options, context, info);
	}

	/**
	 * [migration description]
	 * @param  {[type]} channel [description]
	 * @return {[type]}         [description]
	 */
	migration(object, options) {
		return migration(object, options);
	}

	// /**
	//  * [undoMigration description]
	//  * @param  {[type]} channel [description]
	//  * @return {[type]}         [description]
	//  */
	// undoMigration(migrations) {
	// 	return Promise.resolve();
	// }
}

MongoDBStore.Collection = MongoDBCollection;

module.exports = MongoDBStore;
