/* eslint no-unused-vars:0 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

/**
* MongoDB Model Implementation
* @exports mongodb_model */
const mongodb_model = exports;

function count(store, name, target, options, context, info) {
	function countPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		return new Promise((resolve, reject) => (
			model.countDocuments(target).then((count) => {
				return resolve(count);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, countPromise);
	// }

	return countPromise();
}

/**
* [count description]
* @method
* @param  {[type]} store   [description]
* @param  {[type]} name    [description]
* @param  {[type]} target  [description]
* @param  {[type]} options [description]
* @param  {[type]} context [description]
* @param  {[type]} info    [description]
* @return {[type]}         [description]
*/
mongodb_model.count = count;

function find(store, name, target, options, context, info) {
	function findPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const offset = parseInt(options.offset || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const fields = options.fields || null;

		if (fields && collection.nonNulls && collection.nonNulls.length) {
			collection.nonNulls.forEach((nonNullField) => {
				if (fields.indexOf(nonNullField) === -1) {
					fields.push(nonNullField);
				}
			});
		}

		const sort = {};

		if (options.sort && options.sort.fields && options.sort.fields.length) {
			/* eslint no-nested-ternary: 1 */
			const dir = options.sort.ascending ? 1 : options.sort.descending ? -1 : 0;
			if (dir) { options.sort.fields.forEach((f) => { sort[f] = dir; }); }
		}

		const mongoOpts = {
			lean: fields !== null,
			skip: offset,
			limit: count,
			sort,
		};

		// target = collection.mapQueryOperators(target);

		return new Promise((resolve, reject) => (
			model.find(target, fields, mongoOpts).then((results) => {
				return collection.resolvePagedRelations(resolve, reject, {
					offset,
					count,
					page: collection.filterSecureDataForUser(results, context, target)
						.map((result) => {
							result = collection.convertToGraphQL(result);
							result = collection.redactSecureDataForUser(result, context);

							return result;
						}),
				}, context, info);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, findPromise);
	// }

	return findPromise();
}

/**
 * [find description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.find = find;

function nextPage(store, name, target, options, context, info) {
	function nextPagePromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const offset = parseInt(options.offset || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const mongoOpts = {
			skip: offset,
			limit: count,
			sort: options.sort,
		};

		return new Promise((resolve, reject) => (
			model.find(target, null, options).count().then((count) => {
				return resolve(count > 0);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, nextPagePromise);
	// }

	return nextPagePromise();
}

/**
 * [nextPage description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.find.nextPage = nextPage;

function read(store, name, target, options, context, info) {
	function readPromise() {
		const collection = store.collection(name);
		const { model, idProperty } = collection;
		const id = target[idProperty];

		return new Promise((resolve, reject) => {
			delete target[idProperty];
			if (id) {
				target._id = new store.client.Types.ObjectId(id);
			}

			model.findOne(target).then((result) => {
				result = collection.convertToGraphQL(result);
				result[idProperty] = `${result[idProperty]}`;
				// console.log('GOT HERE', id, result, typeof result[idProperty]);

				if (!collection.hasPermission(result, context)) {
					return resolve(null);
				}

				result = collection.redactSecureDataForUser(result, context);

				return collection.resolveDocumentRelations(resolve, reject, result, context, info);
			}, reject);
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, readPromise);
	// }

	return readPromise();
}

/**
 * [read description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.read = read;

function remove(store, name, target, options, context, info) {
	function removePromise() {
		return new Promise((resolve, reject) => {
			const collection = store.collection(name);
			const { idProperty, model } = collection;
			const id = target[idProperty];

			return collection.prepareForDestruction(target, context, info).then(() => {
				delete target[idProperty];
				if (id) {
					target._id = new store.client.Types.ObjectId(id);
				}

				return model.deleteOne(target).then(
					(result) => {
						const doc = { [idProperty]: id.toString() };
						collection.emit('removed', doc);
						return resolve(doc);
					},
					reject
				);
			});
		});
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, removePromise);
	// }

	return removePromise();
}

/**
 * [remove description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.remove = remove;

function scan(store, name, target, options, context, info) {
	function scanPromise() {
		const collection = store.collection(name);
		const { model } = collection;

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const fields = options.fields || null;

		if (fields && collection.nonNulls && collection.nonNulls.length) {
			collection.nonNulls.forEach((nonNullField) => {
				if (fields.indexOf(nonNullField) === -1) {
					fields.push(nonNullField);
				}
			});
		}

		const mongoOpts = {
			lean: fields !== null,
			skip: cursor,
			limit: count,
			sort: options.sort,
		};

		return new Promise((resolve, reject) => (
			model.find(target, fields, mongoOpts).then((results) => {
				return collection.resolvePagedRelations(resolve, reject, {
					cursor: count === results.length ? cursor + count : 0,
					count,
					page: collection.filterSecureDataForUser(results, context, target)
						.map((result) => {
							result = collection.convertToGraphQL(result);
							result = collection.redactSecureDataForUser(result, context);

							return result;
						}),
				}, context, info);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, scanPromise);
	// }

	return scanPromise();
}

/**
 * [scan description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.scan = scan;

function purge(store, name, target, options, context, info) {
	function purgePromise() {
		const collection = store.collection(name);
		const { model, idProperty } = collection;

		// TODO: DRY from find, and maybe scan
		const sort = {};
		if (options.sort && options.sort.fields && options.sort.fields.length) {
			/* eslint no-nested-ternary: 1 */
			const dir = options.sort.ascending ? 1 : options.sort.descending ? -1 : 0;
			if (dir) { options.sort.fields.forEach((f) => { sort[f] = dir; }); }
		}

		const cursor = parseInt(options.cursor || 0, 10);
		const count = parseInt(options.count || 100, 10);

		const fields = [idProperty];

		const mongoOpts = {
			lean: true,
			skip: cursor,
			limit: count,
			sort: sort,
		};

		return new Promise((resolve, reject) => (
			model.find(target, fields, mongoOpts).then((results) => {
				const ids = results.map(doc => doc[idProperty]);
				// TODO: emit removed events
				return model.deleteMany({ [idProperty]: {$in: ids}, }, {}).then((results) => {
					if (!results || results.deletedCount !== ids.length) { return reject(new Error('not ok')); }
					return resolve(results.deletedCount);
				}, reject);
			}, reject)
		));
	}

	// if (info.parentType.name === 'Query' && info.returnType.auth) {
	// 	return store.authenticate(context, info, purgePromise);
	// }

	return purgePromise();
}

/**
 * [purge description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.purge = purge;

function upsert(store, name, target, options, context, info) {
	const collection = store.collection(name);
	const { model } = collection;

	const { id, document, create } = collection.convertToStore(context, target);

	const originalTargetId = target[collection.idProperty];
	delete target[collection.idProperty];

	const _id = new store.client.Types.ObjectId(id);
	document._id = _id;

	return new Promise((resolve, reject) => {
		// TODO:
		const [relatedPromiseTriggers, relatedError] = (
			collection.gatherRelatedPromiseTriggers(document, context, info)
		);

		if (relatedError) {
			// console.warn(relatedError);
			return reject(relatedError);
		}

		const validationErrors = collection.validateDocument(document);

		if (validationErrors) {
			return reject(validationErrors);
		}

		let d = null;
		let tries = 0;

		const finish = (err, doc) => {
			/* eslint no-use-before-define:1 */
			if (err) {
				if (err.code === 11000) {
					const keys = Object.keys(err.keyPattern);
					// Another upsert occurred during the upsert, try again. You could omit the
					// upsert option here if you don't ever delete docs while this is running.
					if (keys.length === 1 && keys[0] === '_id' && tries <= 5) {
						tries += 1;
						return setTimeout(() => (
							saveDoc(d).resolve((doc) => finish(null, doc), finish)
						), tries * 50);
					}
				}
				return reject(err);
			}

			const resolveDoc = (doc) => {
				doc = collection.convertToGraphQL(doc);
				doc = collection.redactSecureDataForUser(doc, context);

				collection.emit(originalTargetId === id ? 'updated' : 'created', doc);

				return resolve(doc);
			};

			if (relatedPromiseTriggers.length) {
				const relatedPromises = relatedPromiseTriggers.map((trigger) => trigger(doc));
				return Promise.all(relatedPromises).then(() => resolveDoc(doc), reject);
			}

			return resolveDoc(doc);
		};

		const saveDoc = (doc) => {
			d = doc;

			const fields = options.fields || null;

			if (fields && collection.nonNulls && collection.nonNulls.length) {
				collection.nonNulls.forEach((nonNullField) => {
					if (fields.indexOf(nonNullField) === -1) {
						fields.push(nonNullField);
					}
				});
			}
			// debugger;

			return model.findOneAndUpdate(
				{ _id },
				doc,
				{
					new: true,
					lean: fields !== null,
					upsert: true,
					fields,
					sort: options.sort,
					// returnNewDocument: true,
					useFindAndModify: false,
				},
				// finish,
			).then((doc) => finish(null, doc), finish);
		};

		return collection.prepareForStorage(document).then(saveDoc, reject);
	});
}

/**
 * [upsert description]
 * @method
 * @param  {[type]} store   [description]
 * @param  {[type]} name    [description]
 * @param  {[type]} target  [description]
 * @param  {[type]} options [description]
 * @param  {[type]} context [description]
 * @param  {[type]} info    [description]
 * @return {[type]}         [description]
 */
mongodb_model.upsert = upsert;
