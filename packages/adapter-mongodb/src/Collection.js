/* eslint global-require:0 */

const Collection = require('@seaturtle/gmd-adapter-generic/Collection.js');

/**
* MongoDB Collection
* @exports MongoDBCollection
* @class
* @extends Collection */
class MongoDBCollection extends Collection {
	/**
	 * [toMongoose description]
	 * @param  {[type]} mongoose [description]
	 * @return {[type]}          [description]
	 */
	toMongoose(mongoose) {
		mongoose = mongoose || require('mongoose');

		this.generateDocumentId = () => new mongoose.Types.ObjectId();

		const mongooseTypeMap = {
			Array,
			Boolean,
			Buffer,
			Decimal128: mongoose.Decimal128,
			Date,
			DateTime: Date,
			Float: Number,
			ID: mongoose.ObjectId,
			Int: Number,
			JSON: Object,
			Map,
			Object,
			ObjectId: mongoose.ObjectId,
			String,
		};

		const mongooseFields = {};
		const mongooseOptions = {};

		// const compoundUniques = [];
		// const compoundIndexes = [];

		this.fields.forEach((field/* , index */) => {
			const property = this.properties[field];

			if (this.virtuals.indexOf(field) !== -1) {
				return;
			}

			const fieldType = (
				property.modelType
				|| property.field.type.toString().replace(/!/g, '')
			);

			let type;

			if (fieldType.charAt(0) === '[') {
				type = [];

				const arrayOf = fieldType.substring(1, fieldType.length - 1);

				if (mongooseTypeMap[arrayOf]) {
					type.push(mongooseTypeMap[arrayOf]);
				} else if (property.relation) {
					type.push(mongooseTypeMap.ObjectId);
				} else {
					type.push(mongooseTypeMap.Object);
				}
			} else if (mongooseTypeMap[fieldType]) {
				type = mongooseTypeMap[fieldType];
			} else {
				type = mongooseTypeMap.Object;
			}

			const schemaField = {
				type,
			};

			// TODO: mongo indexes
			/*
			if (property.uniques) {
				property.uniques.forEach((unique) => {
					if (!unique.compounds || !unique.compounds.length) {
						schemaField.unique = true;
					} else {
						compoundUniques.push(property);
					}
				});
			}

			if (property.sortedIndexes) {
				property.sortedIndex.forEach((sortedIndex) => {
					if (!sortedIndex.compounds || !sortedIndex.compounds.length) {
						schemaField.index = true;
						schemaField.sparse = true;
					} else {
						compoundIndexes.push(property);
					}
				});
			}
			*/

			mongooseFields[field] = schemaField;
		});

		const mongooseSchema = new mongoose.Schema(mongooseFields, mongooseOptions);

		/*
		compoundUniques.forEach((compoundUnique) => {
			const params = {};
			params[compoundUnique.name] = 1;
			if (compoundUnique.compounds) {
				compoundUnique.compounds.forEach((c) => {
					params[c] = 1;
				});
			}
			mongooseSchema.index(params, { unique: true });
		});

		compoundIndexes.forEach((compoundIndex) => {
			const params = {};
			let { dir } = compoundIndex;
			dir = dir || 1;
			params[compoundIndex.name] = dir;
			if (compoundIndex.compounds) {
				compoundIndex.compounds.forEach((c) => {
					params[c] = dir;
				});
			}
			mongooseSchema.index(params);
		});
		*/

		const model = (mongoose || this.store.client).model(this.name, mongooseSchema);

		this.model = model;

		return model;
	}

	/**
	 * [definePropertyUserTokens description]
	 * @param  {[type]} field [description]
	 * @return {[type]}       [description]
	 */
	definePropertyUserTokens(field) {
		super.definePropertyUserTokens(field);
		return this.definePropertyModelType(field, 'Object');
	}

	/**
	 * [prepareForStorage description]
	 * @method
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	prepareForStorage(document, iterator) {
		return super.prepareForStorage(document, (field, property, promises) => {
			if (field === this.idProperty) {
				delete document[field];
			}
			if (typeof iterator === 'function') {
				iterator(field, property, promises, document);
			}
		});
	}

	/**
	 * [convertToStore description]
	 * @param  {[type]} context  [description]
	 * @param  {[type]} params   [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToStore(context, params, iterator) {
		return super.convertToStore(context, params, (field, property, value, document, id) => {
			/* eslint no-underscore-dangle:0 */
			if (field === this.idProperty) {
				document._id = value;
			} else if (property.relation && !document[field]) {
				delete document[field];
			}
			// if (property.relation) {
			// 	console.log('MONGODB', field, property, value, document, id);
			// }

			if (typeof iterator === 'function') {
				iterator(field, property, value, document, id);
			}
		});
	}

	/**
	 * [convertToGraphQL description]
	 * @param  {[type]} document [description]
	 * @param  {[type]} iterator [description]
	 * @return {[type]}          [description]
	 */
	convertToGraphQL(document, iterator) {
		if (document && document._id) {
			document.id = document._id.toString();
			delete document._id;
		}
		return super.convertToGraphQL(document, (field, property, value, doc) => {
			if (property.relation) {
				const related = this.store.collection(
					property.relation.modelName,
				);

				if (!property.relation.many) {
					if (doc[field] && doc[field][related.idProperty]) {
						doc[field][related.idProperty] = doc[field][related.idProperty].toString('hex');
					} else {
						doc[field] = { [related.idProperty]: '' };
					}
				} else {
					doc[field] = (doc[field] || []).map((id) => ({
						[related.idProperty]: id ? id.toString('hex') : '',
					}));
				}
			}

			if (typeof iterator === 'function') {
				iterator(field, property, value, doc);
			}
		});
	}

	/**
	 * [mapQueryOperators description]
	 * @method
	 * @param  {[type]}  target               [description]
	 * @return {[type]}                       [description]
	 */
	mapQueryOperators(target) {
		/*
		const recursivelyMapTargetOperations = (scope) => {
			const newScope = {};
			Object.keys(scope).forEach((key) => {
				let value = scope[key];
				if (value && typeof value === 'object') {
					if (Array.isArray(value)) {
						value = value.map(recursivelyMapTargetOperations);
					} else {
						value = recursivelyMapTargetOperations(value);
					}
				}
				let isOp = false;
				if (key.charAt(0) === '$') {
					const operationName = key.substr(1);
					if (this.op[operationName]) {
						isOp = true;
						newScope[this.op[operationName]] = value;
					}
					// TODO: check aliases and handle compatability
				}
				if (!isOp) {
					newScope[key] = value;
				}
			});
			return newScope;
		};
		return recursivelyMapTargetOperations(target);
		 */
		return target;
	}
}

module.exports = MongoDBCollection;
