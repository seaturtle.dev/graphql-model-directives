/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

// const crypto = require('../../plugins/crypto');
const { nanoid } = require('@seaturtle/gmd-core/id.js');

/**
* MongoDB User Implementation
* @exports mongodb_user */
const mongodb_user = exports;

const {
	getJWTOptions,
	extractFromToken,
	encodeTokenFromLogin,
	setupIsAuthenticated,
} = require('@seaturtle/gmd-plugin-authorize/authorize.js');

function createHelper(
	context,
	config,
	{
		birthdate,
		login,
		email,
		emailConfirm,
		isSuper,
		now,
		password,
		userId,
	},
	cb,
) {
	const { crypto } = context.store;
	const { userCollection } = context.store;

	const emailName = userCollection.userEmail;
	const loginName = userCollection.userLogin;
	const birthdateName = userCollection.userBirthdate;
	const passwordName = userCollection.userPassword;
	const passwordResetName = userCollection.userPasswordReset;
	const rolesName = userCollection.userRoles;
	const emailConfirmName = userCollection.userEmailConfirm;
	const tokensName = userCollection.userTokens;
	const maxInvitesName = userCollection.userMaxInvites;
	const createdAtName = userCollection.createdAt;
	const updatedAtName = userCollection.updatedAt;

	const User = userCollection.model;

	return crypto.hash(password, 10, (hashErr, hash) => {
		if (hashErr) {
			return cb(hashErr);
		}

		const record = {
			_id: userId.toString(),
			[birthdateName]: birthdate,
			[emailName]: email,
			[emailConfirmName]: emailConfirm,
			[rolesName]: isSuper ? [config.superRole] : [],
			[updatedAtName]: now,
			[createdAtName]: now,
			[tokensName]: {},
			[maxInvitesName]: config.defaultMaxInvites,
			[passwordName]: password,
			[passwordResetName]: '',
		};

		if (loginName !== emailName) {
			record[loginName] = login;
		}

		const validationErrors = userCollection.validateDocument(record);

		if (validationErrors) {
			return cb(validationErrors);
		}

		record[passwordName] = hash;

		return User.create(record).then((data) => cb(null, data), cb);
	});
}

function forgotPassword(context, email) {
	const { userCollection } = context.store;

	const emailName = userCollection.userEmail;
	const passwordResetName = userCollection.userPasswordReset;

	const User = userCollection.model;

	return new Promise((resolve, reject) => (
		User.findOne({ [emailName]: email }, '_id', { lean: true }).then((user) => {
			const userId = user._id;
			if (userId) {
				const passwordReset = nanoid();

				const result = { [emailName]: email, [userCollection.idProperty]: userId };

				const finish = () => {
					if (context.store.mailer) {
						return context.store.mailer
							.sendForgotPassword(context.config, email, `${userId}/${passwordReset}`)
							.then(() => resolve(result), reject);
					}

					return resolve(result);
				};

				return User.updateOne(
					{ _id: userId },
					{ $set: { [passwordResetName]: passwordReset } },
				).then(finish, reject);
			}

			return resolve({ [emailName]: email, [userCollection.idProperty]: '' });
		}, reject)
	));
}

mongodb_user.forgotPassword = forgotPassword;

function resetPassword(
	context,
	email,
	recoveryToken,
	newPassword,
) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;
		const passwordResetName = userCollection.userPasswordReset;

		const User = userCollection.model;

		return User.findOne({ [userCollection.userEmail]: email }, ['_id', passwordResetName], { lean: true }).then((user) => {
			const userId = user._id;

			if (!userId) {
				return reject(new Error('Invalid credentials'));
			}

			const passwordReset = user[passwordResetName];

			if (recoveryToken === passwordReset) {
				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(hashErr);
					}
					return User.updateOne({ _id: userId }, {
						$set: {
							[passwordResetName]: '',
							[passwordName]: hash,
						},
					}).then(() => {
						const finish = (encodeTokenErr, token) => {
							if (encodeTokenErr) {
								return reject(encodeTokenErr);
							}

							return User.findOneAndUpdate(
								{ _id: userId },
								{ $set: { [`${tokensName}.${deviceId}`]: token } },
								{
									new: true,
									fields: [tokensName],
									lean: true,
									useFindAndModify: false,
								}
							).then((tokenWrapper) => {
								const tokens = tokenWrapper[tokensName];
								const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

								return resolve({
									[idProperty]: userId.toString(),
									[tokensName]: tokenList,
									[tokenName]: token,
								});
							}, reject);
						};
						return mongodb_user.encodeTokenFromLogin(email, context.config, finish);
					}, reject);
				});
			}

			return reject(new Error('Invalid credentials'));
		}, reject);
	});
}

mongodb_user.resetPassword = resetPassword;

function updatePassword(
	context,
	userId,
	oldPassword,
	newPassword,
) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;

		const User = userCollection.model;

		if (!userId) {
			return reject(new Error('Invalid user'));
		}

		const objId = new store.client.Types.ObjectId(userId);

		return User.findOne({ _id: objId }, ['_id', passwordName, loginName], { lean: true }).then((user) => {
			const password = user[passwordName];
			const login = user[loginName];

			return crypto.compare(oldPassword, password, (authErr, authed) => {
				if (authErr) {
					return reject(authErr);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return crypto.hash(newPassword, 10, (hashErr, hash) => {
					if (hashErr) {
						return reject(hashErr);
					}

					return User.updateOne(
						{ _id: userId },
						{ $set: { [passwordName]: hash } },
					).then(() => {
						return mongodb_user.encodeTokenFromLogin(
							login,
							context.config,
							(tokenErr, token) => {
								if (tokenErr) {
									return reject(tokenErr);
								}
								return User.findOneAndUpdate(
									{ _id: userId },
									{ $set: { [`${tokensName}.${deviceId}`]: token } },
									{
										new: true,
										fields: [tokensName],
										lean: true,
										useFindAndModify: false,
									},
								).then((tokenWrapper) => {
									const tokens = tokenWrapper[tokensName];
									const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

									return resolve({
										[idProperty]: userId.toString(),
										[tokensName]: tokenList,
										[tokenName]: token,
									});
								}
							);
						}, reject);
					}, reject);
				});
			});
		}, reject);
	});
}

mongodb_user.updatePassword = updatePassword;

function revokeInvite(context, id) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;
		const { idProperty } = userInviteCollection;

		const UserInvite = userInviteCollection.model;

		return UserInvite.deleteOne({ _id: id }).then(() => {
			return resolve({ [idProperty]: id.toString() });
		}, reject);
	});
}

mongodb_user.revokeInvite = revokeInvite;

function sendInvite(context, authorId, email, message) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userInviteCollection } = store;

		const authorName = userInviteCollection.belongsTo[0];
		const emailName = userInviteCollection.userEmail;
		const UserInvite = userInviteCollection.model;

		const objId = userInviteCollection.generateDocumentId();

		return UserInvite.create({
			_id: objId,
			[authorName]: authorId,
			[emailName]: email,
		}).then((result) => {
			if (userInviteCollection.permission && !userInviteCollection.hasPermission(result, context)) {
				return resolve(null);
			}

			result = userInviteCollection.convertToGraphQL(result);
			result = userInviteCollection.redactSecureDataForUser(result, context);

			const finish = () => resolve(result);
			// TODO: why does this hang?, but tests pass without it.
			// const finish = () => (
			// 	collection.resolveDocumentRelations(resolve, reject, result, context, info)
			// )

			if (context.store.mailer) {
				return context.store.mailer
					.sendInviteEmail(context.config, email, message, `${objId.toString()}`)
					.then(finish, reject);
			}

			return resolve(finish);
		}, reject);
	});
}

mongodb_user.sendInvite = sendInvite;

function signIn(context, login, password) {
	return new Promise((resolve, reject) => {
		const { crypto } = context.store;
		const { deviceId, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;
		const passwordName = userCollection.userPassword;

		const User = userCollection.model;

		return User.findOne({ [loginName]: login }, ['_id', passwordName], { lean: true }).then((user) => {
			if (!user) {
				return reject(new Error('Invalid credentials'));
			}

			const userId = user._id;

			if (!userId) {
				return reject(new Error('Invalid credentials'));
			}

			const secret = user[passwordName];

			return crypto.compare(password, secret, (authErr, authed) => {
				if (authErr) {
					return reject(authErr);
				}

				if (!authed) {
					return reject(new Error('Invalid credentials'));
				}

				return encodeTokenFromLogin(login, context.config, (tokenErr, token) => {
					if (tokenErr) {
						return reject(tokenErr);
					}

					return User.findOneAndUpdate(
						{ _id: userId },
						{ $set: { [`${tokensName}.${deviceId}`]: token } },
						{
							new: true,
							fields: [tokensName],
							lean: true,
							useFindAndModify: false,
						}
					).then((tokenWrapper) => {
						const tokens = tokenWrapper[tokensName];
						const tokenList = Object.keys(tokens).map((id) => `${id}:${tokens[id]}`);

						return resolve({
							[idProperty]: userId.toString(),
							[loginName]: login,
							[tokensName]: tokenList,
							[tokenName]: token,
						});
					}, reject);
				});
			});
		}, reject);
	});
}

mongodb_user.signIn = signIn;

function signOut(context, id) {
	return new Promise((resolve, reject) => {
		const { deviceId, user, store } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const tokensName = userCollection.userTokens;

		const User = userCollection.model;

		if (!user) {
			return resolve();
		}

		const userId = context.super ? id : context.userId;

		if (context.userId === userId) {
			context.userId = null;
			context.user = null;
		}

		return User.findOneAndUpdate(
			{ _id: new store.client.Types.ObjectId(userId) },
			{ $unset: { [`${tokensName}.${deviceId}`]: '' } },
			{
				new: true,
				fields: [tokensName],
				lean: true,
				useFindAndModify: false,
			}
		).then(() => {
			return resolve({ [idProperty]: userId.toString() });
		}, reject);
	});
}

mongodb_user.signOut = signOut;

function cleanUserData(rawData, userCollection) {
	const user = {};

	Object.keys(rawData).forEach((property) => {
		let value = rawData[property];

		if (property === '_id') {
			user[userCollection.idProperty] = rawData._id.toString();
		}

		if (property === 'birthdate' || property === 'created' || property === 'updated') {
			value = new Date(value);
		}

		if (value !== null || value !== undefined) {
			user[property] = value;
		}
	});

	return user;
}

mongodb_user.cleanUserData = cleanUserData;

function getByLogin(context, login) {
	const { userCollection } = context.store;

	const loginName = userCollection.userLogin;

	const User = userCollection.model;

	return new Promise((resolve, reject) => (
		User.findOne({ [loginName]: login }, '', { lean: true }).then((user) => {
			if (!user) {
				return resolve(null);
			}

			const cleanUser = cleanUserData(user, userCollection);

			return resolve(cleanUser);
		}, reject)
	));
}

mongodb_user.getByLogin = getByLogin;

mongodb_user.helpers = {
	create: createHelper,
};

function refreshToken(context, login) {
	return new Promise((resolve, reject) => {
		const { deviceId, store, userId } = context;
		const { userCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		const tokenName = userCollection.userToken;
		const tokensName = userCollection.userTokens;

		const User = userCollection.model;

		if (!login) {
			return reject(new Error('Invalid session'));
		}

		return encodeTokenFromLogin(login, context.config, (err, token) => {
			if (err) {
				return reject(err);
			}
			try {
			return User.findOneAndUpdate(
				{ _id: new store.client.Types.ObjectId(userId) },
				{ $set: { [`${tokensName}.${deviceId}`]: token } },
				{
					new: true,
					fields: ['_id', loginName, tokensName],
					lean: true,
					useFindAndModify: false,
				},
			).then((user) => {
				const tokenList = Object.keys(user[tokensName]).map(
					(id) => `${id}:${user[tokensName][id]}`,
				);

				return resolve({
					[idProperty]: user._id.toString(),
					[loginName]: login,
					[tokensName]: tokenList,
					[tokenName]: token,
				});
			}, reject);
		} catch (err) {reject(err)}
		});
	});
}

mongodb_user.refreshToken = refreshToken;

function register(
	context,
	login,
	email,
	password,
	birthdate,
	invitation,
) {
	return new Promise((resolve, reject) => {
		const { store } = context;
		const { userCollection, userInviteCollection } = store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;

		const UserInvite = userInviteCollection.model;

		const User = userCollection.model;

		const userId = new store.client.Types.ObjectId();

		const invitationId = invitation ? new store.client.Types.ObjectId(invitation) : null;

		const isSuper = email === context.config.superLogin;

		const now = new Date();

		function saveUser() {
			return User.countDocuments({ [loginName]: login }).then((amount) => {
				if (amount > 0) {
					return reject(new Error('Exists'));
				}

				const emailConfirm = isSuper ? null : nanoid();

				const inviteEmailName = userInviteCollection.userEmail;

				return UserInvite.deleteMany({ [inviteEmailName]: email }).then(() => {
					function createUser() {
						return createHelper(
							context,
							context.config,
							{
								birthdate,
								login,
								email,
								emailConfirm,
								isSuper,
								now,
								password,
								userId,
							},
							(setErr/* , results */) => {
								if (setErr) {
									return reject(setErr);
								}

								context.userId = userId;

								context.user = {
									[idProperty]: userId.toString(),
									[loginName]: login,
								};

								context.super = isSuper || context.super || false;

								const finish = () => (
									refreshToken(context, login).then(resolve, reject)
								);

								userCollection.emit('created', { [idProperty]: userId });

								if (context.store.mailer) {
									return context.store.mailer
										.sendConfirmEmail(context.config, email, `${userId}/${emailConfirm}`)
										.then(finish, reject);
								}

								return finish();
							},
						);
					}

					if (invitationId) {
						return UserInvite.deleteOne(
							{ _id: invitationId },
						).then(() => {
							return createUser();
						}, reject);
					}

					return createUser();
				}, reject);
			}, reject);
		}

		if (context.config.inviteOnly && (!isSuper && !context.super)) {
			return UserInvite.countDocuments(invitationId ? { _id: invitationId } : {
				[userInviteCollection.userEmail]: email,
			}).then((amount) => {
				if (amount > 0) {
					return saveUser();
				}

				return reject(new Error('Not invited'));
			}, reject);
		}

		return saveUser();
	});
}

mongodb_user.register = register;

function confirmEmail(context, id, emailConfirm) {
	const { userCollection } = context.store;
	const { idProperty } = userCollection;

	const emailConfirmName = userCollection.userEmailConfirm;

	const User = userCollection.model;

	return new Promise((resolve, reject) => {
		if (!emailConfirm) {
			return reject(new Error('Missing @emailConfirm argument'));
		}

		const objId = new context.store.client.Types.ObjectId(id);

		return User.findOne({ _id: objId }, ['_id', emailConfirmName], { lean: true }).then((user) => {
			if (!user) {
				return reject(new Error('Invalid confirmation'));
			}

			const actualEmailConfirm = user[emailConfirmName];

			if (
				emailConfirm !== null
				&& actualEmailConfirm !== ''
				&& actualEmailConfirm !== emailConfirm
			) {
				return reject(new Error('Invalid confirmation'));
			}

			return User.updateOne({ _id: objId }, { $set: { [emailConfirmName]: '' } }).then((setErr) => {
				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

mongodb_user.confirmEmail = confirmEmail;

function updateEmail(context, id, newEmail) {
	return new Promise((resolve, reject) => {
		const { userCollection } = context.store;
		const { idProperty } = userCollection;

		const emailName = userCollection.userEmail;
		const loginName = userCollection.userLogin;
		const emailConfirmName = userCollection.userEmailConfirm;

		const User = userCollection.model;

		if (!newEmail) {
			return reject(new Error('Missing @email argument'));
		}

		const objId = new context.store.client.Types.ObjectId(id);

		return User.findOne({ _id: objId }, ['_id', emailName], { lean: true }).then((user) => {
			const userId = user._id.toString();

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const { email } = user;

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = loginName === emailName && email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			return User.updateOne({ _id: objId }, {
				$set: {
					[emailName]: newEmail,
					[emailConfirmName]: emailConfirm,
				},
			}).then((setErr) => {
				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

mongodb_user.updateEmail = updateEmail;

function updateEmailAndLogin(context, id, newEmail, newLogin) {
	return new Promise((resolve, reject) => {
		newLogin = newLogin || newEmail;

		const { userCollection } = context.store;
		const { idProperty } = userCollection;

		const emailName = userCollection.userEmail;
		const loginName = userCollection.userLogin;
		const emailConfirmName = userCollection.userEmailConfirm;

		const User = userCollection.model;

		if (!newEmail) {
			return reject(new Error('Missing @email argument'));
		}

		if (!newLogin) {
			return reject(new Error('Missing @login argument'));
		}

		const objId = new context.store.client.Types.ObjectId(id);

		return User.findOne(
			{ _id: objId },
			loginName === emailName
				? ['_id', loginName]
				: ['_id', emailName, loginName],
			{ lean: true },
		).then((user) => {
			const userId = user._id.toString();

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const { email } = user;

			if (email === newEmail) {
				return resolve({ [idProperty]: id });
			}

			const isSuper = email === context.config.superLogin;

			const emailConfirm = isSuper ? null : nanoid();

			const changes = {
				[emailName]: newEmail,
				[emailConfirmName]: emailConfirm,
			};

			if (!loginName !== emailName) {
				changes[loginName] = newLogin;
			}

			return User.updateOne({ _id: objId }, {
				$set: changes,
			}).then(() => {
				if (isSuper) {
					return resolve({ [idProperty]: id });
				}

				if (context.store.mailer) {
					return context.store.mailer
						.sendConfirmEmail(context.config, newEmail, `${userId}/${emailConfirm}`)
						.then(() => resolve({ [idProperty]: id }), reject);
				}

				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

mongodb_user.updateEmailAndLogin = updateEmailAndLogin;

function updateLogin(context, id, newLogin) {
	return new Promise((resolve, reject) => {
		const { userCollection } = context.store;
		const { idProperty } = userCollection;

		const loginName = userCollection.userLogin;
		// const emailName = userCollection.userEmail;
		// if (emailName === loginName) {
		// 	return reject(new Error('Do not use updateLogin when @email and @login are the same.'));
		// }

		const User = userCollection.model;

		if (!newLogin) {
			return reject(new Error('Missing @login argument'));
		}

		const objId = new context.store.client.Types.ObjectId(id);

		return User.findOne({ _id: objId }, ['_id', loginName], { lean: true }).then((user) => {
			const userId = user._id.toString();

			if (userId !== id && !context.super) {
				return reject(new Error('Not permitted'));
			}

			const login = user[loginName];

			if (login === newLogin) {
				return resolve({ [idProperty]: id });
			}

			return User.updateOne({ _id: objId }, {
				$set: {
					[loginName]: newLogin,
				},
			}).then(() => {
				return resolve({ [idProperty]: id });
			}, reject);
		}, reject);
	});
}

mongodb_user.updateLogin = updateLogin;

mongodb_user.getJWTOptions = getJWTOptions;

mongodb_user.isAuthenticated = setupIsAuthenticated(mongodb_user.getByLogin);

mongodb_user.extractFromToken = extractFromToken;

mongodb_user.encodeTokenFromLogin = encodeTokenFromLogin;
