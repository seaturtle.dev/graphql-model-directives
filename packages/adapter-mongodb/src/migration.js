/* eslint no-unused-vars:0 no-shadow:0 */
/* eslint no-underscore-dangle:0 */
/* eslint camelcase:1 */

/**
 * Sequel Migration Implementation
 * @ignore
 */
const mongodb_migration = (migration, options) => {
	migration.execute = () => {};
	return migration;
};

exports = module.exports = mongodb_migration;

mongodb_migration.changeFieldName = () => {};
mongodb_migration.changeFieldType = () => {};
mongodb_migration.changeTableName = () => {};

mongodb_migration.dropTable = () => {};
mongodb_migration.createTable = () => {};

mongodb_migration.emptyTableData = () => {};
mongodb_migration.exportTableData = () => {};
mongodb_migration.importTableData = () => {};

mongodb_migration.insertField = () => {};
mongodb_migration.removeField = () => {};
mongodb_migration.renameField = () => {};

mongodb_migration.removeIndex = () => {};
mongodb_migration.createIndex = () => {};
mongodb_migration.renameIndex = () => {};
