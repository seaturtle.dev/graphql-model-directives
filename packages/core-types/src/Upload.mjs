export const metaInfo = {
	"group": "Base Types",
	"type": "Upload",
	"name": "Upload",
	"icon": "file-upload-outline",
	"nature": "organism",
	"action": "addField",
	"actionProps": { "directives": ["file"] },
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
