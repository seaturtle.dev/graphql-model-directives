export const metaInfo = {
	"group": "Base Types",
	"type": "Boolean",
	"name": "Boolean",
	"icon": "checkbox-blank-off-outline",
	"nature": "element",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
