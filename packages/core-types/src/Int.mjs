export const metaInfo = {
	"group": "Base Types",
	"type": "Number",
	"name": "Int",
	"icon": { "source": "Octicons", "name": "number" },
	"nature": "element",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
