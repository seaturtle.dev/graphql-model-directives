export const metaInfo = {
	"group": "Base Types",
	"type": "Date",
	"name": "Date",
	"icon": "calendar",
	"nature": "molecule",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
