export const metaInfo = {
	"group": "Base Types",
	"type": "Number",
	"name": "Float",
	"icon": "decimal",
	"nature": "element",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
