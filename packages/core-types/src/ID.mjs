export const metaInfo = {
	"group": "Base Types",
	"type": "ID",
	"name": "ID",
	"icon": "identifier",
	"nature": "element",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
