export const metaInfo = {
	"group": "Base Types",
	"type": "Date",
	"name": "DateTime",
	"icon": "calendar-clock",
	"nature": "molecule",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
