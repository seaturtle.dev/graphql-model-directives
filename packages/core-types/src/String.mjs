export const metaInfo = {
	"group": "Base Types",
	"type": "String",
	"name": "String",
	"icon": "format-quote-close",
	"nature": "molecule",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
