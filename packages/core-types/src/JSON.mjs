export const metaInfo = {
	"group": "Base Types",
	"type": "JSON",
	"name": "JSON",
	"icon": "code-json",
	"nature": "organism",
	"action": "addField",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};

export default { metaInfo };
