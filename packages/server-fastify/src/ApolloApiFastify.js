module.exports = (async function() {
	// let DefaultApolloServer;
	let fastifyApollo;
	let fastifyApolloHandler;
	let ApolloServerPluginDrainHttpServer;
	try {
		const asFastify = require('@as-integrations/fastify');
		// DefaultApolloServer = asFastify.ApolloServer;
		fastifyApollo = asFastify.default;
		fastifyApolloHandler = asFastify.fastifyApolloHandler;
		ApolloServerPluginDrainHttpServer = asFastify.fastifyApolloDrainPlugin;
	} catch (err) {
		// console.warn('apollo-server-fastify is not installed')
	}

	const { default: processRequest } = await import('graphql-upload/processRequest.mjs');

	const ApolloApiServer = require('@seaturtle/gmd-server-common/ApolloApiServer.js');

	/** @class */
	class ApolloApiFastify extends ApolloApiServer {
		// static DefaultApolloServer = DefaultApolloServer;

		static extraMiddleware = null;

		static installMiddleware({ bodyParser, cors }, params) {
			const {
				cors: corsOptions,
				bodyParser: bodyParserOptions,
			} = params || {}
			ApolloApiFastify.extraMiddleware = [
				[cors, corsOptions],
				[bodyParser, bodyParserOptions],
			];
		}

		/** @method */
		appOptions() {
			return {
				// logger: true,
				forceCloseConnections: true,
			};
		}

		/** @method */
		serverPlugins(options, app, server) {
			return [
				...(options.plugins || []),
				ApolloServerPluginDrainHttpServer(server)
			]
		}

		/** @method */
		async setup(app, apollo, cb, before) {
			await this.ready();

			if (app && apollo) {
				app.addContentTypeParser('application/json', { parseAs: 'string' }, (req, body, done) => {
					try {
						const json = JSON.parse(body);
						done(null, json);
					} catch (err) {
						err.statusCode = 400;
						done(err, undefined);
					}
				})
				// Handle all requests that have the `Content-Type` header set as multipart
				app.addContentTypeParser('multipart', (request, payload, done) => {
					request.isMultipart = true;
					done();
				});
				app.addContentTypeParser('*', (request, payload, done) => {
					let data = '';
					payload.on('data', chunk => { data += chunk });
					payload.on('end', () => {
						done(null, data);
					});
				})
				// Format the request body to follow graphql-upload's
				app.addHook('preValidation', async (request, reply) => {
					if (!request.isMultipart) {
						return;
					}
					request.body = await processRequest(request.raw, reply.raw);
				});

				await apollo.start();

				let extraMiddleware = this.constructor.extraMiddleware || [];
				if (before) {
					await before();
					extraMiddleware = [
						...extraMiddleware,
						...(Array.isArray(beforemiddleware) ? beforemiddleware : []),
					];
				}

				extraMiddleware.forEach((middleware) => {
					app.register(...middleware);
				})

				app.register(fastifyApollo(apollo), {
					context: async (request, reply) => this.getContext({
						req: request,
						res: reply
					}),
				});

				if (cb) {
					cb();
				}
			}
		}

		/** @method */
		postSetup(app, apollo, server) {
			if (apollo && apollo.installSubscriptionHandlers && server) {
				apollo.installSubscriptionHandlers(server);
			}
		}
	}

	return module.exports = ApolloApiFastify;
})();
