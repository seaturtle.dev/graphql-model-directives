/**
 * GraphQL Model Directives unique ID generation.
 * @ignore
 */
const nanoid = exports = module.exports = require('nanoid');
