/**
 * GraphQL Model Directives unique ID generation libraries.
 * @ignore
 */
const ids = exports;

/**
 * Alias to `nanoid`
 */
ids.nanoid = require('nanoid');

/**
 * Alias to `hashids`
 */
ids.hashids = require('hashids');

/**
 * Alias to `shortid`
 */
ids.shortid = require('shortid');
