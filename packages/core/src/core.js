/**
 * Apollo GraphQL Model Directives core library.
 * @ignore
 */
const core = exports;

/**
 * Base class for Collection that provides generic definition,
 * introspection, and invokation methods.
 */
core.ModelDefinitions = require('@seaturtle/gmd-adapter-generic/ModelDefinitions.js');

/**
 * Used by custom adapters to model a collection of documents or rows
 * that map to the desired database.
 */
core.Collection = require('@seaturtle/gmd-adapter-generic/Collection.js');

/**
 * A generic example of a store that has no database model, or user methods implemented.
 */
core.StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');
