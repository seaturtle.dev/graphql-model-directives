module.exports = (async function() {
	const { default: GraphQLUpload } = await import('graphql-upload/GraphQLUpload.mjs');

	const {
		GraphQLDate,
		GraphQLTime,
		GraphQLDateTime,
	} = require('graphql-scalars');

	const { GraphQLJSON } = require('graphql-type-json');

	/**
	 * Common GraphQL types that are automatically included and available in your schema.
	 * @ignore
	 */
	const types = exports;

	/**
	 * Alias to `graphql-scalars`
	 */
	types.GraphQLScalers = GraphQLDate;

	/**
	 * Date type for GraphQL based on `graphql-scalars`
	 */
	types.GraphQLDate = GraphQLDate;

	/**
	 * Time type for GraphQL based on `graphql-scalars`
	 */
	types.GraphQLTime = GraphQLTime;

	/**
	 * DateTime type for GraphQL based on `graphql-scalars`
	 */
	types.GraphQLDateTime = GraphQLDateTime;

	/**
	 * JSON type for GraphQL based on `graphql-type-json`
	 */
	types.GraphQLJSON = GraphQLJSON;

	/**
	 * Upload type for GraphQL based on `graphql-upload`
	 */
	types.GraphQLUpload = GraphQLUpload;

	/** @member {Function[]} List of all GraphQL types that will be
	 * automatically included by {@link prepareSchema}. */
	types.list = [
		GraphQLDate,
		GraphQLTime,
		GraphQLDateTime,
		GraphQLJSON,
		GraphQLUpload,
	];

	return types;
	// return module.exports = types;
})();
