/**
 * GraphQL Model Directives plugins.
 * @ignore
 */
const plugins = exports;

/**
 * Plugin for HTTP header based authorization
 */
plugins.authorize = require('@seaturtle/gmd-plugin-authorize/authorize.js');

/**
 * Plugin for cypto based encryption of fields
 */
plugins.crypto = require('@seaturtle/gmd-plugin-crypto');

/**
 * Plugin for email integegration for notifications
 */
plugins.email = require('@seaturtle/gmd-plugin-email');

/**
 * Plugin for uploading files into fields
 */
plugins.upload = require('@seaturtle/gmd-plugin-upload');
