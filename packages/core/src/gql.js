/**
 * GraphQL Model Directives gql alias.
 * @ignore
 */
const gql = exports = module.exports = require('graphql-tag');
