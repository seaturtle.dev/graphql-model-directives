/**
 * Apollo GraphQL Model Directives server libraries.
 * @ignore
 */
const servers = exports;

/**
 * Create a GraphQL Apollo Server with Model Directives integrated for Express.
 * @constructor
 * @see {@link ApolloApiExpress} */
servers.ApolloApiExpress = require('@seaturtle/gmd-server-express/ApolloApiExpress.js');

/**
 * Create a GraphQL Apollo Server with Model Directives integrated for Fastify.
 * @constructor
 * @see {@link ApolloApiFastify} */
servers.ApolloApiFastify = require('@seaturtle/gmd-server-fastify/ApolloApiFastify.js');
