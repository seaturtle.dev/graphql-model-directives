/**
 * Apollo GraphQL Model Directives collection libraries.
 * @ignore
 */
const collection = exports;

/**
 * Provides a generic intergration for collections.
 */
collection.Collection = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');

/**
 * Provides custom MongoDB integrations for collections.
 */
collection.MemoryCollection = require('@seaturtle/gmd-adapter-memory/Collection.js');

/**
 * Provides custom MongoDB integrations for collections.
 */
collection.MongoDBCollection = require('@seaturtle/gmd-adapter-mongodb/Collection.js');

/**
 * Provides custom Redis integrations for collections.
 */
collection.RedisCollection = require('@seaturtle/gmd-adapter-redis/Collection.js');

/**
 * Provides custom SQL integrations for collections.
 */
collection.SequelCollection = require('@seaturtle/gmd-adapter-sequel/Collection.js');

/**
 * The same as a Generic Collection.
 */
collection.DataCollection = require('@seaturtle/gmd-adapter-data/Collection.js');

/**
 * The same as a Data Collection.
 */
collection.ApiSnocodeCollection = require('@seaturtle/gmd-adapter-api-snocode/Collection.js');
