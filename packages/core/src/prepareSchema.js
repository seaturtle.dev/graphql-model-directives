/**
 * Simplifies getting a schema ready to run as a server from raw GQL.
 * @function
 * @see {@link prepareSchema} */
const prepareSchema = require('@seaturtle/gmd-prepare-schema');

module.exports = exports = prepareSchema;
