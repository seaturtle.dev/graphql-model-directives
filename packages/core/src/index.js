/**
 * GraphQL Model Directives
 * @ignore
 */
const GraphGLModelDirectives = exports;

/**
 * Core collaction and adapter library code.
 */
GraphGLModelDirectives.core = require('./core');

/**
 * Common GraphQL types that are automatically included and available in your schema.
 */
GraphGLModelDirectives.gqlTypes = require('./gqlTypes');
