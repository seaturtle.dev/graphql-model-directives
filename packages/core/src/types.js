/**
 * Library and list of available Types document fields.
 * @exports
 */
module.exports = exports = require('@seaturtle/gmd-core-types');
