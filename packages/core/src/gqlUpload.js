/**
 * GraphQL Model Directives graphql-upload alias.
 * @ignore
 */
const gqlUpload = exports = module.exports = require('graphql-upload');
