/**
 * Apollo GraphQL Model Directives server library.
 * @ignore
 */
const server = exports;

/**
 * Create a GraphQL Apollo Server with Model Directives integrated.
 * @constructor
 * @see {@link ApolloApiServer} */
server.ApolloApiServer = require('@seaturtle/gmd-server-common/ApolloApiServer.js');

/**
 * Helper for getting the Authorization header from a server request headers object.
 * @function
 * @see {@link getAuthorizationHeader} */
server.getAuthorizationHeader = require('@seaturtle/gmd-plugin-authorize/getAuthorizationHeader.js');

/**
  * Helper for getting the DevideId header from a server request headers object.
 * @function
 * @see {@link getDeviceIdHeader} */
server.getDeviceIdHeader = require('@seaturtle/gmd-plugin-authorize/getDeviceIdHeader.js');

/**
  * Helper for creating a context handler for Apollo GraphQL server that is used by resolvers from GraphQL Model Directives.
 * @function
 * @see {@link prepareSchema} */
server.getContextHandler = require('@seaturtle/gmd-server-common/getContextHandler.js');
