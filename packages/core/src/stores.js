/**
 * Library of available database adapters.
 * @exports stores
 */
const stores = exports;

/**
 * A generic example of a store that has no database model, or user methods implemented.
 * @constructor
 * @see {@link StoreAdapter} */
stores.Generic = require('@seaturtle/seaturtle/gmd-adapter-generic/StoreAdapter.js');

/**
 * Uses `redis-mock` on top of the Redis adapter to provide an in-memory adapter.
 * @constructor
 * @see {@link MemoryAdapter} */
stores.Memory = require('@seaturtle/seaturtle/gmd-adapter-memory/StoreAdapter.js');

/**
 * Uses `Mongoose` ORM to provide a MongoDB adapter.
 * @constructor
 * @see {@link MongoDBAdapter} */
stores.MongoDB = require('@seaturtle/seaturtle/gmd-adapter-mongodb/StoreAdapter.js');

/**
 * Uses a hand written `redis` ORM, to provide a comprehensive Redis adapter.
 * @constructor
 * @see {@link RedisAdapter} */
stores.Redis = require('@seaturtle/seaturtle/gmd-adapter-redis/StoreAdapter.js');

/**
 * Uses `Sequelize` ORM to provide adapters for various sequel databases including:
 * `postgres`, `mysql`, `mariadb`, `mssql` and `sqlite3`
 * @constructor
 * @see {@link SequelAdapter} */
stores.Sequel = require('@seaturtle/seaturtle/gmd-adapter-sequel/StoreAdapter.js');

/**
 * Uses simple array<document> for collection storage.
 * @constructor
 * @see {@link DataAdapter} */
stores.Data = require('@seaturtle/seaturtle/gmd-adapter-data/StoreAdapter.js');

/**
 * Uses simple array<document> based on ApiSnocode for collection storage.
 * @constructor
 * @see {@link ApiSnocodeAdapter} */
stores.ApiSnocode = require('@seaturtle/seaturtle/gmd-adapter-api-snocode/StoreAdapter.js');

/**
 * Uses `dexie` for browser based SQL collection storage.
 * @constructor
 * @see {@link DexieAdapter} */
stores.Dexie = require('@seaturtle/seaturtle/gmd-adapter-dexie/StoreAdapter.js');
