/**
 * Library and list of available GraphQL Directives for better schema definition.
 * @exports
 */
module.exports = exports = require('@seaturtle/gmd-core-directives');
