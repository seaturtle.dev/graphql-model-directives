/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to reference another Type by id.
 * @exports @relation
 */
local['@relation'] = (store) => (
	class RelationDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			const { preserve } = this.args;

			const many = field.type.toString().charAt(0) === '[';

			store
				.defineModelProperty(field.modelName, field)
				.definePropertyRelation(field, {
					many,
					autoDelete: !preserve,
				})
				.definePropertyModelType(field, many ? 'Object' : 'ObjectId');

			return field;
		}
	}
);

module.exports = local['@relation'];

module.exports.directiveName = 'relation';

module.exports.typeDef = `
directive @relation(preserve: Boolean) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "Relation",
	"icon": "bookmark-multiple",
	"nature": "element",
	"slug": "relation",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
