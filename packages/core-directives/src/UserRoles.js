/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the users list of roles
 * used to access GraphQL operations.
 * @exports @roles
 */
local['@roles'] = (store) => (
	class UserRolesDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserRoles(field);

			return field;
		}
	}
);

module.exports = local['@roles'];

module.exports.directiveName = 'roles';

module.exports.typeDef = `
directive @roles on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserFeild",
	"name": "UserRoles",
	"icon": "badge-account-horizontal-outline",
	"nature": "molecule",
	"slug": "roles",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
