/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to be a data model.
 * @exports @model
 */
local['@model'] = (store) => (
	class ModelDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			const { name } = object;

			store.defineModel(name, object);

			const fields = object.getFields();
			const fieldNames = Object.keys(fields);

			// TODO: make defineNonNulls method for this, or initializeModelFields?
			const nonNulls = [];

			object.collection.nonNulls = nonNulls;
			object.collection.nonNullId = false;

			fieldNames.forEach((fieldName) => {
				const field = fields[fieldName];

				const { astNode } = field;

				const isNonNull = astNode.type.kind === 'NonNullType';

				field.isNonNull = isNonNull;

				if (field.isNonNull) {
					const directives = astNode?.directives || [];

					const safeDirectives = directives.filter((directive) => {
						if (directive?.name?.value === 'id') {
							object.collection.nonNullId = true;
							return false;
						}
						return true;
					});

					if (safeDirectives.length) {
						nonNulls.push(fieldName);
					}
				}

				store.defineModelProperty(name, field);
			});

			// console.log(name, object.collection);

			return object;
		}
	}
);

module.exports = local['@model'];

module.exports.directiveName = 'model';

module.exports.typeDef = `
directive @model(name: String) on OBJECT
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "Model",
	"icon": "folder-table-outline",
	"nature": "molecule",
	"slug": "model",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
