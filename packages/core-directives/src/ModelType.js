/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to use a special type within its model definition,
 * this is needed for some databases.
 * @exports @modelType
 */
local['@modelType'] = (store) => (
	class ModelTypeDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			if (field.modelName && this.args.type) {
				store
					.defineModelProperty(field.modelName, field)
					.definePropertyModelType(field, this.args.type);
			}

			return field;
		}
	}
);

module.exports = local['@modelType'];

module.exports.directiveName = 'modelType';

module.exports.typeDef = `
directive @modelType(type: String) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "ModelType",
	"icon": "table-settings",
	"nature": "element",
	"slug": "modelType",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
