/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to have the time of creation for the document.
 * @exports @created
 */
local['@created'] = () => null; // for docs
local['@created'] = (store) => (
	class CreatedAtDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyCreatedAt(field)
				.definePropertyDefaultValue(field, () => new Date());

			return field;
		}
	}
);

module.exports = local['@created'];

module.exports.directiveName = 'created';

module.exports.typeDef = `
directive @created on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "CreatedAt",
	"icon": { "source": "MaterialIcons", "name": "history-toggle-off" },
	"nature": "element",
	// "icon": "database-clock-outline",
	"slug": "created",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
