/* eslint camelcase:1 */

const pluralize = require('pluralize');

// const { SchemaDirectiveVisitor } = require('@graphql-tools/utils');
const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const resourceHelpers = require('./common/resourceHelpers');

const local = {};

function lowerFirstChar(str) {
	return str.charAt(0).toLowerCase() + str.slice(1);
}

// NOTE: "evil" code ahead using an exception to halt execution,
//       and inject possibly missing type dependencies.
function throwSchemaInjection(resolvers, typeDefs, state, config) {
	const error = new Error('SchemaInjection');
	error.schemaInjection = {
		resolvers,
		typeDefs,
	};
	// debugger;
	delete state.pendingSchemaInjection;
	throw error;
}

// TODO: automatically add mutations for upserting uploads

const defaultRoleMap = (superRole = 'super') => ({
	upsert: true,
	remove: true,
	count: true,
	read: true,
	find: true,
	purge: superRole,
});

const stringRoleMaps = {
	default: defaultRoleMap,
	super: (superRole = 'super') => ({
		upsert: superRole,
		remove: superRole,
		count: superRole,
		read: superRole,
		find: superRole,
		purge: superRole,
	}),
	open: (superRole = 'super') => ({
		upsert: false,
		remove: false,
		count: false,
		read: false,
		find: false,
		purge: superRole,
	}),
	user: defaultRoleMap,
	read: (superRole = 'super') => ({
		upsert: superRole,
		remove: superRole,
		count: true,
		read: true,
		find: true,
		purge: superRole,
	}),
	write: (superRole = 'super') => ({
		upsert: true, // Anyone can add any email address to the mailing list,
		remove: superRole, // only admin can then manage them.
		count: superRole,
		read: superRole,
		find: superRole,
		purge: superRole,
	}),
};

function getRoleMap(roleMap, superRole) {
	if (typeof roleMap === 'string') {
		roleMap = stringRoleMaps[roleMap];
	}
	roleMap = roleMap || defaultRoleMap;
	if (typeof roleMap === 'function') {
		roleMap = roleMap(superRole);
	}
	return {
		...roleMap,
	};
}

function createSafeBelongsToAttr(firstBelongsTo) {
	return function safeBelongsToAttr(path) {
		if (!firstBelongsTo) { return ''; }
		// TODO: support paths in find target { "a.path.to.some.value": "matcher" }
		return `${firstBelongsTo}: "${path}"`;
	};
}

function createSuperRoleInjector(superAuth, superRole) {
	return function injectSuperRole(overrides) {
		if (Array.isArray(overrides) && overrides.length === 1) {
			[overrides] = overrides;
		}
		if (overrides === superRole) {
			overrides = false;
		}
		if (Array.isArray(overrides) || typeof overrides === 'string') {
			return `(allow: ${JSON.stringify(overrides)})`;
		}
		return !overrides ? superAuth : '';
	};
}

function createAuthSkipper(auth, roleMap) {
	return function skipAuth(type) {
		if (auth === false) {
			return 'skipAuth: true';
		}
		if (roleMap[type] === false) {
			return 'skipAuth: true';
		}
		return 'skipAuth: false';
	};
}

function createAuthorizer(auth, superAuth, superRole, roleMap, injectSuperRole) {
	return function auth(type) {
		if (auth === false) {
			return '';
		}
		if (type === superRole) {
			return `@auth${superAuth}`;
		}
		return `@auth${injectSuperRole(roleMap[type])}`;
	};
}

function createResolvers(name/* , sub */) {
	const resolvers = {
		Query: {},
		Mutation: {},
	}
	if (name) {
		resolvers[name] = {};
	}
	// if (sub) {
	// 	resolvers.Subscription = {};
	// }
	return resolvers;
}

function throwModelSchemaInjection(name, plural, options, model, state, config) {
	const lower = lowerFirstChar(name);
	const lowers = lowerFirstChar(plural);
	const id = model.idProperty;
	const hideSubscriptions = options.hideSubscriptions || false;
	const findMethod = options.findMethod || 'find';
	const superRole = options.superRole || config.superRole || 'super';
	const roleMap = getRoleMap(options.roleMap, superRole);
	const isScanFind = findMethod === 'scan';
	const superAuth = options.noSuper ? '' : `(allow: "${superRole}")`;
	const pageType = !isScanFind ? `${plural}SortedPage` : `${plural}ScannedPage`;
	const pageInputType = !isScanFind ? 'SortedPageInput' : 'ScannedPageInput';
	const injectSuperRole = createSuperRoleInjector(superAuth, superRole);
	const skipAuth = createAuthSkipper(options.auth, roleMap);
	const auth = createAuthorizer(options.auth, superAuth, superRole, roleMap, injectSuperRole);
	const resolvers = createResolvers(name);
	const typeDefs = (`
type ${plural}ScannedPage implements ScannedPage @page @scanned {
	id: ID
	cursor: String
	count: Int
	skipped: Int
	page: [${name}]
}
type ${plural}SortedPage implements SortedPage @page @sorted {
	id: ID
	offset: Int
	count: Int
	skipped: Int
	page: [${name}]
}
${hideSubscriptions ? '' : (`
type Subscription {
	${lower}Created(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "created", "args.filter" ], ${skipAuth(superRole)} }) ${auth(superRole)}
	${lower}Updated(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "updated", "args.filter" ], ${skipAuth(superRole)} }) ${auth(superRole)}
	${lower}Removed(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "removed", "args.filter" ], ${skipAuth(superRole)} }) ${auth(superRole)}
}
`)}
type Query {
	admin${plural}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ "args.filter", [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth(superRole)}
	}) ${auth(superRole)}
	${lower}(${id}: ID!): ${name} @resolve(model: {
		method: "read",
		params: [ { ${id}: "args.${id}" } ],
		${skipAuth('read')}
	}) ${auth('read')}
	${lowers}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ "args.filter", [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth(superRole)}
	}) ${auth('find')}
	${lowers}Count(page: ${pageInputType}, filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "count",
		params: [ "args.filter", "args.page" ],
		${skipAuth('count')}
	}) ${auth('count')}
}
type Mutation {
	remove${name}(${id}: ID!): ${name} @resolve(model: {
		method: "remove",
		params: [ { ${id}: "args.${id}" } ],
		${skipAuth('remove')}
	}) ${auth('remove')}
	upsert${name}(data: JSON): ${name} @resolve(model: {
		method: "upsert",
		params: [ "args.data" ],
		${skipAuth('upsert')}
	}) ${auth('upsert')}
	purge${plural}(filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "purge",
		params: [ "args.filter" ],
		${skipAuth('purge')}
	}) ${auth('purge')}
}
`);
	throwSchemaInjection(resolvers, typeDefs, state, config);
}

function throwResourceSchemaInjection(name, plural, options, model, state, config) {
	const lower = lowerFirstChar(name);
	const lowers = lowerFirstChar(plural);
	const id = model.idProperty;
	const { belongsTo } = model;
	const firstBelongsTo = (belongsTo && belongsTo[0]) || null;
	const safeBelongsToAttr = createSafeBelongsToAttr(firstBelongsTo);
	const hideSubscriptions = options.hideSubscriptions || false;
	const findMethod = options.findMethod || 'find';
	const superRole = options.superRole || config.superRole || 'super';
	const roleMap = getRoleMap(options.roleMap, superRole);
	const isScanFind = findMethod === 'scan';
	const superAuth = options.noSuper ? '' : `(allow: "${superRole}")`;
	const pageType = !isScanFind ? `${plural}SortedPage` : `${plural}ScannedPage`;
	const pageInputType = !isScanFind ? 'SortedPageInput' : 'ScannedPageInput';
	const injectSuperRole = createSuperRoleInjector(superAuth, superRole);
	const skipAuth = createAuthSkipper(options.auth, roleMap);
	const auth = createAuthorizer(options.auth, superAuth, superRole, roleMap, injectSuperRole);
	const resolvers = createResolvers(name);
	const typeDefs = (`
type ${plural}ScannedPage implements ScannedPage @page @scanned {
	id: ID
	cursor: String
	count: Int
	skipped: Int
	page: [${name}]
}
type ${plural}SortedPage implements SortedPage @page @sorted {
	id: ID
	offset: Int
	count: Int
	skipped: Int
	page: [${name}]
}
${hideSubscriptions ? '' : (`
type Subscription {
	${lower}Created(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "created", "args.filter" ], ${skipAuth(superRole)} }) ${auth(superRole)}
	${lower}Updated(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "updated", "args.filter" ], ${skipAuth(superRole)} }) ${auth(superRole)}
	${lower}Removed(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "removed", "args.filter" ], ${skipAuth(superRole)} }) ${auth(superRole)}
}
`)}
type Query {
	adminAll${plural}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ "args.filter", [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth(superRole)}
	}) ${auth(superRole)}
	admin${plural}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ { ${safeBelongsToAttr(`args.${id}`)} }, [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth(superRole)}
	}) ${auth(superRole)}
	${lower}(${id}: ID!): ${name} @resolve(model: {
		method: "read",
		params: [ { ${id}: "args.${id}" } ],
		${skipAuth('read')}
	}) ${auth('read')}
	${lowers}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ [ ".mixin.args.filter", { ${safeBelongsToAttr('context.userId')} } ], [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth(superRole)}
	}) ${auth('find')}
	${lowers}Count(page: ${pageInputType}, filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "count",
		params: [ [ ".mixin.args.filter", { ${safeBelongsToAttr('context.userId')} } ], [ "args.page" ] ],
		${skipAuth('count')}
	}) ${auth('count')}
}
type Mutation {
	remove${name}(${id}: ID!): ${name} @resolve(model: {
		method: "remove",
		params: [ { ${id}: "args.${id}" } ],
		${skipAuth('remove')}
	}) ${auth('remove')}
	upsert${name}(data: JSON): ${name} @resolve(model: {
		method: "upsert",
		params: [ [ ".merge.args.data", { ${safeBelongsToAttr('context.userId')} } ] ],
		${skipAuth('upsert')}
	}) ${auth('upsert')}
	purge${plural}(filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "purge",
		params: [ [ ".mixin.args.filter", { ${safeBelongsToAttr('context.userId')} } ] ],
		${skipAuth('purge')}
	}) ${auth('purge')}
}
`);
	throwSchemaInjection(resolvers, typeDefs, state, config);
}

function throwUserInviteSchemaInjection(name, plural, options, model, state, config) {
	const lower = lowerFirstChar(name);
	const lowers = lowerFirstChar(plural);
	const id = model.idProperty;
	const { userEmail, belongsTo } = model;
	if (!belongsTo || !belongsTo[0]) {
		throw new Error(`${name} must have a 'belongsTo' property on @resource that refers to a user id.`);
	}
	const firstBelongsTo = belongsTo[0];
	const findMethod = options.findMethod || 'scan';
	const superRole = options.superRole || config.superRole || 'super';
	const roleMap = getRoleMap(options.roleMap, superRole);
	const isScanFind = findMethod === 'scan';
	const superAuth = options.noSuper ? '' : `(allow: "${superRole}")`;
	const pageType = !isScanFind ? `${plural}SortedPage` : `${plural}ScannedPage`;
	const pageInputType = !isScanFind ? 'SortedPageInput' : 'ScannedPageInput';
	const injectSuperRole = createSuperRoleInjector(superAuth, superRole);
	const skipAuth = createAuthSkipper(options.auth, roleMap);
	const auth = createAuthorizer(options.auth, superAuth, superRole, roleMap, injectSuperRole);
	const resolvers = createResolvers(name);
	// const resolvers = {
	// 	Query: {},
	// 	Mutation: {},
	// };
	const typeDefs = (`
type ${plural}ScannedPage implements ScannedPage @page @scanned {
	id: ID
	cursor: String
	count: Int
	skipped: Int
	page: [${name}]
}
type ${plural}SortedPage implements SortedPage @page @sorted {
	id: ID
	offset: Int
	count: Int
	skipped: Int
	page: [${name}]
}
type Query {
	adminAll${plural}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ "args.filter", [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth(superRole)}
	}) ${auth(superRole)}
	admin${name}(${id}: ID!): ${pageType} @resolve(model: {
		name: "${name}",
		method: "read",
		params: [ { ${firstBelongsTo}: "args.${id}" } ],
		${skipAuth(superRole)}
	}) ${auth(superRole)}
	${lower}(${id}: ID!): ${name} @resolve(model: {
		method: "read",
		params: [ { ${id}: "args.${id}" } ],
		${skipAuth('read')}
	}) ${auth('read')}
	${lowers}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ [ ".mixin.args.filter", { ${firstBelongsTo}: "context.userId" } ], [ ".mixin.args.page", { sort: "args.sort" } ] ],
		${skipAuth('find')}
	}) ${auth('find')}
	${lowers}Count(page: ${pageInputType}, filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "count",
		params: [ [ ".mixin.args.filter", { ${firstBelongsTo}: "context.userId" } ], [ "args.page" ] ],
		${skipAuth('count')}
	}) ${auth('count')}
}
type Mutation {
	purge${plural}(filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "purge",
		params: [ [ ".mixin.args.filter", { ${firstBelongsTo}: "context.userId" } ] ],
		${skipAuth('purge')}
	}) ${auth('purge')}
	revoke${name}(${id}: ID!): ${name} @resolve(model: {
		method: "revokeInvite",
		params: [ "args" ],
		${skipAuth('remove')}
	}) ${auth('remove')}
	send${name}(${userEmail}: String, message: String): ${name} @resolve(model: {
		method: "sendInvite",
		params: [ "args" ],
		${skipAuth('upsert')}
	}) ${auth('upsert')}
}
`);
	throwSchemaInjection(resolvers, typeDefs, state, config);
}

function throwUserSchemaInjection(name, plural, options, model, state, config) {
	const lower = lowerFirstChar(name);
	const lowers = lowerFirstChar(plural);
	const id = model.idProperty;
	const {
		userEmailConfirm,
		userEmail,
		userLogin,
		userBirthdate,
		userPassword,
		userPasswordReset,
	} = model;
	const findMethod = options.findMethod || 'scan';
	const superRole = options.superRole || config.superRole || 'super';
	const allowSubscriptions = options.allowSubscriptions || false;
	const secureUserList = options.secureUserList || false;
	const isScanFind = findMethod === 'scan';
	const superAuth = options.noSuper ? '' : `(allow: "${superRole}")`;
	const pageType = !isScanFind ? `${plural}SortedPage` : `${plural}ScannedPage`;
	const pageInputType = !isScanFind ? 'SortedPageInput' : 'ScannedPageInput';
	const injectSuperRole = createSuperRoleInjector(superAuth, superRole);
	const resolvers = createResolvers();
	// const resolvers = {
	// 	// Subscription: {},
	// 	Query: {},
	// 	Mutation: {},
	// };
	const typeDefs = (`
type ${plural}ScannedPage implements ScannedPage @page @scanned {
	id: ID
	cursor: String
	count: Int
	skipped: Int
	page: [${name}]
}
type ${plural}SortedPage implements SortedPage @page @sorted {
	id: ID
	offset: Int
	count: Int
	skipped: Int
	page: [${name}]
}
${!allowSubscriptions ? '' : (`
type Subscription {
	userCreated(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "created", "args.filter" ] }) @auth${superAuth}
	userUpdated(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "updated", "args.filter" ] }) @auth${superAuth}
	userRemoved(filter: JSON): ${name} @resolve(model: { method: "subscribe", params: [ "removed", "args.filter" ] }) @auth${superAuth}
}
`)}
type Query {
	adminAll${plural}(page: ${pageInputType}, sort: SortInput, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ "args.filter", [ ".mixin.args.page", { sort: "args.sort" } ] ],
		skipAuth: false
	}) @auth${superAuth}
	admin${name}(${id}: ID!): ${pageType} @resolve(model: {
		name: "${name}",
		method: "read",
		params: [{ ${id}: "args.${id}" }],
		skipAuth: false
	}) @auth${superAuth}
	me: ${name} @resolve(model: { method: "me", params: [ "args" ] })
	${lower}(${id}: ID!): ${name} @resolve(model: {
		method: "read",
		params: [ { ${id}: "args.${id}" } ],
		skipAuth: false
	}) @auth
	${lowers}(page: ${pageInputType}, filter: JSON): ${pageType} @resolve(model: {
		name: "${name}",
		method: "${findMethod}",
		params: [ "args.filter", [ ".mixin.args.page", { sort: "args.sort" } ] ],
		skipAuth: false
	}) @auth${secureUserList ? superAuth : ''}
	${lowers}Count(page: ${pageInputType}, filter: JSON): Int @resolve(model: {
		name: "${name}",
		method: "count",
		params: [ "args.filter", "args.page" ],
		skipAuth: false
	}) @auth${secureUserList ? superAuth : ''}
}
type Mutation {
	register${name}(${userEmail}: String!,${userEmail === userLogin ? '' : `${userLogin}: String!`} ${userBirthdate}: Date!, ${userPassword}: String!, invitation: String): ${name} @resolve(model: { method: "register", params: [ "args" ] })
	remove${name}(${id}: ID!): ${name} @resolve(model: { method: "remove", params: [{ ${id}: "args.${id}" }] })
	signIn(${userLogin}: String!, ${userPassword}: String!): ${name} @resolve(model: { method: "signIn", params: [ "args" ] })
	signOut(${id}: ID): ${name} @resolve(model: { method: "signOut", params: [ "args" ] })
	confirmEmail(${id}: ID, ${userEmailConfirm}: String!): ${name} @resolve(model: { method: "confirmEmail", params: [ "args" ] })
	refreshToken: ${name} @resolve(model: { method: "refreshToken", params: [ "args" ] })
	forgotPassword(${userEmail /* TODO: useLogin */}: String!): ${name} @resolve(model: { method: "forgotPassword", params: [ "args" ] })
	resetPassword(${userEmail /* TODO: useLogin */}: String, recoveryToken: String, newPassword: String!): ${name} @resolve(model: { method: "resetPassword", params: [ "args" ] })
	updatePassword(oldPassword: String, newPassword: String!): ${name} @resolve(model: { method: "updatePassword", params: [ "args" ] })
	updateLogin(${id}: ID, ${userLogin}: String!): ${name} @resolve(model: { method: "updateLogin", params: [ "args" ] })
	updateEmail(${id}: ID, ${userEmail}: String!): ${name} @resolve(model: { method: "updateEmail", params: [ "args" ] })
	updateEmailAndLogin(${id}: ID, ${userEmail}: String!${userEmail === userLogin ? '' : `, ${userLogin}: String!`}): ${name} @resolve(model: { method: "updateEmailAndLogin", params: [ "args" ] })
	purge${plural}(filter: JSON): Int @resolve(model: { name: "${name}", method: "purge" params: [ "args.filter" ], skipAuth: false }) @auth${injectSuperRole(true)}
	upsert${name}(data: JSON): ${name} @resolve(model: {
		method: "upsert",
		params: [ [
			".filterAndMerge.args.data",
			[ ".str", "${userEmail}", ${userEmail !== userLogin ? `"${userLogin}", ` : ''}"${userPassword}", "${userEmailConfirm}", "${userPasswordReset}", "${userBirthdate}" ]
			{ ${id}: "context.userId" }
		] ],
	})
}
`);
	// debugger;
	throwSchemaInjection(resolvers, typeDefs, state, config);
}

/**
 * Annotate an object to have an API for it injected into query and mutation of schema.
 * @exports @api
 * @param {string} [name=`${type.name}`] Name of the model.
 * @param {string} [plural=`${name}s`] Plural name of the model.
 * @param {Object} [options={}] Options used to declare the API.
 */
local['@api'] = (name = '', plural = '', options = {}) => [name, plural, options]; // for docs
local['@api'] = (store, config) => (
	class APIDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			const { state } = this;

			// debugger;
			if (state.pendingSchemaInjection) {
				state.pendingSchemaInjection();
			}

			let { name, plural } = this.args;
			const { options = {} } = this.args;

			name = name || this.visitedType.name;
			plural = plural || pluralize(name); // `${name}s`;

			if (name === plural) {
				plural = `${name}s`;
			}

			// console.log('SCHEMA', this.schema);
			// console.log('API', name, plural, options, object);
			// console.log('MODEL', model.user, model.userInvite, model.resource);

			const model = store.defineModel(name, object, plural);

			const queryType = this.schema.getQueryType();
			const queryFields = queryType
				? queryType.getFields()
				: null;

			if (!config) {
				console.log(new Error('no config passed'));
				config = {};
			}

			// debugger;
			if (!queryFields
				|| !queryFields[lowerFirstChar(name)]
				|| !queryFields[lowerFirstChar(plural)]
			) {
				if (model.user === true) {
					state.pendingSchemaInjection = (
						() => throwUserSchemaInjection(name, plural, options, model, state, config)
					);
				} else if (model.userInvite === true) {
					state.pendingSchemaInjection = (
						() => throwUserInviteSchemaInjection(name, plural, options, model, state, config)
					);
				} else if (model.resource === true) {
					state.pendingSchemaInjection = (
						() => throwResourceSchemaInjection(name, plural, options, model, state, config)
					);
				} else {
					// debugger;
					state.pendingSchemaInjection = (
						() => throwModelSchemaInjection(name, plural, options, model, state, config)
					);
				}
			}
			// debugger;

			return object;
		}
	}
);

module.exports = local['@api'];

module.exports.directiveName = 'api';

module.exports.typeDef = `
directive @api(name: String, plural: String, options: JSON) on OBJECT
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "API",
	"icon": "api",
	"nature": "organism",
	"slug": "api",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
