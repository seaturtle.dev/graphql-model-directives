/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to contain a scan bassed pagination.
 * @exports @scanned
 */
local['@scanned'] = () => (
	class ScanPaginationDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			object.scanned = true;

			return object;
		}
	}
);

module.exports = local['@scanned'];

module.exports.directiveName = 'scanned';

module.exports.typeDef = `
directive @scanned on OBJECT
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "ScanPagination",
	"icon": "data-matrix-scan",
	"nature": "molecule",
	"slug": "scanned",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
