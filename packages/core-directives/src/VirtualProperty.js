/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of a model to be virtual so that it can be transered
 * over GraphQL without being saved to the model.
 * @exports @virtual
 */
local['@virtual'] = (store) => (
	class VirtualPropertyDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyVirtual(field, this.args.params);

			return field;
		}
	}
);

module.exports = local['@virtual'];

module.exports.directiveName = 'virtual';

module.exports.typeDef = `
directive @virtual(params: JSON) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "VirtualProperty",
	"nature": "element",
	"icon": "consolidate",
	"slug": "virtual",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
