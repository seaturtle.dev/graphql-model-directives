/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to be the user model.
 * @exports @user
 */
local['@user'] = (store) => (
	class UserDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			const collection = (
				store
					.defineModel(object.name, object)
					.defineUser()
			);

			store.mixinUser(collection);

			return object;
		}
	}
);

module.exports = local['@user'];

module.exports.directiveName = 'user';

module.exports.typeDef = `
directive @user on OBJECT
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserObject",
	"name": "User",
	"icon": "account-group", // account-multiple
	"nature": "organism",
	"slug": "user",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
