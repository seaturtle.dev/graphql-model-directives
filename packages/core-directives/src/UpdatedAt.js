/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to have the last time updated for the document.
 * @exports @updated
 */
local['@updated'] = (store) => (
	class UpdatedAtDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUpdatedAt(field)
				.definePropertyDefaultValue(field, () => new Date());

			return field;
		}
	}
);

module.exports = local['@updated'];

module.exports.directiveName = 'updated';

module.exports.typeDef = `
directive @updated on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "UpdatedAt",
	"icon": "history",
	"nature": "element",
	"slug": "updated",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
