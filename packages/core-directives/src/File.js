/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to store a file. Also handles file uploading and streaming.
 * @exports @file
 */
local['@file'] = (store) => (
	class FileDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyFile(field, this.args.params)
				.definePropertyModelType(field, 'Object');

			return field;
		}
	}
);

module.exports = local['@file'];

module.exports.directiveName = 'file';

module.exports.typeDef = `
directive @file(params: JSON) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "File",
	"icon": "file",
	"nature": "organism",
	"slug": "file",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
