/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to specify the maximum
 * allowed invites they can have pending.
 * @exports @maxInvites
 */
local['@maxInvites'] = (store) => (
	class UserMaxInvitesDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserMaxInvites(field);

			return field;
		}
	}
);

module.exports = local['@maxInvites'];

module.exports.directiveName = 'maxInvites';

module.exports.typeDef = `
directive @maxInvites on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserField",
	"name": "UserMaxInvites",
	"nature": "element",
	"icon": "account-plus-outline",
	"slug": "maxInvites",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
