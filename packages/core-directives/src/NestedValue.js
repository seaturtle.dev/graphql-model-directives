/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to contain a nested object or set of objects.
 * @exports @nested
 */
local['@nested'] = (store) => (
	class NestedValueDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			const many = field.type.toString().charAt(0) === '[';

			store
				.defineModelProperty(field.modelName, field)
				.definePropertyModelType(field, 'JSON')
				.definePropertyNestedValue(field, many);

			return field;
		}
	}
);

module.exports = local['@nested'];

module.exports.directiveName = 'nested';

module.exports.typeDef = `
directive @nested on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "NestedValue",
	"icon": "table-pivot",
	"nature": "molecule",
	"slug": "nested",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
