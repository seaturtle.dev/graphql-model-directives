/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate the field to be used as the main unique identifier for the document.
 * @exports @id
 */
local['@id'] = (store) => (
	class IdentifierDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyIdentifier(field);

			return field;
		}
	}
);

module.exports = local['@id'];

module.exports.directiveName = 'id';

module.exports.typeDef = `
directive @id on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "Identifier",
	"icon": "identifier",
	"nature": "element",
	"slug": "id",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
