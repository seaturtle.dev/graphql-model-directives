/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a object to have pagination.
 * @exports @page
 */
local['@page'] = () => (
	class PageDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			object.page = true;

			return object;
		}
	}
);

module.exports = local['@page'];

module.exports.directiveName = 'page';

module.exports.typeDef = `
directive @page on OBJECT
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "Page",
	"nature": "element",
	"icon": "table-of-contents",
	"slug": "page",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
