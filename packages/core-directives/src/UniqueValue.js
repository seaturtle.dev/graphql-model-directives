/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field so that the value is indexes uniquely.
 * @exports @unique
 */
local['@unique'] = (store) => (
	class UniqueValueDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			const {
				list,
			} = this.args;

			const defineUniqueByIndex = (args, index = 0) => {
				const {
					compounds,
				} = args;
				store
					.defineModelProperty(field.modelName, field)
					.definePropertyUnique(field, compounds, index);
			}

			if (list) {
				list.forEach((unique, index) => {
					defineUniqueByIndex(unqiue, index);
				});
			} else {
				defineUniqueByIndex(this.args, 0);
			}

			return field;
		}
	}
);

module.exports = local['@unique'];

module.exports.directiveName = 'unique';

module.exports.typeDef = `
directive @unique(compounds: [String], list: JSON) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "UniqueValue",
	"icon": "fingerprint",
	"nature": "molecule",
	"slug": "unique",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
