/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the users virtual token property.
 * @exports @token
 */
local['@token'] = (store) => (
	class UserTokenDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserToken(field)
				.definePropertyVirtual(field, { });

			return field;
		}
	}
);

module.exports = local['@token'];

module.exports.directiveName = 'token';

module.exports.typeDef = `
directive @token on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserFeild",
	"name": "UserToken",
	"icon": "shield-account-variant-outline",
	"nature": "element",
	"slug": "token",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
