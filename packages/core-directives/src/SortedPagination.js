/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to contain a sort bassed pagination.
 * @exports @sorted
 */
local['@sorted'] = () => (
	class SortedPaginationDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			object.sorted = true;

			return object;
		}
	}
);

module.exports = local['@sorted'];

module.exports.directiveName = 'sorted';

module.exports.typeDef = `
directive @sorted on OBJECT
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "SortedPagination",
	"icon": "sort-variant",
	"nature": "organism",
	"slug": "sorted",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
