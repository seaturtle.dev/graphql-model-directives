/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field or object to only be accessible to users who have permission to view it.
 * @exports @permission
 */
local['@permission'] = (store) => (
	class PermissionDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			if (field.modelName) {
				store
					.defineModelProperty(field.modelName, field)
					.definePropertyPermission(field, this.args);
			} else {
				field.permission = true;
			}

			return field;
		}

		visitObject(object) {
			store
				.defineModel(object.name, object)
				.definePermission(this.args);

			return object;
		}
	}
);

module.exports = local['@permission'];

module.exports.directiveName = 'permission';

module.exports.typeDef = `
directive @permission(condition: String) on OBJECT | FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "ObjectOrField",
	"name": "Permission",
	"nature": "element",
	"icon": "table-lock", // "database-lock-outline",
	"slug": "permission",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
