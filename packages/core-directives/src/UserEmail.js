/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the user email.
 * @exports @email
 */
local['@email'] = (store) => (
	class UserEmailDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserEmail(field)
				.definePropertyUnique(field);

			return field;
		}
	}
);

module.exports = local['@email'];

module.exports.directiveName = 'email';

module.exports.typeDef = `
directive @email on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserField",
	"name": "UserEmail",
	"icon": "email-box",
	"nature": "molecule",
	"slug": "email",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
