/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to be a resource that can belong to other documents or users.
 * @exports @resource
 */
local['@resource'] = (store) => (
	class ResourceDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			store
				.defineModel(object.name, object)
				.defineResource(
					this.args.belongsTo, // || ['user']
					this.args.allowAccess,
				);

			return object;
		}
	}
);

module.exports = local['@resource'];

module.exports.directiveName = 'resource';

module.exports.typeDef = `
directive @resource(belongsTo: [String], allowAccess: [String]) on OBJECT
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "Resource",
	"icon": "briefcase-outline",
	"nature": "molecule",
	"slug": "resource",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
