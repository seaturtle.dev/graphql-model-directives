/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field to resolve automatically. You have several strategies available for resolution:
 * `model`, and in the futer `value`, `evaluate`, `rest`, `graphql`, `moleculer`.
 * @exports @resolve
 */
local['@resolve'] = (store) => (
	class ResolveDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineResolver(field, this.args);

			if (this.args.finalType) {
				// debugger;
				const finalType = this.schema._typeMap[this.args.finalType];
				if (finalType) {
					// Hack to inject the right return type for a field when it may not be available in GraphQL
					this.visitedType.astNode.name.value = this.args.finalType;
				}
			}

			return field;
		}
	}
);

module.exports = local['@resolve'];

module.exports.directiveName = 'resolve';

module.exports.typeDef = `
directive @resolve(
	finalType: String,
	order: [String]
	model: JSON,
	value: JSON,
	evaluate: String,
	rest: JSON,
	graphql: JSON,
	moleculer: JSON
) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "Resolve",
	"nature": "molecule", // organism?
	"icon": "bullseye",
	"slug": "resolve",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
