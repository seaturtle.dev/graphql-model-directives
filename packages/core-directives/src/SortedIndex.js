/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to be indexed for sort based pagination.
 * @exports @index
 */
local['@index'] = (store) => (
	class SortedIndexDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			const {
				list,
			} = this.args;

			const defineSortedIndexByIndex = (args, index = 0) => {
				const {
					compounds,
				} = args;
				let { dir } = args;
				dir = dir || 1;
				store
					.defineModelProperty(field.modelName, field)
					.definePropertySortedIndex(field, {
						dir,
						compounds,
					});
			}

			if (list) {
				list.forEach((sortedIndex, index) => {
					defineSortedIndexByIndex(sortedIndex, index);
				});
			} else {
				defineSortedIndexByIndex(this.args, 0);
			}

			return field;
		}
	}
);

module.exports = local['@index'];

module.exports.directiveName = 'index';

module.exports.typeDef = `
directive @index(dir: Int, compounds: [String]) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Object",
	"name": "SortedIndex",
	"icon": "sort-alphabetical-ascending",
	"nature": "molecule",
	"slug": "index",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
