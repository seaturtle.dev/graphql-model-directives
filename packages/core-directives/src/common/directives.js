/**
 * Library and list of available GraphQL Directives for better schema definition.
 * @exports directives
 */
const directives = exports;

/** @member {string[]} A list of all the available directive names */
directives.names = [];

/** @member {Object[]} A list of all the available directives */
directives.list = [];

function add(module) {
	const name = module.directiveName;
	directives.names.push(name);
	directives.list.push(module);
	return module;
}

/**
 * Adds a directive to the list of available directives.
 * @param   {Function} module Directive module.
 * @returns {Function}        Directive module.
 */
directives.add = add;

function map(store, config) {
	const dirs = {};
	directives.list.forEach((module) => {
		const name = module.directiveName;
		dirs[name] = module(store, config);
	});
	return dirs;
}

/**
 * Generates a library of directives from the list of available directives.
 * @param  {StoreAdapter} store Store adapter.
 * @return {Object}             Directives object.
 */
directives.map = map;

/** @member {Object} A library of directives without helper pollution */
directives.lib = {};

/**
 * Annotate an object to be a data model.
 * @contructor
 * @see {@link @model} */
directives.lib.Model = add(require('../Model'));

/**
 * Annotate an object to be a resource that can belong to other documents or users.
 * @contructor
 * @see {@link @resource} */
directives.lib.Resource = add(require('../Resource'));

/**
 * Annotate an object to be the user model.
 * @contructor
 * @see {@link @user} */
directives.lib.User = add(require('../User'));

/**
 * Annotate a object to have pagination.
 * @contructor
 * @see {@link @page} */
directives.lib.Page = add(require('../Page'));


/**
 * Annotate a field to use a special type within its model definition,
 * this is needed for some databases.
 * @contructor
 * @see {@link @modelType} */
directives.lib.ModelType = add(require('../ModelType'));

/**
 * Annotate an object: Only allow access to the annoted Type for an authenticated user.
 * @contructor
 * @see {@link @auth} */
directives.lib.Authentication = add(require('../Authentication'));

/**
 * Annotate a field or object to only be accessible to users who have permission to view it.
 * @contructor
 * @see {@link @permission} */
directives.lib.Permission = add(require('../Permission'));


/**
 * Annotate an object to contain a scan bassed pagination.
 * @contructor
 * @see {@link @scanned} */
directives.lib.ScannedPagination = add(require('../ScanPagination'));

/**
 * Annotate an object to contain a sort bassed pagination.
 * @contructor
 * @see {@link @sorted} */
directives.lib.SortedPagination = add(require('../SortedPagination'));


/**
 * Annotate a field to have the time of creation for the document.
 * @contructor
 * @see {@link @created} */
directives.lib.CreatedAt = add(require('../CreatedAt'));

/**
 * Annotate a field to have a default value.
 * @contructor
 * @see {@link @default} */
directives.lib.DefaultValue = add(require('../DefaultValue'));

/**
 * Annotate a field so the value will be encrypted when saved.
 * @contructor
 * @see {@link @encrypt} */
directives.lib.Encryption = add(require('../Encryption'));

/**
 * Annotate a field to store a file. Also handles file uploading and streaming.
 * @contructor
 * @see {@link @file} */
directives.lib.File = add(require('../File'));

/**
 * Annotate the field to be used as the main unique identifier for the document.
 * @contructor
 * @see {@link @id} */
directives.lib.Identifier = add(require('../Identifier'));

/**
 * Annotate a field to contain a nested object or set of objects.
 * @contructor
 * @see {@link @nested} */
directives.lib.NestedValue = add(require('../NestedValue'));

/**
 * Annotate a field to reference another Type by id.
 * @contructor
 * @see {@link @resolve} */
directives.lib.Resolve = add(require('../Resolve'));

/**
 * Annotate a field to resolve automatically. You have several strategies available for resolution:
 * `model`, and in the futer `value`, `evaluate`, `rest`, `graphql`, `moleculer`.
 * @contructor
 * @see {@link @relation} */
directives.lib.Relation = add(require('../Relation'));

/**
 * Annotate an object to be indexed for sort based pagination.
 * @contructor
 * @see {@link @index} */
directives.lib.SortedIndex = add(require('../SortedIndex'));

/**
 * Annotate a field so that the value is indexes uniquely.
 * @contructor
 * @see {@link @unique} */
directives.lib.UniqueValue = add(require('../UniqueValue'));

/**
 * Annotate a field to have the last time updated for the document.
 * @contructor
 * @see {@link @updated} */
directives.lib.UpdatedAt = add(require('../UpdatedAt'));

/**
 * Annotate a field of a model to have validation on the value when saving.
 * @contructor
 * @see {@link @validation} */
directives.lib.Validation = add(require('../Validation'));

/**
 * Annotate a field of a model to be virtual so that it can be transered
 * over GraphQL without being saved to the model.
 * @contructor
 * @see {@link @virtual} */
directives.lib.VirtualProperty = add(require('../VirtualProperty'));



/**
 * Annotate a field of the user model object to be the user birthdate.
 * @contructor
 * @see {@link @birthdate} */
directives.lib.UserBirthdate = add(require('../UserBirthdate'));

/**
 * Annotate a field of the user model object to be the user email.
 * @contructor
 * @see {@link @email} */
directives.lib.UserEmail = add(require('../UserEmail'));

/**
 * Annotate a field of the user model object to be used internally to confirm a user email.
 * @contructor
 * @see {@link @emailConfirm} */
directives.lib.UserEmailConfirm = add(require('../UserEmailConfirm'));

/**
 * Annotate an object to be the user invite model.
 * @contructor
 * @see {@link @invite} */
directives.lib.UserInvite = add(require('../UserInvite'));

/**
 * Annotate a field of the user model object to be the users login for logging in.
 * @contructor
 * @see {@link @login} */
directives.lib.UserLogin = add(require('../UserLogin'));

/**
 * Annotate a field of the user model object to specify the maximum
 * allowed invites they can have pending.
 * @contructor
 * @see {@link @maxInvites} */
directives.lib.UserMaxInvites = add(require('../UserMaxInvites'));

/**
 * Annotate a field of the user model object to be the users password for logging in.
 * @contructor
 * @see {@link @password} */
directives.lib.UserPassword = add(require('../UserPassword'));

/**
 * Annotate a field of the user model object to be used internally to reset a user password.
 * @contructor
 * @see {@link @passwordReset} */
directives.lib.UserPasswordReset = add(require('../UserPasswordReset'));

/**
 * Annotate a field of the user model object to be the users' list of roles
 * used to access GraphQL operations.
 * @contructor
 * @see {@link @roles} */
directives.lib.UserRoles = add(require('../UserRoles'));

/**
 * Annotate a field of the user model object to be the users list of tokens used to authenticate.
 * @contructor
 * @see {@link @tokens} */
directives.lib.UserTokens = add(require('../UserTokens'));

/**
 * Annotate a field of the user model object to be the users virtual token property.
 * @contructor
 * @see {@link @token} */
directives.lib.UserToken = add(require('../UserToken'));


// NOTE: Must be last to ensure it happens after all the other directives, otherwise templating issues will occur.

/**
 * Annotate an object to have an API for it injected into query and mutation of schema.
 * @contructor
 * @see {@link @api} */
directives.lib.API = add(require('../API'));
