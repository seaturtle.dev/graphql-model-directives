// const { SchemaDirectiveVisitor } = require('@graphql-tools/utils');

const {
	mapSchema,
	getDirective,
	MapperKind
} = require('@graphql-tools/utils');


class SchemaDirectiveVisitor {
	static cache = {
		fields: {},
		types: {},
	};

	static createTransformer(name, visitorClass) {
		return (schema, state) => {
			const context = {
				schema,
				state,
			};
			const callTransformerMethod = (method, config, mixinContext = {}) => {
				const directive = getDirective(schema, config, name);
				const ref = directive?.[0];
				if (directive) {
					const scope = {
						...context,
						...mixinContext,
						args: ref || {},
					};
					config.modelName = config.modelName || scope.modelName;
					return visitorClass.prototype[method].call(scope, config, directive);
				}
			};
			const mapLocationTransformer = (mappers, method, kind, mapper) => {
				if (!visitorClass.prototype[method]) {
					return;
				}
				mappers[kind] = mapper.bind(null, [method]);
			}
			const mappers = {};
			const cache = this.cache || SchemaDirectiveVisitor.cache || {
				fields: {},
				types: {}
			};
			mapLocationTransformer(mappers, 'visitFieldDefinition', MapperKind.OBJECT_FIELD, (method, field, fName, tName, schema) => {
				const newField = field;
				const type = cache.fields[tName] = (cache.fields[tName] || {});
				field = type[fName] = (type[fName] || field);
				Object.assign(field, newField);
				field.name = fName;
				field.modelName = tName;
				return callTransformerMethod(method, field, {
					name: fName,
					modelName: tName,
					visitedType: field.type,
				});
			});
			mapLocationTransformer(mappers, 'visitObject', MapperKind.OBJECT_TYPE, (method, type, schema) => {
				const newType = type;
				type = cache.types[type.name] = (cache.types[type.name] || type);
				Object.assign(type, newType);
				type.modelName = type.name;
				return callTransformerMethod(method, type, {
					modelName: type.name,
					visitedType: type,
				});
			});
			return mapSchema(schema, mappers);
		}
	}
}

SchemaDirectiveVisitor.SchemaDirectiveVisitor = SchemaDirectiveVisitor;

module.exports = SchemaDirectiveVisitor;
