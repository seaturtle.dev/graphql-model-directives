/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the users password for logging in.
 * @exports @password
 */
local['@password'] = (store) => (
	class UserPasswordDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserPassword(field)
				.definePropertyEncryption(field);

			return field;
		}
	}
);

module.exports = local['@password'];

module.exports.directiveName = 'password';

module.exports.typeDef = `
directive @password on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserFeild",
	"name": "UserPassword",
	"nature": "molecule",
	"icon": "account-key-outline",
	"slug": "password",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
