/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const ids = require('@seaturtle/gmd-core/ids.js');

const idsHelpers = {
	nanoid: () => ids.nanoid.nanoid(),
	shortid: () => ids.shortid.generate(),
}

module.exports.nanoid = idsHelpers.nanoid;
module.exports.shortid = idsHelpers.shortid;

const local = {};

/**
 * Annotate a field to have a default value.
 * @exports @default
 * @param {any} [value] If defined, set the default value from that value in the schema.
 * @param {any} [context] idk
 */
local['@default'] = (value, context) => [value, context]; // for docs
local['@default'] = (store) => (
	class DefaultValueDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			const property = store.defineModelProperty(field.modelName, field);
			const {
				value,
				context,
				document,
				prepend,
				append,
				cast
			} = this.args;
			const preprocess = (val) => {
				if (val === '$NOW') {
					val = Date.now();
				}
				if (val === '$RAND') {
					return Number.random();
				}
				if (typeof val === 'string' && val.substr(0, 3) === '$ID' ) {
					if (val.length > 3) {
						const idStrategy = val.split(':')[1];
						if (idsHelpers[idStrategy]) {
							return idsHelpers[idStrategy]();
						}
					}
					return idsHelpers.nanoid();
				}
				return val;
			}
			const processFinalValue = (val) => {
				val = preprocess(val);
				if (append) {
					val = val + preprocess(append);
				}
				if (prepend) {
					val = preprocess(prepend) + val;
				}
				if (cast === 'integer') {
					return parseInt(val, 10);
				}
				if (cast === 'float') {
					return parseFloat(val);
				}
				if (cast === 'string') {
					return val.toString();
				}
				return val;
			};
			if (!context && !document) {
				property.definePropertyDefaultValue(field, () => processFinalValue(this.args.value));
			} else if (context || document) {
				property.definePropertyDefaultValue(field, (ctx, doc, id) => {
					const keys = (context || document).split('.');
					let current = !context ? doc : ctx;
					if (!context && keys.length === 1 && keys[0] === 'id') {
						return processFinalValue(id);
					}
					while (keys.length) {
						const key = keys.shift();
						if (current && current[key]) {
							current = current[key];
						} else {
							current = null;
						}
					}
					return processFinalValue(current || value);
				});
			}
			return field;
		}
	}
);

module.exports = local['@default'];

module.exports.directiveName = 'default';

module.exports.typeDef = `
directive @default(value: JSON, context: JSON, document: JSON, prepend: JSON, append: JSON, cast: String) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "DefaultValue",
	"icon": "select-place",
	"nature": "element",
	"slug": "default",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
