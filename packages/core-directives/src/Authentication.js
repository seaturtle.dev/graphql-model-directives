/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object: Only allow access to the annoted Type for an authenticated user.
 * @resolve @auth
 * @param {string[]} [allow=] Set of allowed user roles to grant authorization.
 */
local['@auth'] = (allow = []) => allow; // for docs
local['@auth'] = (store) => (
	class AuthenticationDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineQueryPropertyAuth(field, this.args.allow);

			return field;
		}
	}
);

module.exports = local['@auth'];

module.exports.directiveName = 'auth';

module.exports.typeDef = `
directive @auth(allow: [String]) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"icon": "shield-account",
	"nature": "organism",
	"name": "Authentication",
	"slug": "auth",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
