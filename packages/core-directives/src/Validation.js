/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of a model to have validation on the value when saving.
 * @exports @validation
 */
local['@validation'] = (store) => (
	class ValidationDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyValidation(field, this.args.params);

			return field;
		}
	}
);

module.exports = local['@validation'];

module.exports.directiveName = 'validation';

module.exports.typeDef = `
directive @validation(params: JSON) on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "Validation",
	"icon": "alert-rhombus-outline",
	"nature": "molecule",
	"slug": "validation",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
