/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be used internally to confirm a user email.
 * @exports @emailConfirm
 */
local['@emailConfirm'] = (store) => (
	class UserEmailConfirmDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserEmailConfirm(field);

			return field;
		}
	}
);

module.exports = local['@emailConfirm'];

module.exports.directiveName = 'emailConfirm';

module.exports.typeDef = `
directive @emailConfirm on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserField",
	"name": "UserEmailConfirm",
	"icon": "email-seal-outline",
	"nature": "element",
	"slug": "emailConfirm",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
