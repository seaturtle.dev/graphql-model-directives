/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the user birthdate.
 * @exports @birthdate
 */
local['@birthdate'] = (store) => (
	class UserBirthdateDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserBirthdate(field);

			return field;
		}
	}
);

module.exports = local['@birthdate'];

module.exports.directiveName = 'birthdate';

module.exports.typeDef = `
directive @birthdate on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserField",
	"name": "UserBirthdate",
	"icon": "cake-variant-outline",
	"nature": "element",
	"slug": "birthdate",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
