/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the users login for logging in.
 * @exports @login
 */
local['@login'] = (store) => (
	class UserLoginDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserLogin(field)
				.definePropertyUnique(field);

			return field;
		}
	}
);

module.exports = local['@login'];

module.exports.directiveName = 'login';

module.exports.typeDef = `
directive @login on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserField",
	"name": "UserLogin",
	"icon": "target-account",
	"nature": "element",
	"slug": "login",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
