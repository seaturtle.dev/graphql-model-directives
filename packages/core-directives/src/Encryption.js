/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field so the value will be encrypted when saved.
 * @exports @encrypt
 */
local['@encrypt'] = (store) => (
	class EncryptionDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyEncryption(field);

			return field;
		}
	}
);

module.exports = local['@encrypt'];

module.exports.directiveName = 'encrypt';

module.exports.typeDef = `
directive @encrypt on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "Directives",
	"type": "Field",
	"name": "Encryption",
	"icon": "safe",
	"nature": "molecule",
	"slug": "encrypt",
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
