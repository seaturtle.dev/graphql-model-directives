/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate an object to be the user invite model.
 * @exports @invite
 */
local['@invite'] = (store) => (
	class UserInviteDirective extends SchemaDirectiveVisitor {
		visitObject(object) {
			const collection = (
				store
					.defineModel(object.name, object)
					.defineUserInvite(this.args.from)
			);

			store.mixinUserInvite(collection);

			return object;
		}
	}
);

module.exports = local['@invite'];

module.exports.directiveName = 'invite';

module.exports.typeDef = `
directive @invite(from: String) on OBJECT
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserObject",
	"name": "UserInvite",
	"icon": "email-newsletter",
	"nature": "molecule",
	"slug": "invite",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
