/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be used internally to reset a user password.
 * @exports @passwordReset
 */
local['@passwordReset'] = (store) => (
	class UserPasswordResetDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserPasswordReset(field);

			return field;
		}
	}
);

module.exports = local['@passwordReset'];

module.exports.directiveName = 'passwordReset';

module.exports.typeDef = `
directive @passwordReset on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserField",
	"name": "UserPasswordReset",
	"nature": "element",
	"icon": "account-lock-open-outline",
	"slug": "passwordReset",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
