/* eslint camelcase:1 */

const { SchemaDirectiveVisitor } = require('./common/SchemaDirectiveVisitor');

const local = {};

/**
 * Annotate a field of the user model object to be the users list of tokens used to authenticate.
 * @exports @tokens
 */
local['@tokens'] = (store) => (
	class UserTokensDirective extends SchemaDirectiveVisitor {
		visitFieldDefinition(field) {
			store
				.defineModelProperty(field.modelName, field)
				.definePropertyUserTokens(field)
				.definePropertyModelType(field, 'Object');

			return field;
		}
	}
);

module.exports = local['@tokens'];

module.exports.directiveName = 'tokens';

module.exports.typeDef = `
directive @tokens on FIELD_DEFINITION
`;

module.exports.metaInfo = {
	"group": "User Directives",
	"type": "UserFeild",
	"name": "UserTokens",
	"icon": "account-details-outline",
	"nature": "molecule",
	"slug": "tokens",
	"govern": { "limit": 1 },
	"action": "addDirective",
	"defaultProps": {},
	"inspectorProps": {
		// "group": { "option": "text" },
	},
};
