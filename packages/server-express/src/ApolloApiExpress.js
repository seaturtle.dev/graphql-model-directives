module.exports = (async function () {
	// let DefaultApolloServer;

	// try {
	// 	DefaultApolloServer = require('apollo-server-express').ApolloServer;
	// 	// console.log('GOT HERE', require('apollo-server-express').ApolloServer)
	// } catch (err) {
	// 	// console.warn('apollo-server-express is not installed')
	// }

	const { default: graphqlUploadExpress } = await import('graphql-upload/graphqlUploadExpress.mjs');

	const { expressMiddleware } = require('@apollo/server/express4');

	const ApolloApiServer = require('@seaturtle/gmd-server-common/ApolloApiServer.js');

	/** @class */
	class ApolloApiExpress extends ApolloApiServer {
		// static DefaultApolloServer = DefaultApolloServer;

		static extraMiddleware = null;

		static installMiddleware({ bodyParser, cors }, params) {
			const {
				cors: corsOptions,
				bodyParser: bodyParserOptions,
			} = params || {}
			ApolloApiExpress.extraMiddleware = [
				cors(corsOptions),
				bodyParser.json(bodyParserOptions),
			];
		}

		/** @method */
		async setup(app, apollo, cb, before) {
			await this.ready();

			if (app && apollo) {
				await apollo.start();

				let extraMiddleware = this.constructor.extraMiddleware || [];
				if (before) {
					const beforemiddleware = await before();
					extraMiddleware = [
						...extraMiddleware,
						...(Array.isArray(beforemiddleware) ? beforemiddleware : []),
					];
				}

				app.use(graphqlUploadExpress());

				// debugger;

				app.use('/graphql', ...extraMiddleware, expressMiddleware(apollo, {
					context: async (params) => this.getContext(params),
				}));
				// apollo.applyMiddleware({ app });

				if (cb) {
					cb();
				}
			}
		}

		/** @method */
		postSetup(app, apollo, server) {
			if (apollo && apollo.installSubscriptionHandlers && server) {
				apollo.installSubscriptionHandlers(server);
			}
		}
	}

	return module.exports = ApolloApiExpress;
})();
