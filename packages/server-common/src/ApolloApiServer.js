/* eslint global-require:0 import/no-extraneous-dependencies:0 */

// const https = require('https');
const http = require('http');

const {
	ApolloServer: DefaultApolloServer,
} = require('@apollo/server');

const {
	ApolloServerPluginDrainHttpServer,
} = require('@apollo/server/plugin/drainHttpServer');

const prepareSchema = require('@seaturtle/gmd-prepare-schema');

const getContextHandler = require('./getContextHandler');

/**
* Create a GraphQL Apollo Server with Model Directives integrated.
* @exports ApolloApiServer
* @class
*/
class ApolloApiServer {
	static DefaultApolloServer = DefaultApolloServer;

	constructor(
		options = {},
		appServer,
		ApolloServer = this.constructor.DefaultApolloServer || DefaultApolloServer,
	) {
		/** @property {Object} config */
		this.config = options.config;

		/** @property {Boolean} started */
		this.started = false;

		/** @property {StoreAdapter} store */
		this.store = options.store || null;

		/** @property {string} rawSchema */
		this.rawSchema = (options && options.rawSchema) || 'Query {}\nMutation {}';

		/** @property {function} getContext */
		this.getContext = () => {};

		/** @property {Boolean} debug */
		this.debug = options.debug || false;

		/** @property {Object} seaturtle */
		this.seaturtle = null;

		/** @property {Object} resolvers */
		this.resolvers = null;

		/** @property {Object} schema */
		this.schema = null;

		/** @property {Object} app */
		this.app = null;

		/** @property {Object} server */
		this.server = null;

		/** @property {Object} apollo */
		this.apollo = null;

		/** @property {Object} readyPromise */
		this.readyPromise = new Promise((resolve, reject) => {
			prepareSchema({
				config: this.config,
				store: this.store,
				rawSchema: this.rawSchema,
				resolvers: options.resolvers || { Query: {}, Mutation: {} },
				customDirectives: options.customDirectives || {},
				schemaDirectives: options.schemaDirectives || {},
			}).then((seaturtle) => {
				this.seaturtle = seaturtle;

				this.resolvers = this.seaturtle.resolvers;

				this.schema = this.seaturtle.schema;

				const { config, store } = this;
				const { models } = store;

				this.getContext = getContextHandler({
					store,
					models,
					config,
					schema: this.schema,
					options,
				});

				// onDisconnect: (webSocket, context) => {},

				this.app = appServer(this.appOptions());

				this.server = this.app.server || http.createServer(this.app);

				this.apollo = new ApolloServer(Object.assign(options, {
					csrfPrevention: true,
					schema: this.schema,
					// context: (params) => this.getContext(params),
					introspection: process.env.NODE_ENV !== 'production',
					playground: process.env.NODE_ENV !== 'production',
					formatError: (error) => this.handleError(error),
					plugins: this.serverPlugins(options, this.app, this.server),
				}));

				resolve(this);
			}, reject);
		});
		// debugger;
	}

	/** @method */
	ready(cb, err) {
		if (cb) {
			this.readyPromise.then(cb, err);
		}
		return this.readyPromise;
	}

	/** @method */
	handleError(error) {
		if (this.debug) {
			global.console.log('APOLLO ERR LOG:', error.message);
			if (error.stack) {
				global.console.log('APOLLO ERR STACK:', error.stack);
			}
		}

		return error;
	}

	/** @method */
	appOptions() {
		return undefined
	}

	/** @method */
	serverPlugins(options, app, server) {
		return [
			...(options.plugins || []),
			ApolloServerPluginDrainHttpServer({ httpServer: server })
		]
	}

	/** @method */
	create(options, callback) {
		if (callback) {
			this.ready(() => {
				callback(options.app || this.app);
			});
		}
	}

	/** @method */
	async setup(app, apollo, cb, before) {
		await this.ready();

		if (app && apollo) {
			await apollo.start();

			if (cb) {
				await before();
			}
			if (cb) {
				cb();
			}
		}
	}

	/** @method */
	postSetup(app, apollo, server, cb) {
		this.ready(() => {
			if (app && apollo && server && cb) {
				cb();
			}
		});
	}

	/**
	 * Starts the server.
	 * @param {string=} host
	 * @param {number=} port
	 * @param {Function=} cb
	 */
	start(host, port, cb, before) {
		return this.create({ app: this.app }, (app) => {
			this.setup(app, this.apollo, () => {
				this.started = true;
				this.postSetup(app, this.apollo, this.server);
				this.listen(
					{
						port,
						host,
					},
					() => {
						if (cb) {
							cb();
						}
					},
				)
			}, before ? () => before(this.app, this.apollo, this.server, this) : null);
		});
	}

	/**
	 * Start listening.
	 * @param  {Object} params
	 * @param  {Function=} cb
	 */
	listen(params, cb) {
		if (this.app.listen) {
			this.app.listen(params, cb);
		} else {
			this.server.listen(params, cb);
		}
	}

	/**
	 * Stops the server.
	 * @param  {Function=} cb
	 */
	stop(cb) {
		if (this.app && this.started) {
			let called = false;
			this.store.close((storeErr) => {
				this.server.close((serverErr) => {
					if (cb && !called) { cb(serverErr || storeErr); }
					called = true;
				});
			});
			setTimeout(() => {
				if (cb && !called) { cb(new Error('Server exit timed out.')); }
				called = true
			}, 1500);
		} else if (cb) {
			cb();
		}
	}
}

module.exports = ApolloApiServer;
