/**
 * Alias to `@apollo/server`
 * @ignore
 */
module.exports = exports = exports.server = require('@apollo/server');
