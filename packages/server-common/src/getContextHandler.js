const getDeviceIdHeader = require('@seaturtle/gmd-plugin-authorize/getDeviceIdHeader.js');

/**
 * @exports getContextHandler
 * @function
 * @ignore
 */
module.exports = function getContextHandler({
	store,
	models,
	config,
	schema,
	options,
}) {
	options.subscriptions = options.subscriptions || {};

	let originalOnConnect = (
		options.subscriptions.onConnect
	);

	options.subscriptions.onConnect = (params, ws, context) => {
		const { headers } = params;

		const deviceId = getDeviceIdHeader(headers);

		context.config = config;
		context.headers = headers;
		context.store = store;
		context.deviceId = deviceId;
		context.model = models;

		Object.assign(context, options.context);

		if (originalOnConnect) {
			originalOnConnect(params, ws, context);
		}

		return store.authenticate(context, {}, () => null);
	};

	return function (params) {
		// console.log('setup context');
		// debugger;

		const headers = (params.req && params.req.headers) || {};

		const deviceId = getDeviceIdHeader(headers);

		const context = {
			...params,
			headers,
			store,
			deviceId,
			model: models,
			config,
			schema,
			...options.context,
		};

		if (params.connection && params.connection.context) {
			return Object.assign(params.connection.context, context);
		}

		// console.log('final context');
		// debugger;

		return context;
	};
};
