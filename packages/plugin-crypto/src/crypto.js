let crypto = null;

// TODO: move this into context.config, then remove unsafe Collection classes

try {
	crypto = require('crypto');
} catch (err) {
	console.warn(err);
}

let bcrypt = null;

try {
	bcrypt = require('bcrypt');
} catch (err) {
	console.warn(err);
}

const { scrypt, randomBytes } = (crypto || {});

module.exports = function (parentConfig) {
	function hash(password, salt = 16, callback, length = 32) {
		if (bcrypt && bcrypt.hash) {
			// TODO: promisify
			return bcrypt.hash(password, salt, callback);
		}
		if (!crypto) {
			return Promise.reject(new Error('missing crypto dependencies'));
		}
		try {
			if (typeof salt === 'number') {
				salt = randomBytes(salt).toString('hex');
			}
			return new Promise((resolve, reject) => {
				scrypt(password, salt, length, (err, derivedKey) => {
					if (err) {
						return reject(err);
					}
					try {
						const hashStr = derivedKey.toString('hex');
						return resolve(callback(hashStr) || hashStr);
					} catch (hashErr) {
						return reject(hashErr);
					}
				});
			});
		} catch (err) {
			return Promise.reject(callback(err) || err);
		}
	}

	crypto.hash = hash;

	function compare(password, secret, callback, salt = 16, length = 32) {
		if (bcrypt && bcrypt.compare) {
			// TODO: promisify
			return bcrypt.compare(password, secret, callback);
		}
		if (!crypto) {
			return Promise.reject(new Error('missing crypto dependencies'));
		}
		try {
			return new Promise((resolve, reject) => {
				Promise.all([
					hash(password, salt, (e, v) => v, length),
					hash(secret, salt, (e, v) => v, length),
				]).then(([a, b]) => {
					if (a === b) {
						callback(null, true);
						resolve(true);
						return;
					}
					callback(null, false);
					resolve(false);
				}, reject);
			});
		} catch (err) {
			return Promise.reject(callback(err) || err);
		}
	}

	crypto.compare = compare;
}
