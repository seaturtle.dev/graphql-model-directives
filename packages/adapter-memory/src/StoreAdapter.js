/* eslint global-require:0 prefer-destructuring:0 */

let redisMock = null;

try {
	// redisMock = require('ioredis-mock');
	// redisMock.createClient = (o) => new redisMock(o);
	redisMock = require('redis-mock');
	redisMock.RedisClient.prototype.isMemory = true;
} catch (err) {
	console.warn('REDIS WARNING:', err);
}

const RedisStoreAdapter = require('@seaturtle/gmd-adapter-redis/StoreAdapter.js');
const RedisCollection = require('@seaturtle/gmd-adapter-redis/Collection.js');

const StoreAdapter = require('@seaturtle/gmd-adapter-generic/StoreAdapter.js');

/**
 * Uses `redis-mock` on top of the Redis adapter to provide an in-memory adapter.
 * @exports MemoryStore
 * @class
 * @extends RedisStore */
class MemoryStore extends RedisStoreAdapter {
	/**
	 *
	 * @param {redisClient} [client=redisMock.createClient()] Redis client to use,
	 * by default it uses `redis-mock` to create a client.
	 * @param {} options [description]
	 * @constructs
	 */
	constructor(client, options) {
		super(
			client || (redisMock ? redisMock.createClient() : null),
			{
				resolvers: options && options.resolvers,
				pubsub: (options && options.pubsub) || new StoreAdapter.PubSub(),
			},
		);
		this.redisMock = redisMock;
		this.withFilter = StoreAdapter.withFilterPubSub;
	}

	/**
	 * [close description]
	 * @param  {Function} cb [description]
	 * @return {[type]}      [description]
	 */
	close(cb) {
		// if (this.pubsub && this.pubsub.close) {
		// 	this.pubsub.close();
		// }
		this.client.quit(cb);
	}

	/**
	 * [toJSON description]
	 */
	toJSON() {
		return JSON.stringify(this.redisMock.storage);
	}

	/**
	 * [fromJSON description]
	 */
	fromJSON(json) {
		return new Promise((resolve, reject) => {
			try {
				const data = JSON.parse(json);
				this.redisMock.databases[this.redisMock.currentDatabase] = data;
				this.redisMock.storage = this.redisMock.databases[this.redisMock.currentDatabase];
				return resolve(data);
			} catch (err) {
				return reject(err);
			}
		});
	}
}

MemoryStore.Collection = RedisCollection;

module.exports = MemoryStore;
