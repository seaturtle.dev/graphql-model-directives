#!/bin/bash

#set -v
#set -x

installs=""

setup () { mkdir -p ./workspaces; }

setupWorkspace () {
	rm -rf "./workspaces/$1"
	git clone $2 "./workspaces/$1"
	cd "./workspaces/$1"
	git pull
	npm install
	cd ../../
}

cleanupWorkspace () {
	echo "";
	echo "Installing: $installs";
	echo "";
	eval "npm install $installs --no-save --install-links --force"
	rm -rf "./workspaces/$1";
}

cleanup () { rm -rf ./workspaces; }

pkgInstall () {
	while (( "$#" >= 2 )); do
		echo "Queued: ./workspaces/$1/packages/$2";
		installs="$installs ./workspaces/$1/packages/$2"
		shift 2
	done
}

pkgInstallSelf() {
	echo "Queued: ./workspaces/$1";
	installs="$installs ./workspaces/$1"
}

if [ -e .no-postinstall ]
then
	setup
	setupWorkspace gmd "https://gitlab.com/seaturtle.dev/graphql-model-directives.git"

	pkgInstall \
		gmd core \
		gmd adapter-generic \
		gmd adapter-data \
		gmd core-directives \
		gmd core-types \
		gmd prepare-schema \
		gmd plugin-authorize \
		gmd plugin-crypto \
		gmd plugin-email \
		gmd plugin-upload \
		gmd server-common

	# pkgInstallSelf gmd

	if [ "$1" = "dev" ]
	then
		pkgInstall \
			gmd adapter-mongodb \
			gmd adapter-sequel \
			gmd server-express \
			gmd server-fastify \
			gmd adapter-api-snocode \
			gmd adapter-automerge \
			gmd adapter-dexie \
			gmd adapter-memory \
			gmd adapter-redis \
			gmd docs \
			gmd test-adapter \
			gmd test-server
	fi

	cleanupWorkspace gmd
	cleanup
fi
